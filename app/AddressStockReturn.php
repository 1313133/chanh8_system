<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AddressStockReturn extends Model
{
    protected $table = "address_stock_returns";
}
