<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
class CarrerItem extends Model
{
    use Translatable;
    protected $translatable = ['title', 'slug','location','content','shortDescription','titleSeo'];
}
