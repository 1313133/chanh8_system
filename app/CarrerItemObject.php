<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class CarrerItemObject extends Model
{

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){

        });

        self::created(function($model){
            $model->slug = Str::slug($model->text).Carbon::now()->format('mdY');

            $model->save();
        });

        self::updating(function($model){
            // ... code here
        });

        self::updated(function($model){
            $model->slug = Str::slug($model->text).Carbon::now()->format('mdY');
            $model->save();
        });

        self::deleting(function($model){
            // ... code here
        });

        self::deleted(function($model){
            // ... code here
        });
    }

}
