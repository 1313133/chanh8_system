<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class CarrerObjectItem extends Model
{
    use Translatable;
    protected $translatable = ['positionName', 'locationName','content','slug','shortDescription','keyword','titleSeo','descriptionSEO','author'];
}
