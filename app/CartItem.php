<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{

    public function Product()
    {
        return $this->hasOne('App\Models\Product');
    }
}
