<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Config;
use Carbon\Carbon;

class ExportFile implements FromArray, WithHeadings, WithStyles, WithEvents, ShouldAutoSize, WithColumnFormatting
{
    use RegistersEventListeners;
    /**
    * @return \Illuminate\Support\Collection
    */

    public $points;

    public function __construct(array $points)
    {
       
        $this->points = $points;
       // dd($this->points);
    }

    public function array(): array
    {
        return $this->points;
    }

    public function headings(): array
    {
        $dt = Carbon::now('Asia/Ho_Chi_Minh')->toDateString();
        $headings = [
            ['THỐNG KÊ DỮ LIỆU ĐƠN HÀNG'],
            ['NGÀY: '.$dt],
            [],
            ['MÃ VẬN ĐƠN','NGƯỜI GỬI','SĐT NGƯỜI GỬI','ĐỊA CHỈ GỬI','NGƯỜI NHẬN','SĐT NGƯỜI NHẬN','ĐỊA CHỈ NGƯỜI NHẬN','ĐẠI LÝ TẠO ĐƠN','NƠI LẤY HÀNG','HÌNH THỨC','PHƯƠNG THỨC GIAO','GIÁ TRỊ BƯU KIỆN','PHÍ GIAO','TỔNG THU','TRẠNG THÁI']
        ];

        return $headings;
    }

    public function styles(Worksheet $sheet)
    {
         $sheet->mergeCells('A1:H1');
         $sheet->mergeCells('A2:H2');
    

        // $sheet->setCellValue('C1', 'Xin chao');

        foreach (range(4, 700) as $number) {
            $sheet->getStyle('N' . $number)->getAlignment()->applyFromArray(
                array('horizontal' => 'right')
            );
            $sheet->getStyle('L' . $number)->getAlignment()->applyFromArray(
                array('horizontal' => 'right')
            );
        }
        $sheet->getStyle('A1')->getFont()
        ->setSize(25)->setBold(true)
        ->getColor()->setRGB('0000ff');
        $sheet->getStyle('A2')->getAlignment()->applyFromArray(
            array('horizontal' => 'center')
        );
        $sheet->getStyle('A1')->getAlignment()->applyFromArray(
            array('horizontal' => 'center')
        );
        $sheet->getStyle('A4')->applyFromArray(array(
            'fill' => array(
                'background' => array('rgb' => 'FF0000')
            )
            ));
        
        // $sheet->cells('A4', function ($cells) {
        //     $cells->setBackground('#008686');
        //     $cells->setAlignment('center');
        // });

    }

    public static function afterSheet(AfterSheet $event)
    {
        $sheet = $event->sheet->getDelegate();

        $sheet->cell('A', function($row) { 
            $row->setBackground('#CCCCCC'); 
        });
    }
    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $cellRange = 'A4:R4';
                $event->sheet->getDelegate()
                    ->getStyle($cellRange)
                    ->getFont()
                    ->setSize(13)
                    ->getColor()->setRGB('0000ff');
                    $event->sheet->getDelegate()->getStyle($cellRange)
                    ->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()
                    ->setARGB('fdd868');
                    $event->sheet->getColumnDimension('D')->setAutoSize(true);
                
            }
        ];
    }

    public function columnFormats(): array
    {
        return [
            'A2' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}