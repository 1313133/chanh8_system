<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function allCategory()
    {
        return $this->hasMany('App\ProductCategory', 'groupCode', 'id')->orderBy('priority','asc')->get();
    }
}
