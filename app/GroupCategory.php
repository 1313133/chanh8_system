<?php

namespace App;
use Carbon\Carbon;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

class GroupCategory extends Model
{


    public function ListProductCategory()
    {
        return $this->hasMany('App\ProductCategory', 'groupCode', 'id')
        ->orderBy('priority','asc')
        ->orderby("updated_at","desc")->paginate(30);
    }


    public static function boot()
    {
        parent::boot();
        static::created(function($model)
        {
           $model->slug =Str::slug($model->title.Carbon::today()->format("dmY").$model->id);
           $model->save();
        });
        static::updating(function($model)
        {
            $model->slug =Str::slug($model->title.Carbon::today()->format("dmY").$model->id);
        });
    }
}

