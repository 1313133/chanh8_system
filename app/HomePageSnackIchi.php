<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class HomePageSnackIchi extends Model

{
    use Translatable;
    protected $translatable = ['title', 'shortDescription'];
    public function ProductCategory()
    {
        return $this->hasMany('App\ProductCategory', 'id', 'categoryId');
    }
}
