<?php
namespace App\Http\Controllers;
use Carbon\Carbon;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\AddressSender;
use Illuminate\Support\Facades\Auth;


class AdminAddressSenderController extends Controller
{

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        return view('vendor.voyager.addressSender.index');
    }


    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
          $dataEdit = AddressSender::where("id",$id)->first();
         if($dataEdit ==null)
        {
            $dataEdit = new AddressSender();

            $dataEdit->id =  -1;
        }else{
            
            if (ctype_digit($dataEdit->ward) && ctype_digit($dataEdit->district)) {
               $w = DB::table("ward")->where('id', $dataEdit->ward)->first();
                $d = DB::table("district")->where('id',$dataEdit->district)->first();
                $dataEdit->wd = $w->name;
                $dataEdit->dt = $d->name;
            }
        }
       return view("vendor.voyager.addressSender.detail", compact("dataEdit"));

    }


    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("address_senders")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"no data picked"];
        }
    }

    private  function InputToAddressSender(Request $request,$newUpdate,$uppdate =false)
    {
        if($uppdate ==false)
        {
            $newUpdate->mobilePhone  = $request->input("editmobilePhone");
        }
        $newUpdate->fullName  =$request->input("editfullName");
        $newUpdate->ward  = $request->input("editward");
        $newUpdate->district  = $request->input("editdistrict");
        $newUpdate->province  = $request->input("editprovince");    
        $newUpdate->addresssInfo  = $request->input("editaddresssInfo");
        $newUpdate->status = $request->input("editstatus");
        return $newUpdate;
    }
    
    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");
        
        if($this->IsNullOrEmptyString($codeRequest) || $codeRequest =="-1")
        {
                $itemInsert = new AddressSender();
                $itemInsert = $this->InputToAddressSender($request, $itemInsert,$uppdate=false);
                $itemInsert->userId = Auth::user()->id;
                $itemInsert->save();

        }
        else
        {
            $newUpdate = AddressSender::where("id",$codeRequest)->first();
           
            if( $newUpdate)
            {
                $itemInsert = $this->InputToAddressSender($request, $newUpdate);
                $newUpdate->save();
            }
        }
        
        return ["success"=>true];

    }

    public function getAll(Request $request)
    {
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            $start = $request->input('start');
            $data =  DB::table('address_senders');
            //$data= $data->join('users','address_senders.userId','=','users.id');
            
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            if ($request->has('status')) {
                $statusinput = $request->input('status');
                if($statusinput != "All")
                {
                    $data= $data->where('address_senders.status','=' ,$request->input('status'));
                }

            }
            if ($request->has('tokenText')) {
                $search = $request->input('tokenText');
                $data= $data->where(function ($query) use ($search) {
                          $query->where('fullName', 'LIKE', "%{$search}%")
                          ->orwhere('mobilePhone', 'LIKE', "%{$search}%")
                         ->orwhere('addresssInfo', 'LIKE', "%{$search}%");

                    });
            }
            if (Auth::user()->role_id == 2) {
                $data = $data->where('userId', Auth::user()->id);
            }
            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            ->select("address_senders.*")
            // ->offset($start)
            ->limit($limit)
            ->get();
            foreach ($data as $key => $value) {
                $ward = DB::table("ward")->where('id', $value->ward)->first();
                $pr = DB::table("province")->where('id', $value->province)->first();
                $dt = DB::table("district")->where('id', $value->district)->first();
                if (isset($ward) && isset($pr) && isset($dt)) {
                    $value->addr = $value->addresssInfo." ".$ward->name." ".$dt->name." ".$pr->name;
                } else {
                    $value->addr = $value->addresssInfo." ".$value->ward." ".$value->district." ".$value->province;
                }
                
               
            }
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            return ["data"=>$data];
    }

}

