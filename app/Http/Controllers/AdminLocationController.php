<?php
namespace App\Http\Controllers;

use Carbon\Carbon;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Location;

use Illuminate\Support\Facades\Auth;

class AdminLocationController extends Controller
{
    public $modeName = "Location" ;
    public $forderViewRoot ="Location";

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        if(!Auth::check())
        {


            return redirect("/admin/login");
        }
        $forderView  = $this->forderViewRoot;
        return view('vendor.voyager.'.$this->forderViewRoot.'.index', compact("forderView"));


    }



    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editid");

        if($this->IsNullOrEmptyString($codeRequest))
        {

        }
        else
        {

            $user = Auth::user();
            $newUpdate = Location::where("id",$codeRequest)->first();
            if( $newUpdate)
            {
                $newUpdate = $this->ToEntry($request, $newUpdate);
                $newUpdate->save();
            }
        }
        return ["success"=>true];


    }


    private  function ToEntry(Request $request,$newUpdate)
    {

        $newUpdate->Ten = $request->input("Ten");
        $newUpdate->Loai = $request->input("Loai");
        $newUpdate->Ma_Cha = $request->input("Ma_Cha");
        $newUpdate->ContentSender = $request->input("editContentSender");
        return $newUpdate;


    }



    public function view(Request $request, $id)
    {

        $data = Location::where("id", $id)->first();
        $urerLogin = Auth::user();
        if($urerLogin)
        {
        }
        else {
            return redirect("/admin/login");

        }
        $role =  $urerLogin->role_id;
        $agencyCurrent =  null;

        if($role ==2)
        {
            $agencyCurrent = Location::where("email", $urerLogin->email)->first();
        }

        if($data ==null)
        {

             return redirect('dmin/location');
        }
        else
        {

            return view('vendor.voyager.'.$this->forderViewRoot.'.indexedit', compact("data"));
        }

    }



    public function GetAll(Request $request)
    {

            $user = Auth::user();
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            $start = $request->input('start');
            $data =  DB::table("locations");
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            // if ($request->has('status')) {
            //     $statusinput = $request->input('status');
            //     if($statusinput != "All")
            //     {
            //         $data= $data->where('statusOrder','=' ,$request->input('status'));
            //     }
            // }

            $roledAdmin = false;
            if($user->role_id ==1)
            {
                $roledAdmin = true;
            }


            if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            // $data= $data->where(function ($query) use ($search) {


            //     });
            }

            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            -> offset($start)
            ->limit($limit)
            ->get();


            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );

            return ["data"=>$data];
    }




}

