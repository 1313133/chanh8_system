<?php
namespace App\Http\Controllers;

use Artisan;
use Session;
use App\News;
use DateTime;
use Response;
use Validator;
use App\Agency;
use App\Orders;

use App\Ordered;
use App\CartOrder;

use App\TypeOrder;
use Carbon\Carbon;
use App\TransitCar;
use App\CategoryNew;
use App\MethodTrans;
use App\MethodPayment;
use App\OrderedDetail;
use App\ProfileStatus;
use App\CustomerContact;
use App\PaymentMethodPost;
use App\ProfileStatusOrder;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Composer\XdebugHandler\Status;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class AdminNewsController extends Controller
{

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function news()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if($user->role_id ==1)
        {
            $roledAdmin = true;
        }

        $allStatus = ProfileStatusOrder::orderby ("created_at", "desc")
        ->get();

        $allAgency = Agency::orderby("created_at","desc")
        ->get();
        return view('vendor.voyager.managementOrder.index',compact('allStatus','allAgency','roledAdmin'));
    }



    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");

        if($this->IsNullOrEmptyString($codeRequest))
        {
            $item  = new Orders();
            $newitemUpdate = $this->InputToOrder($request, $item);
            $item->statusOrder =  "1";
            $item->save();

            $item->code = $this->GenMaDonHang( $item->agentId).$item->id;
            $item->save();

        }
        else
        {

            $user = Auth::user();
            $newUpdate = Orders::where("code",$codeRequest)->first();
            if( $newUpdate)
            {
                $newUpdate = $this->InputToOrder($request, $newUpdate);
                $newUpdate->save();
            }
        }
        return ["success"=>true];


    }

    private function GenMaDonHang ($madaily)
    {

        $agency = Agency::where("id", $madaily)->first();
        $matinh = $agency->tinh;

        if(strlen($matinh) <2)
        {
            $matinh = '0' +$matinh;
        }

        $mahuyen = $agency->huyen;

        if(strlen($matinh) <2)
        {
            $mahuyen = '0' +$matinh;
        }

        $day = Carbon::now()->format('YYmd');

        return $matinh.$mahuyen.$day."00";
    }

    private  function InputToOrder(Request $request,$newUpdate)
    {
        $newUpdate->fullNameSender = $request->input("editfullNameSender");
        $newUpdate->tinh =$request->input("edittinh");
        $newUpdate->huyen =$request->input("edithuyen");
        $newUpdate->tinhnhan =$request->input("edittinhnhan");
        $newUpdate->huyennhan =$request->input("edithuyennhan");
        $newUpdate->fullNameSender = $request->input("editfullNameSender");
        $newUpdate->mobilePhoneSender = $request->input("editmobilePhoneSender");
        $newUpdate->addressSender = $request->input("editaddressSender");
        $newUpdate->ContentSender = $request->input("editContentSender");
        $newUpdate->typeOrderRel = $request->input("edittypeOrderRel");
        $newUpdate->shutleTaxRel = $request->input("editshutleTaxRel");
        $newUpdate->statusOrder = $request->input("editstatusOrder");
        $newUpdate->NotedSender = $request->input("editNotedSender");
        $newUpdate->RecievedSender = $request->input("editRecievedSender");
        $newUpdate->RecievedAddress = $request->input("editRecievedAddress");
        $newUpdate->agentId =$request->input("editagentId");
        $newUpdate->RecievedMobilePhone = $request->input("editRecievedMobilePhone");
        $newUpdate->NotedRecived = $request->input("editNotedRecived");
        $newUpdate->ProviceReceived = $request->input("editProviceReceived");
        $newUpdate->prices = $request->input("editprices");
        $newUpdate->isSenderMoney = $request->input("editisSenderMoney");
        $newUpdate->methodTrans = $request->input("editmethodTrans");
        $newUpdate->isThuHo = $request->input("editisThuHo");
        $newUpdate->package = $request->input("editpackage");

        return $newUpdate;


    }



    public function editOrder(Request $request, $id)
    {
        $allTrans = TransitCar::all();
        $alltypeOrder = TypeOrder::all();
        $allStatus = ProfileStatusOrder::all();
        $allMehtodTrans = MethodTrans::all();
        $allPaymentMethod= MethodPayment::all();

        $data = Orders::where("id", $id)->first();

        $isAdmin = false;

        if($id =="create")
        {

            $id = -1;
        }

        if($id ==-1)
        {

                 $data = new Orders();
                 $data->isThuHo =2;
        }
        $allagency  = Agency::all();

        $urerLogin = Auth::user();



        if($urerLogin)
        {


        }
        else {
            return redirect("/admin/login");

        }
        $role =  $urerLogin->role_id;
        $agencyCurrent =  null;

        if($role ==2)
        {
            $agencyCurrent = Agency::where("email", $urerLogin->email)->first();
        }
        if($role ==1)
        {

            $isAdmin  = true;
        }

        $addNew = false;

        if($id <1)
        {

            $addNew = true;


        }



        if($data ==null)
        {

             return redirect('dmin/management-order');
        }
        else
        {


            return view("vendor.voyager.managementOrder.indexedit", compact("data","addNew","isAdmin","role",
            "agencyCurrent" , "allagency","allTrans","alltypeOrder","allStatus","allMehtodTrans","allPaymentMethod"));
        }

    }

    public function getAll(Request $request)
    {
        $dtFrom  = null;

        $dtTo  = null;

        if($request->tungay !="")
        {


            try {

                $dtFromText = str_replace('/', '-', $request->tungay);
                $dtFrom = new DateTime($dtFromText);
            } catch (\Throwable $th) {

                $dtFrom = null;
            }

        }
        else
        {
            $dtFrom = null;
        }


        if($request->denngay !="")
        {


            try {

                $dtToText = str_replace('/', '-', $request->denngay);
                $dtTo = new DateTime($dtToText);
            } catch (\Throwable $th) {

                $dtTo = null;
            }

        }
        else
        {
            $dtTo = null;
        }


            $user = Auth::user();
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            $start = $request->input('start');
            $data =  DB::table('orders')
            ->leftJoin('transit_cars', 'orders.shutleTaxRel', '=', 'transit_cars.id')
            ->leftJoin('profile_status_orders', 'orders.statusOrder', '=', 'profile_status_orders.id');
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            if ($request->has('status')) {
                $statusinput = $request->input('status');
                if($statusinput != "All")
                {
                    $data= $data->where('statusOrder','=' ,$request->input('status'));
                }
            }
            if ($request->has('selectAgen')) {
                $statusinput = $request->input('selectAgen');
                if($statusinput != "-1")
                {

                    $data= $data->where('agentId','=' ,$statusinput);
                }
            }

            if ($dtFrom) {

                    $data=  $data->whereDate('created_at', '>=', $dtFrom);

            }


            $roledAdmin = false;
            if($user->role_id ==1)
            {
                $roledAdmin = true;
            }



            if(!$roledAdmin)
            {
                $agencyInfo = Agency::where("email",$user->email)->first();
                 if($agencyInfo)
                 {
                    $data= $data->where('agentId','=' ,$agencyInfo->id);
                 }
            }
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            $data= $data->where(function ($query) use ($search) {
                      $query->where('orders.code', 'LIKE', "%{$search}%")
                      ->orwhere('orders.fullNameSender', 'LIKE', "%{$search}%")
                       ->orwhere('orders.mobilePhoneSender', 'LIKE', "%{$search}%");

                });
            }

            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            ->select("orders.*","transit_cars.text as transitText","profile_status_orders.text as profileText" )
            -> offset($start)
            ->limit($limit)
            ->get();
            foreach($data as $item)
            {
                if($item->methodTrans =="S") {

                    $item->methodTransText ="Giao hàng trong 6 tiếng";
                }
                else  if($item->methodTrans =="C") {

                    $item->methodTransText ="Giao hàng trong 12 tiếng";
                }
                else
                {
                    $item->methodTransText ="";
                }


            }

            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );

            return ["data"=>$data];
    }




}

