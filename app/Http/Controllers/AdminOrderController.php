<?php
namespace App\Http\Controllers;

use Artisan;
use Session;
use App\News;
use App\User;
use Response;
use Validator;
use App\Agency;
use App\Ordered;
use App\Product;

use App\CartItem;
use App\CartOrder;

use Carbon\Carbon;
use App\TypeAgency;
use App\CategoryNew;
use App\LevelAgency;
use App\OrderedDetail;
use App\CustomerContact;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;

class AdminOrderController extends Controller
{


    public function addMutipleToCard (Request $request)
    {
        $dataCode = $request->input("data");



        $allProductInfo = DB::table('products')->whereIn('id',$dataCode)->get();
        $totalCard = 0;
        if (Session::has('cardlist')){
            $cardlist = $request->session()->get('cardlist');

           foreach ($allProductInfo as $itemProduct) {
            $isDuplicate =false;
            $cardInsert = new CartItem();
            $cardInsert->productCode = $itemProduct->ProductCode;
            $cardInsert->number = 1;
            $cardInsert->productCodeText = $itemProduct->ProductCode;

            $arrayUpdate = [];

            foreach ($cardlist as $item) {

                    if($item->productCode == $cardInsert->productCode)
                    {

                    $cardInsert->number   =  $cardInsert->number *1 +  $item->number *1;

                    $cardInsert->object = $itemProduct;
                    array_push($arrayUpdate,$cardInsert);
                    $isDuplicate = true;
                    }
                    else
                    {
                    array_push($arrayUpdate,$item);

                    }
                    }

                    $cardlist =$arrayUpdate;



           }

           $request->session()->forget('cardlist');

           Session::put('cardlist', $arrayUpdate);

           $totalCard = 0;
           foreach ($cardlist as $item) {

               $totalCard  += $item->number;

           }


           $request->session()->forget('totalCard');

           Session::put('totalCard', $totalCard);



       }
       else{



       $arrayList = [];


       foreach($allProductInfo as $productinfo)
       {
        $cardInsert = new CartItem();
        $cardInsert->productCode = $productinfo->ProductCode;
        $cardInsert->number = 1;
        $cardInsert->productCodeText = $productinfo->ProductCode;
        $cardInsert->object = $productinfo;
        array_push($arrayList,$cardInsert);
       }



       Session::put('cardlist', $arrayList);


       foreach ($arrayList as $item) {

           $totalCard  += $item->number;

       }
       $request->session()->forget('totalCard');

       Session::put('totalCard', $totalCard);


    }

    }



    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }

    public function informationCartOrder(Request $request)
    {
        $user = Auth::user();

        $cardlist = $request->session()->get('cardlist');





        if($user==null)
        {
            return redirect("/admin/login");
        }
        $dataUser = DB::table('users')
        ->join('agencies', 'users.email', '=', 'agencies.email')
        ->select("agencies.*")
        ->first();
        return view('vendor.voyager.cartinfo.index',compact("dataUser","cardlist"));

    }


public function checkout( Request $request)
{
    $user = Auth::user();
    $dataUser = DB::table('users')
    ->join('agencies', 'users.email', '=', 'agencies.email')
    ->select("agencies.*")
    ->first();
    $cardOrded = new Ordered();

    $cardOrded->fullName = $dataUser->fullName;
    $cardOrded->mobilePhone = $dataUser->mobilePhone;
    $cardOrded->email =  $dataUser->email;
    $cardOrded->addressHoltel = $dataUser->address;
    $cardOrded->agenCode = $dataUser->agencyCode;

    $cardOrded->noted = $request->input("editnoted");
    $cardOrded->status = "0";
    if (Session::has('cardlist')){
        $cardlist = $request->session()->get('cardlist');
        if(count($cardlist)>0)
        {
            $cardOrded->save();
        }
        $total =0;

        $cardOrded->orderCode = "OrderCar_".$cardOrded->id;
        foreach ($cardlist as $item) {
            $product = Product::where("ProductCode",$item->productCode )->first();
            if($product==null)
            {
                continue;
            }


            $cardOrderDetail = new OrderedDetail();

            $cardOrderDetail->orderId =$cardOrded->id;

            $cardOrderDetail->productId = $item->productCode;
            $cardOrderDetail->number = $item->number;
            $cardOrderDetail->price = $item->pricePresent;

            $total += $item->number*1 *$item->pricePresent;
            $cardOrderDetail->save();
        }
        $cardOrded->totalAmount = $total;

        $cardOrded->save();

        $message = [
            "createDate"=>Carbon::now()->format('d/m/Y'),
            'sumAmount'=>$cardOrded->totalAmount,
            "orderId"=>$cardOrded->id,
            "orderCode"=>$cardOrded->orderCode

        ];
        // SendEmail::dispatch($message, null)->delay(now()->addMinute(1));

            $request->session()->forget('cardlist');


            $request->session()->flash('orderStatus', true);

          return ["success"=>true,"data"=>"/admin/search_product"];
    }
    else{

        return ["success"=>false,"data"=>"Không có thông tin đặt hàng"];
    }

}
    public function index()
    {


        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        $allCategoryNews = TypeAgency::orderby ("created_at", "desc")
        ->get();

        $allLevelAgency = LevelAgency::orderby ("created_at", "desc")
        ->get();
        return view('vendor.voyager.OrderSearch.index',compact('allCategoryNews','allLevelAgency'));
    }

    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        DB::table("agencies")->whereIn('id',$dataCode)->delete();
        return ["success"=>true];
    }

    private  function InputToAgency(Request $request,$newUpdate)
    {
        $newUpdate->status =$request->input("editstatus");
        // $newUpdate->totalAmount =$request->input("totalAmount");
        $newUpdate->fullName =$request->input("editfullName");
        $newUpdate->mobilePhone =$request->input("editmobilePhone");
        $newUpdate->address =$request->input("editaddress");

        $newUpdate->noted =$request->input("editnoted");

        $newUpdate->status =$request->input("editstatus");
        $newUpdate->level =$request->input("editlevel");
        $newUpdate->type =$request->input("edittype");

        return $newUpdate;


    }


    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");



        if($this->IsNullOrEmptyString($codeRequest))
        {
            $itemInsert = new Agency();
            $itemInsert->agencyCode = "Car_agency";
            $itemInsert->email =$request->input("editemail");
            $itemInsert = $this->InputToAgency($request, $itemInsert);
            $user = User::where('email','=', $itemInsert->email);
            $user = new User();
            $user->name  = $itemInsert->fullName;
            $user->email  = $itemInsert->email;
            $user->role_id  = 2;
            $user->password =Hash::make($request->input("password"));


            $user->save();


            DB::insert('insert into user_roles (user_id, role_id) values (?, ?)', [$user->id, 2]);
            $itemInsert->save();
            $itemInsert->agencyCode = "Car_agency".$itemInsert->id;


            $itemInsert->save();

        }
        else
        {
            $newUpdate = Agency::where("id",$codeRequest)->first();
            if($request->input("password") !="")
            {
                $user = User::where('email','=', $newUpdate->email)->first();

                if($user)
                {
                    $user->password =Hash::make($request->input("password"));
                    $user->save();
                }
            }
            if( $newUpdate)
            {
                $itemInsert = $this->InputToAgency($request, $newUpdate);
                $newUpdate->save();
            }
        }
         return ["success"=>true];

    }

    public function  getDetail( Request $request)
    {
        $id = $request->input("id");
        $newsDetail=  Agency::where("id",$id)->first();
        if($newsDetail ==null)
        {
            $newsDetail = new Agency();
        }
        return $newsDetail;
    }

    public function updateStatus (Request $request)
    {

        $id = $request->input("id");

        $status = $request->input("status");
        $newsDetail=  Agency::where("id",$id)->first();
        $newsDetail->status = $status;
        $newsDetail->save();
        return  ["sucess"=>true];
    }

    public function  getAllDetail( Request $request)
    {

        $id = $request->input("id");
        $data=  Agency::where("id",$id)->first();
           $view = view("vendor.voyager.agency.cart",compact("dataAll","data"))->render();
            return response()->json(['html'=>$view]);

    }

    public function  delete( Request $request)
    {
        $id = $request->input("code");
        $newsDetail =  Agency::where("code",$id)->first();
        if($newsDetail->status   == "I")
        {


        }
        else
        {
            $newsDetail->status = "I";
            $newsDetail->save();
        }
        return ["success"=>true];

    }


    public function getAll(Request $request)
    {
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
        $start = $request->input('start');
        $data =  DB::table('products');
        $data =  $data->leftJoin('product_categories', 'product_categories.id', '=', 'products.refCategory');
        $isViewInput = $request->input('status');
        $search = $request->input('tokenText');
        if(!$this->IsNullOrEmptyString($search))
        {
                $data= $data->where(function ($query) use ($search) {
                $query->where('name', 'LIKE', "%{$search}%");
                });
        }
        $totalData = $data->count();
        
        $data= $data
        ->select("products.*", "product_categories.title as productCategory" )
        ->orderBy('created_at','desc')
        -> offset($start)
        ->limit(10000)
        ->get();

        return  array(
        "draw"            => intval($request->input('draw')),
        "recordsTotal"    => intval($totalData),
        "recordsFiltered" => intval($totalData),
        "data"            => $data,
        );
        return ["data"=>$data];
    }



    public function newsDetail($slug)
    {

            return view('newsDetail');

    }







}

