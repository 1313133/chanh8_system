<?php

namespace App\Http\Controllers;

use App\OrderNoted;
use Artisan;
use Session;
use App\News;
use DateTime;
use Response;
use Validator;
use App\Agency;
use App\Orders;
use App\User;

use App\Ordered;
use App\CartOrder;

use App\TypeOrder;
use Carbon\Carbon;
use App\TransitCar;
use App\CategoryNew;
use App\Customer;
use App\HistoryStatusOrder;
use App\MethodTrans;
use App\MethodPayment;
use App\ProfileStatusOrder;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\OrderNew;
use App\Shipper;
use Illuminate\Support\Facades\Auth;

class AdminOrderNewController extends Controller
{

    function IsNullOrEmptyString($str)
    {
        return (!isset($str) || trim($str) === '');
    }
    public function create()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }

        $allCustomer = Customer::all();

        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();

        return view('vendor.voyager.managementOrderNew.create', compact('allStatus', 'allAgency', 'roledAdmin', 'allCustomer'));
    }


    public function news()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }

        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();
        return view('vendor.voyager.managementOrderNew.index', compact('allStatus', 'allAgency', 'roledAdmin'));
    }

    public function HistoryStatusOrder( HistoryStatusOrder $request  )
    {
            $item = new HistoryStatusOrder();
            $user = Auth::user();
            $item->status =  $request->status;
            $item->orderId =  $request->orderId;
            $item->noted =   $request->noted;
            $item->createdBy =  $user->id;
            $item->save();
    }

    public function UpdateStatus(Request $request)
    {
        $user = Auth::user();
        if($user==null)
        {
            return response()->json(["message", "Authentication Required!"], 401);
        }
        $status =    $request->input("status");

        $iDUpdate = $request->input("editId");

        if($iDUpdate <1)
        {
            $returnData = array(
                'status' => 'error',
                'isSucess'=> false,
                'message' => 'Missing param'
            );
            return Response::json($returnData, 500);
        }

        //order
        $orderupdate = OrderNew::where("id",$iDUpdate)->first();
        if($orderupdate==null)
        {

            $returnData = array(
                'status' => 'error',
                'isSucess'=> false,
                'message' => 'Missing param'
            );
            return Response::json($returnData, 500);
        }

        $statusOrder = $orderupdate->Status;
        if($statusOrder == 7 || $statusOrder == 8 || $statusOrder == 5 ||$statusOrder == 4       )
        {
            $returnData = array(
                'status' => 'error',
                'isSucess'=> false,
                'message' => 'Không được cập nhật trạng thái'
            );


            return Response::json($returnData, 200);
        }
        else
        {
            $ordernote = new OrderNoted;
            $ordernote->orderId = $iDUpdate;
            $ordernote->content = $request->input("noted");
            $ordernote->status = $status;
            $ordernote->save();

            $orderupdate->Status = $status;
            $orderupdate->save();
        }

        $returnData = array(
            'isSucess'=> true,
            'message' => 'Cập nhật thành công'
        );
        return Response::json($returnData, 200);

    }

    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");

        if ($this->IsNullOrEmptyString($codeRequest)) {
            $item  = new Orders();
            $newitemUpdate = $this->InputToOrder($request, $item);
            $item->statusOrder =  "1";
            $item->save();

            // $item->code = $this->GenMaDonHang($item->agentId) . $item->id;
            $item->code = "CHANH8-TEST";
            $item->save();
        } else {
            $user = Auth::user();
            $newUpdate = Orders::where("code", $codeRequest)->first();
            if ($newUpdate) {
                $newUpdate = $this->InputToOrder($request, $newUpdate);
                $newUpdate->save();
            }
        }
        return ["success" => true];
    }

    private function GenMaDonHang($madaily)
    {

        $agency = Agency::where("id", $madaily)->first();
        $matinh = $agency->tinh;

        if (strlen($matinh) < 2) {
            $matinh = '0' + $matinh;
        }

        $mahuyen = $agency->huyen;

        if (strlen($matinh) < 2) {
            $mahuyen = '0' + $matinh;
        }

        $day = Carbon::now()->format('YYmd');

        return $matinh . $mahuyen . $day . "00";
    }

    private  function InputToOrder(Request $request, $newUpdate)
    {
        $newUpdate->fullNameSender = $request->input("editfullNameSender");
        $newUpdate->tinh = $request->input("edittinh");
        $newUpdate->huyen = $request->input("edithuyen");
        $newUpdate->tinhnhan = $request->input("edittinhnhan");
        $newUpdate->huyennhan = $request->input("edithuyennhan");
        $newUpdate->fullNameSender = $request->input("editfullNameSender");
        $newUpdate->mobilePhoneSender = $request->input("editmobilePhoneSender");
        $newUpdate->addressSender = $request->input("editaddressSender");
        $newUpdate->ContentSender = $request->input("editContentSender");
        $newUpdate->typeOrderRel = $request->input("edittypeOrderRel");
        $newUpdate->shutleTaxRel = $request->input("editshutleTaxRel");
        $newUpdate->statusOrder = $request->input("editstatusOrder");
        $newUpdate->NotedSender = $request->input("editNotedSender");
        $newUpdate->RecievedSender = $request->input("editRecievedSender");
        $newUpdate->RecievedAddress = $request->input("editRecievedAddress");
        $newUpdate->agentId = $request->input("editagentId");
        $newUpdate->RecievedMobilePhone = $request->input("editRecievedMobilePhone");
        $newUpdate->NotedRecived = $request->input("editNotedRecived");
        $newUpdate->ProviceReceived = $request->input("editProviceReceived");
        $newUpdate->prices = $request->input("editprices");
        $newUpdate->isSenderMoney = $request->input("editisSenderMoney");
        $newUpdate->methodTrans = $request->input("editmethodTrans");
        $newUpdate->isThuHo = $request->input("editisThuHo");
        $newUpdate->package = $request->input("editpackage");

        return $newUpdate;
    }



    public function editOrder(Request $request, $id)
    {
        $allTrans = TransitCar::all();
        $alltypeOrder = TypeOrder::all();
        $allStatus = ProfileStatusOrder::all();
        $allMehtodTrans = MethodTrans::all();
        $allPaymentMethod = MethodPayment::all();

        $data = Orders::where("id", $id)->first();

        $isAdmin = false;

        if ($id == "create") {

            $id = -1;
        }

        if ($id == -1) {

            $data = new Orders();
            $data->isThuHo = 2;
        }
        $allagency  = Agency::all();

        $urerLogin = Auth::user();



        if ($urerLogin) {
        } else {
            return redirect("/admin/login");
        }
        $role =  $urerLogin->role_id;
        $agencyCurrent =  null;

        if ($role == 2) {
            $agencyCurrent = Agency::where("email", $urerLogin->email)->first();
        }
        if ($role == 1) {

            $isAdmin  = true;
        }

        $addNew = false;

        if ($id < 1) {

            $addNew = true;
        }

        if ($data == null) {
            return redirect('admin/tao-don-hang-moi');
        } else {
              return view("vendor.voyager.managementOrderNew.indexedit", compact(
                "data",
                "addNew",
                "isAdmin",
                "role",
                "agencyCurrent",
                "allagency",
                "allTrans",
                "alltypeOrder",
                "allStatus",
                "allMehtodTrans",
                "allPaymentMethod"
            ));
        }
    }

    public function getAll(Request $request)
    {
        $user = Auth::user();
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length'); //số lượng record hiển thị trong 1 trang,mặc định là 10
        $start = $request->input('start');
        $data =  DB::table('order_news')
        ->leftJoin('province', 'order_news.provinces', '=', 'province.id')
        ->leftJoin('district', 'order_news.districts', '=', 'district.id')
        ->leftJoin('ward', 'order_news.wards', '=', 'ward.id')
       ->leftJoin('shippers', 'order_news.ShipperId', '=', 'shippers.id')
        ->leftJoin('method_package_deliveries', 'order_news.MethodId', '=', 'method_package_deliveries.code')
        ->leftJoin('address_stocks', 'order_news.AddressStockId', '=', 'address_stocks.id')
        ->leftJoin('customers', 'order_news.CustomerId', '=', 'customers.id')
        ->leftJoin('profile_status_orders', 'order_news.status', '=', 'profile_status_orders.id');
        $limit = $request->input('length'); //số lượng record hiển thị trong 1 trang,mặc định là 10
        if ($request->has('status')) {
            $statusinput = $request->input('status');
            if ($statusinput != "All") {
                $data = $data->where('order_news.Status', $request->input('status'));
            }
        }

        if ($request->has('selectAgen')) {
            $statusinput = $request->input('selectAgen');
            if ($statusinput != "-1") {

                $data = $data->where('order_news.AgencyId', '=', $statusinput);
            }
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }


        if ($user->role_id == 3) {
            $shipperLogin =  Shipper::where("userId", $user->id)->first();


            $data = $data->where('order_news.ShipperId', '=', $shipperLogin->id);
        }
        else if ($user->role_id ==6)
        {
            $data = $data->where('order_news.AgencyId', '=', $user->companyId);
            $data = $data->where('order_news.shopId', '=', $user->shopId);
        }
        else{
            if (!$roledAdmin) {
                 $data = $data->where('order_news.AgencyId', '=', $user->companyId);
            }
        }

        if ($request->has('tokenText')) {

            $search = $request->input('tokenText');
            $fdate = $request->input('tungay');
            $tdate = $request->input('denngay');
            if(isset($search)){
                $data = $data->where('order_news.code', 'LIKE', "%{$search}%");
            }
            if(isset($fdate)){
                $data =  $data->where('order_news.created_at', '>=', $fdate);
            }
            if(isset($tdate)){
                $data =  $data->where('order_news.created_at', '<=', $tdate);
            }


            $totalData = $data->count();
            $data = $data->orderBy('created_at', 'desc')
                ->select(
                    "order_news.*",
                    "address_stocks.fullAddress as stockaddress",
                    "method_package_deliveries.name as methodName",
                    "profile_status_orders.text as profileText",
                    "customers.addresssInfo as addressCustomer",
                    'province.name as provinceText',
                    'district.name as districtText',
                    'ward.name as wardText',
                    'shippers.fullName as shipperName',
                    'shippers.mobilePhone as mobilePhoneName'

                )
                ->offset($start)
                ->limit($limit)
                ->get();

            // echo $data;
            foreach($data as $item)
            {
                if($item->MethodId =="S") {

                    $item->methodTransText ="Giao nhanh 2h";
                }
                else  if($item->MethodId =="C") {

                    $item->methodTransText ="Giao trong ngày";
                }
                else
                {
                    $item->methodTransText ="";
                }

                if ($item->receiveaddress) {
                    $item->addressCustomer = $item->receiveaddress;
                }
                $item->role = $user->role_id;
            }

            return  array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalData),
                "data"            => $data,
            );

            // return ["data" => $data];
        }
    }


      public function filterAll(Request $request)
    {

        $user = Auth::user();
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length'); //số lượng record hiển thị trong 1 trang,mặc định là 10
        $start = $request->input('start');
        $data =  DB::table('order_news')
        ->leftJoin('province', 'order_news.provinces', '=', 'province.id')
        ->leftJoin('district', 'order_news.districts', '=', 'district.id')
        ->leftJoin('ward', 'order_news.wards', '=', 'ward.id')
       ->leftJoin('shippers', 'order_news.ShipperId', '=', 'shippers.id')
        ->leftJoin('method_package_deliveries', 'order_news.MethodId', '=', 'method_package_deliveries.code')
        ->leftJoin('address_stocks', 'order_news.AddressStockId', '=', 'address_stocks.id')
        ->leftJoin('customers', 'order_news.CustomerId', '=', 'customers.id')
        ->leftJoin('profile_status_orders', 'order_news.status', '=', 'profile_status_orders.id');
        $limit = $request->input('length'); //số lượng record hiển thị trong 1 trang,mặc định là 10
        if ($request->has('status')) {
            $statusinput = $request->input('status');
            if ($statusinput != "All") {
                $data = $data->where('order_news.Status', $request->input('status'));
            }
        }

        if ($request->has('selectAgen')) {
            $statusinput = $request->input('selectAgen');
            if ($statusinput != "-1") {

                $data = $data->where('order_news.AgencyId', '=', $statusinput);
            }
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }


        if ($user->role_id == 3) {
            $shipperLogin =  Shipper::where("userId", $user->id)->first();


            $data = $data->where('order_news.ShipperId', '=', $shipperLogin->id);
        }
        else if ($user->role_id ==6)
        {
            $data = $data->where('order_news.AgencyId', '=', $user->companyId);
            $data = $data->where('order_news.shopId', '=', $user->shopId);
        }
        else{
            if (!$roledAdmin) {
                 $data = $data->where('order_news.AgencyId', '=', $user->companyId);
            }
        }

        if ($request->has('tokenText')) {

            $search = $request->input('tokenText');
            $fdate = $request->input('tungay');
            $tdate = $request->input('denngay');
            if(isset($search)){
                $data = $data->where('order_news.code', 'LIKE', "%{$search}%");
            }
            if(isset($fdate)){
                $data =  $data->where('order_news.created_at', '>=', $fdate);
            }
            if(isset($tdate)){
                $data =  $data->where('order_news.created_at', '<=', $tdate);
            }


            $totalData = $data->count();
            $data = $data->orderBy('created_at', 'desc')
                ->select(
                    "order_news.*",
                    "address_stocks.fullAddress as stockaddress",
                    "method_package_deliveries.name as methodName",
                    "profile_status_orders.text as profileText",
                    "customers.addresssInfo as addressCustomer",
                    'province.name as provinceText',
                    'district.name as districtText',
                    'ward.name as wardText',
                    'shippers.fullName as shipperName',
                    'shippers.mobilePhone as mobilePhoneName'

                )
                ->offset($start)
                ->limit($limit)
                ->get();

            // echo $data;
            foreach($data as $item)
            {
                if($item->MethodId =="S") {

                    $item->methodTransText ="Giao nhanh 2h";
                }
                else  if($item->MethodId =="C") {

                    $item->methodTransText ="Giao trong ngày";
                }
                else
                {
                    $item->methodTransText ="";
                }

                if ($item->receiveaddress) {
                    $item->addressCustomer = $item->receiveaddress;
                }
                $item->role = $user->role_id;
            }

            return  array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($totalData),
                "recordsFiltered" => intval($totalData),
                "data"            => $data,
            );

            // return ["data" => $data];
        }
    }
}
