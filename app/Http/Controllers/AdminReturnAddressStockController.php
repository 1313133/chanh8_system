<?php

namespace App\Http\Controllers;

use Carbon\Carbon;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\AddressStockReturn;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminReturnAddressStockController extends Controller
{

    function IsNullOrEmptyString($str)
    {
        return (!isset($str) || trim($str) === '');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        return view('vendor.voyager.addressStockReturn.index');
    }


    public function getDetailInfomation(Request $request, $slug)
    {
        $id = $slug;
        $dataEdit = AddressStockReturn::where("id", $id)->first();
        $managerInfo = null;

        if ($dataEdit == null) {
            $dataEdit = new AddressStockReturn();

            $dataEdit->id = -1;

            $managerInfo = new User();


            $allSenderStock = AddressStockReturn::where("companyId", Auth::user()->companyId)->get();
            $countStock = count($allSenderStock);
            if ($countStock > 0) {
                $dataEdit->managerId = -1;
            }
        } else {

            if (ctype_digit($dataEdit->ward) && ctype_digit($dataEdit->district)) {
                $w = DB::table("ward")->where('id', $dataEdit->ward)->first();
                $d = DB::table("district")->where('id', $dataEdit->district)->first();
                $dataEdit->wd = $w->name;
                $dataEdit->dt = $d->name;
            }

            $managerInfo = User::where("id", $dataEdit->managerId)->first();
        }
        if ($managerInfo == null) {
            $managerInfo = new User();
        }

        $listprovince = DB::table('province')->orderby('name')->get();
        return view("vendor.voyager.addressStockReturn.detail", compact("dataEdit", "managerInfo", "listprovince"));

    }


    public function deleteMutiple(Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("address_stock_returns")->whereIn('id', $dataCode)->delete();
            return ["success" => true];
        } else {
            return ["success" => false, "description" => "no data picked"];
        }
    }

    private function InputToAddressStock(Request $request, $newUpdate, $uppdate = false)
    {
        if ($uppdate == false) {
            $newUpdate->mobilePhone = $request->input("editmobilePhone");
        }
        $newUpdate->title = $request->input("editfullName");
        $newUpdate->ward = $request->input("editward");
        $newUpdate->district = $request->input("editdistrict");
        $newUpdate->province = $request->input("editprovince");
        $newUpdate->addresssInfo = $request->input("editaddresssInfo");


        $provice = DB::table('province')->where('id', $newUpdate->province)->first();
        $huyen = DB::table('district')->where('id', $newUpdate->district)->first();
        $phuong = DB::table('ward')->where('id', $newUpdate->ward)->first();
        $fullAdressText = '';

        if ($phuong) {
            $fullAdressText = $phuong->name;
        }
        if ($huyen) {
            $fullAdressText = $fullAdressText . "," . $huyen->name;
        }

        if ($provice) {
            $fullAdressText = $fullAdressText . "," . $provice->name;
        }

        $newUpdate->fullAddress = $fullAdressText;

        $newUpdate->status = $request->input("editstatus");
        return $newUpdate;
    }

    public function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");

        if ($this->IsNullOrEmptyString($codeRequest) || $codeRequest == "-1") {
            $itemInsert = new AddressStockReturn();
            $itemInsert = $this->InputToAddressStock($request, $itemInsert, $uppdate = false);
            $itemInsert->userId = Auth::user()->id;
            $itemInsert->companyId = Auth::user()->companyId;
            $userManager = new User();
            $userManager->name = $request->editManagerName;
            $userManager->phoneNumber = $request->editphoneNumber;
            $userManager->email = $request->userName;
            $userManager->password = Hash::make($request->password);
            $userManager->role_id = 6;
            $userManager->active = 1;
            $userManager->role_id = 6;
            $userManager->companyId = Auth::user()->companyId;


            $userManager->save();
            $itemInsert->managerId = $userManager->id;
            $itemInsert->save();
            $userManager->shopId = $itemInsert->id;
            $userManager->save();

        } else {
            $newUpdate = AddressStockReturn::where("id", $codeRequest)->first();

            if ($newUpdate) {
                $itemInsert = $this->InputToAddressStock($request, $newUpdate);
                $userManager = User::where("id", $itemInsert->managerId)->first();


                if ($userManager) {
                    $userManager->name = $request->editManagerName;
                    $userManager->phoneNumber = $request->editphoneNumber;
                    $itemInsert->companyId = Auth::user()->companyId;
                    $itemInsert->save();
                    $userManager->save();

                    $userManager->shopId = $itemInsert->id;
                    $userManager->companyId = Auth::user()->companyId;
                    $userManager->save();
                } else {
                    $userManager = new User();
                    $userManager->name = $request->editManagerName;
                    $userManager->phoneNumber = $request->editphoneNumber;
                    $userManager->email = $request->userName;
                    $itemInsert->companyId = Auth::user()->companyId;
                    $userManager->password = Hash::make($request->password);
                    $userManager->role_id = 6;
                    $userManager->active = 1;
                    $userManager->role_id = 6;

                    $userManager->save();
                    $newUpdate->managerId = $userManager->id;

                    $userManager->shopId = $newUpdate->id;
                    $userManager->save();
                }


                $newUpdate->ward = $request->input("editward");
                $newUpdate->district = $request->input("editdistrict");
                $newUpdate->province = $request->input("editprovince");
                $newUpdate->addresssInfo = $request->input("editaddresssInfo");


                $provice = DB::table('province')->where('id', $newUpdate->province)->first();
                $huyen = DB::table('district')->where('id', $newUpdate->district)->first();
                $phuong = DB::table('ward')->where('id', $newUpdate->ward)->first();
                $fullAdressText = $newUpdate->addresssInfo;

                if ($phuong) {
                    $fullAdressText = $fullAdressText . "," . $phuong->name;
                }
                if ($huyen) {
                    $fullAdressText = $fullAdressText . "," . $huyen->name;
                }

                if ($provice) {
                    $fullAdressText = $fullAdressText . "," . $provice->name;
                }

                $newUpdate->fullAddress = $fullAdressText;

                $newUpdate->save();
            }
        }

        return ["success" => true];

    }


    public function createOrUpdateNOBranch(Request $request)
    {
        $codeRequest = $request->input("editcode");

        if ($this->IsNullOrEmptyString($codeRequest) || $codeRequest == "-1") {
            $itemInsert = new AddressStockReturn();
            $itemInsert = $this->InputToAddressStock($request, $itemInsert, $uppdate = false);

            $itemInsert->companyId = Auth::user()->companyId;

            $itemInsert->managerId = null;
            $itemInsert->save();


        } else {
            $newUpdate = AddressStockReturn::where("id", $codeRequest)->first();

            if ($newUpdate) {
                $itemInsert = $this->InputToAddressStock($request, $newUpdate);
                $userManager = User::where("id", $itemInsert->managerId)->first();

                $newUpdate->companyId = Auth::user()->companyId;
                $newUpdate->ward = $request->input("editward");
                $newUpdate->district = $request->input("editdistrict");
                $newUpdate->province = $request->input("editprovince");
                $newUpdate->addresssInfo = $request->input("editaddresssInfo");

                $provice = DB::table('province')->where('id', $newUpdate->province)->first();
                $huyen = DB::table('district')->where('id', $newUpdate->district)->first();
                $phuong = DB::table('ward')->where('id', $newUpdate->ward)->first();
                $fullAdressText = $newUpdate->addresssInfo;

                if ($phuong) {
                    $fullAdressText = $fullAdressText . "," . $phuong->name;
                }
                if ($huyen) {
                    $fullAdressText = $fullAdressText . "," . $huyen->name;
                }

                if ($provice) {
                    $fullAdressText = $fullAdressText . "," . $provice->name;
                }

                $newUpdate->fullAddress = $fullAdressText;
                $newUpdate->save();

            }
        }

        return ["success" => true];

    }

    public function getAll(Request $request)
    {

        $totalData = 0;
        $totalFiltered = $totalData;
        $limitInput = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
        $start = $request->input('start');
        $data = DB::table('address_stock_returns');
        // $data= $data->join('users','address_stock_returns.managerId','=','users.id');

        $limitNumber = intval($limitInput);//số lượng record hiển thị trong 1 trang,mặc định là 10

        if ($limitNumber == -1) {
            $limitNumber = 10;
        }

        if ($request->has('status')) {
            $statusinput = $request->input('status');
            if ($statusinput != "All") {
                $data = $data->where('address_stock_returns.status', '=', $request->input('status'));
            }

        }

        if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            $data = $data->where(function ($query) use ($search) {
                $query->where('title', 'LIKE', "%{$search}%")
                    ->orwhere('mobilePhone', 'LIKE', "%{$search}%")
                    ->orwhere('addresssInfo', 'LIKE', "%{$search}%");

            });
        }

        if (Auth::user()->role_id == 2) {
            $data = $data->where('address_stock_returns.companyId', Auth::user()->companyId);
        }

        $data->leftJoin('users as u', 'u.id', '=', 'address_stock_returns.managerId');

        $data = $data->orderBy('address_stock_returns.created_at', 'desc')
            ->select(DB::raw("address_stock_returns.*, u.name as managerName, u.phoneNumber as managerphoneNumber"))
            ->limit($limitNumber)
            ->get();

        $totalData = $data->count();

        foreach ($data as $key => $value) {
            $ward = DB::table("ward")->where('id', $value->ward)->first();
            $pr = DB::table("province")->where('id', $value->province)->first();
            $dt = DB::table("district")->where('id', $value->district)->first();
            if (isset($ward) && isset($pr) && isset($dt)) {
                $value->addr = $value->addresssInfo . " " . $ward->name . " " . $dt->name . " " . $pr->name;
            } else {
                $value->addr = $value->addresssInfo . " " . $value->ward . " " . $value->district . " " . $value->province;
            }
        }

        return array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $data,
        );
        return ["data" => $data];
    }
}

