<?php

namespace App\Http\Controllers;

use App\PackageShipper;
use App\User;
use App\Agency;
use App\Shipper;
use Carbon\Carbon;

use App\TypeAgency;
use App\LevelAgency;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use App\ShipperRegisterPackage;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminShipperController extends Controller
{

    function IsNullOrEmptyString($str)
    {
        return (!isset($str) || trim($str) === '');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }
        $allCategoryNews = TypeAgency::orderby("created_at", "desc")
            ->get();
        $allagent = Agency::orderby("created_at", "desc")
            ->get();
        $allLevelAgency = LevelAgency::orderby("created_at", "desc")
            ->get();

        $allPackage = PackageShipper::where("active", "1")->orderby("value")->get();

        return view('vendor.voyager.shipper.index', compact('allCategoryNews', 'allLevelAgency', 'allagent', 'allPackage', 'user'));
    }


    public function getDetailInfomation(Request $request, $slug)
    {
        $id = $slug;
        $dataEdit = Shipper::where("id", $id)->first();
        if ($dataEdit == null) {
            $dataEdit = new Shipper();

            $dataEdit->id = -1;
        } else {
            $dataEdit->agnetname = 'Admin';
            $user = User::where("id", $dataEdit->agencyid)->where('role_id', '=', 2)->first();
            if ($user) {
                $agent = Agency::where('email', $user->email)->first();
                if ($agent) {
                    $dataEdit->agnetname = $agent->fullName;
                }
            }
        }
        return view("vendor.voyager.shipper.detail", compact("dataEdit"));
    }


    public function deleteMutiple(Request $request)
    {

        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table('shippers')
                ->whereIn('id', $dataCode)
                ->update(array('isDelete' => true));
            $arrayshipper = Shipper::whereIn('id', $dataCode)->get();
            for ($i = 0; $i < count($arrayshipper); $i++) {

                $shipperItem = $arrayshipper[$i];
                $userAccount = User::find($shipperItem->userId);
                if ($userAccount) {
                    $userAccount->isDeleteUser = true;
                    $userAccount->save();
                }
            }

            return ["success" => true];
        } else {
            return ["success" => false, "description" => "no data picked"];
        }
    }

    private function InputToShipper(Request $request, $newUpdate, $uppdate = false)
    {
        if ($uppdate == false) {
            $newUpdate->mobilePhone = $request->input("mobilePhone");
        }
        $newUpdate->fullName = $request->input("editfullName");
        $newUpdate->addresssInfo = $request->input("editaddresssInfo");
        $newUpdate->status = $request->input("editstatus");

        $newUpdate->province = $request->input("province");
        $newUpdate->ward = $request->input("ward");
        $newUpdate->district = $request->input("district");

        $newUpdate->tinh = $request->input("edittinh");
        $newUpdate->huyen = $request->input("edithuyen");
        return $newUpdate;
    }

    public function regersterShipper(Request $request, ShipperRegisterPackage $iten)
    {
        // $itemInsert =  new ShipperRegisterPackage();
        // $itemInsert->code =  "1";
        // $itemInsert->packageCode =  "6T";
        // $itemInsert->agencyId =  "4";
        // $itemInsert->shipperId =  "17";
        // $itemInsert->prices =  "600000";
        // $itemInsert->monthRegister =  "6";
        // $itemInsert->voucher =  "1";
        // $itemInsert->registerAt = Carbon::now();
        // $itemInsert->validTo = Carbon::now()->addMonth( $itemInsert->monthRegister +  $itemInsert->voucher);
        // $itemInsert->beginTo = Carbon::now();

        // $itemInsert->paymentStatus =  "0";
        // $itemInsert->payMoney =  "600000";

        // $itemInsert->syntaxContent =  "6T0417";
        // $itemInsert->status =  "0";

        // $itemInsert->paymentDate =  null;
        // $itemInsert->save();


    }

    private function isPhoneUsed($mobilePhone, $id) {
        $findShipperUsedPhone;
        if ($this->IsNullOrEmptyString($id) || $id == "-1") {
            // them moi
            $findShipperUsedPhone = Shipper::where('mobilePhone', $mobilePhone)->first();
        } else {
            // edit
            $findShipperUsedPhone = Shipper::where('id', '<>', $id)->where('mobilePhone', $mobilePhone)->first();
        }

        if($findShipperUsedPhone != null) {
            return true;
        } else {
            return false;
        }

    }

    public function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");
        $validatePhone = $this->isPhoneUsed($request->input("mobilePhone"), $codeRequest);

        if ($validatePhone) {
            return [
                "success" => false,
                "message" =>
                    [
                        "mobilePhone" => 'Số điện thoại đã tồn tại trong hệ thống!'
                    ]
            ];
        }
        
        if ($this->IsNullOrEmptyString($codeRequest) || $codeRequest == "-1") {
            
            $itemInsert = new Shipper();
            $itemInsert = $this->InputToShipper($request, $itemInsert, $uppdate = false);
            $user = new User();
            $user->email = $itemInsert->mobilePhone;
            $user->name = $itemInsert->fullName;
           
            $user->role_id = 3;
            $passwordInput = $request->input("editPasword");
            if ($passwordInput == null || $passwordInput == "") {
                $user->password = Hash::make($itemInsert->mobilePhone);
            } else {
                $user->password = Hash::make($passwordInput);
            }


            $shopUser = Auth::user();

            $agency = Agency::where("id", $shopUser->companyId)->first();
            if ($agency == null) {
                return;
            }
            $count = Shipper::where("agencyid", $agency->id)->count();
            if ($count > 0) {
                $itemInsert->status = "upgrade";
            }
            $itemInsert->agencyid = $shopUser->companyId;
            if ($shopUser->role_id == 6) {
                $itemInsert->shopId = $shopUser->shopId;
            }

            $user->save();
            $itemInsert->userId = $user->id;
            $itemInsert->save();
        } else {
            $newUpdate = Shipper::where("id", $codeRequest)->first();

            if ($newUpdate) {
                $itemInsert = $this->InputToShipper($request, $newUpdate);
                $newUpdate->save();
            }
        }


        return ["success" => true];
    }


    public function createAccount(Request $request)
    {
        $codeRequest = $request->input("editcode");

        if ($this->IsNullOrEmptyString($codeRequest) || $codeRequest == "-1") {
            $itemInsert = new User();
            $itemInsert->email = $request->input("email");
            $itemInsert->name = $request->input("name");
            $itemInsert->phoneNumber = $request->input("phoneNumber");
            $itemInsert->active = $request->input("editstatus");

            $itemInsert->avatar = "users/September2021/ljIBbksQqBHe2fIsWytr.jpg";
            // $user->status = 1;
            $itemInsert->role_id = 1;
            $passwordInput = $request->input("editPasword");
            if ($passwordInput == null || $passwordInput == "") {
                $itemInsert->password = Hash::make($itemInsert->mobilePhone);
            } else {
                $itemInsert->password = Hash::make($passwordInput);
            }


            $itemInsert->save();
        } else {
            $itemInsert = User::where("id", $codeRequest)->first();

            if ($itemInsert) {

                $itemInsert->name = $request->input("name");
                $itemInsert->phoneNumber = $request->input("phoneNumber");
                $itemInsert->active = $request->input("editstatus");
                $itemInsert->save();
            }
        }

        return ["success" => true];
    }


    public function getDetail(Request $request)
    {
        $id = $request->input("id");
        $newsDetail = Shipper::where("id", $id)->first();
        if ($newsDetail == null) {
            $newsDetail = new Shipper();
            $newsDetail->id = -1;
        }
        return $newsDetail;
    }


    public function updateStatus(Request $request)
    {

        $id = $request->input("id");

        $status = $request->input("status");
        $newsDetail = Agency::where("id", $id)->first();
        $newsDetail->status = $status;
        $newsDetail->save();
        return ["sucess" => true];
    }

    public function getAllDetail(Request $request)
    {

        $id = $request->input("id");
        $data = Agency::where("id", $id)->first();
        $view = view("vendor.voyager.shipper.cart", compact("dataAll", "data"))->render();
        return response()->json(['html' => $view]);
    }

    public function delete(Request $request)
    {
        $id = $request->input("code");
        return ["success" => true];
    }


    public function getAll(Request $request)
    {
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length'); //số lượng record hiển thị trong 1 trang,mặc định là 10
        $start = $request->input('start');
        $data = DB::table('shippers');
        $data = $data->where("isDelete", 0);
        $data = $data->join('users', 'shippers.userId', '=', 'users.id');

        $limit = $request->input('length'); //số lượng record hiển thị trong 1 trang,mặc định là 10
        if ($request->has('status')) {
            $statusinput = $request->input('status');
            if ($statusinput != "All") {
                $data = $data->where('shippers.status', '=', $request->input('status'));
            }
        }
        if (Auth::user()->role_id == 2) {
            $data = $data->where('agencyid', Auth::user()->companyId);
        }
        if (Auth::user()->role_id == 6) {
            $data = $data->where('agencyid', Auth::user()->companyId);
            $data = $data->where('shippers.shopId', Auth::user()->shopId);
        }
        if ($request->has('agency') && $request->input('agency') > 0) {

            $data = $data->where('agencyid', $request->input('agency'));
        }
        if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            $data = $data->where(function ($query) use ($search) {
                $query->where('fullName', 'LIKE', "%{$search}%")
                    ->orwhere('mobilePhone', 'LIKE', "%{$search}%")
                    ->orwhere('addresssInfo', 'LIKE', "%{$search}%");
            });
        }

        $totalData = $data->count();
        $data = $data->orderBy('created_at', 'desc')
            ->select(DB::raw("shippers.*"))
            ->offset($start)
            ->limit($limit)
            ->get();

        $currentDate = date("Y-m-d",time());

        foreach ($data as $key => $value) {
            $ward = DB::table("ward")->where('id', $value->ward)->first();
            $pr = DB::table("province")->where('id', $value->province)->first();
            $dt = DB::table("district")->where('id', $value->district)->first();
            if (isset($ward) && isset($pr) && isset($dt)) {
                $value->addr = $value->addresssInfo . " " . $ward->name . " " . $dt->name . " " . $pr->name;
            } else {
                $value->addr = $value->addresssInfo . " " . $value->ward . " " . $value->district . " " . $value->province;
            }

            $paymentQuery = DB::table("payments")
                ->join('package_shippers', 'payments.packageId', '=', 'package_shippers.id')
                ->where("payments.shipperId", "=", $value->id)
                ->selectRaw(DB::raw("payments.*, package_shippers.text as packageName, package_shippers.value as packageTime, package_shippers.price"))
                ->orderByDesc('isPayment')
                ->orderByDesc('approvalDate')
                ->orderByDesc('created_at')
            ;

            $dataPayment = $paymentQuery->first();

            if($dataPayment != null && $dataPayment->isPayment == true) {
                if($dataPayment->expiredDate == null) {
                    $dataPayment->isExpired = false;
                } else {
                    $dataPayment->isExpired = Carbon::now()->gt($dataPayment->expiredDate);
                }
            }

            if($dataPayment != null) {
                $value->payments = $dataPayment;
            } else {
                $value->payments = null;
            }
        }
        return array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $data,
        );
    }


    public function newsDetail($slug)
    {

        return view('newsDetail');
    }
}
