<?php
namespace App\Http\Controllers;


use DateTime;

use App\Agency;
use App\Orders;


use App\TypeOrder;

use App\TransitCar;

use App\MethodTrans;
use App\MethodPayment;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use stdClass;
use TCG\Voyager\Models\Role;

class AdminUserController extends Controller
{

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        $roledAdmin = false;
        if($user->role_id ==1)
        {
            $roledAdmin = true;
        }
        $allCategoryNews =  [];
        $allLevelAgency = [];
        
        return view('vendor.voyager.userMangement.index', compact("allCategoryNews","allLevelAgency"));
    }

    public function profile(Request $request)

     {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if($user->role_id ==1)
        {
            $roledAdmin = true;
        }
        $allCategoryNews =  [];
        $allLevelAgency = [];
        $dataEdit = $user;
       

        
        $allRoles = Role::all();
        return view('vendor.voyager.userMangement.profileedit', compact("allCategoryNews","allLevelAgency","dataEdit","allRoles"));
    }
    public function edit(Request $request, $slug)
    {
        $dataEdit = User::where("id",$slug)->first();

        $allRoles = Role::all();
           

        if($slug =="tao-moi")
        {
            $dataEdit = new User();
            $dataEdit->id = -1;
            $dataEdit->role_id =1;

        }

      
        if($dataEdit==null)
        {
            return;
        }
        return view('vendor.voyager.userMangement.indexedit', compact("dataEdit","allRoles"));

    }

    public function update(Request  $request)
    {

       
        $user = Auth::user();
        $user->name = $request->input("name");
        $user->phoneNumber = $request->input("phoneNumber");
        $roleInput = $request->input("roleId");
        if($user->role_id ==  1)
        {
            //    $user->role_id = $roleInput;
            //    $editstatus = $request->input("editstatus");
            //    $user->active =  $editstatus;
            $user->role_id =  $request->input("roleId"); 
        }
      
        $shop  = Agency::where("email",  $user->email)->first(); 
       if($shop)
       {
        $user->companyId = $shop->id;
       }
       
        $user->save();
        return ["success"=>true];
    
    }



    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");

        if($this->IsNullOrEmptyString($codeRequest))
        {
            $item  = new Orders();
            $newitemUpdate = $this->InputToOrder($request, $item);
            $item->statusOrder =  "1";
            $item->save();

            $item->code = $this->GenMaDonHang( $item->agentId).$item->id;
            $item->save();

        }
        else
        {

            $user = Auth::user();
            $newUpdate = Orders::where("code",$codeRequest)->first();
            if( $newUpdate)
            {
                $newUpdate = $this->InputToOrder($request, $newUpdate);
                $newUpdate->save();
            }
        }
        return ["success"=>true];


    }


    private  function ToUser(Request $request,$update)
    {
        $newUpdate->fullNameSender = $request->input("editfullNameSender");
       

    }



    public function editOrder(Request $request, $id)
    {
        $allTrans = TransitCar::all();
        $alltypeOrder = TypeOrder::all();
        $allStatus = ProfileStatusOrder::all();
        $allMehtodTrans = MethodTrans::all();
        $allPaymentMethod= MethodPayment::all();

        $data = Orders::where("id", $id)->first();

        $isAdmin = false;

        if($id =="create")
        {

            $id = -1;
        }

        if($id ==-1)
        {

                 $data = new Orders();
                 $data->isThuHo =2;
        }
        $allagency  = Agency::all();

        $urerLogin = Auth::user();



        if($urerLogin)
        {


        }
        else {
            return redirect("/admin/login");

        }
        $role =  $urerLogin->role_id;
        $agencyCurrent =  null;

        if($role ==2)
        {
            $agencyCurrent = Agency::where("email", $urerLogin->email)->first();
        }
        if($role ==1)
        {

            $isAdmin  = true;
        }

        $addNew = false;

        if($id <1)
        {

            $addNew = true;


        }



        if($data ==null)
        {

             return redirect('dmin/management-order');
        }
        else
        {


            return view("vendor.voyager.managementOrder.indexedit", compact("data","addNew","isAdmin","role",
            "agencyCurrent" , "allagency","allTrans","alltypeOrder","allStatus","allMehtodTrans","allPaymentMethod"));
        }

    }

    public function GetAll(Request $request)
    {
        $dtFrom  = null;

        $dtTo  = null;
        
            $user = Auth::user();
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            $start = $request->input('start');
            $data =  DB::table('users');
            $data =  $data->leftJoin('roles', 'users.role_id', '=', 'roles.id');
            $data=$data->where("users.role_id",1);
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            if ($request->has('status')) {
                
            }
          
    
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            $data= $data->where(function ($query) use ($search) {
                      $query->where('users.name', 'LIKE', "%{$search}%")
                      ->orwhere('users.email', 'LIKE', "%{$search}%");

                });
            }

            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            ->select("users.*" ,"roles.name as roleName" )
            -> offset($start)
            ->limit($limit)
            ->get();
           
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );

            return ["data"=>$data];
    }




}

