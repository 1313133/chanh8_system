<?php
namespace App\Http\Controllers;
use App\Agency;
use Carbon\Carbon;
use App\TypeAgency;
use App\LevelAgency;

use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Ward;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdminWardController extends Controller
{

    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        return view('vendor.voyager.ward.index');
    }


    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
          $dataEdit = Ward::where("id",$id)->first();
         if($dataEdit ==null)
        {
            $dataEdit = new Ward();

            $dataEdit->id =  -1;
        }
       return view("vendor.voyager.ward.detail", compact("dataEdit"));

    }


    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("ward")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"no data picked"];
        }
    }

    private  function InputToAddressSender(Request $request,$newUpdate,$uppdate =false)
    {
        $newUpdate->name  =$request->input("editfullName");
        $newUpdate->gso_id  = $request->input("editgso");
        $newUpdate->district_id = $request->input("editdistrict_id");
        return $newUpdate;
    }
    
    public  function createOrUpdate(Request $request)
    {
        $codeRequest = $request->input("editcode");
        
        if($this->IsNullOrEmptyString($codeRequest) || $codeRequest =="-1")
        {
                $itemInsert = new Ward();
                $itemInsert = $this->InputToAddressSender($request, $itemInsert,$uppdate=false);
                $itemInsert->save();

        }
        else
        {
            $newUpdate = Ward::where("id",$codeRequest)->first();
           
            if( $newUpdate)
            {
                $itemInsert = $this->InputToAddressSender($request, $newUpdate);
                $newUpdate->save();
            }
        }
        
        return ["success"=>true];

    }

    public function getAll(Request $request)
    {
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            $start = $request->input('start');
            $data =  DB::table('ward');
            //$data= $data->join('users','address_senders.userId','=','users.id');
            
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            if ($request->has('status')) {
                $statusinput = $request->input('status');
                if($statusinput != "All")
                {
                    $data= $data->where('ward.status','=' ,$request->input('status'));
                }

            }
            if ($request->has('tokenText')) {
                $search = $request->input('tokenText');
                $data= $data->where(function ($query) use ($search) {
                          $query->where('name', 'LIKE', "%{$search}%");

                    });
            }
            $totalData = $data->count();
            if (isset($start)) {
                $data= $data->orderBy('district_id')
                ->select("ward.*")
                ->offset($start)
                ->limit($limit)
                ->get();
            } else {
                $data= $data->orderBy('district_id')
            ->select("ward.*")
            // ->offset($start)
            ->limit($limit)
            ->get();
            }
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            //dd($data);  
            return ["data"=>$data];
    }

}

