<?php
namespace App\Http\Controllers;
use App\User;
use App\Agency;
use Carbon\Carbon;
use App\TypeAgency;
use App\LevelAgency;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class AgencyController extends Controller
{
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        $allCategoryNews = TypeAgency::orderby ("created_at", "desc")
        ->get();
        $allLevelAgency = LevelAgency::orderby ("created_at", "desc")
        ->get();
        return view('vendor.voyager.agency.index',compact('allCategoryNews','allLevelAgency'));
    }
    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
        $dataEdit = Agency::where("id",$id)->first();
        
      
        if($dataEdit ==null)
        {
            $dataEdit = new Agency();
            $dataEdit->id = -1;
        }
        $allCategoryNews = TypeAgency::orderby ("created_at", "desc")
        ->get();
        $allLevelAgency = LevelAgency::orderby ("created_at", "desc")
        ->get();
       return view("vendor.voyager.agency.detail", compact("dataEdit","allCategoryNews"));
    }
    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("agencies")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"no data picked"];
        }
    }
    private  function InputToAgency(Request $request,$newUpdate)
    {
        $newUpdate->status =$request->input("editstatus");
        $newUpdate->fullName =$request->input("editfullName");
        $newUpdate->mobilePhone =$request->input("editmobilePhone");
        $newUpdate->address =$request->input("editaddress");
        $newUpdate->noted =$request->input("editnoted");
        $newUpdate->status =$request->input("editstatus");
        $newUpdate->level =$request->input("editlevel");
        $newUpdate->type =$request->input("edittype");
        $newUpdate->tinh =$request->input("edittinh");
        $newUpdate->huyen =$request->input("edithuyen");
        if ($newUpdate) {
            $user  = User::where("email", $newUpdate->email)->first();
            if($user)
            {
                $user->password =  Hash::make($request->input("editPassword"));
                 $user->save();
            }
            else 
            {
            }
         } 
        else {
            $newUpdate->agencyCode =$request->input("editagencyCode");
        }
        return $newUpdate;
    }
    public  function createOrUpdate(Request $request)
    {
   
         $codeRequest = $request->input("editId");
        if($codeRequest <1)
        {
            try {
                DB::beginTransaction();
                $itemInsert = new Agency();
            
                $itemInsert->email =$request->input("editemail");
                $itemInsert = $this->InputToAgency($request, $itemInsert);
                $itemInsert->agencyCode = $request->input("editagencyCode");
                $user = User::where('email','=', $itemInsert->email);
                $user = new User();
                $user->name  = $itemInsert->fullName;
                $user->email  = $itemInsert->email;
                $user->role_id  = 2;
                $user->password =  Hash::make($request->input("editPassword"));
                $user->email_verified_at = Carbon::now();
                $itemInsert->save();
                $itemInsert->save();
                $user->companyId =  $itemInsert->id;
                $user->save();
                DB::insert('insert into user_roles (user_id, role_id) values (?, ?)', [$user->id, 2]);

                DB::commit();
                
            } catch (\Throwable $th) {
                DB::rollBack();
            }
        }
        else
        {
            $newUpdate = Agency::where("id",$codeRequest)->first();
            if( $newUpdate)
            {
                $newUpdate = $this->InputToAgency($request, $newUpdate);
                $user = User::where('email','=', $newUpdate->email)->first();
                if($request->input("editPassword") !="")
                {
                    $user->password =  Hash::make($request->input("editPassword"));
                }
               
                $user->companyId =  $newUpdate->id;
                $user->save();
              
                $newUpdate->save();
            }
        }
         return ["success"=>true];
    }
    public function  getDetail( Request $request)
    {
        $id = $request->input("id");
        $newsDetail=  Agency::where("id",$id)->first();
        if($newsDetail ==null)
        {
            $newsDetail = new Agency();
        }
        return $newsDetail;
    }
    public function updateStatus (Request $request)
    {
        $id = $request->input("id");
        $status = $request->input("status");
        $newsDetail=  Agency::where("id",$id)->first();
        $newsDetail->status = $status;
        $newsDetail->save();
        return  ["sucess"=>true];
    }
    public function  getAllDetail( Request $request)
    {
        $id = $request->input("id");
        $data=  Agency::where("id",$id)->first();
           $view = view("vendor.voyager.agency.cart",compact("dataAll","data"))->render();
            return response()->json(['html'=>$view]);
    }
    public function  delete( Request $request)
    {
        $id = $request->input("code");
        $newsDetail =  Agency::where("code",$id)->first();
        if($newsDetail->status   == "I")
        {
        }
        else
        {
            $newsDetail->status = "I";
            $newsDetail->save();
        }
        return ["success"=>true];
    }
    public function getAll(Request $request)
    {
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            $start = $request->input('start');
            $data =  DB::table('agencies');
            $data =  $data->leftJoin('level_agencies', 'agencies.level', '=', 'level_agencies.id');
            $data =  $data->leftJoin('type_agencies', 'agencies.type', '=', 'type_agencies.id');
            $data= $data->where('status','<>' ,0);
            $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
            if ($request->has('status')) {
                $statusinput = $request->input('status');
                if($statusinput != "All")
                {
                    $data= $data->where('status','=' ,$request->input('status'));
                }
            }
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            $data= $data->where(function ($query) use ($search) {
                      $query->where('agencyCode', 'LIKE', "%{$search}%")
                      ->orwhere('mobilePhone', 'LIKE', "%{$search}%")
                     ->orwhere('fullName', 'LIKE', "%{$search}%");
                });
            }
            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            ->select("agencies.*","level_agencies.text as levelText","type_agencies.text as typeText")
            -> offset($start)
            ->limit($limit)
            ->get();
         
            foreach ($data as $key => $value) {
                $pr = DB::table("locations")->where('id', $value->tinh)->where('Loai',1)->first();
                $dt = DB::table("locations")->where('id', $value->huyen)->where('Loai',2)->first();
                if (isset($pr) && isset($dt)) {
                    $value->addr = $value->address." ".$dt->Ten." ".$pr->Ten;
                } else {
                    $value->addr = $value->address;
                }
            }
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            return ["data"=>$data];
    }
    public function newsDetail($slug)
    {
            return view('newsDetail');
    }
}
