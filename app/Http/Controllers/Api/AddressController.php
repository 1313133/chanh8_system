<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;


class AddressController extends Controller
{
    public function getprovince(Request $request){ 
        $data = DB::table('province')
        ->orderby('name')->get();
        return $data;
       
    }
    public function getdistrict(Request $request){ 
        $data = DB::table('district')->where('province_id', $request->provinces_id)
        
        ->orderby('name')->get();
        return $data;
       
    }
    public function getward(Request $request){
        $data = DB::table('ward')->where('district_id', $request->districts_id)
        ->orderby('name')->get();
        return $data;
    }
    public function get(Request $request){
        $data = DB::table('shippers')->get();
        return $data;
    }
    public function getdistricts(Request $request){ 
        $data = DB::table('district')
                    ->where('province_id', $request->provinces_id)
                   ->orderby('name')->get();
        $xhtml = null;
        foreach($data as $d)
        {
            $xhtml .= '<option data-id="'.$d->id.'" value="'.$d->id.'">'.$d->name.'</option>';
        }
      
        return $xhtml;
       
    }

    public function getwards(Request $request){
        $data = DB::table('ward')->where('district_id', $request->districts_id)->orderby('name')->get();
        $xhtml = null;
        foreach($data as $d)
        {
            $xhtml .= '<option data-id="'.$d->id.'" value="'.$d->id.'">'.$d->name.'</option>';
        }
      
        return $xhtml;
    }
}
