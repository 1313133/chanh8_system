<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\AddressSender;

class AddressSenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AddressSender::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [   
            'mobilePhone' => 'required|regex:/(0)[0-9]{9}/',
            'fullName' => 'required|string',
          ]);
          if ($validator->fails())
          {
              return response(['errors'=>$validator->errors()->all()], 422);
          }
          $data = new AddressSender; 
          $array = $request->only(['mobilePhone', 'avatarLink','fullName','userId','district','ward','province','addresssInfo','status']);
          foreach ($array as $key => $value)
          {
              $data->$key = $value;
          }
          $data->save();
          $response = ["message" =>'successful',"data"=>$data];
          return response($response, 200);
  
    }

    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function Search($phone)
    {
        $data = AddressSender::where('mobilePhone',$phone)->get();
        if (isset($data)) {
           return $data;
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [   
            'mobilePhone' => 'required|regex:/(0)[0-9]{9}/',
            'fullName' => 'required|string',
          ]);
          if ($validator->fails())
          {
              return response(['errors'=>$validator->errors()->all()], 422);
          }
          $data = AddressSender::find($id); 
          $array = $request->only(['mobilePhone', 'avatarLink','fullName','userId','district','ward','province','addresssInfo','status']);
          foreach ($array as $key => $value)
          {
              $data->$key = $value;
          }
          $data->save();
          $response = ["message" =>'successful',"new-data"=>$data];
          return response($response, 200);
    }


    public function CreateOrupdate(Request $request)
    {
        $validator = Validator::make($request->all(), [   
            'mobilePhone' => 'required|regex:/(0)[0-9]{9}/',
            'fullName' => 'required|string',
          ]);
          if ($validator->fails())
          {
              return response(['errors'=>$validator->errors()->all()], 422);
          }
          $data = AddressSender::where("mobilePhone",$request->input("mobilePhone"))->first();
          if($data)
          {
             return $this->update($request,$data->id);
          }
          else 
          {
             return  $this->store($request);
          }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (AddressSender::destroy($id)) {
            $response = ["message" =>'successful'];
            return response($response, 200);
        } else {
            $response = ["message" =>'something went wrong'];
            return response($response, 201);
        }
    }
}
