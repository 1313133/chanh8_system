<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Shipper;
use App\Agency;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use phpseclib\Crypt\Hash as CryptHash;

class AuthController extends Controller
{
    public function loginEmail (Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->email)->where('email_verified_at','<>',null)->first();
        $isagency = Agency::where('email', $request->email)->where('status',1)->first();
        if ($user && $isagency) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('Grant Shipper')->accessToken;
                $response = ['token' => $token,
                            "message" =>'successful'];
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'User does not exist or not active, please contact the admin'];
            return response($response, 422);
        }
    }
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'curpassword'=> 'required',
            'newpassword' => 'required|min:8',
            'password_confirmation'=> 'required|same:newpassword'
        ]);
        if ($validator->fails())
        {
            return response(['error'=>$validator->errors()->all()], 422);
        }
        $user = User::find($request->id);
        if (!$user) {

            $ship = Shipper::find($request->id);
            $user = User::find($ship->userId);
        }
        $curPassword = $request->curpassword;
        $newPassword = $request->newpassword;

            if (Hash::check($curPassword, $user->password)) {

                $user_id = $user->id;

                DB::table('users')->where('id',$user_id)->update(['password'=>Hash::make($newPassword)]);
                $response = ["mess" =>'Thông tin của bạn đã được cập nhật'];
                return response($response, 200);
            }
            else
            {
                $response = ["error" =>'Mật khẩu cũ không đúng'];
                return response($response, 422);
            }
    }

    public function changePasswordAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'curpassword'=> 'required',
            'newpassword' => 'required|min:8',
            'password_confirmation'=> 'required|same:newpassword'
        ]);
        if ($validator->fails())
        {
            return response(['error'=>$validator->errors()->all()], 422);
        }
        $user = User::find($request->id);

        // if (!$user) {

        //     $user = User::find($request->id);
        //     $user = User::find($ship->userId);
        // }
        $curPassword = $request->curpassword;
        $newPassword = $request->newpassword;

            if (Hash::check($curPassword, $user->password)) {

                $user_id = $user->id;

                DB::table('users')->where('id',$user_id)->update(['password'=>Hash::make($newPassword)]);
                $response = ["mess" =>'Thông tin của bạn đã được cập nhật'];
                return response($response, 200);
            }
            else
            {

                $response = ["error" =>'Mật khẩu cũ không đúng'];
                return response($response, 422);
            }
    }
    public function resgiteragent (Request $request) {

        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|min:6',
            'fullName' => 'required|string|min:6',
            'mobilePhone' => 'regex:/(0)[0-9]{9}/',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::where('email', $request->email)->first();
        $phone = Agency::where('mobilePhone',$request->mobilePhone)->first();
        if ($user || $phone) {
            $response = ["message" =>'User exist'];
            return response($response, 422);
        } else {
            try {
                DB::beginTransaction();
                $itemInsert = new Agency();
                $itemInsert->email =$request->input("email");
                $itemInsert->fullName =$request->input("fullName");
                $itemInsert->mobilePhone =$request->input("mobilePhone");
                $itemInsert->status = 0;
                $user = new User();
                $user->name  = $itemInsert->fullName;
                $user->email  = $itemInsert->email;
                $user->email_verified_at  = Carbon::now()->format('Y-m-d H:i:s');
                $user->role_id  = 2;
                $user->password =Hash::make($request->input("password"));
                $user->save();
                $itemInsert->save();
                $itemInsert->agencyCode = "Car_agency".$itemInsert->id;

                //$user->sendEmailVerificationNotification();
                $itemInsert->save();
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollBack();
            }
            $response = ["message" =>'successful', 'agent'=> $itemInsert];
                return response($response, 200);
        }
    }
    public function login (Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/(0)[0-9]{9}/',
        ]);
        $typeLogin = "otp";
        if ($request->has('typeLogin')) {
            $typeLogin = $request->input("typeLogin");

        }

        if($typeLogin =="password")
        {
            $validator = Validator::make($request->all(), [
                'password' => 'required',
            ]);
        }

        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $shipper = Shipper::where('mobilePhone', $request->phone)
                          ->where("isDelete",false)
                         ->first();



        if ($shipper) {
            $user = User::where('id', $shipper->userId)
                        ->where("isDeleteUser",false);
            $user = $user->first();

            if($typeLogin =="password")
           {
                if (Hash::check($request->input("password"), $user->password))
                {

                }
                else
                {
                    $user = null;
                }
            }

            if ($user) {

                $token = $user->createToken('Grant Shipper')->accessToken;
                $shipper->status = 'successful';
                $shipper->save();
                $response = ['token' => $token,
                            "message" =>'successful'];
                return response($response, 200);
            } else {
                $response = ["message" =>'User does not exist'];
                return response($response, 422);
            }

        }
        else {
            $response = ["message" =>'User does not exist'];
            return response($response, 422);
        }

    }
    public function otpverify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|regex:/(0)[0-9]{9}/',
            'otp' => 'required',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $shipper = Shipper::where('mobilePhone', $request->phone)->first();
        if ($shipper) {
            if ($shipper->status == $request->otp) {
                $user = User::where('id', $shipper->userId)->first();
                $token = $user->createToken('Grant Shipper')->accessToken;
                $shipper->status = 'successful';
                $shipper->save();
                $response = ['token' => $token,
                            "message" =>'successful'];
                return response($response, 200);
            } else {
                $response = ["message" =>'OTP does not exist'];
                return response($response, 422);
            }

        } else {
            $response = ["message" =>'OTP does not exist'];
            return response($response, 422);
        }
    }
    public function logout (Request $request) {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }
    public function getuserdata ()
    {
        $userLogin  = Auth::user();


        return $userLogin;
    }
}

