<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Services\ImgurService;
use App\OrderNew;
use App\ProfileStatusOrder;
use App\TypeOrder;
use App\OrderNewDetail;
use App\TransitCar;
use App\Agency;
use App\AddressSender;
use App\AddressStock;
use App\AddressStockReturn;
use App\Customer;
use App\MethodPackageDelivery;
use App\MethodTrans;
use App\User;
use App\OrderNoted;
use PDFS;
use QrCode;
use Carbon\Carbon;
use App\Shipper;
use App\StatusOrder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OderController extends Controller
{
    public function Store(Request $request)
    {
        $validator = Validator::make($request->all(), [
          //  'mobilePhoneSender' => 'required|regex:/(0)[0-9]{9}/',
            'AddressSenderId' => 'required|numeric',
            'CustomerId' => 'required|numeric',
            'AddressStockId' => 'numeric',
            'AgencyId' => 'required|numeric',
            'ShipperId' => 'required|numeric',
            'MethodId' => 'regex:/[S,C]{1}/|max:1',
            'TotalValue' => 'required',
            'isSenderPayment' => 'required|regex:/[0,1]{1}/|max:1',
            'isThuHO' => 'required|regex:/[0,1]{1}/|max:1',
            'ChargeOrder' => 'required',
            'COD' => 'required',
            'TypeOrderId' => 'required|numeric',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $sender = AddressSender::find($request->AddressSenderId);
        $cust = Customer::find($request->CustomerId);
       // dd($sender);
        if (is_null($sender) || is_null($cust)) {
            $response = ["message" =>'Sender or Customer not exists'];
            return response($response, 402);
        }
        $agen = User::where('id',$request->AgencyId)->where('role_id',2)->first();
        $tmpshipper = Shipper::where('id',$request->ShipperId)->first();
        $tmpshipper ? $tmpshipper : 0;
        $ship = User::where('id',$tmpshipper->userId)->where('role_id',3)->first();
        if (!isset($agen) || !isset($ship) ) {
            $response = ["message" =>'Agency or shipper not exists'];
            return response($response, 402);
        }
        $data = new OrderNew;
        $array = $request->only(['AddressSenderId', 'CustomerId','AddressStockId','AgencyId','ShipperId','MethodId','TotalValue','COD','isSenderPayment','isThuHO','TypeOrderId','BusinessTime','ChargeOrder']);
        foreach ($array as $key => $value)
        {
            $data->$key = $value;
        }
        $data->save();
        $response = ["message" =>'successful',"data"=>$data];
        return response($response, 200);

    }
    public function Update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), [
        //  'mobilePhoneSender' => 'required|regex:/(0)[0-9]{9}/',
          'AddressSenderId' => 'required|numeric',
          'CustomerId' => 'required|numeric',
          'AddressStockId' => 'numeric',
          'AgencyId' => 'required|numeric',
          'ShipperId' => 'required|numeric',
          'MethodId' => 'regex:/[S,C]{1}/|max:1',
          'TotalValue' => 'required',
          'isSenderPayment' => 'required|regex:/[0,1]{1}/|max:1',
          'isThuHO' => 'required|regex:/[0,1]{1}/|max:1',
          'ChargeOrder' => 'required',
          'COD' => 'required',
          'TypeOrderId' => 'required|numeric',
          'Status' => 'numeric',
      ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $sender = AddressSender::find($request->AddressSenderId);
        $cust = Customer::find($request->CustomerId);
       // dd($sender);
        if (is_null($sender) || is_null($cust)) {
            $response = ["message" =>'Sender or Customer not exists'];
            return response($response, 402);
        }
        $agen = User::where('id',$request->AgencyId)->where('role_id',2)->first();
        $tmpshipper = Shipper::where('id',$request->ShipperId)->first();
        $tmpshipper ? $tmpshipper : 0;
        $ship = User::where('id',$tmpshipper->userId)->where('role_id',3)->first();
        if (!isset($agen) || !isset($ship) ) {
            $response = ["message" =>'Agency or shipper not exists'];
            return response($response, 402);
        }
        $data = OrderNew::find($id);
        if (isset($data)) {
            $array = $request->only(['AddressSenderId', 'CustomerId','AddressStockId','AgencyId','ShipperId','MethodId','TotalValue','COD','isSenderPayment','isThuHO','TypeOrderId','BusinessTime','ChargeOrder','Status']);
            foreach ($array as $key => $value)
            {
                $data->$key = $value;
            }
            $data->save();
            $response = ["message" =>'successful',"new-data"=>$data];
            return response($response, 200);
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }


    }

    # Hàm update và store cần dữ liệu đầu vào có tên (Field Name ) trùng với tên cột trong database

    public function Destroy($id)
    {
        if (OrderNew::destroy($id)) {
            $response = ["message" =>'successful'];
            return response($response, 200);
        } else {
            $response = ["message" =>'something went wrong'];
            return response($response, 201);
        }

    }
    public function Find($id)
    {
        $tmpshipper = Shipper::where('userId',Auth::user()->id)->first();


        if($tmpshipper)
        {

        }
        else {

            $response = ["message" =>'Không tìm thấy shipper'];
            return response($response, 401);
        }
        $data = OrderNew::Where("id",$id)
                        ->where("ShipperId", $tmpshipper->id)
                        ->first();
        if (isset($data)) {
            return $this->getdata($data);
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }


    }
    public function getHistoryNote(Request $request)
    {
        $tmpshipper = Shipper::where('userId',Auth::user()->id)->first();
        if($tmpshipper)
        {

        }
        else {

            $response = ["message" =>'Không tìm thấy shipper'];
            return response($response, 401);
        }
        $orderCode = $request->input("orderCode");

        $order = OrderNew::where("code",$orderCode )
                ->where("ShipperId", $tmpshipper->id)
                ->first();



        $orderNodes=  [];
        if($order)
        {
             $orderNodes = OrderNoted::leftJoin('profile_status_orders', function($join) {
                                    $join->on('profile_status_orders.id', '=', 'order_noteds.status');
                                })
                                ->where("orderId", $order->id)
                                ->where("companyId", $order->companyId)
                                ->select("order_noteds.*", "profile_status_orders.text as statusName")
                                ->orderBy("created_at", "desc")
                                ->get();
        }

        $response = ["message" =>'successful',"data"=>$orderNodes];
        return response($response, 200);


    }
    public function Search($code)
    {


        $tmpshipper = Shipper::where('userId',Auth::user()->id)->first();


        if($tmpshipper)
        {

        }
        else {

            $response = ["message" =>'Không tìm thấy shipper'];
            return response($response, 401);
        }
        $data = OrderNew::where('code',$code)
             ->where("ShipperId", $tmpshipper->id)
             ->first();
        if (isset($data)) {
            $response = array();
            array_push($response, $this->getdata($data));
           return $response;
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }

    }
    public function getstatus($code)
    {
        $data = OrderNew::where('code',$code)->first();
        if (isset($data)) {
            $detail = ProfileStatusOrder::where('id',intval($data->Status))->first();
           // dd($detail);
            $response = ["Status" =>$detail->text];
            return response($response, 200);
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }

    }
    public function getdata($data)
    {


            $returnStockIt = $data->AddressStockReturnId;

            $stockReturnNameText = "";
            $stockReturnPhoneText ="";
            $stockReturnAddressText ="";

            $returnStock = AddressStockReturn::where("id", $returnStockIt)->first();
            $fullAdressTextStockReceived ="";
            if($returnStock)
            {
                $fullAdressTextStockReceived = $returnStock->addresssInfo.",".$returnStock->fullAddress;
                $stockReturnPhoneText = $returnStock->mobilePhone;
                $stockReturnNameText = $returnStock->title;
            }


            $response = [];
             $sender  = AddressStock::where("id", $data->AddressStockId)->first();

            // $sender = AddressStock::find($data->AddressSenderId);

            if (isset($sender)) {
                $pase2 =[
                    "sender" =>$sender->title,
                    "sendermobile"=>$sender->mobilePhone,
                    "senderaddress"=>$sender->addresssInfo.",".$sender->fullAddress,
                ];
                $response = array_merge($response,$pase2);
            }
            $provice = DB::table('province')->where('id', $data->provinces)->first();
            $huyen = DB::table('district')->where('id', $data->districts)->first();
            $phuong = DB::table('ward')->where('id', $data->wards)->first();

            $fullAdressText = $data->receiveaddress;
            if($phuong)
            {
                $fullAdressText = $fullAdressText.",".$phuong->name;
            }
            if($huyen)
            {
                $fullAdressText = $fullAdressText.",".$huyen->name;
            }

            if($provice)
            {
                $fullAdressText = $fullAdressText.",".$provice->name;
            }

            $cust = Customer::find($data->CustomerId);

            if (isset($cust)) {
                $pase2 =[
                    "customer" =>$cust->fullName,
                    "customermobile"=>$cust->mobilePhone,
                    "customeraddress"=> $fullAdressText
                ];
                $response = array_merge($response,$pase2);
            }
            $agen = User::where('id',$data->AgencyId)->where('role_id',2)->first();
            if (isset($agen)) {
                $pase2 =[
                    "agency"=> $agen->name,
                ];
                $response = array_merge($response,$pase2);
            }
            $stock = AddressStock::find($data->AddressStockId);

            if (isset($stock)) {
                $pase2 =[
                    "Stockname" =>$sender->title,
                    "stockmobile"=>$sender->mobilePhone,
                    "addressstock"=>$sender->addresssInfo.",".$sender->fullAddress,
                ];

                $response = array_merge($response,$pase2);
            }

            $statusItem = ProfileStatusOrder::where("id", $data->Status)->first();


            $statusText = "";
            if($statusItem)
            {
                $statusText =  $statusItem->text;
            }
            
            $totalValueText = 0;
          
            
            if($data->isPaymentOrder == 1 )
            {
                $totalValueText+=  $data->ChargeOrder;
              
            }

           

            if($data->isSenderPayment == 1 )
            {
                $temp =str_replace('.','',$data->COD);
                $totalValueText+= $temp*1;
             
            }
            $methodOrder = MethodPackageDelivery::where("code",$data->MethodId)->first();
            $methodText =  "";
            if($methodOrder)
            {
                $methodText =  $methodOrder->name;
            }
            $responsedata = [
            "price"=>$data->TotalValue,
            "transportfee"=>$data->ChargeOrder,
            'methodTransportName'=>$methodText,
            "cod"=> $totalValueText,
            "isSenderPayment"=>$data->isSenderPayment,
            "isThuHO"=>$data->isThuHO,
            "status"=>$data->Status,
            "id"=>$data->id,
            "code"=>$data->Code,

            "statusName"=>$statusText,
            "description"=> $data->description,

            "link"=>'/admin/printpdf/'.$data->Code,
            "statusOrder"=> [
                "code"=> $data->Status,
                "titleDisplay"=> $statusText

            ],
            "stockReturnOrder" => [
                "phoneNumber" => $stockReturnPhoneText,
                "name" => $stockReturnNameText,
                "address"=>$fullAdressTextStockReceived
            ],
            "stockSenderOrder" => [
                "phoneNumber" => $sender->mobilePhone,
                "name" => $sender->title,
                "address"=>$sender->addresssInfo.",".$sender->fullAddress,
            ],
             "MethodShipper" => [
                "code" => $methodOrder->code,
                "titleDisplay" => $methodText,
                "charge"=> $data->ChargeOrder,
             ],
             "notedOrder"=> $data->notedOrder
            ];

            $response = array_merge($response,$responsedata );
            $OrderNewDetail = OrderNewDetail::where('orderId',$data->id)->first();
            if (isset($OrderNewDetail)) {
                $pase2 =[
                    "name"=>$OrderNewDetail->name,
                    "SizeOrder"=>$OrderNewDetail->SizeOrder,
                    "isDangerous"=>$OrderNewDetail->isDangerous,
                    "description"=>$OrderNewDetail->shortdescription
                ];
                //$response = $pase2;
                $response = array_merge($pase2,$response);
            }
            return $response;
    }


    public function saveImage($image)
    {
        $imageUrl = ImgurService::uploadImage($image->getRealPath());
        return $imageUrl;
    }

    public function getlistorder()
    {
        if ( Auth::user() ==null )
        {
            $response = ["message" =>'you are not a shipper'];
            return response($response, 404);

        }

        if ( Auth::user()->role_id == 3) {
                 $tmpshipper = Shipper::where('userId',Auth::user()->id)->first();

                if($tmpshipper == null)
                {
                    $response = ["message" =>'you are not a shipper'];
                    return response($response, 404);
                }

                $data = OrderNew::where('ShipperId',$tmpshipper->id)->get();
                $response = array();
                foreach ($data as $key => $value) {
                    $detail= $this->getdata($value);
                    array_push($response,$detail);
                }
                return $response;

        }
         else {

            $response = ["message" =>'you are not a shipper'];
            return response($response, 404);
        }
    }
    public function getordernote($id)
    {
        $data =OrderNoted::where('orderId',$id)->first();
        if (isset($data)) {
            // $orderlog = ;
            $data->content = explode('-@-',$data->content);
           return $data;
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }
    }

    public function getnote($id)
    {
        $data =OrderNoted::where('orderId',$id)->first();
        if (isset($data)) {
            $orderlog = explode('@',$data->content);
            return $orderlog;
        } else {
            $response = ["message" =>'No result match'];
            return response($response, 404);
        }
    }
    public function updateaddr(Request $request)
    {
        $urlprefer = request()->headers->get('referer');
        $pos = strrpos($urlprefer, '/', -1) + 1;
        $id = substr($urlprefer,$pos);
        $order = OrderNew::find($id);
        if ($request->has('ward') && $order) {
            $ward = DB::table("ward")->where('id', $request->ward)->first();
            $pr = DB::table("province")->where('id', $request->province)->first();
            $dt = DB::table("district")->where('id', $request->district)->first();
            if (isset($ward) && isset($pr) && isset($dt)) {
                $order->receiveaddress = $request->addresssInfo." ".$ward->name." ".$dt->name." ".$pr->name;
            }
            $order->save();
        }
    }
    public function getsomeinfortostore()
    {
        $typeoforder = TypeOrder::all();
        $transcar = TransitCar::all();
        $agency = User::where('role_id',2)->get();
        $shipper = User::where('role_id',3)->get();;
        $response = ["typeofoder" =>$typeoforder,"transcar"=> $transcar,"agency"=> $agency,"shipper"=>$shipper];
        return response($response, 200);
    }
    public function generate_pdf()
    {
        return view('vendor.voyager.pdf.index');
    }
    public function generatepdf($code)
    {
        $data = OrderNew::where('order_news.code',$code)
        ->leftJoin('province', 'order_news.provinces', '=', 'province.id')
        ->leftJoin('district', 'order_news.districts', '=', 'district.id')
        ->leftJoin('ward', 'order_news.wards', '=', 'ward.id')
        ->leftJoin('customers', 'customers.id', '=', 'order_news.CustomerId')
        ->leftJoin('address_stocks', 'order_news.AddressStockId', '=', 'address_stocks.id')
        ->leftJoin('method_package_deliveries', 'order_news.MethodId', '=', 'method_package_deliveries.code')
        ->select("address_stocks.fullAddress as stockAdress",
        "address_stocks.title as stockName",
        "address_stocks.mobilePhone as stockmobilePhone",
        'province.name as provinceText',
        'district.name as districtText',
        'ward.name as wardText',
         'customers.fullName as customerName',
        'customers.mobilePhone as customerPhone',
        'method_package_deliveries.name as methodName',

        'method_package_deliveries.rateShipper as 	rateShipper',
        "order_news.*")
        ->first();
        if (isset($data)) {
             return view('pdf.document',['data'=>$data]);
        } else {

          return redirect()->route('genpdf')->withErrors('mã không tồn tại');
        }
    }
    public function generate(Request $request)
    {
        $array = $request->only(['serachText','type']);
        //dd($array);
        if ($array['type'] == 1) {
            $data = OrderNew::where('code',$array['serachText'])->first();
        } else {
            $data = OrderNew::where('id',$array['serachText'])->first();
        }
        if (isset($data)) {
            $response = array();
            array_push($response, $this->getdata($data));
            return $response;
        } else {
            $response = ["message" =>'No result match'];
            return response($response);
    }
        $data = [
			'code' => $code,
		];

        return view('vendor.voyager.pdf.index',$data);
		$pdf = PDFS::loadView('pdf.document', $data);
		return $pdf->stream();

    }


    public function addNoted(Request $request)
    {
        $validator = Validator::make($request->all(), [

                'id' =>'required|string'
         ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $order = OrderNew::find($request->input("id"))->first();

        $ordernote = new OrderNoted;
        $ordernote->orderId = $request->input("id");
        $ordernote->content = $request->input("shortdescription");
        $ordernote->status = $request->input("Status");
        $ordernote->LinkImage = $request->input("imageLink");

        $ordernote->shopId = $order->shopId;
        $ordernote->companyId = $order->companyId;
        $ordernote->save();

//        Log::debug('addNoted');
//        Log::debug($request->all());

        $response = ["success" =>'Thêm mới thành công'];
        return response($response, 200);

    }


    public function updateStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [

                'id' =>'required|string'
         ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }

        $order = OrderNew::where("id",$request->input("id"))->first();

        $order->Status = $request->input("Status");

        $ordernote = new OrderNoted;
        $ordernote->orderId = $request->input("id");
        $ordernote->content = $request->input("shortdescription");
        $ordernote->status = $request->input("Status");
        $ordernote->LinkImage = $request->input("imageLink");

        $ordernote->shopId = $order->shopId;
        $ordernote->companyId = $order->companyId;
        $ordernote->save();
        $order->save();

        Log::debug('updateStatus');
        Log::debug($request->all());

        $response = ["success" =>'Cập nhật thành công'];
        return response($response, 200);

    }
}
