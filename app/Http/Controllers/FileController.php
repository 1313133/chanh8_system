<?php
namespace App\Http\Controllers;



use App\Imports\FileHandlerBusiness;
use League\Flysystem\Util;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use TCG\Voyager\Facades\Voyager;
use Intervention\Image\Constraint;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use TCG\Voyager\FormFields\FileHandler;

class FileController extends Controller
{


    function remoteFileExists($url) {
        $curl = curl_init($url);

curl_setopt( $curl, CURLOPT_AUTOREFERER, TRUE );
curl_setopt( $curl, CURLOPT_HEADER, 0 );
curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt( $curl, CURLOPT_URL, $url );
curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, TRUE );

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
        }

    function dfCurl($url){

        $ch     =   curl_init($url);
        if(!$this->remoteFileExists($url))

        {
            return "";
        }
        $dir            =   "storage\\products\\";
        $fileName       =   basename($url);
        $saveFilePath   =   $dir . $fileName;
        $content = file_get_contents($url);

        file_put_contents($saveFilePath, $content);

        return  Voyager::image("products\\".$fileName);
 }
    public function CreateFile(Request $request)
    {

        // $url = "https://viflive.demo-maskcodex.com/storage/products/January2021/EH2fxT7rSaJe0PjPu7dv.png";
         $url = "http://vif.dev/storage/products/EH2fxT7rSaJe0PjPu7dv.png";
        $external_link = $url;

        $fileHandle = new FileHandlerBusiness();

        dd($fileHandle->CopyFileTo($url));



    }

    public function upload(Request $request)
    {

        $fullFilename = null;
        $resizeWidth = 1800;
        $resizeHeight = null;
        $slug = $request->input('type_slug');
        $file = $request->file('image');

        $path = $slug.'/'.date('F').date('Y').'/';

        $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
        $filename_counter = 1;

        // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        while (Storage::disk(config('voyager.storage.disk'))->exists($path.$filename.'.'.$file->getClientOriginalExtension())) {
            $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).(string) ($filename_counter++);
        }

        $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();

        $ext = $file->guessClientExtension();

        if (in_array($ext, ['jpeg', 'jpg', 'png', 'gif'])) {
            $image = Image::make($file)
                ->resize($resizeWidth, $resizeHeight, function (Constraint $constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            if ($ext !== 'gif') {
                $image->orientate();
            }
            $image->encode($file->getClientOriginalExtension(), 75);

            // move uploaded file from temp to uploads directory
            if (Storage::disk(config('voyager.storage.disk'))->put($fullPath, (string) $image, 'public')) {
                $status = __('voyager::media.success_uploading');
                $fullFilename = $fullPath;
            } else {
                $status = __('voyager::media.error_uploading');
            }
        } else {
            $status = __('voyager::media.uploading_wrong_type');
        }

        return ["shortUrlLink" =>$fullFilename,
                "fullLink" =>Voyager::image($fullFilename)
        ];
        // echo out script that TinyMCE can handle and update the image in the editor
        //return "<script> parent.helpers.setImageValue('".Voyager::image($fullFilename)."'); </script>";
    }



    public function uploadFile(Request $request)
    {

        // $fullFilename = null;
        // $resizeWidth = 1800;
        // $resizeHeight = null;
        // $slug = $request->input('type_slug');
        // $file = $request->file('image');

        // $path = $slug.'/'.date('F').date('Y').'/';

        // $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension());
        // $filename_counter = 1;

        // // Make sure the filename does not exist, if it does make sure to add a number to the end 1, 2, 3, etc...
        // while (Storage::disk(config('voyager.storage.disk'))->exists($path.$filename.'.'.$file->getClientOriginalExtension())) {
        //     $filename = basename($file->getClientOriginalName(), '.'.$file->getClientOriginalExtension()).(string) ($filename_counter++);
        // }

        // $fullPath = $path.$filename.'.'.$file->getClientOriginalExtension();

        // $ext = $file->guessClientExtension();

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $file->move( storage_path('app/public').'/video/', $name);
                $data = [
                    'download_link'=>'\\'.'video'.'\\'. $name,
                      'original_name'=>$name];

                $dataResponse= json_encode($data);
                $dataResponse = '['.$dataResponse.']';
                return [
                    "success" => "true",
                    "data" => $dataResponse
                ];

        }
        else
        return [
            "success" => "false",
            "data" => $data
        ];



    }


    /**
     * mobile Upload Image 
     * only 1 image file with file type in ['pdf','jpg','png']
     */
    public function mobileUploadImage(Request $request) {
        try {

            if(!$request->hasFile('image')) {
                return response()->json(["status" => 'ng', "message" => "Không tồn tại file upload"], 400);
            }

            $allowedfileExtension=['pdf','jpg','png'];
            $files = $request->file('image'); 
        
            $extension = $files->getClientOriginalExtension();
    
            $check = in_array($extension,$allowedfileExtension);
            
            $fileName = time() ."_". $files->getClientOriginalName();

            if($check) {
                $files->move(public_path('uploads'), $fileName);
            } else {
                return response()->json(["status" => 'ng', "message" => "Không đúng định dạng hình ảnh"], 422);
            }
    
            return response()->json(["status" => 'ok', "message" => "Upload thành công", "file_name" => env('APP_URL') ."uploads/".$fileName], 200);
        } catch (Exception $e) {
            return response()->json(["status" => 'ng', "message" => $e->getMessage()], 500);
        }
    }

}

