<?php

namespace App\Http\Controllers;

use App\GroupFood;
use Illuminate\Http\Request;

use App\User;
use App\Agency;
use Carbon\Carbon;

use App\LevelAgency;
use Jenssegers\Agent\Agent;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class GroupFoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        return view('vendor.voyager.groupfood.index');
    }
    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
        $dataEdit = GroupFood::where("id",$id)->first();
        if($dataEdit ==null)
        {
            $dataEdit = new GroupFood();
        }
        $allCategoryNews = GroupFood::orderby ("created_at", "desc")
        ->get();
       return view("vendor.voyager.groupfood.detail", compact("dataEdit","allCategoryNews"));
    }
    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("group_food")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"no data picked"];
        }
    }

    public function getAll(Request $request)
    {
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $data =  DB::table('group_food');
            $limit = $request->input('length');
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText'); 
            $data= $data->where(function ($query) use ($search) {
                      $query->where('code', 'LIKE', "%{$search}%")
                      ->orwhere('id', 'LIKE', "%{$search}%")
                      ->orWhere('name', 'LIKE', "%{$search}%");
                });
            }
            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            -> offset($start)
            ->limit($limit)
            ->get();
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            return ["data"=>$data];
    }
    private  function InputToAddressSender(Request $request,$newUpdate,$uppdate =false)
    {
        $newUpdate->code  =$request->input("editCodeGroup");
        $newUpdate->name  =$request->input("editName");
        return $newUpdate;
    }
    
    public  function CreateOrEdit(Request $request)
    {
        $codeRequest = $request->input("editcode");
        
        if($this->IsNullOrEmptyString($codeRequest) || $codeRequest =="-1")
        {
                $itemInsert = new GroupFood();
                $itemInsert = $this->InputToAddressSender($request, $itemInsert,$uppdate=false);
                $itemInsert->save();

        }
        else
        {
            $newUpdate = GroupFood::where("id",$codeRequest)->first();
           
            if( $newUpdate)
            {
                $itemInsert = $this->InputToAddressSender($request, $newUpdate);
                $newUpdate->save();
            }
        }
        
        return ["success"=>true];

    }
}
