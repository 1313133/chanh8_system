<?php

namespace App\Http\Controllers;

use App\User;
use App\Agency;
use App\OrderNew;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect('/admin');
    }

    public function createCode($maxId=57 ) 
    {
        $order = OrderNew::where("id",57)->first();

        // $maxId = DB::table('order_news')->max('id');
        // dd($maxId);
        $user=  User::where("id",$order->AgencyId )->first();
        $agency = Agency::where("email", $user->email)->first();
    

        $customerCode = str_pad($maxId, 4, '0', STR_PAD_LEFT);
        $giaonhanh = "0";
        if($order->MethodId =="C")
        {
            $giaonhanh = "1";

        }
        $code = $order->provinces.$agency->id.$customerCode.$giaonhanh."1".Carbon::now()->format('d').Carbon::now()->format('m'.Carbon::now()->format('y'));


        return $code;
        

    }
}
