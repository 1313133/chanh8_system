<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use App;
use Lang;
use Session;
use App\News;
use stdClass;
use App\Group;
use App\Dstrict;
use App\Ordered;
use App\Partner;
use App\Product;
use App\Rewards;
use App\Rewarđs;
use App\SeoPage;
use App\CartItem;
use App\Province;
use App\StepItem;
use App\CartOrder;
use App\BannerItem;
use App\BannerPage;
use App\CarrerItem;
use App\FooterInfo;
use App\Infomation;

use App\PageObject;
use App\AboutusItem;
use App\ColorSelect;
use App\ContactInfo;
use App\ContactItem;
use App\ProjectItem;
use App\SlideBanner;
use App\VoucherItem;
use App\ContactItemm;
use App\DeleveryPost;
use App\HomePageIchi;
use App\HowtobuyPost;
use App\Introduction;
use App\MaterialType;
use App\ReturnObject;
use App\DeveloperItem;
use App\DeveloperPage;
use App\GroupCategory;
use App\GuaranteePost;
use App\NumberImpress;
use App\OrderedDetail;
use App\PrivacyPolicy;
use App\TechologyItem;
use App\ApplyJobCarrer;
use App\BannerHomePage;
use App\ContactRequest;
use App\HomeManagement;
use App\NewsDirectItem;
use App\TermConditions;
use App\HomePageKidIchi;
use App\LanguageDisplay;
use App\ProductCategory;
use App\QualityLicenece;
use App\StoreAtLocation;
use App\CarrerItemObject;
use App\CarrerObjectItem;
use App\CarrerObjectPage;
use App\ProductCategory3;
use App\SocialFooterLink;
use App\AboutusObjectPage;
use App\HomePageSnackIchi;
use App\InfomationCompany;
use App\PaymentMethodPost;
use App\HomepageObjectPage;
use App\CollectionImageItem;
use App\ContactFooterLink;
use App\EcatalogueConfigUrl;
use Illuminate\Http\Request;
use App\ProductCategoryImage;
use App\StoreAtLocationHaNoi;
use Laravel\Ui\Presets\React;
use App\HomePageKidIchiCranker;
use App\IntroductionObjectPage;
use Illuminate\Support\Facades\DB;
use App\HomepageIntroductAboutItem;
use Illuminate\Support\Facades\Cache;
use Composer\DependencyResolver\Problem;
use Larapack\Hooks\Commands\InfoCommand;
use App\Jobs\SendEmail;

class HomePageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     public function swichRouter($slug, Request $request)
     {


        if($slug =="" || $slug =="/")
        {
            return  $this->index();
        }

        else if( $slug =="lien-he" )
        {
            return $this->contact2();


        }

        else if( $slug =="tuyen-dung" )
        {

        }



        else if( $slug =="doi-tac" )

        {


            return $this->partner($request);

        }
        else

        {
            $data = CarrerItemObject::where("slug",$slug)->first();

            if($data !=null)
            {
                return  view("carrer", compact("data"));
            }
            $data = PageObject::where("slug",$slug)->first();



            if($data !=null)
            {
                return  view("pageObject", compact("data"));

            }

        }

     }



     public function contact2()
     {

             $hompageObjectIntroductionPage = ContactItem::orderby("priorites")->get();

             return view('contact',compact("hompageObjectIntroductionPage"));

     }


     public function carrer()
     {


        $allDoiTac = CarrerItemObject::orderby("priorities","asc")

        ->orderby("created_at","asc")->get();

        return view("tuyendung", compact("allDoiTac"));

     }



     public function voucher2()
     {


             $hompageObjectIntroductionPage = VoucherItem::orderby("priorites")->get();

             return view('welcome2',compact("hompageObjectIntroductionPage"));

     }

    public function index()
    {

            return  redirect("/admin");
            $this->createCache();
            $hompageObjectIntroductionPage = HomepageObjectPage::first();
            $hompageIntroductionItem = HomepageIntroductAboutItem::orderby("priorites")->get();
            $allProduct = Product::where("isActive",1)->where("isShownHomePage",1)->orderby("priority","asc")->get();
            return view('welcome',compact("hompageObjectIntroductionPage","hompageIntroductionItem","allProduct"));

    }

    public function vechungtoi (Request $request)

    {

        $data = AboutusObjectPage::first();
        return view("vechungtoi",compact("data"));
    }
    public function partnerDetail (Request $request,$slug)

    {

        $data = Partner::where("url", $slug)->first();
        if($data)
        {
            return view("partnerDetail",compact("data"));
        }
        else
        {
            return redirect("/doi-tac");
        }

    }




    public function resetCache()
    {
        Cache::flush();
        $this->createCache();

    }

    public function comingsoon()
    {

        return view("comingsoon");
    }








    public function productCategory(Request $request,$slug)
    {
        $data = ProductCategory::whereTranslation('slug', $slug)->first();
        $allProduct = $data->ListProduct;

        $count = count($allProduct);
        $allProductCategoryImage = ProductCategoryImage::where("categoryId",$data->id)->get();

        $allProduct = Product::where("categoryId",$data->id)
        ->orderby("priority","asc")->orderby("updated_at","asc")
        ->get();

        if($count>0)
        {
            return view("chitetdanhmuc",compact("data","allProductCategoryImage","allProduct"));
        }
        else
        {

            return view("chitetdanhmuc2",compact("data","allProductCategoryImage","allProduct"));
        }



    }

    public function SearchProduct (Request $request)
    {

        $colorCode  =  $request->input("colorCode");
        $matarialType  =  $request->input("matarialType");
        $orderType  =  $request->input("orderType");
        $slug = $request->input("slug");
        $productCategory= ProductCategory::where("slug",$slug)->first();
        $dataAll = DB::table('products')->where("isActive",1);

        if( $colorCode !="" )
        {
        $dataAll= $dataAll->where('colorSelect', $colorCode);
        }
        if( $matarialType !="" )
        {
        $dataAll= $dataAll->where('materialSelect', $matarialType);
        }


        if($productCategory)
        {
            $dataAll = $dataAll->where("refCategory",$productCategory->id);
        }

        else

        {
            $dataGroup= Group::where("slug",$slug)->first();
            if($dataGroup==null)
            {
                $dataAll =[];
                $view = view("dataproductList",compact("dataAll"))->render();
                return response()->json(['html'=>$view]);
            }
            $dataAll =$dataAll->join('product_categories', function ($join) {
                $join->on('products.refCategory', '=', 'product_categories.id');
            })
            ->join('groups', function ($join) {
                $join->on('product_categories.groupCode', '=', 'groups.id');
            })
            ->where('groups.id', $dataGroup->id);

        }


        if( $orderType =="" )
        {
           $dataAll= $dataAll->orderby('priority',"asc")->orderby("created_at","desc");
        }
        else
        {

            if($orderType == "0")
            {
                $dataAll= $dataAll->orderby('pricePresent',"asc")->orderby("created_at","desc");
            }
            else
            {
                $dataAll= $dataAll->orderby('pricePresent',"desc")->orderby("created_at","desc");
            }
        }
        if($productCategory)
        {

        }
        else
        {
            $dataAll = $dataAll ->select("products.*");
        }


        $dataAll = $dataAll->paginate(100);

        $view = view("dataproductList",compact("dataAll"))->render();
        return response()->json(['html'=>$view]);
    }



    public function SearchAllProduct (Request $request)
    {

        $colorCode  =  $request->input("colorCode");
        $matarialType  =  $request->input("matarialType");
        $orderType  =  $request->input("orderType");


        $dataAll = DB::table('products')->where("isActive",1);

        if( $colorCode !="" )
        {
           $dataAll= $dataAll->where('colorSelect', $colorCode);
        }
        if( $matarialType !="" )
        {
        $dataAll= $dataAll->where('materialSelect', $matarialType);
        }

        if( $orderType =="" || $orderType ==null )
        {
           $dataAll= $dataAll->orderby('priority',"asc")->orderby("created_at","desc");
        }
        else
        {

                if($orderType == "0")
                {
                    $dataAll= $dataAll->orderby('pricePresent',"asc")->orderby("created_at","desc");
                }
                else
                {
                    $dataAll= $dataAll->orderby('pricePresent',"desc")->orderby("created_at","desc");
                }
        }


        $dataAll = $dataAll->paginate(100);


        $view = view("dataproductList",compact("dataAll"))->render();
        return response()->json(['html'=>$view]);
    }


    public function productList(Request $request, $slug =null)
    {
        $dataAll =Product::where("isActive",1);
        $productCategory= ProductCategory::where("slug",$slug)->first();
        if($productCategory)
        {

        $allProdcutLevel3 =  ProductCategory3::where("parrentCode", $productCategory->id)->get()->pluck('id');
        $dataSameProductCategory =[];



        if(count($allProdcutLevel3) <1)
        {
            $allProdcutLevel3 = ProductCategory::where("groupCode", $productCategory->groupCode)->get()->pluck('id');
            $dataSameProductCategory =  ProductCategory::where("groupCode", $productCategory->groupCode)
            ->orderby("priority")->get();
            $dataAll=  $dataAll->where("refCategory",$productCategory->id)
            ->orderby("priority","asc")->orderby("created_at","desc")
            ->paginate(9);



            return view("productList2New",compact("dataAll","dataSameProductCategory","productCategory"));
        }
        else
        {
            $allProdcutLevel3 = ProductCategory3::where("parrentCode", $productCategory->id)->get()->pluck('id');

            $dataSameProductCategory =  ProductCategory3::where("parrentCode", $productCategory->id)->orderby("priorites")->get();


            $dataAll=  $dataAll->wherein("productCategory3code",$allProdcutLevel3)

            ->orderby("priority","asc")->orderby("created_at","desc")

           ->paginate(9);




            return view("productList2New",compact("dataAll","dataSameProductCategory","productCategory"));

        }

        }
        else
        {


            $productCategory = ProductCategory3::where("slug",$slug)->first();
            $allProdcutLevel3 = ProductCategory3::where("parrentCode", $productCategory->parrentCode)
              ->get()->pluck('id');
            $dataSameProductCategory =  ProductCategory3::where("parrentCode", $productCategory->parrentCode)

            ->orderby("priorites")->get();
            $dataAll=  $dataAll->wherein("productCategory3code",[$productCategory->id])
            ->orderby("priority","asc")->orderby("created_at","desc")

           ->paginate(9);
            $productparrent = ProductCategory::where("id", $productCategory->parrentCode)->first();


            return view("productList2New",compact("dataAll", "productparrent" , "dataSameProductCategory","productCategory"));
        }

        // get all


        //level 2

    }

    public function   getallProduct(Request $request)
    {

            $dataAll =Product::where("isActive",1) ->orderby("priority","asc")
            ->orderby("priority","asc")->orderby("created_at","desc")
            ->paginate(200);
            $dataColorArray = ColorSelect::all();

            $dataMaterialArray = MaterialType::all();



            return view("productListAll",compact("dataAll","dataMaterialArray","dataColorArray"));

    }


    public function   partner(Request $request)
    {


        $allDoiTac = Partner::orderby("priorites")->get();

        return view("doitac", compact("allDoiTac"));

    }
    public function   aboutus(Request $request)
    {

            // $dataAll =Product::where("isActive",1)->orderby("updated_at","desc")
            // ->orderby("priority")
            // ->paginate(200);
            // $dataColorArray = ColorSelect::all();

            // $dataMaterialArray = MaterialType::all();

        $allDoiTac = Partner::orderby("priorites")->get();

        return view("doitac", compact("allDoiTac"));

    }


    public function product (Request $request,$slug)
    {

        $data = Product::whereTranslation("slug",$slug)->first();

        if($data==null)
        {
            return redirect("/");
        }



        $datacolor = $data->colorSelect;
        $dataColorArray = explode(";",$datacolor);



        $arraySelect = ColorSelect::whereIn("colorCode",$dataColorArray)->get();


        $dataOther = Product::where("refCategory",$data->refCategory)
                    ->wherenotin("id",[$data->id])
                    ->orderby("priority","asc")
                    ->orderby("created_at","desc")
                    ->take(4)
                    ->get();




        $dataProductCategory = $data->produtCategory;
        $dataGroup = null;

        if($dataProductCategory ==null)
        {

        }

        else

        {

            $dataGroup =  $dataProductCategory->group;

        }
        if($data)
        {



            return view("productDetail",compact("data","dataOther","dataGroup","dataProductCategory","arraySelect"));


        }
        return redirect("/");

    }



    public function sendOrder(Request $request)
    {
        $itemInsert=  new CartOrder();
        $itemInsert->fullName =$request->input("fullName");
        $itemInsert->mobilePhone =$request->input("phoneNumber");
        $itemInsert->email =$request->input("email");
        $itemInsert->province =$request->input("province");
        $itemInsert->dist =$request->input("district");;
        $itemInsert->reward =$request->input("ward");;
        $itemInsert->street =$request->input("street");;
        $itemInsert->productid =$request->input("productid");
        $itemInsert->status ="Requested";

        $itemInsert->save();
        return ["success"=>true];
    }



    public function careerdetails ($slug)
   {

        $data = CarrerObjectItem::whereTranslation("slug",$slug)->first();
        if($data ==null)
        {
            $data = new CarrerObjectItem();
        }

       return view("chitiettuyendung",compact("data"));
   }


   public function phattrienbenvung()
   {

       return view("phattrienbenvung");
   }

    public function quytrinhsanxuat()
    {

        return view("quytrinhsanxuat");
    }
    public function createCache()
    {
        $footerInfo = ContactInfo::first();
        Cache::forever('footerInfo', $footerInfo);


        $allProductCategory =ProductCategory::orderby("groupCode")->orderby("priority")->get();

        Cache::forever('allGroup', $allProductCategory);

        $allGroup = Group::orderby("priorites")->get();


        $homeManagement = HomeManagement::orderby("priorities")->get();
        Cache::forever('homeManagement', $homeManagement);



        Cache::forever('allGroup', $allGroup);

        $allImageItem = CollectionImageItem::orderby("priorites")->get();

        Cache::forever('allImageItem', $allImageItem);

        $allNewsDirectItem= NewsDirectItem::orderby("priorites")->get();
        Cache::forever('allNewsDirectItem', $allNewsDirectItem);




        $bannerHomePage =BannerHomePage::first();
        Cache::forever('bannerHomePage', $bannerHomePage);

        $introductionPage =IntroductionObjectPage::first();
        Cache::forever('introductionPage', $introductionPage);


        $languageDisplay =LanguageDisplay::first();

        Cache::forever('languageDisplay', $languageDisplay);
        $contactInfo =ContactInfo::first();

        Cache::forever('contactInfo', $contactInfo);




        $allBanner = BannerItem::all();
        Cache::forever('allBanner', $allBanner);


        $allNew = News::orderby("updated_at","desc")->orderby("priorites","asc")->take(5)->get();
        Cache::forever('allNew', $allNew);
        $allProduct = Product::orderby("priority","asc")->orderby("created_at","desc")->get();
        Cache::forever('allProduct', $allProduct);

        $companyInfo =InfomationCompany::first();
        Cache::forever('companyInfo', $companyInfo);

        $allbannerItem = SlideBanner::orderby("priorites","asc")->get();

        Cache::forever('allbannerItem', $allbannerItem);



        $allContactItem = ContactItemm::all();
        Cache::forever('allContactItem', $allContactItem );

        $SeoPage = SeoPage::all();
        Cache::forever('SeoPage', $SeoPage);
        $Allsocial_footer_links = SocialFooterLink::orderby("priorities")->get();
        Cache::forever('allsocial_footer_links', $Allsocial_footer_links );
        $AllContact_footer_links = ContactFooterLink::orderby("priorities")->get();
        Cache::forever('allContact_footer_links', $AllContact_footer_links );


    }






    public function contact()
    {
            $hompageObjectIntroductionPage = ContactItem::orderby("priorites")->get();

            return view('welcome2',compact("hompageObjectIntroductionPage"));

    }


    public function career()
    {
        return view('career');
    }

    public function changLanuage($locale)
	{


		if($locale =="vi"  || $locale =="en")
		{
            \Session::put('website_language', $locale);
        }
        $this->createCache();

        return back();

    }



    public function createRequest(Request $request)
    {
        $itemInsert =new ContactRequest();
        $itemInsert->type =$request->input("type");
        $itemInsert->fullName =$request->input("fullName");
        $itemInsert->address = $request->input("address");
        $itemInsert->email =$request->input("email");
        $itemInsert->content =$request->input("message");
        $itemInsert->phone =$request->input("mobilePhone");
        $itemInsert->isRead ="0";
        $itemInsert->save();
        return ["success"=>true];
    }



//     public function new()
//     {
//         return view('news');
//     }



//     public function newDetail ($slug)
//    {



//        return view("chitiettintuc",compact("data"));
//    }


   public function new()
   {
        $dataAll =News::where("isActive",1)->paginate(6);

       return view("news",compact('dataAll'));
   }


   function fetch_dataNew(Request $request)
   {
        if($request->ajax())
        {
        $data = DB::table('posts')->paginate(6);
        return view('pagination_data', compact('data'))->render();
        }
   }

    public function catalogue()
    {

        $allData = EcatalogueConfigUrl::orderby("stt","asc")->get();

        return view("ecatalogue",compact("allData"));
    }

   public function newDetail(Request $request,$slug)
   {
       $data = News::whereTranslation("slug",$slug)->where("isActive",1)->First();
       if($data)
       {

       }
       else
       {
           return redirect('/tin-tuc');
       }

       $dataOther =News::where("isActive",1)
       ->whereNotIn('id', [$data->id])
       ->orderby("priorites","desc")->orderby("created_at","desc")->take(3)->get();
       return view("chitiettintuc",compact('data','dataOther'));
   }




public function checkAge (Request $request)
{
	return view("checkAge");

}

public function return (Request $request)
{

    $data = ReturnObject::first();

    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");
}


public function howtobuy (Request $request)
{

    $data = HowtobuyPost::first();

    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");
}





public function termCondition (Request $request)
{

    $data = TermConditions::first();

    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");
}

public function privacypolycy (Request $request)
{

    $data = PrivacyPolicy::first();

    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");

}


public function PaymentMethodPost (Request $request)
{

    $data = PaymentMethodPost::first();
    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");


}


public function DeleveryPost (Request $request)
{

    $data = DeleveryPost::first();

    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");
}



public function cartInfo(Request $request)

{

    $totalCard = 0;
    if (Session::has('cardlist')){
        $cardlist = $request->session()->get('cardlist');
        $totalCard = 0;
        foreach ($cardlist as $item) {

            $totalCard  += $item->number;

        }
    }

    if($totalCard ==  0)
    {
        $request->session()->forget('totalCard');
        return view ("cartEmpty");
    }
    else
    {
        return view ("cartInfo");
    }


}

public function baohanh (Request $request)
{

    $data = GuaranteePost::first();

    if($data)
    {

        return view("post", compact("data"));
    }

    return \redirect("/");
}


public function GetAllProductByCategory(Request $request)
{

    $dataAll = GroupCategory::all();

    return view("productList",compact("dataAll"));



}
function filter($id)
{
    // returns whether the input integer is odd
    return $id & 1;
}

public function updateInfomationCart(Request $request)
{
    $cardInsert = new Infomation();
    $cardInsert->fullName = $request->input("fullName");
    $cardInsert->mobilePhone = $request->input("mobilePhone");
    $cardInsert->email = $request->input("email");
    $cardInsert->nameHoltel = "'";
    $cardInsert->addressHoltel = $request->input("addressHoltel");
    $cardInsert->quatinityHoltel = $request->input("");
    $cardInsert->noted = $request->input("noted");

    $hasError = false;
    if( $cardInsert->fullName =="")
    {

        $hasError = true;

        $request->session()->flash('fullName','Họ và tên bắt buộc nhập');
    }


    if(   $cardInsert->mobilePhone =="")
    {
        $hasError = true;

        $request->session()->flash('mobilePhone','Số điện thoại bắt buộc nhập');
    }



    // if(   $cardInsert->nameHoltel =="")
    // {
    //     $hasError = true;
    //     $request->session()->flash('nameHoltel','Tên khách sạn bắt buộc nhập');
    // }


    $request->session()->forget('infomation');

    Session::put('infomation', $cardInsert);

    if($hasError)
    {
        return redirect('/thong-tin-thanh-toan');
    }

    else {

     return   $this->checkout($request);

    }



}




public function updateCarOrder (Request $request)
{



    $totalCard = 0;
     if (Session::has('cardlist')){

        $cardlist = $request->session()->get('cardlist');
        $cardInsert = new CartItem();
        $cardInsert->productCode = $request->input("productId");
        $cardInsert->number = $request->input("number");
        $cardInsert->productCodeText = $request->input("productCode");


        $arrayUpdate = [];
        $isDuplicate =false;
        foreach ($cardlist as $item) {
                if($item->productCode == $cardInsert->productCode)
                {

                $cardInsert->productCodeText   =  $item->productCodeText;
                array_push($arrayUpdate,$cardInsert);
                $isDuplicate = true;
                }
                else
                {
                     array_push($arrayUpdate,$item);
                }
        }
        if($isDuplicate==false)
        {
            array_push($arrayUpdate,$cardInsert);
        }
        $request->session()->forget('cardlist');

        Session::put('cardlist', $arrayUpdate);

        $totalCard = 0;
        foreach ($arrayUpdate as $item) {

            $totalCard  += $item->number;

        }
        $request->session()->forget('totalCard');

        Session::put('totalCard', $totalCard);

    }
    $this->getCardOrder($request);
    return ["success"=>true, "totalCard"=>$totalCard];
}

public function cartOrderRequest (Request $request)
{

    $totalCard = 0;
     if (Session::has('cardlist')){

        $cardlist = $request->session()->get('cardlist');
        $cardInsert = new CartItem();
        $cardInsert->productCode = $request->input("productId");
        $cardInsert->number = $request->input("number");
        $cardInsert->productCodeText = $request->input("productCode");


        $arrayUpdate = [];
        $isDuplicate =false;
        foreach ($cardlist as $item) {
                if($item->productCode == $cardInsert->productCode)
                {

                $cardInsert->number   =  $cardInsert->number *1 +  $item->number *1;
                array_push($arrayUpdate,$cardInsert);
                $isDuplicate = true;
                }
                else
                {
             array_push($arrayUpdate,$item);
                }
        }
        if($isDuplicate==false)
        {
            array_push($arrayUpdate,$cardInsert);
        }
        $request->session()->forget('cardlist');

        Session::put('cardlist', $arrayUpdate);

        $totalCard = 0;
        foreach ($arrayUpdate as $item) {

            $totalCard  += $item->number;

        }
        $request->session()->forget('totalCard');

        Session::put('totalCard', $totalCard);

    }
    else{

    $arrayList = [];
    $cardInsert = new CartItem();
    $cardInsert->productCode = $request->input("productId");
    $cardInsert->number = $request->input("number");
    array_push($arrayList,$cardInsert);


    Session::put('cardlist', $arrayList);


    foreach ($arrayList as $item) {

        $totalCard  += $item->number;

    }
    $request->session()->forget('totalCard');

    Session::put('totalCard', $totalCard);

    }


    $this->getCardOrder($request);
    return ["success"=>true, "totalCard"=>$totalCard];
}


public function updateCart (Request $request)
{


         $arrayUpdate = $request->input("array");



     if (Session::has('cardlist')){

        $cardlist = $request->session()->get('cardlist');

        foreach ($cardlist as $item) {



                $itemUpdate = null;

                foreach( $arrayUpdate  as  $itemUpdate1)
                {


                    if($itemUpdate1["productId"] == $item->productCode)
                    {
                        $itemUpdate  = $itemUpdate1;
                    }
                }
                if($itemUpdate)

                {
                    $item->number =$itemUpdate["number"];
                }

        }

        $request->session()->forget('cardlist');
        Session::put('cardlist', $cardlist);
    }
    else{

    }
    $this->getCardOrder($request);
    return ["success"=>true];
}

public function removeCard( Request $request)

{
    $productId = $request->input("productId");
    if (Session::has('cardlist')){
        $cardlist = $request->session()->get('cardlist');
        $arrayUpdate = [];
        foreach ($cardlist as $item) {


                if($item->productCode ==$productId)
                {

                }
                else
                {
                 array_push($arrayUpdate,$item);
                }
        }

        $request->session()->forget('cardlist');
        Session::put('cardlist', $arrayUpdate);

         $this->getCardOrder($request);

         $totalCard = 0;
         foreach ($arrayUpdate as $item) {

             $totalCard  += $item->number;

         }
         $request->session()->forget('totalCard');

         Session::put('totalCard', $totalCard);

         return ["success"=>true ,"totalCard"=>$totalCard ];
    }

    return ["success"=>true];

}


public function checkout( Request $request)
{


    $cardOrded = new Ordered();
    $infomation = $request->session()->get('infomation');
    $cardOrded->fullName = $infomation->fullName;
    $cardOrded->mobilePhone = $infomation->mobilePhone;
    $cardOrded->email =  $infomation->email;
    $cardOrded->nameHoltel =  $infomation->nameHoltel;
    $cardOrded->addressHoltel = $infomation->addressHoltel;
    $cardOrded->quatinityHoltel =  $infomation->quatinityHoltel;
    $cardOrded->noted = $infomation->noted;
    $cardOrded->status = "0";




    if (Session::has('cardlist')){
        $cardlist = $request->session()->get('cardlist');
        if(count($cardlist)>0)
        {
            $cardOrded->save();
        }
        $total =0;

        $cardOrded->orderCode = "OrderVIF_".$cardOrded->id;
        foreach ($cardlist as $item) {
            $product = Product::where("id",$item->productCode )->first();
            if($product==null)
            {
                continue;
            }
            $dataProductCategory = $product->produtCategory;
            $dataGroup = null;

            if($dataProductCategory ==null)
            {

            }

            else

            {

                $dataGroup =  $dataProductCategory->group;

            }

            $cardOrderDetail = new OrderedDetail();

            $cardOrderDetail->orderId =$cardOrded->id;
            $cardOrderDetail->productId = $item->productCode;
            $cardOrderDetail->number = $item->number;
            $cardOrderDetail->price = $item->pricePresent;

            $total += $item->number*1 *$item->pricePresent;
            $cardOrderDetail->save();
        }
        $cardOrded->totalAmount = $total;

        $cardOrded->save();
        $message = [
            "createDate"=>Carbon::now()->format('d/m/Y'),
            'sumAmount'=>$cardOrded->totalAmount,
            "orderId"=>$cardOrded->id,
            "orderCode"=>$cardOrded->orderCode

        ];
        SendEmail::dispatch($message, null)->delay(now()->addMinute(1));


         $request->session()->forget('cardlist');
         $request->session()->forget('cardOrder');
         $request->session()->forget('ininfomationfomation');
         $request->session()->flash('orderStatus', true);
        return redirect("/dat-hang-thanh-cong");
    }
    else{

        return ["success"=>false,"data"=>"Không có thông tin đặt hàng"];
    }

}


public function getCardOrder (Request $request)
{


     if (Session::has('cardlist')){
        $cardlist = $request->session()->get('cardlist');
        foreach ($cardlist as $item) {
            $product = Product::where("id",$item->productCode )->first();
            if($product==null)
            {
                continue;
            }
            $dataProductCategory = $product->produtCategory;
            $dataGroup = null;

            if($dataProductCategory ==null)
            {

            }

            else

            {

                $dataGroup =  $dataProductCategory->group;

            }

            $item->imageProduct = $product->FeauterImage;
            $item->nameProduct = $product->title;
            $item->slug = $product->slug;

            $item->priceOrginal = $product->priceOrginal;
            $item->pricePresent = $product->pricePresent;

            $item->productCategoryName = $dataProductCategory->title;
            $item->productCategorySlug = $dataProductCategory->slug;
            $item->productCodeText = $product->ProductCode;
            // $item->groupName = $dataGroup->text;

        }
        $request->session()->forget('cardOrder');
        Session::put('cardOrder', $cardlist);



        $totalCard = 0;
        foreach ($cardlist as $item) {

            $totalCard  += $item->number;

        }
        $request->session()->forget('totalCard');

        Session::put('totalCard', $totalCard);

        return ["success"=>true,"caroder"=>$cardlist];


    }
    else{

        $request->session()->forget('cardOrder');
        Session::put('cardOrder', []);
        return ["success"=>true,"caroder"=>[]];

    }

}



public function cart (Request $request)
{

    if (Session::has('cardOrder')){


        return view("cart");


    }
    else

    {
        $request->session()->forget('totalCard');
        return view("cartEmpty");
    }
}

public function cartsuccess (Request $request)
{



    $totalCard = 0;
    if (Session::has('cardlist')){
        $cardlist = $request->session()->get('cardlist');
        $totalCard = 0;
        foreach ($cardlist as $item) {

            $totalCard  += $item->number;

        }
    }

    if($totalCard ==  0)
    {
        $request->session()->forget('totalCard');
        return view("cartSucess");
    }
    return redirect("/gio-hang");
}





public function timkiem (Request $request)
{

    $keysearch = $request->input("keysearch");
    $request->session()->flash('keysearch',$keysearch);
    $dataAll = null;
    $request->session()->flash('keysearch', $keysearch);

    if($keysearch!="")
    {
        $dataAll =Product::where('title', 'like', '%'.$keysearch.'%')
        ->orwhere('ProductCode','%'.$keysearch.'%')
        ->orderby("priority","asc")->orderby("created_at","desc")
        ->get();

    }
    else
    {
        $dataAll = [];
    }
    return view("search",compact('dataAll','keysearch'));

}



public function search (Request $request)
{


$keysearch = $request->input("keysearch");
$request->session()->flash('keysearch',$keysearch);
$dataAll = null;

if($keysearch!="")
{
    $dataAll =Product::where('title', 'like', '%'.$keysearch.'%')
    ->orderby("priority","desc")->orderby("created_at","desc")
    ->get();

}
else
{
    $dataAll = [];



}
return view("search",compact('dataAll'));

}


}
