<?php

namespace App\Http\Controllers;

use App\Rewards;
use App\District;
use App\Location;
use App\Province;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getAllTinh(Request $request)
    {

        $allData = Location::where("Loai",1)->get();

        return ["data"=>$allData];
    }

    public function getallHuyen(Request $request)
    {
        $allData = Location::where("Loai",2)
                        ->where("Ma_Cha",$request->input("Macha"))
                        ->get();

        return ["data"=>$allData];

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\location  $location
     * @return \Illuminate\Http\Response
     */
    public function show(location $location)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\location  $location
     * @return \Illuminate\Http\Response
     */
    public function edit(location $location)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\location  $location
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, location $location)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\location  $location
     * @return \Illuminate\Http\Response
     */
    public function destroy(location $location)
    {
        //
    }

    public function getFullAdress (Request $request)
    {
        
        $idTinh = $request->input("province");
        $idHuyen = $request->input("district");
        $idReward = $request->input("ward");
        if($idTinh == null ||  $idTinh  == '')
        {
            $idTinh  =  32;
            $idHuyen  =  358;
            $idReward  =  6393;
        }
        $provice = DB::table('province')->where('id', $idTinh)->first();
        $huyen = DB::table('district')->where('id', $idHuyen)->first();
        $phuong = DB::table('ward')->where('id', $idReward)->first();
        $fullAdressText = '';
        if($phuong)
        {
            $fullAdressText = $phuong->name;
        }
        if($huyen)
        {
            $fullAdressText = $fullAdressText.",".$huyen->name;
        }

        if($provice)
        {
            $fullAdressText = $fullAdressText.",".$provice->name;
        }

        return response($fullAdressText, 200)
        ->header('Content-Type', 'text/plain');
      
    }
}
