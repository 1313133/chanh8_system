<?php

namespace App\Http\Controllers;

use App\MethodPackageDelivery;
use Illuminate\Http\Request;

class MethodPackageDeliveryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MethodPackageDelivery  $methodPackageDelivery
     * @return \Illuminate\Http\Response
     */
    public function show(MethodPackageDelivery $methodPackageDelivery)
    {
        

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MethodPackageDelivery  $methodPackageDelivery
     * @return \Illuminate\Http\Response
     */
    public function edit(MethodPackageDelivery $methodPackageDelivery)
    {
        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MethodPackageDelivery  $methodPackageDelivery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MethodPackageDelivery $methodPackageDelivery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MethodPackageDelivery  $methodPackageDelivery
     * @return \Illuminate\Http\Response
     */
    public function destroy(MethodPackageDelivery $methodPackageDelivery)
    {
        //
    }
}
