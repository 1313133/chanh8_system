<?php

namespace App\Http\Controllers;

use App\User;
use App\Agency;
use App\Shipper;
use App\Customer;
use App\OrderNew;
use App\GroupFood;
use App\TypeOrder;
use Carbon\Carbon;
use App\AddressSender;
use App\OrderNewDetail;
use App\HistoryStatusOrder;
use App\ProfileStatusOrder;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }
      

        $allShipper = Shipper::all();
        $roledAdmin = false;
        $allStock = DB::table('address_stocks');
        $allTypeGood = DB::table('group_food');
        $allTypeGood = $allTypeGood->orderby("created_at", "desc")
        ->get();


        if ($user->role_id == 2) {
            $allShipper = Shipper::where('agencyid', $user->id)->get();

            $allStock  = $allStock->where("userId",$user->id);
        }
        $allStock = $allStock->get(); 
    

        $allTypeOrder = DB::table('type_orders')->get();

        $allCustomer = Customer::all();

        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();

      


        $provinces = DB::table('province')->orderby('name')->get();;
        $districts = DB::table('district')->orderby('name')->get();;
        $wards = DB::table('ward')->orderby('name')->get();;


        return view('vendor.voyager.managementOrderNew.create', compact(
            'allStatus',
            'allAgency',
            'roledAdmin',
            'allCustomer',
            'allShipper',
            'allStock',
            'allTypeOrder',
            'provinces',
            'districts',
            'wards',
            'allTypeGood'

        ));
    }

    public function information(Request $request, $id)
    {

        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }


        $orderInformation =  OrderNew::where("id", $id)->first();
        // dd($orderInformation);

        if ($orderInformation) {
        } else {
            return;
        }

        $orderInformation->customer = Customer::where("id", $orderInformation->CustomerId)->first();

        if ($roledAdmin) {
            $allShipper = Shipper::all();
        } else {
            $allShipper = Shipper::where('agencyid', $user->id)
                ->where("isDelete",0)
                ->get();
        }


      

    

        $allStock = DB::table('address_stocks');
        if($user->role_id  ==2)
        {
           $allStock= $allStock->where("userId",$user->id);
        }
        $allStock = $allStock->get();

        $allTypeOrder = DB::table('type_orders')->get();

        $allCustomer = Customer::all();

        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();

        $provinces = DB::table('province')->get(['id', 'name']);
        $districts = DB::table('district')->get(['id', 'name']);
        $wards = DB::table('ward')->get(['id', 'name']);
        $allTypeGood = DB::table('group_food');
        $allTypeGood = $allTypeGood->orderby("created_at", "desc")
        ->get();

        return   view('vendor.voyager.managementOrderNew.edit', compact(
            'allStatus',
            'allAgency',
            'roledAdmin',
            'allCustomer',
            'allShipper',
            'allStock',
            'allTypeOrder',
            'provinces',
            'districts',
            'wards',
            'orderInformation',
            'allTypeGood'

        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }

        $isUpdated = true;
        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();
        $allAgency = Agency::orderby("created_at", "desc")
            ->get();
        $allCustomer = Customer::all();
        $request->request->add(['AgencyId' => $user->id]);
        // $request->request->add(['CustomerId' => $request->input("CustomerIdData")]);

        $request->request->add(['isThuHO' => '1']);

        if ($request->ChargeOrder === null) {
            $request->merge(['ChargeOrder' => '0']);
        }

        if ($request->IntercityCost === null) {
            $request->merge(['IntercityCost' => '0']);
        }

        $validator = Validator::make($request->all(), [
            //  'mobilePhoneSender' => 'required|regex:/(0)[0-9]{9}/',
            // 'AddressSenderId' => 'required|numeric',

            'AddressStockId' => 'numeric',
            'AgencyId' => 'required|numeric',
            'ShipperId' => 'required|numeric',
            'MethodId' => 'regex:/[S,C]{1}/|max:1',
            'TotalValue' => 'required',
            'isSenderPayment' => 'required|regex:/[0,1]{1}/|max:1',
            'isThuHO' => 'required|regex:/[0,1]{1}/|max:1',
            'ChargeOrder' => 'required',
            'COD' => 'required',
            'TypeOrderId' => 'required|numeric',
        ]);
        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        $sender = AddressSender::find(1);
        // $sender = AddressSender::find($request->AddressSenderId);
        $cust = Customer::find($request->CustomerId);
        if (is_null($sender) || is_null($cust)) {
            $response = ["message" => 'Sender or Customer not exists'];
            return response($response, 402);
        }
        $agen = User::where('id', $request->AgencyId)->first();
        $tmpshipper = Shipper::where('id',$request->ShipperId)->first();
        $user=  User::find($tmpshipper->userId);

        $tmpshipper ? $tmpshipper : 0;
        $ship = User::where('id',$tmpshipper->userId)->where('role_id',3)->first();

        if (!isset($agen) || !isset($ship)) {
            $response = ["message" => 'Agency or shipper not exists'];
            return response($response, 402);
        }
        $data = new OrderNew();


    
        $array = $request->only(['provinces','districts', 'wards','receiveaddress' , 'AgencyId','IntercityCost', 'CustomerId', 'AddressStockId', 'ShipperId', 'MethodId', 'TotalValue', 'COD', 'isSenderPayment', 'TypeOrderId', 'ChargeOrder','IntercityCost', 'orderSize','description', 'isPayment','typegoood']);
        foreach ($array as $key => $value) {
            $data->$key = $value;
        }
        $data->ShipperId  = $tmpshipper->id;
        //hard code
        $data->AddressSenderId = 1;
       
        $data->save();

        $orderDetail = new OrderNewDetail();
        $orderDetail->orderId  =  $data->id;
        $orderDetail->SizeOrder = "";
        $orderDetail->isDangerous = 0;
        $orderDetail->name = "";
        $orderDetail->shortdescription = "";
        $orderDetail->price = 0;
        $orderDetail->save();

        $code = $this->createCode($data->id);
        $data->Code = $code;
        $data->save();

        $response = ["message" => 'successful', "data" => $data];
        return view('vendor.voyager.managementOrderNew.createmsg', ['isUpdated']);
    }

    public function createCode($maxId=57 ) 
    {
        $order = OrderNew::where("id",57)->first();
        $user=  User::where("id",$order->AgencyId )->first();
        $agency = Agency::where("email", $user->email)->first();
    

        $customerCode = str_pad($maxId, 4, '0', STR_PAD_LEFT);
        $giaonhanh = "0";
        if($order->MethodId =="C")
        {
            $giaonhanh = "1";

        }
        $code = $order->provinces.$agency->id.$customerCode.$giaonhanh."1".Carbon::now()->format('d').Carbon::now()->format('m'.Carbon::now()->format('y'));


        return $code;
        

    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }

        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();

        $allCustomer = Customer::all();

        return view('vendor.voyager.managementOrderNew.index', compact('allStatus', 'allAgency', 'roledAdmin', 'allCustomer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $isUpdated = true;
        //
        $CustomerId = $request->input('CustomerId');
        $MethodId = $request->input('MethodId');
        $COD = $request->input('COD');
        // $indicate = $request->input('indicate');
        // $province = $request->input('province');
        // $district = $request->input('district');
        // $reward = $request->input('reward');
        $AddressStockId = $request->input('AddressStockId');
        $ShipperId = $request->input('ShipperId');
        $TypeOrderId = $request->input('TypeOrderId');
        $TotalValue = $request->input('TotalValue');
        $ChargeOrder = $request->input('ChargeOrder');
        $isSenderPayment = $request->input('isSenderPayment');
        $orderSize = $request->input('orderSize');
        $description = $request->input('description');
        $typegoood = $request->input('typegoood');
        $isPayment = $request->input('isPayment');
        $IntercityCost = $request->input('IntercityCost');


        $provinces = $request->input('provinces');
        $districts = $request->input('districts');
        $wards = $request->input('wards');
        $receiveaddress = $request->input("receiveaddress");


        DB::table('order_news')->where('id', $id)->update([
            'provinces'=>$provinces,
            'districts'=>$districts,
            'wards'=>$wards,
            'receiveaddress'=>$receiveaddress,
             'CustomerId' => $CustomerId,
            'MethodId' => $MethodId,
            "typegoood"=>$typegoood,
            'COD' => $COD,
            // 'indicate'=>$indicate,
            // 'province'=>$province,
            // 'district'=>$district,
            // 'reward'=>$reward,
            'AddressStockId' => $AddressStockId,
            'ShipperId' => $ShipperId,
            'TypeOrderId' => $TypeOrderId,
            'TotalValue' => $TotalValue,
            'ChargeOrder' => $ChargeOrder,
            'isSenderPayment' => $isSenderPayment,
            'orderSize' => $orderSize,
            'description' => $description,
            'isPayment' => $isPayment,
            'intercityCost' => $IntercityCost,
        ]);
        return view('vendor.voyager.managementOrderNew.createmsg', ['isUpdated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

        /**
     * Tracking order status.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function track($id)
    {
        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
        ->get();

        $orderInfo =  DB::table('order_news')
        ->select('profile_status_orders.text as statusText','order_news.*')
        ->join('profile_status_orders','profile_status_orders.id','=','order_news.status')
        ->where('order_news.id',$id)
        ->first();
        if($orderInfo == null)
        {
            return;
        }

         $allLog =  DB::table('history_status_orders')
        ->select('profile_status_orders.text as statusText','history_status_orders.*')
        ->join('profile_status_orders','profile_status_orders.id','=','history_status_orders.status')
        ->where('history_status_orders.orderId',$id)
        ->orderby("created_at","desc")
        ->get();

        return view('vendor.voyager.managementOrderNew.trackingorder', ['allStatus'=>$allStatus, 'orderInfo'=>$orderInfo, 'allLog'=>$allLog]);
    }
}
