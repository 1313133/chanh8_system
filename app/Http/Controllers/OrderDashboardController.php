<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\OrderNew;
use App\Agency;
use App\Orders;
use App\Shipper;
use Carbon\Carbon;
use App\AddressSender;
use App\AddressStock;
use App\Customer;
use App\User;
use App\ProfileStatusOrder;
use App\TypeOrder;
use App\OrderNewDetail;
use App\TransitCar;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExportFile;
use Excel;

class OrderDashboardController extends Controller
{
    const S_NEW = "1";
    const S_TRANSFERRING = "2";
    const S_RECEIVED = "3";
    const S_COMPLETED = "4";
    const S_WAITING = "6";
    const S_CANCELLED = "7";
    const S_FAILED_DELIVERED = "8";

    // convert money to number
    private function convertMoney($str)
    {
        return str_replace(".", "", $str) + 0;
    }

    // calculate sum of COD
    private function sumOfCOD($list)
    {
        $sum = 0;
        foreach ($list as $item) {
            if (isset($item->COD)) {
                $sum += $this->convertMoney($item->COD);
            }
        }

        return $sum;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->role_id !== 1) {
            $agencies = Agency::all()->where('email', $user->email);
            $allOrders = OrderNew::all()->where('AgencyId', $user->id);
        } else {
            $agencies = Agency::all();
            $allOrders = OrderNew::all();
        }
        if ($user->role_id == 3) {
            $agencies = Shipper::all()->where('userId', $user->id);
            $allOrders = OrderNew::all()->where('ShipperId', $user->id);
        }
        $shippers = DB::table('shippers')->get();
        $filter_date = false;
        $filter_shipper = false;
        $filter_agency = false;
        $start = "";
        $end = "";
        $shipperName = "";
        $agencyName = "";

        // get list of orders
        $newOrders = $this->addaddr( $allOrders->where("Status", self::S_NEW));
        $tranferringOrders = $this->addaddr($allOrders->where("Status", self::S_TRANSFERRING));
        $receivedOrders = $this->addaddr($allOrders->where("Status", self::S_RECEIVED));
        $waitingOrders = $this->addaddr($allOrders->where("Status", self::S_WAITING));
        $completedOrders = $this->addaddr($allOrders->where("Status",  self::S_COMPLETED));
        $OrdersCompleted= $this->addaddr($allOrders->where("Status",  "14"));
        $cancelledOrders = $this->addaddr($allOrders->where("Status", self::S_CANCELLED));
        $failedDeliveredOrders = $this->addaddr($allOrders->where("Status", self::S_FAILED_DELIVERED));

        // COD of orders
        $completedCOD = number_format($this->sumOfCOD($completedOrders));
        $processingCOD = number_format(( $this->sumOfCOD($tranferringOrders)
                             + $this->sumOfCOD($receivedOrders))
                            + $this->sumOfCOD($waitingOrders));
        $cancelledCOD = number_format($this->sumOfCOD($cancelledOrders) + $this->sumOfCOD($failedDeliveredOrders));

        $totalOfOrders = $allOrders->count();
        $newCount = $newOrders->count();
        $tranferringCount = $tranferringOrders->count();
        $receivedCount = $receivedOrders->count();
        $completedCount = $completedOrders->count();
        $waitingCount = $waitingOrders->count();
        $cancelledCount = $cancelledOrders->count();
        $OrdersCompletedCount = $OrdersCompleted->count();

        $sumCompletedCount = $OrdersCompletedCount+ $completedCount;
        $failedDeliveredCount = $failedDeliveredOrders->count();

        $allStore = Agency::orderby("created_at","desc")->get();
        return view('vendor.voyager.analytics.index',compact(
            'totalOfOrders',
            'completedCOD',
            'processingCOD',
            'cancelledCOD',
            'newCount',
            'tranferringCount',
            'receivedCount',
            'completedCount',
            'waitingCount',
            'cancelledCount',
            'failedDeliveredCount',
            'filter_date',
            'filter_shipper',
            'filter_agency',
            'start',
            'end',
            'shippers',
            'shipperName',
            'agencies',
            'agencyName',
            'newOrders',
            'tranferringOrders',
            'waitingOrders',
            'receivedOrders',
            'completedOrders',
            'failedDeliveredOrders',
            'OrdersCompletedCount',
            'sumCompletedCount',
            'allStore'
        ));
    }

    public function search(Request $request)
    {
        $user = Auth::user();

        if ($user->role_id !== 1) {
            $agencies = Agency::all()->where('email', $user->email);
            $filtered_orders = OrderNew::all()->where('AgencyId', $user->id);
        } else {
            $agencies = Agency::all();
            $filtered_orders = OrderNew::all();
        }
        if ($user->role_id == 3) {
            $agencies = Shipper::all()->where('userId', $user->id);
            $filtered_orders = OrderNew::where('ShipperId', $user->id);
        }
        
        $shippers = DB::table('shippers')->get();
        // $agencies = DB::table('agencies')->get();
        $selectedShipper = $shippers->where('id', $request->shipper)->first();
        $selectedAgency = $agencies->where('id', $request->agency)->first();
        $shipperName = $selectedShipper !== null ? $selectedShipper->fullName : "";
        $agencyName = $selectedAgency !== null ? $selectedAgency->fullName : "";
        $filtered_orders = $request->shipper === 'all' ? $filtered_orders : $filtered_orders->where('ShipperId',  $this->getid($request->shipper,3));
        $filtered_orders = $request->agency === 'all' ? $filtered_orders : $filtered_orders->where('AgencyId', $this->getid( $request->agency,2));
        $start = Carbon::parse($request->get('from-date'))->startOfDay();
        $end = Carbon::parse($request->get('to-date'))->endOfDay();
        // filter state
    
        $filter_date = true;
        $filter_shipper = true;
        $filter_agency = true;

        if (
            $request->get('from-date') !== null &&
            $request->get('to-date') !== null
        ) {
            $filtered_orders = $filtered_orders->where('created_at', '>=', $start)
                ->where('created_at', '<=', $end);
        } else if (
            $request->get('from-date') !== null &&
            $request->get('to-date') === null
        ) {
            $end = "hiện tại";
            $filtered_orders = $filtered_orders->where('created_at', '>=', $start);
        } else if (
            $request->get('from-date') === null &&
            $request->get('to-date') !== null
        ) {
            $start = "đầu";
            $filtered_orders = $filtered_orders->where('created_at', '<=', $end);
        } else {
            $filter_date = false;
        }

        if ($selectedShipper === null) {
            $filter_shipper = false;
        }

        if ($selectedAgency === null) {
            $filter_agency = false;
        }

        // get list of orders
        $allOrders = $filtered_orders;
        $newOrders = $this->addaddr( $filtered_orders->where("Status", self::S_NEW));
        $tranferringOrders = $this->addaddr($filtered_orders->where("Status", self::S_TRANSFERRING));
        $receivedOrders = $this->addaddr($filtered_orders->where("Status", self::S_RECEIVED));
        $completedOrders = $this->addaddr($filtered_orders->where("Status", self::S_COMPLETED));
        $waitingOrders = $this->addaddr($filtered_orders->where("Status", self::S_WAITING));
        $cancelledOrders = $this->addaddr($filtered_orders->where("Status", self::S_CANCELLED));
        $failedDeliveredOrders = $this->addaddr($filtered_orders->where("Status", self::S_FAILED_DELIVERED));

        // COD of orders
        $completedCOD = number_format($this->sumOfCOD($completedOrders));
        $processingCOD = number_format(( $this->sumOfCOD($tranferringOrders)
                        + $this->sumOfCOD($failedDeliveredOrders) + $this->sumOfCOD($receivedOrders)
                        + $this->sumOfCOD($waitingOrders)));
        $cancelledCOD = number_format($this->sumOfCOD($cancelledOrders));

        $totalOfOrders = $allOrders->count();
        $newCount = $newOrders->count();
        $tranferringCount = $tranferringOrders->count();
        $receivedCount = $receivedOrders->count();
        $completedCount = $completedOrders->count();
        $waitingCount = $waitingOrders->count();
        $cancelledCount = $cancelledOrders->count();
        $failedDeliveredCount = $failedDeliveredOrders->count();

        return view('vendor.voyager.analytics.index', compact(
            'totalOfOrders',
            'completedCOD',
            'processingCOD',
            'cancelledCOD',
            'newCount',
            'tranferringCount',
            'receivedCount',
            'completedCount',
            'waitingCount',
            'cancelledCount',
            'failedDeliveredCount',
            'filter_date',
            'filter_shipper',
            'filter_agency',
            'start',
            'end',
            'shippers',
            'shipperName',
            'agencies',
            'agencyName',
            'newOrders',
            'tranferringOrders',
            'waitingOrders',
            'receivedOrders',
            'completedOrders',
            'failedDeliveredOrders'
        ));
    }

    public function getOrders(Request $request)
    {
        return $request->shipper;
    }
    public function getid($id ,$type)
    {
       if ($type === 2) {
            $agencies = Agency::findOrFail($id);
            return $agencies->userId;
       } else {
        $agencies = Shipper::findOrFail($id);
        return $agencies->userId;
       }
       
    }

    public function addaddr($arr)
    {
       
        foreach ($arr as  $data) {
            $sender = AddressSender::find($data->AddressSenderId);
            if (isset($sender)) {
                $data->sender = $sender->fullName;
                $data->sphone = $sender->mobilePhone;
                $data->senderaddress = $sender->addresssInfo .", ".$sender->ward." ".$sender->district." ".$sender->province.".";
            }   
            $cust = Customer::find($data->CustomerId);
            if (isset($cust)) {
                $data->customer = $cust->fullName;
                $data->cphone = $cust->mobilePhone;
                $data->customeraddress = $cust->addresssInfo .", ".$cust->ward." ".$cust->district." ".$cust->province.".";
            }   
        }
        return $arr;
    }

    public function export(Request $request)
    {
        $user = Auth::user();
        $request->q ? $q= $request->q : $q = 'all';
       
        if ($user->role_id !== 1) {
            $agencies = Agency::all()->where('email', $user->email);
            $allOrders = OrderNew::all()->where('AgencyId', $user->id);
        } else {
            $agencies = Agency::all();
            $allOrders = OrderNew::all();
        }
        if ($user->role_id == 3) {
            $agencies = Shipper::all()->where('userId', $user->id);
            $allOrders = OrderNew::all()->where('ShipperId', $user->id);
        }
        $shippers = DB::table('shippers')->get();
        $filter_date = false;
        $filter_shipper = false;
        $filter_agency = false;
        $start = "";
        $end = "";
        $shipperName = "";
        $agencyName = "";

        // get list of orders
        $allod =$this->addaddr( $allOrders);
        $newOrders = $this->addaddr( $allOrders->where("Status", self::S_NEW));
        $tranferringOrders = $this->addaddr($allOrders->where("Status", self::S_TRANSFERRING));
        $receivedOrders = $this->addaddr($allOrders->where("Status", self::S_RECEIVED));
        $waitingOrders = $this->addaddr($allOrders->where("Status", self::S_WAITING));
        $completedOrders = $this->addaddr($allOrders->where("Status", self::S_COMPLETED));
        $cancelledOrders = $this->addaddr($allOrders->where("Status", self::S_CANCELLED));
        $failedDeliveredOrders = $this->addaddr($allOrders->where("Status", self::S_FAILED_DELIVERED));
        
        
        $response = array();
        switch ($q) {
            case '1':
                foreach ($newOrders as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                break;
            case '2':
                foreach ($waitingOrders as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                break;
            case '3':
                foreach ($tranferringOrders as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                foreach ($receivedOrders as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                break;
            case '4':
                foreach ($completedOrders as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                break;
            case '5':
                foreach ($failedDeliveredOrders as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                break;
            default:
                foreach ($allod as $key => $value) {
                    $detail= $this->getdata($value);
                array_push($response,$detail);
                }
                break;
        }
        
        $export = new ExportFile($response);

        return Excel::download($export, 'file-thong-ke-chanh8.xlsx');
    }

    public function process($newOrders)
    {
       
       foreach ($newOrders as $value) {
           $value->acode = $value->Code;

       }


    }

    public function getdata($data)
    {
            $response = ["code"=>$data->Code];
            $sender = AddressSender::find($data->AddressSenderId);
            if (isset($sender)) {
                $pase2 =[
                    "sender" =>$sender->fullName,
                    "sendermobile"=>$sender->mobilePhone,
                    "senderaddress"=>$sender->addresssInfo .", ".$sender->ward." ".$sender->district." ".$sender->province.".",
                ];
                $response = array_merge($response,$pase2);
            }   
            $cust = Customer::find($data->CustomerId);
            if (isset($cust)) {
                $pase2 =[
                    "customer" =>$cust->fullName,
                    "customermobile"=>$cust->mobilePhone,
                    "customeraddress"=>$cust->addresssInfo .", ".$cust->ward." ".$cust->district." ".$cust->province.".",
                ];
                $response = array_merge($response,$pase2);
            }   
            $agen = User::where('id',$data->AgencyId)->where('role_id',2)->first();
            if (isset($agen)) {
                $pase2 =[
                    "agency"=> $agen->name,
                ];
                $response = array_merge($response,$pase2);
            }   
            else {
                $pase2 =[
                    "agency"=> 'ADMIN',
                ];
                $response = array_merge($response,$pase2);
            }
            $stock = AddressStock::find($data->AddressStockId);
            if (isset($stock)) {
                $pase2 =[
                    "addressstock"=>$stock->addresssInfo .", ".$stock->ward." ".$stock->district." ".$stock->province.".",
                ];
                $response = array_merge($response,$pase2);
            }   
            $typeoforder = TypeOrder::find($data->TypeOrderId);
            if (isset($typeoforder)) {
                $pase2 =[
                    "TypeOrderId"=>$typeoforder->text, 
                ];
                $response = array_merge($response,$pase2);
            }
            if ($data->MethodId ==='S') {
                $pase2 =[
                    "Methodtrans"=>'Giao hàng trong 6 tiếng', 
                ];
                $response = array_merge($response,$pase2);
            } 
            if ($data->MethodId === 'C') {
                $pase2 =[
                    "Methodtrans"=>'Giao hàng trong 12 tiếng', 
                ];
                $response = array_merge($response,$pase2);
            } 
            if ($data->MethodId != 'C' && $data->MethodId !='S') {
                $pase2 =[
                    "Methodtrans"=>'Khác', 
                ];
                $response = array_merge($response,$pase2);
            }
            $responsedata = [
            "price"=>$data->TotalValue,
            "transportfee"=>$data->ChargeOrder,
            "cod"=>$data->COD,
            ];
            $response = array_merge($response,$responsedata );

            if ($data->Status == 1 ) {
                $pase2 =[
                    "Status"=>'Tạo mới', 
                ];
                $response = array_merge($response,$pase2);
            }
            if ($data->Status > 1 && $data->Status < 7 && $data->Status != 4) {
                $pase2 =[
                    "Status"=>'Đang Tiến hành', 
                ];
                $response = array_merge($response,$pase2);
            }
            if ($data->Status == 4) {
                $pase2 =[
                    "Status"=>'Đã Giao Hàng', 
                ];
                $response = array_merge($response,$pase2);
            }
            if ($data->Status >= 7 ) {
                $pase2 =[
                    "Status"=>'Giao không thành công', 
                ];
                $response = array_merge($response,$pase2);
            }
            return $response;
    }
}
