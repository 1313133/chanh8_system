<?php

namespace App\Http\Controllers;

use App\OrderNoted;
use App\User;
use App\Agency;
use App\Shipper;
use App\Customer;
use App\OrderNew;
use App\GroupFood;
use App\TypeOrder;
use Carbon\Carbon;
use App\AddressStock;
use App\AddressSender;
use App\OrderNewDetail;
use App\HistoryStatusOrder;
use App\ProfileStatusOrder;

use Illuminate\Http\Request;

use App\MethodPackageDelivery;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class OrderNewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $allShipper = Shipper::all();
        $allCustomer = Customer::where('userId', $user->id)->groupBy('mobilePhone', 'fullName')->orderBy('mobilePhone')->get();
        $roledAdmin = false;

        $allTypeGood = DB::table('group_food');
        $allTypeGood = $allTypeGood->orderby("created_at", "desc")
            ->get();

        if ($user->role_id == 2) {
            $allShipper = Shipper::where('agencyid', $user->companyId)->get();
            //$allCustomer = Customer::where('companyId', $user->companyId)->get();
        } else if ($user->role_id == 6) {
            $allShipper = Shipper::where('agencyid', $user->companyId)
                ->where('shopId', $user->shopId)
                ->get();
            //$allCustomer = Customer::where('companyId', $user->companyId)->get();
        }

        $allTypeOrder = DB::table('type_orders')->get();

        $allStock = $this->getAllStoreAddress($user);
        $allStockReturn = $this->getAllStoreAddressReturn($user);


        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();

        $provinces = DB::table('province')->orderby('name')->get();;
        $districts = DB::table('district')->orderby('name')->get();;
        $wards = DB::table('ward')->orderby('name')->get();;

        $allChargeShipper = MethodPackageDelivery::orderBy('priorite')
            ->orderBy('created_at', 'desc')->get();

        return view('vendor.voyager.managementOrderNew.create', compact(
            'allStatus',
            'allAgency',
            'roledAdmin',
            'allCustomer',
            'allShipper',
            'allStock',
            'allStockReturn',
            'allTypeOrder',
            'provinces',
            'districts',
            'wards',
            'allTypeGood',
            'allChargeShipper'

        ));
    }

    public function information(Request $request, $id)
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }

        $orderInformation = OrderNew::where("id", $id)->first();

        if ($orderInformation == null) {
            return;
        }

        $allChargeShipper = MethodPackageDelivery::all();

        $orderInformation->customer = Customer::where("id", $orderInformation->CustomerId)->first();

        if ($roledAdmin) {
            $allShipper = Shipper::all();
        } else {
            $allShipper = Shipper::all()->where('agencyid', $user->companyId);
        }

        //todo hung
        $allStock = $this->getAllStoreAddress($user);
        $allStockReturn = $this->getAllStoreAddressReturn($user);

        $allTypeOrder = DB::table('type_orders')->get();

        $allCustomer = Customer::where("companyId", $user->companyId);


        if ($user->shopId != null || $user->shopId != '') {
            $allCustomer = $allCustomer->where("id", $user->shopId);
        }

        // dd($allCustomer);
        $allCustomer = $allCustomer->get();
        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();


        $provinces = DB::table('province')->get(['id', 'name']);
        $districts = DB::table('district')->get(['id', 'name']);
        $wards = DB::table('ward')->get(['id', 'name']);
        $allTypeGood = DB::table('group_food');
        $allTypeGood = $allTypeGood->orderby("created_at", "desc")
            ->get();


        return view('vendor.voyager.managementOrderNew.edit', compact(
            'allStatus',
            'allAgency',
            'roledAdmin',
            'allCustomer',
            'allShipper',
            'allStock',
            'allStockReturn',
            'allTypeOrder',
            'provinces',
            'districts',
            'allChargeShipper',
            'wards',
            'orderInformation',
            'allTypeGood'

        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }
        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }
        $isUpdated = true;
        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();
        $allAgency = Agency::orderby("created_at", "desc")
            ->get();
        $allCustomer = Customer::all();

        $request->request->add(['isThuHO' => '1']);

        if ($request->ChargeOrder === null) {
            $request->merge(['ChargeOrder' => '0']);
        }

        if ($request->IntercityCost === null) {
            $request->merge(['IntercityCost' => '0']);
        }

        if ($user->role_id == 6) {
            $request->request->add(['AgencyId' => $user->companyId]);
        }

        if ($user->role_id == 2) {
            $request->request->add(['AgencyId' => $user->companyId]);
        }

        $validator = Validator::make($request->all(), [
            'AddressStockId' => 'numeric',
            'AddressStockReturnId' => 'numeric',
            'AgencyId' => 'required|numeric',
            'ShipperId' => 'required|numeric',
            'TotalValue' => 'required',
            'isSenderPayment' => 'required|regex:/[0,1]{1}/|max:1',
            'isThuHO' => 'required|regex:/[0,1]{1}/|max:1',
            'ChargeOrder' => 'required',
            'COD' => 'required'
        ]);

        if ($validator->fails()) {
            return response(['errors' => $validator->errors()->all()], 422);
        }

        // $sender = AddressSender::find($request->AddressSenderId);
        $cust = Customer::find($request->CustomerId);
        if (is_null($cust)) {
            $response = ["message" => 'Sender or Customer not exists'];
            return response($response, 402);
        }
        $agen = User::where('id', $request->AgencyId)->first();
        $tmpshipper = Shipper::where('id', $request->ShipperId)->first();


        $user = User::find($tmpshipper->userId);

        $data = new OrderNew();

        $array = $request->only(['provinces', 'districts', 'wards', 'receiveaddress', 'AgencyId', 'IntercityCost', 'CustomerId', 'AddressStockId', 'AddressStockReturnId', 'ShipperId', 'MethodId', 'TotalValue', 'COD', 'isSenderPayment', 'TypeOrderId', 'ChargeOrder', 'IntercityCost', 'orderSize', 'description', 'isPayment', 'typegoood', 'countProduct', 'relOrder']);
        foreach ($array as $key => $value) {
            $data->$key = $value;
        }
        $data->ShipperId = $request->input('ShipperId');
        //hard code
        $data->AddressSenderId = $request->input("AddressStockId");

        $provinces = $request->input('province');
        $districts = $request->input('district');
        $wards = $request->input('reward');
        $receiveaddress = $request->input("receiveaddress");

        $data->provinces = $provinces;
        $data->districts = $districts;
        $data->wards = $wards;


        $data->receiveaddress = $receiveaddress;

        // dd($data);

        if (Auth::user()->role_id == 6) {

            $addressStock = AddressStock::where("id", Auth::user()->shopId)->first();
            $data->shopId = $addressStock->id;
            $data->companyId = Auth::user()->companyId;
        }
        $data->notedOrder = $request->input('notedOrder');
        $data->save();
        $orderDetail = new OrderNewDetail();
        $orderDetail->orderId = $data->id;
        $orderDetail->SizeOrder = "";
        $orderDetail->isDangerous = 0;
        $orderDetail->name = "";
        $orderDetail->shortdescription = "";
        $orderDetail->price = 0;
        $orderDetail->save();
        $code = $this->createCode($data->id);
        $data->Code = $code;

        $data->isPaymentOrder = $request->input('isPaymentOrder');
        $data->Status = config('chanh8.STT_ORDER_PROCESSING', 6);
        $valid = $data->save();

        $response = ["success" => $valid, "message" => 'successful', "data" => $data];
        return $response;

    }

    public function createCode($maxId = 57)
    {
        $order = OrderNew::where("id", $maxId)->first();

        $agency = Agency::where("id", $order->AgencyId)->first();
        $customerCode = str_pad($maxId, 4, '0', STR_PAD_LEFT);
        $giaonhanh = "0";
        if ($order->MethodId == "C") {
            $giaonhanh = "1";

        }
        $code = $order->provinces . $agency->id . $customerCode . $giaonhanh . "1" . Carbon::now()->format('d') . Carbon::now()->format('m' . Carbon::now()->format('y'));
        return $code;


    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        $roledAdmin = false;
        if ($user->role_id == 1) {
            $roledAdmin = true;
        }

        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();

        $allCustomer = Customer::all();

        return view('vendor.voyager.managementOrderNew.index', compact('allStatus', 'allAgency', 'roledAdmin', 'allCustomer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $isUpdated = true;
        //
        $idInput = $request->input('profileId');
        $CustomerId = $request->input('CustomerId');
        $MethodId = $request->input('MethodId');
        $COD = $request->input('COD');

        $AddressStockId = $request->input('AddressStockId');
        $AddressStockReturnId = $request->input('AddressStockReturnId');
        $ShipperId = $request->input('ShipperId');
        $TypeOrderId = $request->input('TypeOrderId');
        $TotalValue = $request->input('TotalValue');
        $ChargeOrder = $request->input('ChargeOrder');
        $isSenderPayment = $request->input('isSenderPayment');
        $orderSize = $request->input('orderSize');
        $description = $request->input('description');
        $typegoood = $request->input('typegoood');
        $isPayment = $request->input('isPayment');
        $IntercityCost = $request->input('IntercityCost');


        $provinces = $request->input('provinces');
        $districts = $request->input('districts');
        $wards = $request->input('wards');
        $receiveaddress = $request->input("receiveaddress");


        $orderDetail = OrderNew::where("id", $idInput)->first();
        $orderDetail->provinces = $provinces;
        $orderDetail->districts = $districts;
        $orderDetail->wards = $wards;

        $orderDetail->receiveaddress = $receiveaddress;
        $orderDetail->CustomerId = $CustomerId;
        $orderDetail->MethodId = $MethodId;


        $orderDetail->typegoood = $typegoood;
        $orderDetail->COD = $COD;
        $orderDetail->ShipperId = $ShipperId;

        $orderDetail->TotalValue = $TotalValue;
        $orderDetail->ChargeOrder = $ChargeOrder;
        $orderDetail->isSenderPayment = $isSenderPayment;

        $orderDetail->orderSize = $orderSize;
        $orderDetail->description = $description;
        $orderDetail->isPayment = $IntercityCost;

        $orderDetail->intercityCost = $isPayment;
        $orderDetail->AddressSenderId = $AddressStockId;
        $orderDetail->AddressStockReturnId = $AddressStockReturnId;
        $orderDetail->isPayment = $isPayment;

        $userLogin = Auth::user();


        $orderDetail->companyId = $userLogin->companyId;

        $orderDetail->AgencyId = $userLogin->companyId;

        $orderDetail->countProduct = $request->input("countProduct");

        $orderDetail->relOrder = $request->input("relOrder");


        if ($userLogin->role_id == 6) {
            $orderDetail->shopId = $userLogin->id;
        }
        $orderDetail->notedOrder = $request->input('notedOrder');
        $orderDetail->isPaymentOrder = $request->input('isPaymentOrder');


        $valid = $orderDetail->save();


        $response = ["success" => $valid, "message" => 'successful'];
        return $response;
    }

    /**
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Tracking order status.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function track($id)
    {
        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $orderInfo = DB::table('order_news')
            ->select('profile_status_orders.text as statusText', 'order_news.*')
            ->join('profile_status_orders', 'profile_status_orders.id', '=', 'order_news.status')
            ->where('order_news.id', $id)
            ->first();
        if ($orderInfo == null) {
            return;
        }

        $allLog = OrderNoted::where('orderId', $id)->orderby("created_at", "desc")->get();

        return view('vendor.voyager.managementOrderNew.trackingorder', ['allStatus' => $allStatus, 'orderInfo' => $orderInfo, 'allLog' => $allLog]);
    }

    private function getAllStoreAddress($user)
    {
        $allStock = DB::table('address_stocks');
        $allStock = $allStock->where("companyId", $user->companyId);

        $allStock = $allStock->leftJoin('province', 'address_stocks.province', '=', 'province.id')
            ->leftJoin('district', 'address_stocks.district', '=', 'district.id')
            ->leftJoin('ward', 'address_stocks.ward', '=', 'ward.id');

        if ($user->shopId != null || $user->shopId != '') {
            $allStock = $allStock->where("id", $user->shopId);
        }

        if ($user->role_id == 2) {
            $allStock = $allStock->where("companyId", $user->companyId);
        }

        if ($user->role_id == 6) {
            $allStock = $allStock->where("companyId", $user->companyId)
                ->where("id", $user->shopId);
        }

        $allStock = $allStock->select('address_stocks.*', 'province.name as provinceText',
            'district.name as districtText',
            'ward.name as wardText')->get();

        return $allStock;
    }

    private function getAllStoreAddressReturn($user)
    {
        $allStockReturn = DB::table('address_stock_returns')->where("companyId", $user->companyId)
            ->leftJoin('province', 'address_stock_returns.province', '=', 'province.id')
            ->leftJoin('district', 'address_stock_returns.district', '=', 'district.id')
            ->leftJoin('ward', 'address_stock_returns.ward', '=', 'ward.id');

        if ($user->shopId != null || $user->shopId != '') {
            $allStockReturn->where("id", $user->shopId);
        }

        return $allStockReturn->select('address_stock_returns.*', 'province.name as provinceText',
            'district.name as districtText',
            'ward.name as wardText')->get();
    }
}
