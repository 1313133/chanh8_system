<?php

namespace App\Http\Controllers;

use App\OrderNoted;
use Illuminate\Http\Request;

class OrderNotedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderNoted  $orderNoted
     * @return \Illuminate\Http\Response
     */
    public function show(OrderNoted $orderNoted)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderNoted  $orderNoted
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderNoted $orderNoted)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderNoted  $orderNoted
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderNoted $orderNoted)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderNoted  $orderNoted
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderNoted $orderNoted)
    {
        //
    }
}
