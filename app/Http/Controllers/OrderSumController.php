<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\OrderNew;
use App\Agency;
use App\Orders;
use App\Shipper;
use Carbon\Carbon;
use App\AddressSender;
use App\AddressStock;
use App\Customer;
use App\User;
use App\ProfileStatusOrder;
use App\TypeOrder;
use App\OrderNewDetail;
use App\TransitCar;
use Illuminate\Http\Request;
use phpDocumentor\Reflection\Types\This;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Exports\ExportFile;
use Excel;
use stdClass;

class OrderSumController extends Controller
{
    const S_NEW = "1";
    const S_TRANSFERRING = "2";
    const S_RECEIVED = "3";
    const S_COMPLETED = "4";
    const S_WAITING = "6";
    const S_CANCELLED = "7";
    const S_FAILED_DELIVERED = "8";

    // convert money to number
    private function convertMoney($str)
    {
        return str_replace(".", "", $str) + 0;
    }

    // calculate sum of COD
    private function sumOfCOD($list)
    {
        $sum = 0;
        foreach ($list as $item) {
            if (isset($item->COD)) {
                $sum += $this->convertMoney($item->COD);
            }
        }

        return $sum;
    }

    public function filterOrder(Request $request)
    {
        // counOrderStatus
      
        $fromDate = $request->input("fromdate");
        $endDate = $request->input("todate");
        $dayType = $request->input("dayType");
        $shopInput =$request->input("shop");
        $orderSumary = OrderNew::orderby("created_at");
        
        if(isset($fromDate))
        {
            $orderSumary = OrderNew::whereDate("created_at", '>=',$fromDate);
        }
       
        if(isset($endDate))
        {
            $orderSumary = $orderSumary->whereDate("created_at", '<=',$endDate);  
        };

        if($shopInput>0)
        {
            $orderSumary = $orderSumary->where("AgencyId",$shopInput);
           
        }
        $orderSumany = clone $orderSumary;
        $orderAll = clone $orderSumary;
        $queryConfirm  = clone $orderSumary;
        $queryShipping  = clone $orderSumary;
        $queryOrderNewComplete  = clone $orderSumary;
        $queryOrderSuccess  = clone $orderSumary;
        $queryOrderCacel = clone $orderSumary;
        
        $orderNewCreate = $orderSumary->where("status", 1);
      
        $orderNewConfirm = $queryConfirm->where("status", "6");
        $orderNewShipping = $queryShipping->where("status", "2");
        $orderNewComplete = $queryOrderNewComplete->where("status", "14");
        $orderSuccess = $queryOrderSuccess->where("status", "4");

        $orderCancel = $queryOrderCacel->wherein("status", ["7"]);
       
        $countOrderNewCreate =  $orderNewConfirm->count();
        $countorderNewConfirm =  $queryConfirm->count();
        $countOrderNewComplete  =  $orderNewComplete->count();
        $countorderSuccess  =  $orderSuccess->count();
        $countOrderCacel = $queryOrderCacel->count();

        $data =  new stdClass();
        
        $data->countOrder  =$orderSumany->count();
        $data->countOrderNewCreate = $countOrderNewCreate;
        $data->countorderNewConfirm = $countorderNewConfirm;
        $data->countOrderNewComplete = $countOrderNewComplete;
        $data->countorderSuccess = $countorderSuccess;
        $data->countOrderCacel = $countOrderCacel;
        $data->codCreateNew  =  $orderNewCreate->sum("cod");
        $data->codOrderNewConfirm =   $orderNewConfirm->sum("cod");
        $data->codOrderNewComplete = $orderNewComplete->sum("cod");
        $data->codorderSuccess =  $orderSuccess->sum("cod");
         $data->codorderCancel =  $orderCancel->sum("cod");
         $data->AllMoney = $orderAll->sum("cod");
         $trackStatus = view("vendor.voyager.analytics.trackerStatus", compact("data"))->render();
        return response()->json(array(
                'success' => true,
                 'html'=>$trackStatus)
                );
}

    public function filterShipper(Request $request)
    {
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
        $start = $request->input('start');
        $data =  DB::table('shippers');
      
        $limit = $request->input('length');//số lượng record hiển thị trong 1 trang,mặc định là 10
        
        $totalData = $data->count();
        $data= $data->orderBy('created_at','desc')
        ->select("shippers.*")
        -> offset($start)
        ->limit($limit)
        ->get();
     
        // foreach ($data as $key => $value) {
        //     $pr = DB::table("locations")->where('id', $value->tinh)->where('Loai',1)->first();
        //     $dt = DB::table("locations")->where('id', $value->huyen)->where('Loai',2)->first();
        //     if (isset($pr) && isset($dt)) {
        //         $value->addr = $value->address." ".$dt->Ten." ".$pr->Ten;
        //     } else {
        //         $value->addr = $value->address;
        //     }
        // }
        return  array(
        "draw"            => intval($request->input('draw'))  ,
        "recordsTotal"    => intval($totalData),
        "recordsFiltered" => intval($totalData),
        "data"            => $data,
        );
        return ["data"=>$data];

            
    }

    public function searchorder(Request $request)
    {

        $roledAdmin = true;
       
        $allStatus = ProfileStatusOrder::orderby("created_at", "desc")
            ->get();

        $allAgency = Agency::orderby("created_at", "desc")
            ->get();
        return view('vendor.voyager.analytics.searchOrder', compact('allStatus', 'allAgency', 'roledAdmin'));

    }
}

  
    

