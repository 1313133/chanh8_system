<?php

namespace App\Http\Controllers;

use App\OrderedDetail;
use Illuminate\Http\Request;

class OrderedDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderedDetail  $orderedDetail
     * @return \Illuminate\Http\Response
     */
    public function show(OrderedDetail $orderedDetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderedDetail  $orderedDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderedDetail $orderedDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderedDetail  $orderedDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderedDetail $orderedDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderedDetail  $orderedDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderedDetail $orderedDetail)
    {
        //
    }
}
