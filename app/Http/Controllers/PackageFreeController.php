<?php

namespace App\Http\Controllers;

use App\GroupFood;
use Illuminate\Http\Request;

use App\User;
use App\Agency;
use Carbon\Carbon;

use App\LevelAgency;
use Jenssegers\Agent\Agent;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\MethodPackageDelivery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PackageFreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        return view('vendor.voyager.packageFee.index');
    }
    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
        $dataEdit = MethodPackageDelivery::where("id",$id)->first();
        if($dataEdit ==null)
        {
            $dataEdit = new MethodPackageDelivery();
        }
        $allCategoryNews = MethodPackageDelivery::orderby ("created_at", "desc")
        ->get();
       return view("vendor.voyager.packageFee.detail", compact("dataEdit","allCategoryNews"));
    }
    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("method_package_deliveries")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"no data picked"];
        }
    }

    public function getAll(Request $request)
    {
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $data =  DB::table('method_package_deliveries');
            $limit = $request->input('length');
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText'); 
            $data= $data->where(function ($query) use ($search) {
                      $query->where('name', 'LIKE', "%{$search}%")
                      ->orwhere('code', 'LIKE', "%{$search}%")
                      ->orwhere('id', 'LIKE', "%{$search}%");
                });
            }
            $totalData = $data->count();
            $data= $data->orderBy('priorite')
            ->orderBy('created_at','desc')
            -> offset($start)
            ->limit($limit)
            ->get();
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            return ["data"=>$data];
    }
    private  function InputToAddressSender(Request $request,$newUpdate,$uppdate =false)
    {
        $newUpdate->code  =$request->input("editCodeGroup");
        $newUpdate->name  =$request->input("editName");

        $newUpdate->rateShipper  =$request->input("rateShipper");
        return $newUpdate;
    }
    
    public  function CreateOrEdit(Request $request)
    {
        $codeRequest = $request->input("editcode");
        if($request->input("default") == "on")
        {
            DB::table('method_package_deliveries')->update(array('default' => 0));
        }
        if($this->IsNullOrEmptyString($codeRequest) || $codeRequest =="-1")
        {
                $itemInsert = new MethodPackageDelivery();
              
                $itemInsert = $this->InputToAddressSender($request, $itemInsert,$uppdate=false);
                $itemInsert->companyId = Auth::user()->companyId;
                $itemInsert->shopId = Auth::user()->id;
                $itemInsert->chargeFee = $request->input("chargeFee");
                
                $itemInsert->priorities = 0;

                $itemInsert->default = $request->input("default") == "on";


                $itemInsert->priorite = $request->input("priorite");

                if($itemInsert->priorite)
                {

                }
                $itemInsert->save();

        }
        else
        { 
            $newUpdate = MethodPackageDelivery::where("id",$codeRequest)->first();
           
            if( $newUpdate)
            {
                $itemInsert = $this->InputToAddressSender($request, $newUpdate);
                $itemInsert->chargeFee = $request->input("chargeFee");
                $itemInsert->default = $request->input("default") == "on";
                $itemInsert->priorite = $request->input("priorite");
                $newUpdate->save();
            }
        }

       
      
        
        return ["success"=>true];

    }
}
