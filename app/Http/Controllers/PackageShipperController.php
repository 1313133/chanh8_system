<?php

namespace App\Http\Controllers;

use App\GroupFood;
use Illuminate\Http\Request;

use App\User;
use App\Agency;
use Carbon\Carbon;

use App\LevelAgency;
use Jenssegers\Agent\Agent;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\PackageShipper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PackageShipperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function IsNullOrEmptyString($str)
    {
        return (!isset($str) || trim($str) === '');
    }

    public function index()
    {
        $user = Auth::user();
        if ($user == null) {
            return redirect("/admin/login");
        }

        return view('vendor.voyager.packageShipper.index');
    }

    public function getDetailInfomation(Request $request, $slug)
    {
        $id = $slug;
        $dataEdit = PackageShipper::where("id", $id)->first();
        if ($dataEdit == null) {
            $dataEdit = new PackageShipper();
        }

        return view("vendor.voyager.packageShipper.detail", compact("dataEdit"));
    }

    public function deleteMutiple(Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("package_shippers")->whereIn('id', $dataCode)->delete();
            return ["success" => true];
        } else {
            return ["success" => false, "description" => "Dữ liệu chưa được chọn"];
        }
    }

    public function getAll(Request $request)
    {
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');
        $data = DB::table('package_shippers');
        $limit = $request->input('length');
        if ($request->has('tokenText')) {
            $search = $request->input('tokenText');
            $data = $data->where(function ($query) use ($search) {
                $query->where('text', 'LIKE', "%{$search}%")
                    ->orwhere('code', 'LIKE', "%{$search}%")
                    ->orwhere('id', 'LIKE', "%{$search}%");
            });
        }

        $totalData = $data->count();
        $data = $data->orderBy('created_at', 'desc')
            ->offset($start)
            ->limit($limit)
            ->get();
        return array(
            "draw" => intval($request->input('draw')),
            "recordsTotal" => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data" => $data,
        );
        return ["data" => $data];
    }

    private function InputToAddressSender(Request $request, $itemUpdate, $update = false)
    {
        if (!$update) {
            $itemUpdate->code = $request->input("code");
        }

        $itemUpdate->text = $request->input("text");
        $itemUpdate->value = $request->input("value");
        $itemUpdate->voucherMonth = $request->input("voucherMonth");
        $itemUpdate->description = $request->input("description");
        $itemUpdate->active = $request->input("active");
        $itemUpdate->price = $request->input("price");

        return $itemUpdate;
    }

    public function CreateOrEdit(Request $request)
    {
        $codeRequest = $request->input("editId");

        if ($this->IsNullOrEmptyString($codeRequest) || $codeRequest == "-1") {
            $itemInsert = new PackageShipper();
            $itemInsert = $this->InputToAddressSender($request, $itemInsert, false);
            $itemInsert->save();

        } else {
            $itemUpdate = PackageShipper::where("id", $codeRequest)->first();

            if ($itemUpdate) {
                $itemUpdate = $this->InputToAddressSender($request, $itemUpdate);
                $itemUpdate->save();
            }
        }

        return ["success" => true];

    }
}
