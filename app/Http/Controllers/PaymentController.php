<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\PackageShipper;
use App\Payment;
use App\Shipper;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public  function  index() {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }

        $allPackage = PackageShipper::where('active',"=", "1");

        return view('vendor.voyager.payment.index', compact('allPackage'));
    }

    public function registerPackage(Request $request) {

        $user = Auth::user();
        if ($user == null) {
            return response()->json(["status" => 'ng', "message" => "Unauthenticated"], 401);
        }

        try {
            $payment = new Payment();
            $payment->shopId = $user->id;
            $payment->shipperId = $request->input("shipperId");
            $payment->packageId = $request->input("packageId");

            $payment->save();

            return response()->json(["status" => 'ok', "message" => ""], 200);

        }catch (\Exception $e) {
            return response()->json(["status" => 'ng', "message" => $e->getMessage()], 500);
        }
    }

    public function getAll(Request $request)
    {
        $totalData = 0;
        $totalFiltered = $totalData;
        $limit = $request->input('length');
        $start = $request->input('start');

        $data =  DB::table('payments');

        if ($request->has('isCustomerPayment') && $request->has('isCustomerPayment') != null && $request->has('isCustomerPayment') != '') {
            $data->where("payments.isCustomerPayment", "=", $request->has('isCustomerPayment'));
        }
        $totalData = $data->count();
        $data= $data->orderBy('created_at','desc')
            -> offset($start)
            ->limit($limit)
            ->get();

        foreach ($data as $key => $value) {
            //$value
            $package =  PackageShipper::where("id", "=", $value->packageId)->first();
            $shipper =  Shipper::where("id", "=", $value->shipperId)->first();
            $shop =  User::where("id", "=", $value->shopId)->first();

            $package == null ? $value->package = null : $value->package = $package;
            $shipper == null ? $value->shipper = null : $value->shipper = $shipper;
            $shop == null ? $value->shop = null : $value->shop = $shop;

            if($value->approvalDate != null) {
                $value->endDate = Carbon::parse($value->approvalDate)->addDay(intval($package->value));
            } else {
                $value->endDate = null;
            }
        }

        return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
        );
    }

    public function paymentConfirm(Request $request) {
        $user = Auth::user();
        if ($user == null || $user->role_id != 1) {
            return response()->json(["status" => 'ng', "message" => "Unauthenticated"], 401);
        }

        try {
            $id = $request->input('id');
            $payment = Payment::where('id',$id)->first();
            $package = PackageShipper::where("id", $payment->packageId)->first();

            $updateData = [
                "isPayment" => true,
                "approvalDate" => Carbon::now()->format('Y-m-d H:i:s'),
            ];
            if(intval($package->value) != 0) {
                $updateData["expiredDate"] = Carbon::now()->addDay(intval($package->value))->format('Y-m-d H:i:s');
            }

            Payment::where('id',$id)->update($updateData);

            return response()->json(["status" => 'ok', "message" => ""], 200);

        }catch (\Exception $e) {
            return response()->json(["status" => 'ng', "message" => $e->getMessage()], 500);
        }

    }

    public function customerPaymentConfirm(Request $request) {
        $user = Auth::user();
        if ($user == null || ($user->role_id != 1 && $user->role_id != 2)) {
            return response()->json(["status" => 'ng', "message" => "Không có quyền truy cập"], 401);
        }

        try {
            $id = $request->input('id');

            $updateData = [
                "isCustomerPayment" => true,
                "customerPaymentDate" => Carbon::now()->format('Y-m-d H:i:s'),
            ];

            Payment::where('id',$id)->update($updateData);

            return response()->json(["status" => 'ok', "message" => "Xác nhận thanh toán thành công"], 200);

        }catch (\Exception $e) {
            return response()->json(["status" => 'ng', "message" => $e->getMessage()], 500);
        }

    }

    public function customerPaymentCancel(Request $request) {
        $user = Auth::user();
        if ($user == null || ($user->role_id != 1 && $user->role_id != 2)) {
            return response()->json(["status" => 'ng', "message" => "Không có quyền truy cập"], 401);
        }

        try {
            $id = $request->input('id');
            $deleteRecord = Payment::where('id',$id)->first();

            if($deleteRecord == null) {
                return response()->json(["status" => 'ng', "message" => "Không tồn tại record"], 500);
            } else if($deleteRecord->isCustomerPayment) {
                return response()->json(["status" => 'ng', "message" => "Không xoá được"], 500);
            }

            Payment::where('id',$id)->delete();

            return response()->json(["status" => 'ok', "message" => "Huỷ thành công"], 200);

        }catch (\Exception $e) {
            return response()->json(["status" => 'ng', "message" => $e->getMessage()], 500);
        }

    }

}
