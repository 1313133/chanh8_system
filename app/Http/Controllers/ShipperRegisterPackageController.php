<?php

namespace App\Http\Controllers;

use App\GroupFood;
use Illuminate\Http\Request;

use App\User;
use App\Agency;
use Carbon\Carbon;

use App\LevelAgency;
use Jenssegers\Agent\Agent;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\PackageShipper;
use App\Shipper;
use App\ShipperRegisterPackage;
use Composer\Package\Package;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use stdClass;

class ShipperRegisterPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
    
        return view('vendor.voyager.agencyRegisterPackage.index');
    }
    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
        $dataEdit = ShipperRegisterPackage::where("id",$id)->first();
        $agency  = new Agency();
        $shipper = new Shipper();
        $packageShipper = new  PackageShipper();
        if($dataEdit ==null)
        {
            $dataEdit = new ShipperRegisterPackage();
        }
        else 
        {

            $agency = Agency::where("id", $dataEdit->agencyId)->first();

            $shipper = Shipper::where("id", $dataEdit->shipperId)->first();
            
            return view("vendor.voyager.agencyRegisterPackage.detail", compact("dataEdit","agency","shipper"));
        }
   
 
       
    }
    public function  deleteMutiple( Request $request)
    {
        return;
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("package_shippers")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"Dữ liệu chưa được chọn"];
        }
    }

    public function getAll(Request $request)
    {

       
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $data =  DB::table('shipper_register_packages');
            $limit = $request->input('length');
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText'); 
            $data= $data->where(function ($query) use ($search) {
                      $query->where('code', 'LIKE', "%{$search}%")
                      ->orwhere('packageCode', 'LIKE', "%{$search}%")
                      ->orwhere('id', 'LIKE', "%{$search}%");
                });
            }
            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            -> offset($start)
            ->limit($limit)
            ->get();
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            return ["data"=>$data];
    }
    private  function InputToAddressSender(Request $request,$itemUpdate,$uppdate =false)
    {
        return;
        if($uppdate ==false)
        {
            $itemUpdate->code  =$request->input("code");
        }

        $itemUpdate->text  =$request->input("text");
        $itemUpdate->value  =$request->input("value");
        $itemUpdate->voucherMonth  =$request->input("voucherMonth");
        $itemUpdate->description  =$request->input("description");
        $itemUpdate->active	  =$request->input("active");
        $itemUpdate->price  = $request->input("price");

        return $itemUpdate;
    }
    
    public  function agencyRegisterSipper(Request $request)
    {

        $itemInsert =  new ShipperRegisterPackage();
        $itemInsert->code =  "1";
        $itemInsert->packageCode =  "6T";
        $itemInsert->agencyId =  "4";
        $itemInsert->shipperId =  "17";
        $itemInsert->prices =  "600000";
        $itemInsert->monthRegister =  "6";
        $itemInsert->voucher =  "1";
        $itemInsert->registerAt = Carbon::now();
        $itemInsert->validTo = Carbon::now()->addMonth( $itemInsert->monthRegister +  $itemInsert->voucher);
        $itemInsert->beginTo = Carbon::now();

        $itemInsert->paymentStatus =  "0";
        $itemInsert->payMoney =  "600000";

        $itemInsert->syntaxContent =  "6T0417";
        $itemInsert->status =  "0";

        $itemInsert->paymentDate =  null;
        $itemInsert->save();

      



      

        // $codeRequest = $request->input("editId");
        
        // if($this->IsNullOrEmptyString($codeRequest) || $codeRequest =="-1")
        // {
        //         $itemInsert = new PackageShipper();
        //         $itemInsert = $this->InputToAddressSender($request, $itemInsert,false);
        //         $itemInsert->save();

        // }
        // else
        // {
        //     $itemUpdate = PackageShipper::where("id",$codeRequest)->first();
           
        //     if( $itemUpdate)
        //     {
        //         $itemInsert = $this->InputToAddressSender($request, $itemUpdate);
        //         $itemUpdate->save();
        //     }
        // }
        
        return ["success"=>true];

    }

       
    public  function CreateOrEdit(Request $request)
    {

        $codeRequest = $request->input("editId");
        $itemUpdate = ShipperRegisterPackage::where("id",$codeRequest)->first();
        if( $itemUpdate)
        {
            $itemUpdate->status =  $request->input("status");
            $itemUpdate->paymentStatus =  $request->input("paymentStatus");
            $itemUpdate->save();
            return ["success"=>true];
        }
      
       

    }
}
