<?php
namespace App\Http\Controllers;
use App\User;
use App\Agency;
use Carbon\Carbon;
use App\TypeAgency;
use App\LevelAgency;
use Jenssegers\Agent\Agent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class TypeAgencyController extends Controller
{
    function IsNullOrEmptyString($str){
        return (!isset($str) || trim($str) === '');
    }
    public function index()
    {
        $user = Auth::user();
        if($user==null)
        {
            return redirect("/admin/login");
        }
        return view('vendor.voyager.typeagent.index');
    }
    public function   getDetailInfomation ( Request $request,$slug)
    {
        $id =$slug;
        $dataEdit = TypeAgency::where("id",$id)->first();
        if($dataEdit ==null)
        {
            $dataEdit = new TypeAgency();
        }
        $allCategoryNews = TypeAgency::orderby ("created_at", "desc")
        ->get();
       return view("vendor.voyager.typeagent.detail", compact("dataEdit","allCategoryNews"));
    }
    public function  deleteMutiple( Request $request)
    {
        $dataCode = $request->input("data");
        if (isset($dataCode)) {
            DB::table("type_agencies")->whereIn('id',$dataCode)->delete();
            return ["success"=>true];
        } else {
            return ["success"=>false,"description"=>"no data picked"];
        }
    }

    public function getAll(Request $request)
    {
            $totalData = 0;
            $totalFiltered = $totalData;
            $limit = $request->input('length');
            $start = $request->input('start');
            $data =  DB::table('type_agencies');
            $limit = $request->input('length');
            if ($request->has('tokenText')) {
            $search = $request->input('tokenText'); 
            $data= $data->where(function ($query) use ($search) {
                      $query->where('text', 'LIKE', "%{$search}%")
                      ->orwhere('id', 'LIKE', "%{$search}%");
                });
            }
            $totalData = $data->count();
            $data= $data->orderBy('created_at','desc')
            -> offset($start)
            ->limit($limit)
            ->get();
            return  array(
            "draw"            => intval($request->input('draw'))  ,
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalData),
            "data"            => $data,
            );
            return ["data"=>$data];
    }
    private  function InputToAddressSender(Request $request,$newUpdate,$uppdate =false)
    {
        $newUpdate->text  =$request->input("editName");
        return $newUpdate;
    }
    
    public  function CreateOrEdit(Request $request)
    {
        $codeRequest = $request->input("editcode");
        
        if($this->IsNullOrEmptyString($codeRequest) || $codeRequest =="-1")
        {
                $itemInsert = new TypeAgency();
                $itemInsert = $this->InputToAddressSender($request, $itemInsert,$uppdate=false);
                $itemInsert->save();

        }
        else
        {
            $newUpdate = TypeAgency::where("id",$codeRequest)->first();
           
            if( $newUpdate)
            {
                $itemInsert = $this->InputToAddressSender($request, $newUpdate);
                $newUpdate->save();
            }
        }
        
        return ["success"=>true];

    }
}
