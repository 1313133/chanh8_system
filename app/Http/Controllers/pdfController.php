<?php

namespace App\Http\Controllers;

use App\Orders;
use App\TransitCar;
use Illuminate\Http\Request;
Use PDF;
use Dompdf\Dompdf;

 


class pdfController extends Controller
{
    public function index($slug , Request $request)
    {


        $data = Orders::where("id",$slug)->first();
        if(!$data)
        {

            dd("không tìm thấy");
         }

        $transa =   TransitCar::where("id", $data->shutleTaxRel)->first();
    
       
        $pdf = PDF::loadView('invoice', compact('data','transa'));


        return $pdf->download('invoice.pdf');
     
     
    }

    public function index2()
    {
       

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml('hello world');
        
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A5', 'landscape');
        
        // Render the HTML as PDF
        $dompdf->render();
        
        // Output the generated PDF to Browser
        $dompdf->stream();
    }
}
