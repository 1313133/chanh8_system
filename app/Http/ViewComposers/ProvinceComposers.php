<?php
 namespace App\Http\ViewComposers;
 use Illuminate\Support\Facades\DB;
 use Datetime;
 use Illuminate\View\View;

 class ProvinceComposers
 {
     public $province;
     /**
      * Create a movie composer.
      *
      * @return void
      */
     public function __construct()
     {  
        

        $this->province =  DB::table('province')->orderby('name')->get();
        
     }

     /**
      * Bind data to the view.
      *
      * @param  View  $view
      * @return void
      */
     public function compose(View $view)
     {
        $view->with('listprovince', end($this->province));
     }
 }