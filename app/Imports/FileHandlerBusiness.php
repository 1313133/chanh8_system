<?php

namespace App\Imports;


    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\File;
    use Illuminate\Support\Facades\Storage;
    use Illuminate\Support\Str;
    use Intervention\Image\Constraint;
    use Intervention\Image\Facades\Image;
    use League\Flysystem\Util;
    use TCG\Voyager\Facades\Voyager;

    class FileHandlerBusiness
    {


    function remoteFileExists($url) {
        $curl = curl_init($url);

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);

        curl_setopt( $curl, CURLOPT_AUTOREFERER, TRUE );
        curl_setopt( $curl, CURLOPT_HEADER, 0 );
        curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $curl, CURLOPT_URL, $url );
        curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, TRUE );

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

            if ($statusCode == 200) {
                $ret = true;
            }
        }

        curl_close($curl);

        return $ret;
        }

    function dfCurl($url, $dir){

        if(!$this->remoteFileExists($url))

        {
            return "";
        }
        // $dir            =   "storage\\products\\";
        $dir = $dir;
        $fileName       =   basename($url);
        $saveFilePath   =   $dir . $fileName;
        $content = file_get_contents($url);

        file_put_contents($saveFilePath, $content);

        return  Voyager::image("products\\".$fileName);
    }
    public function CopyFileTo($url, $dir ="products\\"."products" )
    {
        return $this->dfCurl($url, $dir);

    }

    }

