<?php

namespace App\Imports;

use App\Product;
use Illuminate\Support\Collection;
use App\Imports\FileHandlerBusiness;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;


class ProductImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function headingRow(): int
    {
        return 2;
    }


    public function collection(Collection $rows)
    {

        $index =0;
        $fileHandle = new FileHandlerBusiness();
        foreach ($rows as $row)
        {

            $index++;
            if($index==1)
            {
                continue;
            }
            else
            {
            }


            $product = new Product();
            $product->ProductCode  = $row[1];

            if($product->ProductCode =="")
            {
                continue;
            }
            $productExits = Product::where("ProductCode",$product->ProductCode)->first();
            if($productExits ==null)
            {
                 $productExitsSlug =  Product::where("slug",$product->slug)->first();
                if($productExitsSlug)
                {
                    continue;
                }
            }
            else
            {
                $product = $productExits;
            }
            $product->title  = $row[2];
            $product->isActive  =  $row[3];
            $product->priceOrginal  =  $row[4];



            $product->pricePresent  =  $row[5];
            $product->weight  =  $row[6];

            $product->FeauterImage  = "";
            $productCategory = $row[8];
            $productCategoryLevel3 = $row[9];
            if( $productCategory !=null ||   $productCategory !="")
            {
                $product->refCategory  = $productCategory;
            }
            if( $productCategoryLevel3 !=null ||   $productCategoryLevel3 !="")
            {
                $product->productCategory3code  = $productCategoryLevel3;

            }
            $urlLinkImage = $row[10];
            $product->linkImageShare  = $fileHandle->CopyFileTo($urlLinkImage);
            $product->FeauterImage  = $fileHandle->CopyFileTo($urlLinkImage);

            $product->introduction  = $row[12];
            $product->vendor  = $row[13];
            $product->shortDescription  =  $row[15];
            $product->videoEmbed  =$fileHandle->CopyFileTo($row[14]);

            $product->titleSeo = $row[16];
            $product->shortDescription =$row[17];

            $product->save();
        }


    }
}
