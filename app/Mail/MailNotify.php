<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailNotify extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
     public function __construct($data)
    {
        $this->data = $data;
    }
    public function build()
   {
        $message = $this->data;
       return $this->from('orderInfo@email.com')
           ->view('mails.mail-notify')
           ->subject('Thông báo có đơn đặt hàng mới');
   }
}
