<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderNew extends Model
{
    public static function boot()
    {
        parent::boot();
        static::created(function($model)
         {
            $model->Status =  1;
            $model->code = "CHANH8_0000000".$model->id."ABC";
            $model->save();
        });
        
    }
}
