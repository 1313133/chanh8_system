<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Orders extends Model
{

    public static function boot()
    {
        parent::boot();
        static::created(function($model)
         {
             $user = Auth::user();
             if($user)
             {
                 $model->createBy =  $user->id;
             }
             $userLogin = Auth::user();

             if($userLogin)
             {
                $model->createBy = $userLogin->id;
             }



             $model->code = "CHANHSI_0000000".$model->id."ABC";
             $model->save();
        });
        
    }

}
