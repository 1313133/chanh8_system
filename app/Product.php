<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
use Carbon\Carbon;
use Illuminate\Support\Str;

class Product extends Model
{
    use Translatable;

    protected $translatable = ['title','slug','description','shortDescriptionContent','shortDescription','weight','slug','content','shortDescriptionTitle','titleSeo','descriptionSEO'];
    public function produtCategory()
    {
        return $this->belongsTo('App\ProductCategory',"refCategory","id");
    }


    public static function boot()
    {
        parent::boot();
        static::created(function($model)
         {

            if($model->slug == "")
            {
                $model->slug =Str::slug($model->title.Carbon::today()->format("dmY").$model->id);
            }
            else
            {

            }

           $model->save();
        });

    }
}
