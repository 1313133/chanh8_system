<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Str;

use Illuminate\Database\Eloquent\Model;

use TCG\Voyager\Traits\Translatable;
class ProductCategory extends Model
{

    use Translatable;
    protected $translatable = ['title','titleDetail','titleHomePage','descriptionHomePage','descriptionSEO','titleSeo','titleDisplay','shortDescription','weight'];
    public function ListProduct()
    {
        return $this->hasMany('App\Product', 'refCategory', 'id')->orderBy('priority','desc')->orderby("updated_at","asc")->paginate(30);
    }


    public function AllProduct()
    {
        return $this->hasMany('App\Product', 'refCategory', 'id')->orderBy('priority','desc')->orderby("updated_at","asc")->paginate(30);
    }


    public function group()
    {
        return $this->belongsTo('App\Group',"groupCode","id");
    }


    public function AllProductCategory()
    {
        return $this->hasMany('App\ProductCategory3', 'parrentCode', 'id')
        ->orderBy('priority','asc')
        ->orderby("updated_at","desc")->paginate(30);
    }



    public static function boot()
    {
        parent::boot();
        static::created(function($model)
        {

           $model->slug =Str::slug($model->title.Carbon::today()->format("dmY").$model->id);
           $model->save();

        });
        static::updating(function($model)
        {


            $model->slug =Str::slug($model->title.Carbon::today()->format("dmY").$model->id);


        });
    }
}


