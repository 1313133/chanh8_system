<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileStatusOrder extends Model
{
    protected $table = 'profile_status_orders';
}
