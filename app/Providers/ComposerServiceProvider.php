<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['vendor.voyager.addressSender.edit','vendor.voyager.addressStock.edit','vendor.voyager.customer.edit'],'App\Http\ViewComposers\ProvinceComposers');
    }
}
