<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;
class TechologyItem extends Model
{
    use Translatable;
    protected $translatable = ['title', 'descripton'];
}
