<?php
return [
    'tables' => [
        'provinces' => 'province',
        'districts' => 'district',
        'wards'     => 'ward',
    ],

    'columns' => [
        'name'        => 'name',
        'gso_id'      => 'gso_id',
        'province_id' => 'province_id',
        'district_id' => 'district_id',
    ],
];
