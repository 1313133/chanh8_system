<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOTPAcessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('o_t_p_acesses', function (Blueprint $table) {
            $table->id();
            $table->string('userId',10)->nullable();
            $table->string('code',10)->nullable();
            $table->string('status',10)->nullable();
            $table->string('typeAccount',10)->nullable();
            $table->dateTime('valid')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('o_t_p_acesses');
    }
}
