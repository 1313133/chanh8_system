<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_stocks', function (Blueprint $table) {
            $table->id();
            $table->string('mobilePhone',20)->nullable();
            $table->string('title',191)->nullable();
            $table->string('avatarLink')->nullable();
            $table->string('userId',10)->nullable();
            $table->string('district')->nullable();
            $table->string('ward')->nullable();
            $table->string('province')->nullable();
            $table->string('addresssInfo')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_stocks');
    }
}
