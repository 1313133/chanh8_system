<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_news', function (Blueprint $table) {
            $table->id();
            $table->string('Code',20)->nullable();
            $table->unsignedBigInteger('AddressSenderId');
            $table->foreign('AddressSenderId')->references('id')->on('address_senders');
            $table->unsignedBigInteger('CustomerId');
            $table->foreign('CustomerId')->references('id')->on('customers');
            $table->unsignedBigInteger('AddressStockId');
            $table->foreign('AddressStockId')->references('id')->on('address_stocks');
            $table->unsignedBigInteger('ShipperId');
            $table->foreign('ShipperId')->references('id')->on('users');
            $table->unsignedBigInteger('AgencyId');
            $table->foreign('AgencyId')->references('id')->on('users');
            $table->string('MethodId',20)->nullable();
            $table->string('TotalValue')->nullable();
            $table->string('COD',20)->nullable();
            $table->boolean('isSenderPayment')->nullable();
            $table->boolean('isThuHO')->nullable();
            $table->integer('TypeOrderId')->nullable();
            $table->string('Status',20)->nullable();
            $table->string('ChargeOrder',20)->nullable();
            $table->timestamp('BusinessTime', 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_news');
    }
}
