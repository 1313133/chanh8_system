<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackageShippersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_shippers', function (Blueprint $table) {
            $table->id();
            $table->string('code',3)->nullable();
            $table->string('active',1)->nullable();
            $table->string('value',2)->nullable();
            $table->string('text',191)->nullable();
            $table->string('voucherMonth',2)->nullable();
            $table->string('description',191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_shippers');
    }
}
