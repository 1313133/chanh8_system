<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipperRegisterPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipper_register_packages', function (Blueprint $table) {
            $table->id();
            $table->string('code',5)->nullable();
            $table->string('packageCode',10)->nullable();
            $table->string('agencyId',2)->nullable();
            $table->string('shipperId',191)->nullable();
            $table->decimal('prices',18)->nullable();
            $table->string('monthRegister',2)->nullable();
            $table->string('voucher',191)->nullable();
            $table->dateTime('registerAt')->nullable();
            $table->dateTime('validTo')->nullable();
            $table->dateTime('beginTo')->nullable();
            
            $table->string('paymentStatus',2)->nullable();
            $table->decimal('payMoney',18)->nullable();
            $table->string('syntaxContent',191)->nullable();
            $table->string('status')->nullable();
            $table->dateTime('paymentDate')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipper_register_packages');
    }
}
