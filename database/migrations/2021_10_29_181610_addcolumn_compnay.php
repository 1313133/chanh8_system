<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddcolumnCompnay extends Migration
{
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function(Blueprint $table)
        {
            $table->string('companyId',10)->nullable();
        });

        Schema::table('address_stocks', function(Blueprint $table)
        {
            $table->string('companyId',10)->nullable();
        });

        Schema::table('order_news', function(Blueprint $table)
        {
            $table->string('companyId',10)->nullable();
        });

        Schema::table('order_new_details', function(Blueprint $table)
        {
            $table->string('companyId',10)->nullable();
        });

        Schema::table('users', function(Blueprint $table)
        {
            $table->string('companyId',10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
