<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMethodPackageDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('method_package_deliveries', function (Blueprint $table) {
                $table->id();
                $table->string('code',10)->nullable();
                $table->string('companyId',10)->nullable();
                $table->string('shopId',10)->nullable();
                $table->string('name',50)->nullable();
                $table->string('priorities')->nullae();
                $table->string('rateShipper',18)->nullable();
                $table->timestamps();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('method_package_deliveries');
    }
}
