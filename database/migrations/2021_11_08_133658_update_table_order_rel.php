<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTableOrderRel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_news', function(Blueprint $table)
        {
            
            $table->string('relOrder',50)->nullable();
            $table->string('rateShipper',50)->nullable();

            //  $table->string('companyId',10)->nullable();
            //  $table->string('shopId',10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
