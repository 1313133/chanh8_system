<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      

        Schema::table('order_noteds', function(Blueprint $table)
        {
               $table->longText('LinkImage')->nullable();
               $table->string('status',8)->nullable();

                $table->string('shopId',10)->nullable();
                $table->string('companyId',10)->nullable();
       });

      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
