const { update } = require("lodash");

function sendRequestOrderNew() {

   var url = "/them-moi-vao-gio-hang";
    $.ajax({
      type: "post",
      url: url,
      data: $("#orderRequest1").serialize(), // serializes the form's elements.
      success: function(data)
         {

            updateBasketNumber(data.totalCard);



           swal({
            timer: 2000,
            text: "Thêm mới giở hàng thành công",
            icon: "success",
            buttons: false,
          });
      }
    });
}


function sendRequestOrder(btnadd) {
    var x = btnadd.closest(".row");
    var numberInput =x.getElementsByClassName("cart-t__qtt");
    var url = "/cap-nhat-order";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
     $.ajax({
       type: "post",
       url: url,
       data: {
        "productId" :  numberInput[0].getAttribute("productid"),
        "number" : numberInput[0].value,
       }, // serializes the form's elements.
       success: function(data)
          {

             updateBasketNumber(data.totalCard);

       }
     });
 }



function checkOutOrder() {


    if(!validateFormOrderInfo())
    {
            return;
    }


    CheckOutOrderComplete();


 }

function sendOrder() {
    if(!validateFormOrder())
    {
            return;
    }


    updateCardOrder();





 }

 function updateCartRequest()

 {
    updateCardOrder();



 }


 function updateCartRequestOrderPage()
 {




 }

 function updateCardOrder()
 {



    var totalAmount = 0;
    var totalCard =0;
    var listReqest = [];
    $(".cart-t__qtt").each(function() {
        var element = $(this)[0];
        var pricePresent = element.getAttribute("pricePresent");
            var productCode =   element.getAttribute("productid");
            var number = element.value;
        if(pricePresent==null)
        {
            pricePresent =0;
        }
        var tiem = {
            number  : number,
            productId :  productCode

        }
        listReqest.push(tiem);
     });
 var url = "/cap-nhat-gio-hang";
 $.ajax({
    type: "post",
    url: url,
    data: {
     "_token": $('#csrf-token')[0].content,
     "array": listReqest,

     }, // serializes the form's elements.
    success: function(data)
    {
        window.location.href ="/thong-tin-thanh-toan";
    }


 });
}

function updateCartPage()
{


    var x =$(".deletebtn");

    if(x.length <1)
    {
        location.reload();
    }
    var totalAmount = 0;
    var totalCard =0;
    $(".cart-t__qtt").each(function() {


        var element = $(this)[0];




        var pricePresent = element.getAttribute("pricePresent");

        if(pricePresent==null)
        {
            pricePresent =0;
        }

        var number =1;

        number =element.value;
        if(number ==null || number <1)
        {
            number =1;
        }
        totalAmount+= pricePresent*1 * number;
        totalCard ++;


    });




    totalAmount =  totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    $('#totalMoney').text(''+totalAmount  +' đ' );




}
 function numberWithCommas(number) {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
 function removeCard(productID) {
    var url = "/remove-card";

    $.ajax({
       type: "POST",
       url: url,
       data: {
        "_token": $('#csrf-token')[0].content,
        "productId": productID
        }, // serializes the form's elements.
       success: function(data)
       {

                updateCartPage();

                updateBasketNumber(data.totalCard);


       }
     });
 }
function validateFormOrderInfo ()

{



}



 function validateFormOrder()
 {



    var txtName = document.getElementById("txtfullName");
    var txtPhone = document.getElementById("phoneNumber");
    var txtEmail = document.getElementById("email");
    var txtAddress =  document.getElementById("txtAddress");

    var txtNoted =  document.getElementById("txtNoted");


    var index = 0;
    if (checkIsEmpty(txtName)) {

        addError(txtName, resources.contactPage.errorRequire.fullName);
        index++
    } else {
        removeError(txtName)
    }

    if (checkIsEmpty(txtPhone)) {
        addError(txtPhone,  resources.contactPage.errorRequire.phoneNumber);
        index++
    } else {
        removeError(txtPhone)
    }
    if (checkIsEmpty(txtEmail)) {
        addError(txtEmail,  resources.contactPage.errorRequire.email);
        index++
    } else {
        removeError(txtEmail)
    }


    if (checkIsEmpty(txtAddress)) {
        addError(txtAddress,  "Địa chỉ yêu cầu bắt buộc nhập");
        index++
    } else {
        removeError(txtAddress)
    }




    if (index > 0) {
        return false;
    }

    return true;

 }




function isNumberKey(evt) {
    var theEvent = evt || window.event;
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain')
    } else {
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key)
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = !1;
        if (theEvent.preventDefault) theEvent.preventDefault()
    }
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email)
}

function resetContact() {
    showOrHideResultAflterSubmit(!1);

}

function addError(objectTextBox, messageError) {

    var parrentDiv = objectTextBox.parentNode;
    if (!parrentDiv)
        return;
    if(parrentDiv.childElementCount>1)
    {
        objectTextBox.parentNode.removeChild(objectTextBox.parentElement.lastElementChild);
    }
    parrentDiv.innerHTML += '<div class="invalid-feedback d-block"> ' + messageError +' </div>';
}

function removeError(objectTextBox) {
    var parrentDiv = objectTextBox.parentNode;
    if (!parrentDiv)
        return;
    if(parrentDiv.childElementCount>1)
    {
        objectTextBox.parentNode.removeChild(objectTextBox.parentElement.lastElementChild);
    }
    else
    {
        return;
    }
}

function showOrHideResultAflterSubmit(isShow) {

}

function showOrHideDataSubmit(isShow) {
    var display = document.getElementById("submitData");
    if (isShow) {
        display.style.display = "block"
    } else {
        display.style.display = "none"
    }
}

function checkIsEmpty(textBox) {
    var textBoxValue = textBox.value;
    if (!textBoxValue) {
        return !0
    }
    return textBoxValue.length < 1
}


function validateForm() {


    var txtName = document.getElementById("txtName");
    var txtPhone = document.getElementById("txtPhone");
    var txtEmail = document.getElementById("txtEmail");




    var index = 0;
    if (checkIsEmpty(txtName)) {

        addError(txtName, resources.contactPage.errorRequire.fullName);
        index++
    } else {
        removeError(txtName)
    }

    if (checkIsEmpty(txtPhone)) {
        addError(txtPhone,  resources.contactPage.errorRequire.phoneNumber);
        index++
    }
    // else  if ( checkValidPhoneNumber(txtPhone.value) ==false)
    // {
    //     addError(txtPhone,  "Số điện thoại không hợp lệ");
    //     index++
    // }

    else {
        removeError(txtPhone)
    }
    if (checkIsEmpty(txtEmail)) {

    }
    else

    {
        if ( validateEmail(txtEmail.value) ==false)
        {
            addError(txtEmail,  "Email không hợp lệ");
            index++
        }
        else
        {
            removeError(txtEmail)
        }

    }



    if( document.getElementById("upload").files.length == 0 ){

        $("#resultfile").val(resources.tuyendungPage.errorRequire.fileNotSelect);
        // addError(document.getElementById("upload"),  resources.tuyendungPage.errorRequire.fileNotSelect);
        index++;
    }
    else
    {


    }


    if (index > 0) {
        return false;
    }

    return true;

}

function openUloadFile() {
    $('#uploadfileId').trigger('click');
}

function prepareLoad(file)
{
    var fileneedUpload =  file.files[0];
     if(fileneedUpload)
     {
         document.getElementById("resultfile").innerHTML  = fileneedUpload.name;
         document.getElementById("resultfile").style.display = block;

     }


}
