
function sendRequest ()
{
    if(!validateFormContact())
    {
            return;
    }
    var url = "/api/contact/createRequest";
    $.ajax({
       type: "POST",
       url: url,
       data: $("#contactForm").serialize(), // serializes the form's elements.
       success: function(data)
       {

         $(".hideFormContact").hide();
         $(".resultRequest").show();

       }
     });

}

function sendApplyForm() {


    if(!validateForm())
    {
            return;
    }

    var formData = new FormData();
    formData.append('fullName',  $("#txtName").val());
    formData.append('id',  $("#txtcode").val());
    formData.append('phoneNumber', $("#txtPhone").val());
     formData.append('email', $("#txtEmail").val());
     formData.append('attachfile',$("#upload")[0].files[0]);

    var url = "/api/tuyen-dung/applyjob";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }

       });


       $.ajax({
       type: "POST",
       contentType: false,
       processData: false,
       data        : formData,
       url: url,
       success: function(data)
       {
        //  alert("success");
         $(".hideFormContact").hide();
         $("#lableSucess").show();

       }
     });
 }

 function validateFormContact()
 {


    

    var txtName = document.getElementById("txtName");
    var txtPhone = document.getElementById("txtPhone");
    var txtEmail = document.getElementById("txtEmail");
    var txtMsg =  document.getElementById("txtaddress");
    var index = 0;
    if (checkIsEmpty(txtName)) {

        addError(txtName, resources.contactPage.errorRequire.fullName);
        index++
    } else {
        removeError(txtName)
    }

    if (checkIsEmpty(txtPhone)) {
        addError(txtPhone,  resources.contactPage.errorRequire.phoneNumber);
        index++
    } else {
        removeError(txtPhone)
    }



    if (checkIsEmpty(txtMsg)) {
        addError(txtMsg,  resources.contactPage.errorRequire.address);
        index++
    } else {
        removeError(txtMsg)
    }

    // if (checkIsEmpty(txtMsg)) {
    //     addError(txtMsg,resources.contactPage.errorRequire.yourMessage);
    //     index++
    // } else {
    //     removeError(txtMsg)
    // }

    if (index > 0) {
        return false;
    }

    return true;

 }




function isNumberKey(evt) {
    var theEvent = evt || window.event;
    if (theEvent.type === 'paste') {
        key = event.clipboardData.getData('text/plain')
    } else {
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key)
    }
    var regex = /[0-9]|\./;
    if (!regex.test(key)) {
        theEvent.returnValue = !1;
        if (theEvent.preventDefault) theEvent.preventDefault()
    }
}

function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email)
}

function resetContact() {
    showOrHideResultAflterSubmit(!1);

}

function addError(objectTextBox, messageError) {

    var parrentDiv = objectTextBox.parentNode;
    if (!parrentDiv)
        return;
    if(parrentDiv.childElementCount>1)
    {
        objectTextBox.parentNode.removeChild(objectTextBox.parentElement.lastElementChild);
    }
    parrentDiv.innerHTML += '<div class="invalid-feedback d-block"> ' + messageError +' </div>';
}

function removeError(objectTextBox) {
    var parrentDiv = objectTextBox.parentNode;
    if (!parrentDiv)
        return;
    if(parrentDiv.childElementCount>1)
    {
        objectTextBox.parentNode.removeChild(objectTextBox.parentElement.lastElementChild);
    }
    else
    {
        return;
    }
}

function showOrHideResultAflterSubmit(isShow) {

}

function showOrHideDataSubmit(isShow) {
    var display = document.getElementById("submitData");
    if (isShow) {
        display.style.display = "block"
    } else {
        display.style.display = "none"
    }
}

function checkIsEmpty(textBox) {
    var textBoxValue = textBox.value;
    if (!textBoxValue) {
        return !0
    }
    return textBoxValue.length < 1
}


function validateForm() {


    var txtName = document.getElementById("txtName");
    var txtPhone = document.getElementById("txtPhone");
    var txtEmail = document.getElementById("txtEmail");




    var index = 0;
    if (checkIsEmpty(txtName)) {

        addError(txtName, resources.contactPage.errorRequire.fullName);
        index++
    } else {
        removeError(txtName)
    }

    if (checkIsEmpty(txtPhone)) {
        addError(txtPhone,  resources.contactPage.errorRequire.phoneNumber);
        index++
    }
    // else  if ( checkValidPhoneNumber(txtPhone.value) ==false)
    // {
    //     addError(txtPhone,  "Số điện thoại không hợp lệ");
    //     index++
    // }

    else {
        removeError(txtPhone)
    }
    if (checkIsEmpty(txtEmail)) {

    }
    else

    {
        if ( validateEmail(txtEmail.value) ==false)
        {
            addError(txtEmail,  "Email không hợp lệ");
            index++
        }
        else
        {
            removeError(txtEmail)
        }

    }



    if( document.getElementById("upload").files.length == 0 ){

        $("#resultfile").val(resources.tuyendungPage.errorRequire.fileNotSelect);
        // addError(document.getElementById("upload"),  resources.tuyendungPage.errorRequire.fileNotSelect);
        index++;
    }
    else
    {


    }


    if (index > 0) {
        return false;
    }

    return true;

}

function openUloadFile() {
    $('#uploadfileId').trigger('click');
}

function prepareLoad(file)
{
    var fileneedUpload =  file.files[0];
     if(fileneedUpload)
     {
         document.getElementById("resultfile").innerHTML  = fileneedUpload.name;
         document.getElementById("resultfile").style.display = block;

     }


}
