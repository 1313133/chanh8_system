
// search page common

function updateBreadCrump(name = null) {
    var x = $('.breadcrumb').children().eq(2);
    if (x.length < 1) {
        $('.breadcrumb').append("<li> </li>");
    }

    x = $('.breadcrumb').find('li:last')

    if (name) {
        x.html(name);
    }
    else {
        x.empty();
    }
}

function activeTab(tab, resetBreadCrump = false) {
    if (resetBreadCrump) {
        updateBreadCrump();
    }
    $('[href=' + '"#' + tab + '"]').tab('show');


    inittinymce();

};


function refeshSearch(tableId = null) {
    if (tableId == null) {
        tableId = "table_id";
    }
    var tableReload = $("#" + tableId).DataTable();
    tableReload.ajax.reload();
}


function BindingBasic(items) {
    var propertiesKey = Object.keys(items);
    propertiesKey.map(function (key, index) {        //find by id
        var namelement = "edit" + key;
        var element = document.getElementById(namelement);
        if (element != undefined) {
            if (element.type == "textarea") {

                try {
                    tinymce.get(namelement).setContent(items[key]);
                }
                catch (err) {
                    tinymce.get(namelement).setContent("");
                }
            }
            else {
                element.value = items[key];
            }

        }

    });
    initView(items);

    inittinymce();
}


function backToSearch() {

    activeTab("search", true);
    updateBreadCrump("Product list");
    refeshSearch();
}

function inittinymce() {


    tinymce.init({
        menubar: true,

        selector: 'textarea.richTextBox',
        skin_url: $('meta[name="assets-path"]').attr('content') + '?path=js/skins/voyager',
        min_height: 600,
        resize: 'vertical',
        plugins: '  searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help code textcolor colorpicker',
        extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
        file_browser_callback: function (field_name, url, type, win) {

            $('#upload_file').trigger('click');

        },
        // toolbar: 'formatselect | bold italic strikethrough | forecolor backcolor | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | ltr rtl | removeformat | code',
        toolbar: 'codesample | bold italic underline strikethrough sizeselect fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image | insertfile undo redo | forecolor backcolor emoticons | code | media| link | numlist bullist ',
        convert_urls: false,
        image_caption: true,
        image_title: true,
        content_css: "/css/admin.css,/css/adminFont.css",
        valid_elements: 'div[*],p[*],span[*],ul[*],li[*],ol[*],hr,br,img[*],i[*],em,table[*],tr[*],td[*],th[*],sup[*],sub[*],strong[*],b,h1[*],h2[*],h3[*],h4[*],h5[*],h6[*],small[*],a[*], svg,path',
        valid_children: '+li[span|p|div]',
        content_style: '.mce-annotation { background: #red; } .tc-active-annotation {background: #ffe168; color: black; }',
        body_class: "mceBlackBody",
        fontsize_formats: "8pt pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 36pt 48pt 60px",


        external_plugins: {
            'print': '/tinymce/plugins/print/plugin.min.js',
            'preview': '/tinymce/plugins/preview/plugin.min.js',
            'searchreplace': '/tinymce/plugins/searchreplace/plugin.min.js',
            'directionality': '/tinymce/plugins/directionality/plugin.min.js',
            'visualchars': '/tinymce/plugins/visualchars/plugin.min.js',
            'visualblocks': '/tinymce/plugins/visualblocks/plugin.min.js',
            'autolink': '/tinymce/plugins/autolink/plugin.min.js',
            'fullscreen': '/tinymce/plugins/fullscreen/plugin.min.js',
            'media': '/tinymce/plugins/media/plugin.min.js',
            'template': '/tinymce/plugins/template/plugin.min.js',
            'codesample': '/tinymce/plugins/codesample/plugin.min.js',
            'charmap': '/tinymce/plugins/charmap/plugin.min.js',
            'pagebreak': '/tinymce/plugins/pagebreak/plugin.min.js',
            'hr': '/tinymce/plugins/hr/plugin.min.js',
            'anchor': '/tinymce/plugins/anchor/plugin.min.js',
            'nonbreaking': '/tinymce/plugins/nonbreaking/plugin.min.js',
            'insertdatetime': '/tinymce/plugins/insertdatetime/plugin.min.js',
            'toc': '/tinymce/plugins/toc/plugin.min.js',
            'advlist': '/tinymce/plugins/advlist/plugin.min.js',
            'wordcount': '/tinymce/plugins/wordcount/plugin.min.js',
            'imagetools': '/tinymce/plugins/imagetools/plugin.min.js',
            'textpattern': '/tinymce/plugins/textpattern/plugin.min.js',
            'help': '/tinymce/plugins/help/plugin.min.js',
            'colorpicker': '/tinymce/plugins/colorpicker/plugin.min.js'


        },



        external_filemanager_path: "/filemanager/",
        filemanager_title: "Responsive Filemanager",
        media_live_embeds: true,

        setup: function (editor) {
            editor.on("change keyup", function (e) {
                console.log('saving');
                //tinyMCE.triggerSave(); // updates all instances
                editor.save(); // updates this instance's textarea
                $(editor.getElement()).trigger('change'); // for garlic to detect change
            });
        },
        extended_valid_elements: 'video[onclick|controlslist|controls],iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
        audio_template_callback: function (data) {
            return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
        },
        video_template_callback: function (data) {

            return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
        }


    })
}

function tinymce_init_callback(editor) {


    editor.remove();
    editor = null;

    tinymce.init({
        menubar: true,

        selector: 'textarea.richTextBox',
        skin_url: $('meta[name="assets-path"]').attr('content') + '?path=js/skins/voyager',
        min_height: 600,
        resize: 'vertical',
        plugins: ' preview searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern help code textcolor colorpicker',
        extended_valid_elements: 'input[id|name|value|type|class|style|required|placeholder|autocomplete|onclick]',
        file_browser_callback: function (field_name, url, type, win) {

            $('#upload_file').trigger('click');

        },
        // toolbar: 'formatselect | bold italic strikethrough | forecolor backcolor | link image media | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent | ltr rtl | removeformat | code',
        toolbar: 'codesample | bold italic underline strikethrough sizeselect  fontsizeselect | hr alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image | insertfile undo redo | forecolor backcolor emoticons | code | media| link | numlist bullist ',
        convert_urls: false,
        image_caption: true,
        image_title: true,
        content_css: "/css/admin.css,/css/adminFont.css",
        valid_elements: 'div[*],p[*],span[*],ul[*],li[*],ol[*],hr,br,img[*],i[*],em,table[*],tr[*],td[*],th[*],sup[*],sub[*],strong[*],b,h1[*],h2[*],h3[*],h4[*],h5[*],h6[*],small[*],a[*], svg,path',
        valid_children: '+li[span|p|div]',
        content_style: '.mce-annotation { background: #red; } .tc-active-annotation {background: #ffe168; color: black; }',
        body_class: "mceBlackBody",
        fontsize_formats: "8pt 9pt 10pt 11pt 12pt 13pt 14pt 15pt 16pt 17pt 18pt 19pt 20pt 21pt 22pt 23pt 24pt 36pt 48pt 60px",

        external_plugins: {

            'preview': '/tinymce/plugins/preview/plugin.min.js',
            'searchreplace': '/tinymce/plugins/searchreplace/plugin.min.js',
            'directionality': '/tinymce/plugins/directionality/plugin.min.js',
            'visualchars': '/tinymce/plugins/visualchars/plugin.min.js',
            'visualblocks': '/tinymce/plugins/visualblocks/plugin.min.js',
            'autolink': '/tinymce/plugins/autolink/plugin.min.js',
            'fullscreen': '/tinymce/plugins/fullscreen/plugin.min.js',
            'media': '/tinymce/plugins/media/plugin.min.js',
            'template': '/tinymce/plugins/template/plugin.min.js',
            'codesample': '/tinymce/plugins/codesample/plugin.min.js',
            'charmap': '/tinymce/plugins/charmap/plugin.min.js',
            'pagebreak': '/tinymce/plugins/pagebreak/plugin.min.js',
            'hr': '/tinymce/plugins/hr/plugin.min.js',
            'anchor': '/tinymce/plugins/anchor/plugin.min.js',
            'nonbreaking': '/tinymce/plugins/nonbreaking/plugin.min.js',
            'insertdatetime': '/tinymce/plugins/insertdatetime/plugin.min.js',
            'toc': '/tinymce/plugins/toc/plugin.min.js',
            'advlist': '/tinymce/plugins/advlist/plugin.min.js',
            'wordcount': '/tinymce/plugins/wordcount/plugin.min.js',
            'imagetools': '/tinymce/plugins/imagetools/plugin.min.js',
            'textpattern': '/tinymce/plugins/textpattern/plugin.min.js',
            'help': '/tinymce/plugins/help/plugin.min.js',
            'colorpicker': '/tinymce/plugins/colorpicker/plugin.min.js'

        },

        media_live_embeds: true,
        extended_valid_elements: 'video[onclick|controlslist|controls],iframe[src|frameborder|style|scrolling|class|width|height|name|align]',
        audio_template_callback: function (data) {
            return '<audio controls>' + '\n<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + '</audio>';
        },
        video_template_callback: function (data) {
            return '<video width="' + data.width + '" height="' + data.height + '"' + (data.poster ? ' poster="' + data.poster + '"' : '') + ' controls="controls">\n' + '<source src="' + data.source1 + '"' + (data.source1mime ? ' type="' + data.source1mime + '"' : '') + ' />\n' + (data.source2 ? '<source src="' + data.source2 + '"' + (data.source2mime ? ' type="' + data.source2mime + '"' : '') + ' />\n' : '') + '</video>';
        }


    })
}

function tinymce_setup_callback(editor) {

    editor.setContent("");


}

function logoutAdmin()
{
    $("#btnLogout").click();
}

function isValid(p) {
    var phoneRe = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    var digits = p.replace(/\D/g, "");
    return phoneRe.test(digits);
  }

  $('#mobile-number').bind("keypress", function (event) {
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

    var phoneRe = /^[+]*[(]{0,1}[0-9]{1,3}[)]{0,1}[-\s\./0-9]*$/g;
    if (!phoneRe.test(key)) {
        event.preventDefault();
        return false;
    }

    
});