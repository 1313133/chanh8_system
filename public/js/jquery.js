$(document).ready(function () {

    $(".fa-search").click(function () {
        $("#searchArea").css("display", "block");
    })

    $("#exitSearch").click(function () {
        $("#searchArea").css("display", "none");
    })

    // $('.btnDown').on('click', function (e) {
    //     e.preventDefault();

    //     var numProduct = Number($(this).next().val());
    //     if (numProduct > 0) $(this).next().val(numProduct - 1);
    //     return false;
    // });
    // $('.btnUp').on('click', function (e) {
    //     e.preventDefault();

    //     var numProduct = Number($(this).prev().val());
    //     $(this).prev().val(numProduct + 1);
    //     return false;
    // });

    $('#scrollTop').hide();
    $(window).scroll(function () {
        if ($(document).scrollTop() > 100) {
            $('#scrollTop').show();
        } else {
            $('#scrollTop').hide();
        }
    });
    $("#scrollTop").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    });
    $('.proShow').owlCarousel({
        loop: true,
        nav: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 5000,
        autoplayHoverPause: true,
        items: 1
    })
    $('.another-pros__slide').owlCarousel({
        loop: false,
        margin: 10,
        nav: false,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1,
            },
            400: {
                items: 2,
            },
            650: {
                items: 3,
            },
            768: {
                items: 4,
            },
            992: {
                items: 5
            }
        }
    });


    // Carousel with thumbnails--------
    var bigImg = $("#big-img");
    var thumbs = $("#thumbs");
    var syncedSecondary = true;
    bigImg
        .owlCarousel({
            items: 1,
            slideSpeed: 2000,
            nav: false,
            dots: false,
            loop: true,
            autoHeight: true,
            responsiveRefreshRate: 200,
        })
        .on("changed.owl.carousel", syncPosition);
    thumbs
        .on("initialized.owl.carousel", function () {
            thumbs
                .find(".owl-item")
                .eq(0)
                .addClass("current");
        })
        .owlCarousel({
            items: 3,
            nav: false,
            dots: false,
            smartSpeed: 200,
            slideSpeed: 500,
            margin: 4,
            responsiveRefreshRate: 100,
        })
        .on("changed.owl.carousel", syncPosition2);

    function syncPosition(el) {
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - el.item.count / 2 - 0.5);
        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }
        thumbs
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = thumbs.find(".owl-item.active").length - 1;
        var start = thumbs
            .find(".owl-item.active")
            .first()
            .index();
        var end = thumbs
            .find(".owl-item.active")
            .last()
            .index();
        if (current > end) {
            thumbs.data("owl.carousel").to(current, 100, true);
        }
        if (current < start) {
            thumbs.data("owl.carousel").to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            bigImg.data("owl.carousel").to(number, 100, true);
        }
    }
    thumbs.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        bigImg.data("owl.carousel").to(number, 300, true);
    });

    $('.selectpicker').selectpicker();

    var itemHeight = $("#big-img .owl-stage-outer").height();
    if ($(window).width() > 575) {
        $(".proDetail").css("height", itemHeight + 2.7 + "px");
    } else {
        $(".proDetail").css("height", "240px");
    }
});