

  function LayDSHuyen(maTinh, idHuyen) {
    idHuyen =idHuyen;
    $.ajax({
        type: "get",
        url: '/admin/location/getallHuyen',
        data: {'Macha':maTinh},
        success: function (data) {
            $(idHuyen).empty();
            $(idHuyen).append("<option value='0'></option>");
            if (data != null) {
                $.each(data.data, function (index, optionData) {
                    $(idHuyen).append("<option value='" + optionData.id + "'>" + optionData.Ten + "</option>");
                });
            }
            $(idHuyen).trigger("chosen:updated");
        },
        complete: function () {
        },
        error: function (jqXHR, exception) {

        }
    });
}



function laydanhsachtinh(controlId, value = null, districtValue = null, districtControlId  ) {

$.ajax({
 type: "GET",
   url: '/admin/location/getAllTinh',
 success: function (data) {
     $(controlId).empty();
     $(controlId).append("<option value='0'></option>");
     if (data !== null) {
           $.each(data.data, function (index, optionData) {
                  $(controlId).append("<option value='" + optionData.id + "'>" + optionData.Ten + "</option>");
             });
     }




 },
 complete: function () {
      if (value !== null)
     $(controlId).val(value);
     $(controlId).trigger("chosen:updated");

     getDistricts(districtControlId, value,districtValue );

 },
 error: function (jqXHR, exception) {

 }
});
}


function getDistricts(controlId,provinceId, value = null) {


$.ajax({
 type: "GET",
 url: '/admin/location/getallHuyen?Macha=' + provinceId,
 success: function (data) {
     $(controlId).empty();
     $(controlId).append("<option value='0'></option>");
     if (data !== null) {
        $.each(data.data, function (index, optionData) {
                 $(controlId).append("<option value='" + optionData.id + "'>" + optionData.Ten + "</option>");
             });
     }


 },
 complete: function () {

      if (value !== null)
      {

         $(controlId).val(value);
         $(controlId).trigger("chosen:updated");

      }



 },
 error: function (jqXHR, exception) {
     showError(jqXHR, exception);
 }
});
}

function bindDatinh(datatinh, idtinh ="#ddlTinh", idHuyen ="#ddlHuyen", valuetinh =null, valuehuyen =null)
{
   

    if(valuetinh==null)
    {
        laydanhsachtinh(idtinh, datatinh.tinh, datatinh.huyen,idHuyen);

    }
    else
    {
        laydanhsachtinh(idtinh, valuetinh,valuehuyen,idHuyen);

    }
    $(idtinh).on('change', function () {
     LayDSHuyen(this.value,idHuyen);
});
}
