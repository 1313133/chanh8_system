const collEl = document.getElementsByClassName("collapsible");
const stepsEl = document.querySelectorAll(".btn-step");
const forms = document.querySelectorAll(".box-content");
const nextBtn = document.querySelector('.nextBtn');
const confirmBtn = document.querySelector('.confirmBtn');
const mobileNumber = document.getElementById("mobile-number");
const ladingCode = document.getElementById('lading-code');
const customerName = document.getElementById('customer-name');

// const customerName = document.getElementById('customer-name');
// const customerName = document.getElementById('customer-name');
// const customerName = document.getElementById('customer-name');

const orderValue = document.getElementById('order-value');
const moneyTextbox = document.getElementsByClassName("money-textbox");
const codValueEl = document.getElementById("cod-value");
const confirmCodValueEl = document.getElementById("confirm-cod-value");
const totalCostEl = document.getElementById("total-cost");
const shippingCostEl = document.getElementById("shipping-cost");
const intercityCostEl = document.getElementById("intercity-cost");
const confirmTotalCostEl = document.getElementById("confirm-total-cost");
const confirmShippingCostEl = document.getElementById("confirm-shipping-cost");
const confirmIntercityCostEl = document.getElementById("confirm-intercity-cost");
const shipperNameEl = document.getElementById("shipper-name");
const stockAddressEl = document.getElementById("stock-address");
const confirmShipperNameEl = document.getElementById("confirm-shipper-name");

const ddList = document.getElementById("dd-list");
const addressDetails = document.getElementById("address-details");
const dateTime = document.getElementById("datetime-input");
const paymentStatusEl = document.getElementById("payment-status");

addressProvince = document.getElementById("province");
addressHuyen = document.getElementById("huyen");
addressReward = document.getElementById("reward");


// Show default info

$("#confirm-shipper-name").text($("#shipper-name option:selected").text());

$("#confirm-recipient-name").text($("#customer-name").text());
$("#confirm-recipient-phone").text($("#mobile-number").text());
$("#confirm-recipient-address").text($("#address-details").text());
$("#confirm-trans-method").text($("input[name='MethodId']:checked").parent('div').text().trim());

$("input[name='MethodId']").on('change', function () {
    $("#confirm-trans-method").text($(this).parent('div').text().trim());
    if ($(this).val() === 'S') {
        $("#confirm-trans-method").addClass('highlight-text');
    } else { // 'C'
        $("#confirm-trans-method").removeClass('highlight-text');
    }
})

// format time: yyyy-mm-ddThh:mm:ss
getDate();

function getDate() {
    const d = new Date();
    let year, month, date, hour, min, sec;

    year = d.getFullYear();
    month = d.getMonth() + 1;
    date = d.getDate();
    hour = d.getHours();
    min = d.getMinutes();
    sec = d.getSeconds();

    dateTime.value = year + "-" +
        (month < 10 ? "0" + month : month) + "-" +
        (date < 10 ? "0" + date : date) + "T" +
        (hour < 10 ? "0" + hour : hour) + ":" +
        (min < 10 ? "0" + min : min) + ":" +
        (sec < 10 ? "0" + sec : sec);
}

// Validate methods
const isValidPhoneNumber = (number) => number.match(/(\+?(\d+)?\d)\d{9}$/);
const isEmpty = (str) => str.trim() === "";
const isEmptyEl = (el, msgEl) => {

    if (isEmpty(el.value)) {
        msgEl.innerHTML = "Không được để trống thông tin này.";
        msgEl.classList.remove('disabled');
        return true;
    }

    msgEl.classList.add('disabled');
    return false;
}

const isEmptySelectedEl = (el, msgEl) => {

    if (isEmpty(el.value)
        || el.value.trim() == "Chọn tỉnh thành"
        || el.value.trim() == "Chọn quận/huyện"
        || el.value.trim() == "Chọn xã/phường"
    ) {
        msgEl.innerHTML = "Không được để trống thông tin này.";
        msgEl.classList.remove('disabled');
        return true;
    }
    msgEl.classList.add('disabled');
    return false;
}

const isValidPhoneNumberEl = (el, msgEl) => {
    if (!isValidPhoneNumber(el.value)) {
        msgEl.classList.remove('disabled');
        msgEl.innerHTML = "Không đúng định dạng.";
        return false;
    }

    msgEl.classList.add('disabled');
    return true;
}

function validateForm() {
    const mobileMsgEl = document.getElementById("mobile-danger");
    const laddingMsgEl = document.getElementById("ladding-danger");
    const customerNameMsgEl = document.getElementById("customer-name-danger");
    const orderValueMsgEl = document.getElementById("order-value-danger");

    const diaChiMsgEl = document.getElementById("diachi-danger");
    const diaChiCuTheMsgEl = document.getElementById("diachicuthe-danger");


    if (addressProvince == null) {
        addressProvince = document.getElementById("provinces");
    }

    if (addressHuyen == null) {
        addressHuyen = document.getElementById("districts");
    }

    if (addressReward == null) {
        addressReward = document.getElementById("wards");
    }


    if (isEmptyEl(mobileNumber, mobileMsgEl) ||
        !isValidPhoneNumberEl(mobileNumber, mobileMsgEl) ||
        isEmptyEl(ladingCode, laddingMsgEl) ||
        isEmptyEl(customerName, customerNameMsgEl) ||
        isEmptyEl(addressDetails, diaChiCuTheMsgEl) ||

        isEmptySelectedEl(addressProvince, diaChiMsgEl) ||
        isEmptySelectedEl(addressHuyen, diaChiMsgEl) ||
        isEmptySelectedEl(addressReward, diaChiMsgEl) ||
        isEmptyEl(orderValue, orderValueMsgEl) ||
        isProductPriceNull()
    ) {
        return false;
    }

    return true;
}

function removeActive(stepID) {
    for (let i = 0; i < stepsEl.length; i++) {
        if (stepID !== i) {
            stepsEl[i].classList.remove("btn-info");
            forms[i].classList.add("disabled");
        }
    }
}

function formatMoney(number) {
    let numberString = number.replace(/^0+/, '');
    numberString = numberString.split(".").join("").replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
    return numberString;
}

function filterFunction() {
    var li, i;
    li = ddList.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        let txtValue = li[i].textContent || li[i].innerText;
        if (txtValue.toUpperCase().indexOf(mobileNumber.value) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

var index = 0;
const autoFill = (obj) => {

    index++;

    const customerId = document.getElementById("customer-id");
    mobileNumber.value = obj.mobilePhone;
    customerName.value = obj.fullName;
    customerId.value = obj.id;
    if (index == 1) {
        return;

    }
    addressDetails.value = obj.addresssInfo;
};
const handleSelectNumber = (obj) => {
    mobileNumber.value = obj.mobilePhone;
    $("#confirm-recipient-name").text(obj.fullName);
    $("#confirm-recipient-phone").text(obj.mobilePhone);
    $("#confirm-recipient-address").text(obj.addresssInfo);
};

for (let i = 0; i < stepsEl.length; i++) {

    stepsEl[i].addEventListener('click', function () {

        if (!validateForm())
            return;
        if (!this.classList.contains("btn-info")) {
            this.classList.add("btn-info");
            forms[i].classList.remove("disabled");
            removeActive(i);

        }
        if (i !== stepsEl.length - 1) {
            nextBtn.classList.remove('disabled');
            if (confirmBtn) {
                confirmBtn.classList.add('disabled');
            }


        } else {
            nextBtn.classList.add('disabled');
            if (confirmBtn) {
                confirmBtn.classList.remove('disabled');
            }

        }
    })
}

function removeDots(moneyStr) {
    return moneyStr.split(".").join("");
}

function calculateMoney() {
    var priceOrder = document.getElementById("order-value");
    var totalCode = document.getElementById("total-cost");

    var chargeOrder = document.getElementById("shipping-cost");
    let shippingCost = isEmpty(priceOrder.value) ? 0 : parseInt(removeDots(priceOrder.value));
    let intercityCost = isEmpty(chargeOrder.value) ? 0 : parseInt(removeDots(chargeOrder.value));

    var isSenderPaymentRd = $("input[name='isSenderPayment']:checked").val();

    var isPaymentOrder = $("input[name='isPaymentOrder']:checked").val();

    let totalCost = shippingCost + intercityCost;

    let totalCostText = 0;

    if (isSenderPaymentRd == "0") {
        totalCost = intercityCost;
        $("#confirm-shipping-cost").text("0");
        totalCostText += 0;
    } else {
        $("#confirm-shipping-cost").text(intercityCost);
        totalCostText += intercityCost;
    }


    if (isPaymentOrder == "0") {
        // totalCost = shippingCost;
        // $("#confirm-shipping-cost").text("0");
        totalCostText += 0;
    } else {

        totalCostText += shippingCost;
    }


    totalCode.value = !totalCostText ? 0 : formatMoney(`${totalCostText}`);

    var codText = document.getElementById("confirm-cod-value");
    var textDisplay = !shippingCost ? 0 : formatMoney(`${shippingCost}`);
    $("#confirm-cod-value").text(textDisplay);

    var intercityCostDisplay = !intercityCost ? 0 : formatMoney(`${intercityCost}`);
    $("#confirm-shipping-cost").text(intercityCostDisplay);


    // const options = document.getElementsByName("isSenderPayment");

    // const selectedEl = Array.from(options).find(item => item.checked);


    var totalDisplay = !totalCostText ? 0 : formatMoney(`${totalCostText}`);
    $("#confirm-cod-value-textr").text(totalDisplay);

    if (isSenderPaymentRd == "1") {
        $("#confirm-cost-payer").text("Có");
    } else {
        $("#confirm-cost-payer").text("Không");
    }
    // const confirmPayer = document.getElementById("confirm-cost-payer");

}


function handleSelectedPerson(obj) {
    let selectedItem = Object.values(obj).find((item) => item.id == $("#shipper-name").val());
    // Update confirm information
    $("#shipper-phone").val(selectedItem.mobilePhone);
    $("#confirm-shipper-name").text(selectedItem.fullName);
}

function handleSelectedPayer() {
    isProductPriceNull();

    // const options = document.getElementsByName("isSenderPayment");
    //   const confirmPayer = document.getElementById("confirm-cost-payer");
    //   const selectedEl = Array.from(options).find(item => item.checked);

    //  confirmPayer.innerHTML = selectedEl.nextSibling.data.trim();

    calculateMoney();
}

function isProductPriceNull() {

    let isPaymentOrder = $("input[name='isPaymentOrder']:checked").val();

    if(isPaymentOrder == 1) {
        let productValue = $('#order-value').val();
        if(productValue == 0 || productValue.trim() == "") {

            $('#order-value-danger').removeClass("disabled");
            $('#order-value-danger').html("Vui lòng nhập giá hàng hoá");
            return true;
        } else {
            if(!$('#order-value-danger').hasClass("disabled")) {
                $('#order-value-danger').addClass("disabled");
                $('#order-value-danger').html("");
            }
        }
    }

    return false;
}

Array.from(moneyTextbox).forEach((item) => {
    item.addEventListener('keyup', (e) => {

        e.preventDefault();
        // e.target.value = formatMoney(e.target.value);
        // codValueEl.innerHTML = orderValue.value;
        // confirmCodValueEl.innerHTML = codValueEl.innerHTML;
        // confirmShippingCostEl.innerHTML = isEmpty(shippingCostEl.value) ? 0 : shippingCostEl.value;
        // confirmIntercityCostEl.innerHTML = isEmpty(intercityCostEl.value) ? 0 : intercityCostEl.value;
        calculateMoney();
        // confirmTotalCostEl.innerHTML = totalCostEl.value;
    });
});


shippingCostEl.addEventListener('change', calculateMoney);
totalCostEl.addEventListener('focusout', calculateMoney);

Array.from(moneyTextbox).forEach((item) => {
    item.addEventListener('focusin', (e) => {
        e.target.value = removeDots(e.target.value);
    });
});

Array.from(moneyTextbox).forEach((item) => {
    item.addEventListener('focusout', (e) => {
        e.target.value = isEmpty(e.target.value) || e.target.value === "0" ? 0 : formatMoney(e.target.value);
    });
});

mobileNumber.addEventListener('focusout', () => {
    validateForm();

    ddList.classList.add("disabled");
});
mobileNumber.addEventListener('keyup', filterFunction);
mobileNumber.addEventListener('focusin', () => {
    filterFunction();
    ddList.classList.remove("disabled");
});

ladingCode.addEventListener('focusout', validateForm);
customerName.addEventListener('focusout',
    validateForm);
addressDetails.addEventListener('focusout',
    validateForm);
orderValue.addEventListener('focusout', validateForm);
paymentStatusEl.addEventListener('change', () => {
    if (paymentStatusEl.checked) {
        // orderValue.setAttribute("readonly", "");
        // orderValue.value = 0;
    } else {
        // orderValue.removeAttribute("readonly");
    }
})
nextBtn.addEventListener('click', () => {
    const currentStep = Array.from(stepsEl).find(x => x.classList.contains("btn-info"));
    const currentId = currentStep.getAttribute('id');

    if (!validateForm()) return;

    if (currentId == 'step-1') {
        addOrUpdateCustomer();

        description = document.getElementById("order-description").value;
        document.getElementById("notedDisplay").innerHTML = description;
    }

    if (currentId == 'step-3') {
        var confirmBtn = document.getElementsByClassName("confirmBtn")[0];
        if (confirmBtn) {
            confirmBtn.classList.remove('disabled');
        }
    }

    if (currentId !== 'step-4') {
        const nextStep = currentStep.parentElement.nextElementSibling.getElementsByTagName(
            'div')
            .item(0);


        const nextId = nextStep.getAttribute('id');
        if (nextId === "step-4") {

            nextBtn.classList.add('disabled');
            var step4 = document.getElementById("step-4-form");
            var step3 = document.getElementById("step-3-form");
            var confirmBtn = document.getElementById("confirmBtn");
            step3.classList.add('disabled');
            step4.classList.remove('disabled');
            // confirmBtn.classList.remove('disabled');
            var confirmBtn = document.getElementsByClassName("confirmBtn")[0];
            confirmBtn.classList.remove('disabled');
            document.getElementById("btnSave").classList.remove("disabled");


        }
        const currentForm = document.getElementById(currentId + "-form");
        const nextForm = document.getElementById(nextId + "-form");
        currentStep.classList.remove("btn-info");
        nextStep.classList.add("btn-info");
        currentForm.classList.add("disabled");
        nextForm.classList.remove("disabled");
    }
})

for (let el of collEl) {
    el.addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
            content.style.display = "none";
        } else {
            content.style.display = "block";
        }
    });
}

function addOrUpdateCustomer() {

    var txtName = $("#customer-name").val();
    var txtMobile = $("#mobile-number").val();
    var addresssInfo = $("#address-details").val();

    if (txtName == "" || txtMobile == "") {
        return;
    }

    var district = $("#districts").val();
    var ward = $("#wards").val();
    var province = $("#provinces").val();
    var dataTransfer = {

        "mobilePhone": txtMobile,
        "fullName": txtName,
        "addresssInfo": addresssInfo,
        "district": district,
        "ward": ward,
        "province": province
    };
    $.ajax({
        type: 'POST',
        url: '/createOrUpdateCustomer',
        data: dataTransfer,
        success: function (data) {

            if (data == "")
                return;
            //   $("#CustomerIdData").val(data.data.id);
            $("#customer-id").val(data.data.id);
            $("#confirm-recipient-name").text(data.data.fullName);
            $("#confirm-recipient-phone").text(data.data.mobilePhone);
            $("#confirm-recipient-address").text(data.data.addresssInfo);
            setTimeout(() => {

                var provinceText = $("#provinces option:selected").text();

                var huyentext = $("#districts option:selected").text();
                var phuongtext = $("#wards option:selected").text();
                if (provinceText == "" || provinceText == null) {
                    provinceText = $("#province option:selected").text();

                    huyentext = $("#huyen option:selected").text();
                    phuongtext = $("#reward option:selected").text();
                }
                var informationAddress = data.data.addresssInfo;
                if (informationAddress == null) {
                    informationAddress = "";
                } else {
                    informationAddress += ",";
                }


                $("#confirm-recipient-address").text(informationAddress + "" + phuongtext + "," + huyentext + "," + provinceText);
            }, 200);

        }
    });
}

function updateStatusOrder(noted, statusId) {

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success btn-confirm',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    });

    var dataTransfer = {
        "status": statusId,
        "editId": window.location.pathname.split("/").pop(),
        "noted": noted

    };

    $.ajax({
        type: 'POST',
        url: '/admin/api/quan-ly-don-hang/updateStatus',
        data: dataTransfer,
        success: function (data) {
            swalWithBootstrapButtons.fire(
                'Trạng thái!',
                'Đơn hàng đã được cập nhật',
                'success'
            );

            if (statusId == "6") {
                $(".confirm-order-btn").hide();
            }
        }
    });
}

$('.confirm-order-btn').on('click', () => {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success btn-confirm',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Bạn có chắc chắn muốn duyệt đơn này?',
        text: "Nhập ghi chú hoặc lý do",
        input: 'text',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Đồng ý',
        cancelButtonText: 'Quay lại',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {

            // Put the code for update status of order here!!
            updateStatusOrder(result.value, "6");

        } else {
            // swalWithBootstrapButtons.fire(
            //     'Đã hủy duyệt đơn!',
            //     'Đơn hàng đang chờ xử lý',
            //     'error'
            // )
        }
    })
})

$('.cancel-order-btn').on('click', () => {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success btn-confirm',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Bạn có chắc chắn muốn hủy đơn này?',
        text: "Nhập ghi chú hoặc lý do",
        input: 'text',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Huỷ đơn hàng',
        cancelButtonText: 'Quay lại',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            updateStatusOrder(result.value, "7");

        }

    })
})


function handleSelectedAddress() {

    let valuechange = $("#stock-address").val();
    if (valuechange > 0) {
        setTextLocation(valuechange);
    }

}

function setTextLocation(valueCheck) {


    var tempValue = valueCheck;
    if (!tempValue) {
        return;

    }

    var feeInfo = null;
    allStoreLocation.forEach((item) => {
        if (feeInfo != null) {
            return;
        }
        if (item.id == tempValue) {
            feeInfo = item;
        }
    });


    if (feeInfo != null) {
        var valueText = feeInfo.addresssInfo + ','
            + feeInfo.wardText + ','
            + feeInfo.districtText + ','
            + feeInfo.provinceText + ',';

        $("#confirm-stock-address").text(valueText);
    } else {
        $("#confirm-stock-address").text("");
    }

}

document.addEventListener("DOMContentLoaded", function (event) {
    var step4 = document.getElementById("step-4-form");
    step4.classList.add('disabled');

    var chargeFeeSelect = document.querySelector('input[name="MethodId"]:checked').value
    changePrice(chargeFeeSelect);
    setTextLocation($("#stock-address").val());

});


function createOrder() {
    // disable button
    $('.saveOrder').prop('disabled', true);

    $.ajax({
        type: 'POST',
        url: '/admin/tao-don-hang-moi',
        data: $("#order-form").serialize(),
        success: function (data) {
            if (data.success == true) {


                // toastr.success("Tạo mới thành công");
                toastr.success(
                    'Tạo đơn hàng thành công!',
                    '',
                    {
                        timeOut: 500,
                        fadeOut: 500,
                        onHidden: function () {
                            window.location.href = "/admin/quan-ly-don-hang";
                        }
                    }
                );


            }

            // enable button
            //$('.saveOrder').prop('disabled', false);
            return;
        }
    });
}


function changePrice(valueCheck) {

    var tempValue = valueCheck;
    if (!tempValue) {
        return;

    }


    var feeInfo = null;

    allChargeShipperGlobal.forEach((item) => {
        if (feeInfo != null) {
            return;
        }
        if (item.code == tempValue) {
            feeInfo = item;

        }
    });


    if (feeInfo != null) {

        //console.log(feeInfo);
        $("#shipping-cost").val(feeInfo.chargeFee);
        $("#feeShippingTextDisplay").text('"' + feeInfo.name + '"');
        // $("#confirm-shipping-cost").text(feeInfo.chargeFee);
    } else {
        $("#shipping-cost").val("");
        $("#feeShippingTextDisplay").text("Cước giao hàng");
    }
    calculateMoney();
}


$('input[type=radio][name=MethodId]').change(function () {
    var temp = this.value;
    // alert("3");
    changePrice(temp);


});
