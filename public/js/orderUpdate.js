$(document).ready(function() {
    $("#mobile-number").val(customerData.mobilePhone).change();
    $(".track-order-btn").on('click', () => {
        window.location.href = "/admin/thong-tin-don-hang/tracking/" + orderInformation.id;
    })
    autoFill(customerData);
    bindData();
    changePrice(orderInformation.MethodId);
    calculateMoney();
});


function bindData() {
    console.log(orderInformation);
    $("#lading-code").val(orderInformation.Code);
    var checkProduct = "#trans-method-" + orderInformation.MethodId;
    $(checkProduct).prop('checked', true);
    $("#total-cost").val(orderInformation.TotalValue);
    $("#order-value").val(orderInformation.COD);
    $("#TypeOrderId").val(orderInformation.TypeOrderId).change();

    $("#shipper-name").val(orderInformation.ShipperId).change();
    $("#confirm-recipient-name").text(customerData.fullName);
    $("#confirm-recipient-phone").text(customerData.mobilePhone);
    $("#confirm-recipient-address").text(customerData.addresssInfo);
    $("#confirm-total-cost").text(orderInformation.TotalValue);
    $("#confirm-cod-value").text(orderInformation.COD);
    $("#confirm-shipping-cost").text(orderInformation.ChargeOrder);
    $("#confirm-intercity-cost").text(orderInformation.IntercityCost);
    $("#shipping-cost").val(orderInformation.ChargeOrder);
    $("#intercity-cost").val(orderInformation.IntercityCost);
    $("#order-size").val(orderInformation.orderSize);
    $("#order-description").text(orderInformation.description);
    $("#notedDisplay").text(orderInformation.description);

    $("#payment-status").prop('checked', orderInformation.isPayment == "1");
    $("#confirm-trans-method").text($("input[name='MethodId']:checked").parent('div').text().trim());
    if (orderInformation.MethodId === 'S') {
        $("#confirm-trans-method").addClass('highlight-text');
    } else { // 'C'
        $("#confirm-trans-method").removeClass('highlight-text');
    }

    if (orderInformation.isSenderPayment == 0) {
        $("#confirm-cost-payer").text("Người gửi");
        $("#sender").prop('checked', true);
    } else {
        $("#confirm-cost-payer").text("Người nhận");
        $("#recipient").prop('checked', true);
    }


    if (orderInformation.isSenderPayment == 0) {
        $("#confirm-cost-payer").text("Người gửi");
        $("#sender").prop('checked', true);
    } else {
        $("#confirm-cost-payer").text("Người nhận");
        $("#recipient").prop('checked', true);
    }


    if (orderInformation.isPaymentOrder == 0) {
       
        $("#paymentYes").prop('checked', true);
    } else {
       
        $("#paymentNo").prop('checked', true);
    }

    
}


function updateOrder() {
    
    $.ajax({
        type: 'POST',
        url: '/admin/thong-tin-don-hang/update',
        data: $("#order-form").serialize(),
        success: function(data) {
             if(data.success==true)
             {  
               
                // toastr.success("Tạo mới thành công");
                toastr.success(
                    'Cập nhật đơn hàng thành công!',
                    '',
                    {
                      timeOut: 500,
                      fadeOut: 500,
                      onHidden: function () {
                          window.location.reload();
                          
                        }
                    }
                  );


             }
             return;
         }
    });
}

function changePrice(valueCheck) {
    
    var tempValue  = valueCheck;
    if(!tempValue)
    {
        return;
        
    }
    
    var feeInfo = null;
    allChargeShipperGlobal.forEach((item) => {
        if(feeInfo !=null)
        {
            return;
        }
        if(item.code ==  tempValue )
        {
            feeInfo = item;
           
        }
    });

  
    if(feeInfo != null)
    {
         $("#shipping-cost").val(feeInfo.chargeFee);
         $("#confirm-shipping-cost").text(feeInfo.chargeFee);
        $("#feeShippingTextDisplay").text('"'+feeInfo.name + '"');
    }
    else 
    {
        $("#shipping-cost").val("");
        $("#feeShippingTextDisplay").text("");
    }
}

$('input[type=radio][name=MethodId]').change(function() {
    var temp = this.value;
    changePrice(temp);
    
    return;
    if (this.value == 'allot') {
        alert("Allot Thai Gayo Bhai");
    }
    else if (this.value == 'transfer') {
        alert("Transfer Thai Gayo");
    }
});