let priceItems = document.querySelectorAll('.vnd');
let weights = document.querySelectorAll('.weight');

let navBtn = document.querySelector(".topnav__btn");
let btnList = document.querySelector(".pageTitle");
let listPro = document.querySelector(".listPro");
let mainHome = document.querySelector(".main__home");
let topHeader = document.querySelectorAll(".header");
let wrapper = document.querySelector(".wrapper");
let oldPrice = document.querySelector(".cardPrm__oldPrice");

let boxEffCode = ``


if (mainHome) {
    wrapper.insertAdjacentHTML('afterbegin', boxEffCode);
}

window.onscroll = () => {
    for (let i = 0; i < topHeader.length; i++) {
        if (document.body.scrollTop > 10 || document.documentElement.scrollTop > 10) {
            topHeader[i].style.boxShadow = "0rem 0.3rem 0.3rem #eeeeee";
            topHeader[i].style.marginTop = "0";
        } else if (window.innerWidth >= 992) {
            topHeader[i].style.marginTop = "3rem";
            topHeader[i].style.boxShadow = "none";
        } else {
            topHeader[i].style.boxShadow = "none";
        }
    }
}


if (btnList) {
    btnList.addEventListener("click", () => {
        if (listPro.style.display === "none") {
            listPro.style.display = "block";
            btnList.style.color = "#0781e6";
        } else {
            listPro.style.display = "none";
            btnList.style.color = "#043db0";
        }
    });
}

let descTitles = document.querySelectorAll(".boxDesc__title");
let descItems = document.querySelectorAll(".boxDesc__item");
if (descTitles.length > 0) {
    descTitles[0].classList.add("link-active");
    for (let i = 0; i < descTitles.length; i++) {
        descTitles[i].addEventListener("click", () => {
            for (let j = 0; j < descTitles.length; j++) {
                descTitles[j].classList.remove("link-active");
                descItems[j].style.display = "none";
            }
            descTitles[i].classList.add("link-active");
            descItems[i].style.display = "block";
        });
    }
}

let prices = document.querySelectorAll(".cart-t__price");
let qtts = document.querySelectorAll(".cart-t__qtt");
let subTotals = document.querySelectorAll(".cart-t__sub-total");
let total = document.querySelector(".cart-t__total");

// $(document).ready(function () {
$('.btnDown').on('click', function (e) {
    e.preventDefault();

    var numProduct = Number($(this).next().val());
    if (numProduct > 0) $(this).next().val(numProduct - 1);
    caculator();
    return false;
});
$('.btnUp').on('click', function (e) {
    e.preventDefault();

    var numProduct = Number($(this).prev().val());
    $(this).prev().val(numProduct + 1);
    caculator();
    return false;
});
// });

function caculator() {
    price();
    money();
}

function price() {
    for (let i = 0; i < prices.length; i++) {
        if (typeof (price[i]) != "Number") {
            prices[i].textContent = prices[i].textContent.replace(/[^0-9]/g, '');
        }
        subTotals[i].textContent = (parseInt(prices[i].textContent)) * (qtts[i].value);
    }
    let totalNum = 0;
    for (let j = 0; j < subTotals.length; j++) {
        totalNum += parseInt(subTotals[j].textContent);
        total.textContent = totalNum;
    }
}
price();

function money() {
    for (let i = 0; i < priceItems.length; i++) {
        if (priceItems[i].textContent != '') {
            priceItems[i].textContent = priceItems[i].textContent.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
            if (!priceItems[i].textContent.includes(" VNĐ")) {
                priceItems[i].textContent += " VNĐ";
            }
        } else if (oldPrice) {
            if (oldPrice.textContent != '') {
                priceItems[i].textContent += "Giá đang cập nhật ...";
                priceItems[i].style.color = "#527cb3cc";
            }
        }
    }
}
money();

// for(let j = 0; j < weights.length; j++){

//     weights[j].textContent = weights[j].textContent.replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
//     // weights[j].textContent += " gram";

// }

navBtn.addEventListener("click", () => {
    var x = document.getElementById("topnavId");
    if (x.className === "topnav") {
        x.className += " topnavRsp";
    } else {
        x.className = "topnav";
    }
});

function updateBasketNumber(total) {
    $('#quantityPro').html(total);
}