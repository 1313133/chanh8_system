$(document).ready(function() {
    var getColor = function(text) {
        if (text === "Tạo mới") return '#46CB18';
        if (text === "Đang xử lý đơn hàng") return '#eaea00';
        if (text === "Đã nhận đơn hàng") return '#E48900';
        if (text === "Giao hàng thành công") return '#9EDE73';
        if (text === "Đang vận chuyển") return '#E48955';
        if (text === "Hủy đơn hàng") return '#fe3e00';
        if (text === "Giao không thành công") return '#ff3b00';
        return "#BE0000";
    };

    $('#example').DataTable({
        "info": false,
                "searching": false,
                "paging": true,
        // "info": false,
        // "searching": false,
        // "paging": true,
        // 'select': 'multi',
        "columnDefs": [{
            'targets': 0,
            'render': function(data, type, row, meta) {
                if (type === 'display') {
                    data =
                        '<div style="text-align: center;" class=" checkbox"><input type="checkbox" value= "' +
                        row["id"] +
                        '" name ="productCodeList[]"  class="checkBoxCode"><label></label></div>';
                }

                return data;
            },
            'checkboxes': {
                'selectRow': true,
                'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
            }
        },

        {
            "render": function(data, type, row) {
                if (row.role == 3) {
                    return '<a href ="#" >' + data + '</a>';
                } else {
                    return '<a href ="javascript:void(0)" onclick ="editData(' + row["id"] +
                    ')">' + data + '</a>';
                }

            },
            "targets": 1
        },
        {
            "render": function(data) {
                return `<span style="background-color:${getColor(data)};padding: 3px;color: white;">` +
                    data + '</span>';
            },
            "targets": 3
        },
        

        {
            "render": function(data, type, row) {
              
                var addressFullText = row.receiveaddress;

                if(row.wardText)
                {
                    addressFullText += "," + row.wardText;
                }
                if(row.districtText)
                {
                    addressFullText += "," + row.districtText;
                }
                
                if(row.provinceText)
                {
                    addressFullText += "," + row.provinceText;
                }
                return  addressFullText;
            },
            "targets": 4
        }
       
       
    ],
        "processing": true,
        "language": {
            "processing": "<div id='loader'></div>",
            "paginate": {
                     "previous": "<div class='paginate previous'>Trước</div>",
                     "next": "<div class='paginate next'>Sau</div>",
                  }
        },
        "lengthChange": false,
        "deferRender": true,
        "serverSide": true,
        
        "ajax": {
            "url": "/admin/quan-ly-don-hang/filterAll",
            "dataType": "json",
            "type": "get",
            data: function(d) {
                         d.tokenText = $("#serachText").val();
                        d.status = $("#selectStatus").val();
                        d.selectAgen = $("#selectAgen").val();
                        d.tungay = $("#tungay").val();
                        d.denngay = $("#denngay").val();
            }
        },
        "columns": [

            {
                "data": "id"
            },
            {
                "data": "Code"
            },
            {
                "data": "shipperName"
            },
            {
                "data": "profileText"
            },
            {
                "data": null
            },
            {
                "data": "methodName"
            },
            {
                "data": "COD"
            },
            {
                "data": "ChargeOrder"
            },
            {
                "data": "TotalValue"
            },
            {
                "data" :"stockaddress"
            },

            {
                "data": "created_at"
            }
        ]
    });

    
} );