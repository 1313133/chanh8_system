<html>
<head>

</head>

<body>
@if (isset($data))
    <a class="btn btn-dark" href="{{ URL::previous() }}" style="float: left">Back</a>
    <input type='button' class="btn btn-success" id='some-id' value='Print' onclick='printDiv();' style="float: right">
@else
    <input type='button' id='someid' value='Close' onclick='closee();'>
@endif
<div id="DivIdToPrint">
    <link rel="stylesheet" href="/css/printPdf.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <div class="container printArea" style="max-width: 960px">
        <div class="row row-section">
            <div class="col-6 border text-center border-dark logo-area">
                <div class="logo-chanh8">
                    <img src="/logo_header.jpg"/>
                </div>
            </div>
            <div class="col-6 border border-dark">
                @php
                    $barcode = '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($data->code, "C39+") . '" alt="barcode"   />';
                @endphp

                <div class="barcode">
                    {!! $barcode !!}

                </div>
                {{-- <p >Mã vận đơn: </p> --}}
                <div> Mã vận đơn:<span class="bold-text codePrint"> {{$data->Code}} </span></div>
                <div>Mã tham chiếu:<span class="bold-text codePrint"> {{$data->relOrder}} </span></div>
                <div>Ngày in phiếu:<span class="bold-text codePrint"> {{ date('d-m-Y H:i ' ) }} </span></div>
            </div>
        </div>
        <div class="row row-info" style="
                height: 250px;
            ">
            @php
                $fullAdressText =  $data->receiveaddress;
                $provice = $data->provinceText;
                $huyen = $data->districtText;
                $phuong = $data->wardText;
                if($phuong) {
                    $fullAdressText = $fullAdressText.",".$phuong;
                }
                if($huyen) {
                    $fullAdressText = $fullAdressText.",".$huyen;
                }
                if($provice) {
                $fullAdressText = $fullAdressText.",".$provice;
                }
            @endphp
            <div class="col-12 border-bottom border-dark">
                <div class="row">
                    <p class="col-6 titleText border-right border-dark my-0">Từ:</p>
                    <p class="col-6 titleText">Đến:</p>
                </div>
                <div class="row">
                    <p class="col-6 bold-text fullName border-right border-dark my-0"> {{ $data->stockName  }} </p>
                    <p class="col-6 bold-text fullName"> {{ $data->customerName  }} </p>
                </div>
                <div class="row">
                    <p class="col-6 border-right border-dark my-0"> {{ $data->stockAdress  }} </p>
                    <p class="col-6"> {{ $fullAdressText  }} </p>
                </div>
                <div class="row">
                    <p class="col-6 border-right border-dark my-0">SĐT: <strong>{{ $data->stockmobilePhone }}</strong>
                    </p>
                    <p class="col-6">SĐT: <strong>{{ $data->customerPhone }}</strong></p>
                </div>

            </div>
        </div>
        <div class=" row  " style="
                border-left: 1px solid #212529;
                border-right: 1px solid #212529;
                height: 100px;
                
                text-align: center;
             ">
            @php
                $codeDeliversy = $data->MethodId;
                $agencyId = $data->AgencyId;
                $addressStockId = $data->AddressStockId;
                $codePrint = $codeDeliversy.$agencyId.".C".$addressStockId;
            @endphp
            <div class="code" style="
                font-weight: bolder;
                font-size: 60px;
                width: 100%;
            ">{{ $codePrint }} </div>
        </div>

        <div class="row " style="height: 700px">
            <div class="col-9 border border-dark" style="font-size: 18px">
                @php
                    $countProduct = $data->countProduct;

                @endphp
                @if ($countProduct !=  '')
                    <p class="" style="margin-top:10px!important"><strong>*** Số lượng sản phẩm trong đơn hàng: <span
                                    class="count-order">{{str_pad($data->countProduct,2,'0',STR_PAD_LEFT)}}</span></strong>
                @else

                @endif

                <p class="" style="margin-top:10px!important"><strong>*** Nội dung đơn hàng:</strong>
                </p>

                <p>
                    {{$data->description }}

                </p>

                @if ($data->notedOrder != "")
                    <p class="" style="margin-top:10px!important"><strong>*** Ghi chú:</strong>
                    </p>

                    <p>
                        {{ $data->notedOrder }}

                    </p>
                @else

                @endif


                <div class="noted-order" style="
    width: 100%;
    font-weight: bolder;
    position: absolute;
    bottom: 0;
    text-align: justify;
    font-weight: 500;
    margin-bottom: 10px;
">
                           <span style="
    font-style: italic;
">Kiểm tra kỹ bưu kiện và đối chiếu mã vận đơn/mã tham chiếu trước khi nhận hàng. </span>
                </div>
            </div>
            <div class="col-3 border border-dark p-0">
                <div style="height: 35%;position: relative" class="border ">
                    <div class="qr" style="
                            position: absolute;
                            top: 50%;
                            left: 50%;
                            margin-top: -100px;
                            margin-left: -100px;
                        ">{!! QrCode::size(200)->generate($data->Code); !!}</div>
                </div>
                <div class="border method-giaohang " style="
                       position: relative;
                       text-align: center;
                       height: 35%;
                   ">
                    <img src="/now2h.png" style="
                       width: 100%;
                   ">
                    <p class="bold-text" style="
                       position: absolute;
                       bottom: 2vh;
                       width: 100%;
                       margin: auto;
                       font-weight: bolder;
                   "> {{ $data->methodName }}</p>
                </div>
                <div style="height: 30%;display: block;position: relative;" class="border text-center datePrint">
                    <div class="vertical-center" style="
                     width: 100%;
                     margin: 0;
                     position: absolute;
                     top: 50%;
                     -ms-transform: translateY(-50%);
                     transform: translateY(-50%);
                 ">
                        <p class="title" style="margin-top:10px!important;margin-bottom: 10px;">Ngày đặt hàng</p>
                        <span class="dateDisplay" style="
                     display: block !important;
                     font-size: 1.5rem;
                     font-weight: bolder;
                   
                 ">
                 @php
                     $createdAt = Carbon\Carbon::parse($data->created_at);
                 @endphp
                            {{ $createdAt->format('d-m-Y') }} </span>
                        <span class="dateDisplay" style="
                     display: block !important;
                     font-size: 1.5rem;
                     font-weight: bolder;
                 ">{{ $createdAt->format('H:i') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row " style="height: 150px">
            <div class="col border text-center border-dark">
                <div class="text-center " style="margin-top:10px!important">
                    <h4>Tổng thanh toán: </h4>
                    <p class="sum-amount" style="
    font-weight: bolder;
    font-size: 50px;
">
                        {{ $data->TotalValue }}

                        VNĐ </p>
                </div>
            </div>
            <div class="col border border-dark text-center" style="position: relative;">
                <h4 style="margin-top:10px!important"><strong>Chữ kí người nhận</strong></h4>
                <p class="tips-sign" style="
    width: 100%;
    font-style: italic;
"> Xác nhận hàng nguyên vẹn, không móp méo, không bể vỡ</p>
            </div>

        </div>
        {{-- <div class="row ">
            <div class="noted" style="width: 100%;text-align:center;">
                <strong>Chỉ dẫn giao hàng không đồng kiểm, chuyển hoàn sau 3 lần phátm lưu kho tối đa 5 ngày</strong>
            </div>
        </div> --}}
        <div class="design-by" style="
                width: 100%;
                text-align: center;
                font-weight: bolder;
                font-size: 1rem;
                padding:10px;
            ">
            <span>Designed by  <strong>chanhexpress.com</strong> </span>

        </div>
    </div>

</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    function printDiv() {

        var divToPrint = document.getElementById('DivIdToPrint');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');

        newWin.document.close();

        setTimeout(function () {
            newWin.close();
        }, 10);

    }

    function closee() {
        window.close();
    }
</script>
<script>
    $(document).ready(function () {
        $('#someid').trigger('click');
        $('#some-id').trigger('click');

    });
</script>
</body>
</html>



