<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
        integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
        integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
        crossorigin="anonymous" />
</head>

<style>
    body{
        font-family: sans-serif;
    }
</style>
<body >
<div id="DivIdToPrint"> 
    <div class="container" >
        <div class="row " style="height: 150px">
            <div class="col-md-6 col-xl-6 border text-center border-dark">
                <div class="text-center">
                    <img src="https://chanh8.com/assets/img/logo/logo_header.jpg" class="img-fluid rounded" alt="..."
                        style="max-height: 130px">
                </div>
            </div>
            <div class="col-md-6 col-xl-6 border border-dark" >
                <p >mã : {{$code}}</p>
            </div>
        </div>
        <div class="row " style="height: 120px">
            <div class="col-md-6 col-xl-6 border border-dark">
                <p class="m-0" style="margin-top:10px!important">Từ: <strong>Vũ Hoàng Long</strong> </p class="m-0">
                <p class="m-0">SĐT: <strong>0702959014</strong> </p class="m-0">
                <p class="m-0">Địa chỉ: 534/4 Đường ĐIện Biên Phủ Phường 21, Quận Bình Thạnh, TP.Hồ Chí Minh</p>
            </div>
            <div class="col-md-6 col-xl-6 border border-dark">
                <p class="m-0" style="margin-top:10px!important">Đến: <strong>Vũ Hoàng Long</strong> </p class="m-0">
                <p class="m-0">SĐT: <strong>0702959014</strong> </p class="m-0">
                <p class="m-0">Địa chỉ: 534/4 Đường ĐIện Biên Phủ Phường 21, Quận Bình Thạnh, TP.Hồ Chí Minh</p>
            </div>
        </div>
        <div class="row " style="height: 250px ;margin-top: 35px">
            <div class="col-9 border border-dark" style="font-size: 18px">
                <p class="m-0" style="margin-top:10px!important"><strong>Nội dung hoặc ghi chú đơn hàng</strong> </p
                    class="m-0">
                <p class="m-0">Sản phẩm 1: <strong>bàn phím cơ DAREU 352</strong> </p class="m-0">
            </div>
            <div class="col-3 border border-dark p-0">
                <div style="height: 40%" class="border "></div>
                <div style="height: 30%" class="border "></div>
                <div style="height: 30%" class="border text-center">
                    <p class="m-0" style="margin-top:10px!important">Ngày xuất đơn</p>
                    <p>{{ date('d-m-Y H:i') }}</p>
                </div>
            </div>
        </div>
        <div class="row " style="height: 150px">
            <div class="col border text-center border-dark">
                <div class="text-center " style="margin-top:10px!important">
                    <h3>Tiền thu hộ: <br> <br><strong>{{number_format(12020000,2)}} VNĐ</strong></h3>
                </div>
            </div>
            <div class="col border border-dark text-center" style="position: relative;">
                <h3 style="margin-top:10px!important"><b>Chữ kí người nhận</b> </h3>
            </div>
        </div>
    </div>
</div>
    
</body>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script>
            function printDiv() 
{

  var divToPrint=document.getElementById('DivIdToPrint');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

  newWin.document.close();

  setTimeout(function(){newWin.close();},10);

}
          
    </script>
    <script>
//         $(document).ready(function(){
//   $('#some-id').trigger('click');
// });
    </script>

