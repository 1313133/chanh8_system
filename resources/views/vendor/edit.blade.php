
<div class="editArea">
    <div class="form-group col-md-12 left-alignment" >
        <button  onclick="formSubmit()" class="btn btn-primary">Lưu</button>
        <button type="button" onclick ="backToSearch()" class="btn btn-primary">Quay trở lại</button>
        <button  type="submit" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Delete</button>

    </div>
    <form role="form" id ="formEditData"  onsubmit="return false"  >
        {{ csrf_field()}}


        <div class="form-group">
            <label for="inputEmail4">Mã bài viết</label>
        <input type="text" readonly class="form-control" id ="editcode"   name ="editcode" placeholder="Mã, Tiêu đề">
        </div>


        <div class="form-group">
            <label >Danh mục bài viết</label>
            <select class="form-control" id="editrelationCode"  name="editrelationCode">
                <option value="">Tất cả</option>
                @foreach ($allCategoryNews as $itemCategory)
                  <option value="{{$itemCategory->code}}">{{$itemCategory->title}}</option>
                @endforeach

            </select>
         </div>


        <div class="form-group">
            <label for="inputEmail4">Tiêu đề bài viết</label>
            <input type="text" class="form-control" id ="edittitle" name ="edittitle" placeholder="Mã, Tiêu đề">
        </div>
        <div class="form-group">
            <label for="inputEmail4">Ưu tiên hiển thị</label>
            <input type="number"   min="1" max="100" class="form-control" name ="editpriorites" id ="editpriorites" placeholder="Mã, Tiêu đề">
        </div>
        <div class="form-group">
            <label for="inputEmail4">Slug</label>
            <input type="text" class="form-control" name ="editslug" id ="editslug" placeholder="url Bài Viết">
        </div>
        <div class="form-group">
            <label for="inputEmail4">mô tả ngắn </label>
            <input type="text" class="form-control" id ="editshortDescription" name ="editshortDescription" placeholder="Mô tả ngắn">
        </div>
            <div class="form-group">
            <label for="comment">Thông tin SEO:</label>
          </div>
          <div class="form-group">
            <label for="inputEmail4">Từ khóa </label>
            <input type="text" class="form-control" id ="editKeyWord" name ="editKeyWord" placeholder="Mã, Tiêu đề">
        </div>
        <div class="form-group">
            <label for="inputEmail4">mô tả share </label>
            <input type="text" class="form-control" id ="editdescriptionShare" name ="editdescriptionShare" placeholder="Mã, Tiêu đề">

        </div>
        <div class="form-group">
            <label >Trạng thái Hoạt động</label>
            <select class="form-control" id="editisActive"  name="editisActive">
              <option value="1">Hoạt động</option>
              <option value="0">Không hoạt động</option>
            </select>
         </div>


         <div class="form-group">
            <label >Trạng thài bài viết</label>
            <select class="form-control" id="editstatus"  name="editstatus">
              <option value="P">Xuất bản</option>
              <option value="D">Bản soạn thảo</option>
            </select>
         </div>




         <div class="form-group">
            <label class="control-label">Linkshare</label>

            <img src="" id ="linkShareImage" style="width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">

            <button style="display:block;width:120px; height:30px;" onclick="openUploadFile(this)">Chọn file</button>

            <input type="file" id="uploadlinkshare" accept="image/*"  onchange="uploadFile(this)" style="display:none">
            <input type="hidden" class="form-control" id ="linkShare" name ="linkShare"  placeholder="Mã, Tiêu đề" >

        </div>





	  <div class="form-group">
		<label >Nội dung bài viết</label>
		<textarea class="form-control richTextBox"  name="editcontent" id="editcontent">

		</textarea>
	</div>

</div>




    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
                 onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="news">
        {{ csrf_field() }}
    </form>





</div>



<script>



function formSubmit () {

    saveData();
}



function openUploadFile(element)
{
    element.parentElement.getElementsByTagName("input")[0].click();
}
function initView(data)
{

    setValueImageControl("linkShare",data.linkShare);
}

function setValueImageControl(id,value)
{
 var inputtextLink = document.getElementById(id);
 inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/"+value;
// inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
}

function setImageRead(fileinput, fullLink,shortUrlLink) {

    fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
    // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
    fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


}

function uploadFile(element){
     console.log(element);
    // return;

    var file = element.files[0];
    var formData = new FormData();
    formData.append('image', file);
    formData.append('type_slug', 'news');
    formData.append('_token', '{{ csrf_token() }}');


    $.ajax({
        url: '{{ route('uploadImage') }}',  //Server script to process data
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        success: function(data){

            setImageRead(element, data.fullLink,data.shortUrlLink);



        }
    });
}


function saveData()
{
// e.preventDefault(); // avoid to execute the actual submit of the form.
tinyMCE.triggerSave();
var url = "api/managemet-new/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), // serializes the form's elements.
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backToSearch();
             }

       }
     });

}





function deleteSingle()
{
        var urlapi ="/admin/api/managemet-new/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editcode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

</script>
