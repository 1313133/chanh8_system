@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{!! html_entity_decode( str_replace(
    array("Please click the button below to verify your email address", 'If you did not create an account, no further action is required.', "Regards",'If you’re having trouble clicking the "Verify Email Address" button, copy and paste the URL below into your web browser'), 
    array("Vui lòng nhấp vào nút bên dưới để xác minh địa chỉ email của bạn", "Nếu bạn không tạo tài khoản, bạn có thể bỏ qua email này một cách an toàn.", "Trân Trọng" ,"
nếu bạn gặp vấn đề khi sử dụng nút 'Verify Email Address', copy và dán URL này vào thanh địa chỉ của trình duyệt của bạn"), 
    $slot))!!}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{'nếu bạn gặp vấn đề khi sử dụng nút "Verify Email Address", copy và dán URL này vào thanh địa chỉ của trình duyệt của bạn: '}} {!! Str::substr($subcopy,120) !!}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ 'Chanh8 Services' }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent
