
@if ($paginator->hasPages())
    <nav class="pagi">
        <ul >
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="pagi__item disabled" aria-disabled="true" aria-label="@lang('pagination.previous')">
                    <span class="pagi__link" aria-hidden="true">&lsaquo;</span>
                </li>
            @else
                <li class="pagi__item">
                    <a class="pagi__link" href="{{ $paginator->previousPageUrl() }}" rel="prev" aria-label="@lang('pagination.previous')">&lsaquo;</a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="pagi__item disabled" aria-disabled="true"><span class="pagi__link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="pagi__item " aria-current="page"><span class="pagi__link pagi__active">{{ $page }}</span></li>
                        @else
                            <li class="pagi__item"><a class="pagi__link" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="pagi__item">
                    <a class="pagi__link" href="{{ $paginator->nextPageUrl() }}" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                </li>
            @else
                <li class="pagi__item disabled" aria-disabled="true" aria-label="@lang('pagination.next')">
                    <span class="pagi__link" aria-hidden="true">&rsaquo;</span>
                </li>
            @endif
        </ul>
    </nav>
@endif
