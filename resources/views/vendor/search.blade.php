<div class="searchArea">
    <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Từ khóa</label>
            <input type="text" class="form-control" id ="serachText"  name ="serachText" placeholder="Mã, Tiêu đề">
          </div>
          <div class="form-group col-md-6">
            <label for="inputPassword4">Trạng thái</label>
            <select id="selectInactive" name ="status" class="form-control">

                <option value="1">Hoạt động</option>
                <option value="0">Không hoạt động</option>
             </select>
          </div>


          <div class="form-group col-md-6">
            <label for="inputPassword4">Trạng thái bài viết</label>
            <select id="selectStatus" name ="status" class="form-control">
                <option value="All">Tất cả</option>
                <option value="P">Xuất bản</option>
                <option value="D">Bài nháp</option>
                <option value="I">Bài đã xóa</option>
             </select>
          </div>

          <div class="form-group col-md-6">
            <label for="inputPassword4">Danh mục bài viết</label>
            <select id="idselectCategoryCode" name ="selectCategoryCode" class="form-control">
                <option value="All">Tất cả</option>
                @foreach ($allCategoryNews as $itemCategory)
                <option value="{{$itemCategory->code}}">{{$itemCategory->title}}</option>
              @endforeach
             </select>
          </div>


        </div>



        <div class="form-group col-md-12 left-alignment" >
            {{-- <button type="submit" class="btn btn-primary">Search</button> --}}

            <button type  = "button" onclick="refeshSearch();" class = "btn btn-outline-dark">

                Search

            </button>


        </div>
    </form>

    <div class="form-group col-md-12 left-alignment" >
        <button type="button" onclick="editData(-1)" class="btn btn-info">Thêm mới</button>

        <button  type="button" class="btn btn-primary" data-toggle="modal" data-target="#deleteSearchModal">Xóa</button>
    </div>
</div>

<div class="searchData">
    <table id="table_id" class="display" style="width:100%">
        <thead>
            <tr>
                <th> </th>
                <th>Mã tin tức</th>
                <th>Tiêu đề</th>
                <th>Slug</th>
                <th>Hoạt động</th>
                <th>Trạng thái</th>
                <th>Ngày cập nhật</th>
                <th>Ngày tạo</th>
            </tr>
        </thead>
        <tbody>
        </tbody>

    </table>

</div>

<script>

window.onload = function ()
 {
        $('#table_id').DataTable({
        "info":     false,
        "searching":false,
        "paging": true,
        "columnDefs": [
            {
            'targets': 0,
            'render': function(data, type, row, meta){
               if(type === 'display'){
                  data = '<div style="text-align: center;" class=" checkbox"><input type="checkbox" value= "'+  row["id"]+ '" name ="productCodeList[]"  class="checkBoxCode"><label></label></div>';
               }

               return data;
            },
            'checkboxes': {
               'selectRow': true,
               'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
            }
            },

            {
                 "render": function ( data, type, row ) {

                    return '<a href ="javascript:void(0)" onclick ="editData('+ row["id"]+')">' +data + '</a>';
                },
                "targets": 1
            },
            {
                "render": function ( data, type, row ) {
                    return '<a href ="javascript:void(0)" onclick ="editData('+ row["id"]+')">' +data + '</a>';
                },
                "targets": 2
            },
            {
                "render": function ( data, type, row ) {

                    if(data==0)
                    {
                        return "Không hoạt động";
                    }
                    else
                    {
                        return "Hoạt động";
                    }

                },
                "targets": 4
            },
            {
                "render": function ( data, type, row ) {

                    if(data=="D")
                    {
                        return "Bản nháp";
                    }
                    else if(data =="P")
                    {
                        return "Xuất bản";
                    }
                    else if(data =="I")
                    {
                        return "Đã xóa";
                    }
                    else {
                        return "";
                    }

                },
                "targets": 5
            }

        ],
        'select': 'multi',
        "processing": true,
        "lengthChange": false,
        "ajax": {
                 "url": "/admin/api/managemet-new/getAll",
                "dataType": "json",
                "type": "get",
                data: function(d) {

                    d.tokenText = $("#serachText").val(),
                    d.inactive = $("#selectInactive").val(),
                    d.status = $("#selectStatus").val(),
                    d.selectCategoryCode = $("#idselectCategoryCode").val()


                }
             },
                "deferRender": true,
                "serverSide": true,
                "columns": [

                    { "data": "id" },
                    { "data": "code" },
                    { "data": "title" },
                    { "data": "slug" },
                    { "data": "isActive" },
                    { "data": "status" },
                    { "data": "updated_at" },
                    { "data": "created_at" }

        ]

        });

    }




    function editData(id =-1)
    {
        if( id == -1)
        {
            document.getElementById("formEditData").reset();
            updateBreadCrump("Thêm mới");
            activeTab("edit");
            return;
        }
        else
        {

        }
        getdataById(id);
    }

    function getdataById(id)
    {
    var urlapi ="/admin/api/managemet-new/getDetail";
    $.ajax({
        type:'get',
        url:urlapi,
        data:
        {
            id: id,

        },
        success:function(data) {
            updateBreadCrump(data.title);
            BindingBasic(data);

            bindadditional(data);
            activeTab("edit");

        }
    });

    }

    function bindadditional(data)
    {
        var image = "http://localhost:8000/storage/" + data.linkShare;
        $("#linkShareImage").attr("src",image);
        $("#linkShare").val(data.linkShare);

        $("#editKeyWord").val(data.keyWord);
        $("#editisActive").val(data.isActive);
        $("#editstatus").val(data.status);
        $("#editrelationCode").val(data.relationCode);
    }

    function deleteMutipleRecord()

{

     var arr = [];


     var arr = [];
     $('.checkBoxCode:checkbox:checked').each(function () {
        arr.push($(this).val());
    });


        var urlapi ="/admin/api/managemet-new/deleteMutiple";
        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "data": arr

            },
            success:function(data) {

                    if(data.success)
                    {

                        toastr.success("Đã xóa thành công");
                        $('#deleteSearchModal').modal('toggle');
                        backToSearch();
                    }
            }
        });

 }






 </script>
