@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
@endsection
@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/customadmin.js') }}"></script>
@endsection
@section('page_title', 'Quản lý người dùng')
@section('search')
    @include('vendor.voyager.account.search',["allCategoryNews"=>$allCategoryNews,"allLevelAgency"=>$allLevelAgency])
@endsection
@section('edit')

    @include('vendor.voyager.account.edit',["allCategoryNews"=>$allCategoryNews,"allLevelAgency"=>$allLevelAgency])
@endsection
