
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css" integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g==" crossorigin="anonymous" />

        <style>

.checkbox-inline input[type=checkbox], .checkbox input[type=checkbox], .radio-inline input[type=radio], .radio input[type=radio] {
    position: absolute;
    margin-top: 4px\9;
     margin-left: unset !important;
}
        </style>
</head>

<div class="searchArea">
    <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Từ khóa</label>
            <input type="text" class="form-control" id ="serachText"  name ="serachText" placeholder="Từ khóa">
          </div>



          <div class="form-group col-md-6">
            <label for="inputPassword4">Trạng thái</label>
            <select id="selectStatus" name ="status" class="form-control">
                <option value="All">Tất cả</option>
                <option value="1">Hoạt động</option>
                <option value="0">Không hoạt động</option>
             </select>
          </div>



        </div>



        <div class="form-group col-md-12 left-alignment" >
            {{-- <button type="submit" class="btn btn-primary">Search</button> --}}

            <button type  = "button" onclick="refeshSearch();" class = "btn btn-outline-dark">

                Tìm kiếm

            </button>


        </div>
    </form>

    <div class="form-group col-md-12 left-alignment" >
        <button type="button" onclick="openNavPage()" class="btn btn-info">Danh sách đơn hàng</button>
        <button type="button" onclick="openNewPage(-1)" class="btn btn-info">Thêm mới</button>
        <button type="button" onclick="opendeletemodal()" class="btn btn-info">Xóa</button>
     </div>
</div>

<div class="searchData">
    <table id="table_id" class="display" style="width:100%">
        <thead>
            <tr>
            <th> </th>
            <th>Mã đại lý</th>
            <th>Tên đại lý</th>
            <th>Số điện thoại</th>

            <th>Địa chỉ</th>
            <th>Email</th>
            <th>Cấp đại lý</th>
            <th>Loại đại lý</th>
            <th>Ngày tạo</th>
            <th>Trạng thái</th>

            </tr>
        </thead>


        <tbody>
        </tbody>

    </table>

</div>

<script>



window.onload = function ()
 {
        $('#table_id').DataTable({
        "info":     false,
        "searching":false,
        "paging": true,
        "columnDefs": [
            {
            'targets': 0,
            'render': function(data, type, row, meta){
               if(type === 'display'){
                  data = '<div style="text-align: center;" class=" checkbox"><input type="checkbox" value= "'+  row["id"]+ '" name ="productCodeList[]"  class="checkBoxCode"><label></label></div>';
               }

               return data;
            },
            'checkboxes': {
               'selectRow': true,
               'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
            }
            },

            {
                 "render": function ( data, type, row ) {

                    return '<a href ="javascript:void(0)" onclick ="editData('+ row["id"]+')">' +data + '</a>';
                },
                "targets": 2
            },
            {
                "render": function ( data, type, row ) {
                    return '<a href ="javascript:void(0)" onclick ="editData('+ row["id"]+')">' +data + '</a>';
                },
                "targets": 3
            }

        ],
        'select': 'multi',
        "processing": true,
        "lengthChange": false,
        "ajax": {
                 "url": "/admin/api/quan-ly-shop/getAll",
                "dataType": "json",
                "type": "get",
                data: function(d) {

                    d.tokenText = $("#serachText").val(),

                    d.status = $("#selectStatus").val()


                }
             },
                "deferRender": true,
                "serverSide": true,
                "columns": [

                    { "data": "id" },
                   { "data": "agencyCode" },
                    { "data": "fullName" },
                    { "data": "mobilePhone" },
                    { "data": "addr" },
                    { "data": "email" },
                    { "data": "levelText" },
                    { "data": "typeText" },
                    { "data": "created_at" },
                       { "data": "status" }
        ]

        });


        var pathway = window.location.pathname.split('/');

        if(pathway.length==4)
        {
                var id= pathway[3];
                editData(id);
        }

    }


    function openNewPage()
    {
     getdataById(-1);
    }

    function openNavPage()
    {
        window.location.href ="/admin/management-order";
    }


    function editData(id =-1)
    {
        if( id == -1)
        {
            document.getElementById("formEditData").reset();
            updateBreadCrump("Thêm mới");
            activeTab("edit");
            return;
        }
        else
        {

        }
        getdataById(id);
    }

    function getdataById(id)
    {
            var urlapi ="/admin/api/quan-ly-shop/getDetail";
            $.ajax({
                type:'get',
                url:urlapi,
                data:
                {
                    id: id,

                },
                success:function(data) {
                    updateBreadCrump(data.title);
                    BindingBasic(data);
                    bindadditional(data);

                    openTab(event, 'London');
                    bindadditional(data);
                    activeTab("edit");

                    getAllCart(data.id);

                }
            });

    }

    function bindadditional(data)
    {
        var image = "http://localhost:8000/storage/" + data.linkShare;
        $("#linkShareImage").attr("src",image);
        $("#linkShare").val(data.linkShare);

        $("#editKeyWord").val(data.keyWord);
        $("#editisActive").val(data.isActive);
        $("#editstatus").val(data.status);
        $("#editrelationCode").val(data.relationCode);

        bindDatinh(data);


    }
    function opendeletemodal()
    {
        $('#deleteSearchModal').modal('toggle');
    }

    function deleteMutipleRecord()

{

     var arr = [];


     var arr = [];
     $('.checkBoxCode:checkbox:checked').each(function () {
        arr.push($(this).val());
    });


        var urlapi ="/admin/api/quan-ly-shop/deleteMutiple";
        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "data": arr

            },
            success:function(data) {
                    if(data.success)
                    {
                        $('#deleteSearchModal').modal('hide');
                        toastr.success("Đã xóa thành công");

                        backToSearch();
                    }else{
                        $('#deleteSearchModal').modal('hide');
                        toastr.error("Bạn chưa chọn đối tượng");
                    }
            }
        });

 }














 </script>
