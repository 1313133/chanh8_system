@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/edit.css') }}">
@endsection
@section('page_title', 'Thông tin địa chỉ')
@section('search')


    @include('vendor.voyager.addressSender.edit')


    <script>
        var data = {{ $dataEdit }}


        $(document).ready(function() {

            if (data.id < 0) {
                updateBreadCrump("thêm mới");
            } else {
                updateBreadCrump(data.title);
            }
            // BindingBasic(data);
            // bindadditional(data);

            activeTab("search", true);
        });
    </script>
@endsection
