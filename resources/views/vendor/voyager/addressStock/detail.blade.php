@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/edit.css') }}">
@endsection
@section('page_title', 'Thông tin địa chỉ')
@section('search')

    @if (  $dataEdit->managerId != null )
      @include('vendor.voyager.addressStock.edit')
    @else
    @include('vendor.voyager.addressStock.editOneRecored')
    @endif
   


    <script>
        var data = {{ $dataEdit }}


        $(document).ready(function() {

            if (data.id < 0) {
                updateBreadCrump("thêm mới");
            } else {
                updateBreadCrump(data.title);
            }
            // BindingBasic(data);
            // bindadditional(data);

            activeTab("edit", true);
        });
    </script>
@endsection
