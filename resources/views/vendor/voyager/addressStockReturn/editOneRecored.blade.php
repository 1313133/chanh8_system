<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
        <button onclick="formSubmit()" class="btn btn-primary">Lưu</button>
        <button type="button" onclick="backTo()" class="btn btn-primary">Quay trở lại</button>

    </div>


    <div class="tab row">
        <div class="tablinks largeTitle">Thông tin kho trả hàng</div>
    </div>

    <div class="py-3"></div>
    <!-- Tab content -->

    <form role="form" id="formEditData" class="row" onsubmit="return false">
        {{ csrf_field() }}

        <p class="titleSection">Thông tin kho trả hàng </p>
        <div class="form-group" style="display:none">
            <label for="inputEmail4">id</label>
            <input type="text" readonly class="form-control" id="id" value="{{ $dataEdit->id }}" name="editcode"
                   placeholder="Mã shipper">
        </div>

        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Tên kho hàng </label>
            <input type="text" class="form-control" id="fullName" value="{{ $dataEdit->title }}"
                   name="editfullName" placeholder="Họ và tên">
        </div>

        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Số điện thoại</label>
            <input type="text" class="form-control" id="editmobilePhone" name="editmobilePhone"
                   value="{{ $dataEdit->mobilePhone }}" placeholder="Số điện thoại">
        </div>
        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Tỉnh/Thành Phố</label>
            <select class="form-control" id="editprovince" name="editprovince" placeholder="Tỉnh/Thành Phố">
                @foreach ($listprovince as $item)
                    @if ($item->id == $dataEdit->province)
                        <option value="{{ $item->id }}" data-id="{{ $item->id }}" selected>
                            {{ $item->name }}</option>
                    @endif
                    <option value="{{ $item->id }}" data-id="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Quận/Huyện</label>
            <select class="form-control" id="editdistrict" name="editdistrict" placeholder="Quận/Huyện">
                @if (isset($dataEdit->district))
                    <option value="{{ $dataEdit->district }}" data-id="{{ $dataEdit->district }}" selected>
                        {!! $dataEdit->dt !!}</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Phường/xã</label>
            <select class="form-control" id="editward" name="editward" placeholder="Phường/xã">
                @if (isset($dataEdit->ward))
                    <option value="{{ $dataEdit->ward }}" data-id="{{ $dataEdit->ward }}" selected>
                        {!! $dataEdit->wd !!}</option>
                @endif
            </select>
        </div>
        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Địa chỉ chi tiết</label>
            <input type="text" class="form-control" id="editaddresssInfo" value="{{ $dataEdit->addresssInfo }}"
                   name="editaddresssInfo" placeholder="Địa chỉ chi tiết">
        </div>
        <div class="form-group">
            <label class="requriedInput">Trạng thái</label>
            <select class="form-control" id="editstatus" name="editstatus">
                @if ($dataEdit->status == '0')
                    <option value="0" selected>Không hoạt động</option>
                    <option value="1">Hoạt động</option>
                @else
                    <option value="0">Không hoạt động</option>
                    <option value="1" selected>Hoạt động</option>
                @endif
            </select>
        </div>


    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
          enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="news">
        {{ csrf_field() }}
    </form>


</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    function formSubmit() {
        if (!validateAllInputHasRequired()) {
            return;
        }
        saveData();
    }


    function openUploadFile(element) {
        element.parentElement.getElementsByTagName("input")[0].click();
    }

    function initView(data) {

        if (data.linkShare == null || data.linkShare == "") {
            return;
        }
        setValueImageControl("linkShare", data.linkShare);
    }


    function setValueImageControl(id, value) {
        var inputtextLink = document.getElementById(id);
        inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/" + value;
        // inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
    }

    $('#editprovince').change(function () {
        var optionSelected = $(this).find('option:selected').attr('data-id');
        console.log(optionSelected); //this will show the value of the atribute of that option.
        $.ajax({
            type: "POST",
            url: '/api/getdistricts',
            data: {
                provinces_id: optionSelected
            },
            success: function (result) {
                $("#editdistrict").find('option').remove();
                $('#editdistrict').html(result);
            }
        });
    });
    $('#editdistrict').change(function () {
        var optionSelected = $(this).find('option:selected').attr('data-id');
        console.log(optionSelected); //this will show the value of the atribute of that option.
        $.ajax({
            type: "POST",
            url: '/api/getwards',
            data: {
                districts_id: optionSelected
            },
            success: function (result) {
                $("#editward").find('option').remove();
                $('#editward').html(result);
            }
        });
    });

    function setImageRead(fileinput, fullLink, shortUrlLink) {

        fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
        // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
        fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


    }

    function uploadFile(element) {
        console.log(element);
        // return;

        var file = element.files[0];
        var formData = new FormData();
        formData.append('image', file);
        formData.append('type_slug', 'news');
        formData.append('_token', '{{ csrf_token() }}');


        $.ajax({
            url: '{{ route('uploadImage') }}', //Server script to process data
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function (data) {

                setImageRead(element, data.fullLink, data.shortUrlLink);


            }
        });
    }


    function saveData() {
        // e.preventDefault(); // avoid to execute the actual submit of the form.
        tinyMCE.triggerSave();
        var url = "/admin/api/dia-chi-tra-hang/createOrUpdateNOBranch";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#formEditData").serialize(), // serializes the form's elements.
            success: function (data) {
                if (data.success) {
                    toastr.success("Cập nhật thành công");
                    backTo();
                }

            }
        });

    }


    function deleteSingle() {
        var urlapi = "/admin/api/dia-chi-tra-hang/delete";

        $.ajax({
            type: 'post',
            url: urlapi,
            data: {
                "_token": "{{ csrf_token() }}",
                "code": $("#editcode").val()

            },
            success: function (data) {

                if (data.success) {
                    location.reload();
                }
            }
        });

    }

    function backTo() {
        window.location.href = "/admin/dia-chi-tra-hang";
    }
</script>
