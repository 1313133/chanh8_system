<div id="adminmenu">



    <ul class="nav navbar-nav">
        <p class="title-module"> Quản lý đơn hàng</p>
        <li class=""><a  href="/admin/tao-don-hang-moi"><span
            class="icon voyager-plus"></span> <span class="title">Tạo đơn hàng
            mới</span></a>
        <!---->
    </li>
    <li class=""><a  href="/admin/quan-ly-don-hang"><span
            class="icon voyager-list"></span> <span class="title">Danh sách đơn
            hàng </span></a>
        <!---->
    </li>
    <li class=""><a  href="/admin/genpdf">
        <span class="icon voyager-file-text"></span> <span class="title">Phiếu
            In</span></a>
        <!---->
    </li>

    <li class=""><a  href="/admin/loai-hang-hoa"><span
        class="icon voyager-list"></span> <span class="title">Nhóm hàng hoá </span></a>
    <!---->
    </li>
    <li class=""><a  href="/admin/thong-tin-goi-cuoc"><span
        class="icon voyager-list"></span> <span class="title">Thông tin gói cước </span></a>
    <!---->
</li>
    </ul>
<ul class="nav navbar-nav">

        <p class="title-module"> Quản Lý </p>
        <li class=""><a  href="/admin/type-agencies"><span
            class="icon voyager-basket"></span> <span class="title">
            Kênh phân phối
        </span></a>
        <!---->
    </li>
 
    <li class=""><a  href="/admin/quan-ly-shop"><span
            class="icon voyager-people"></span> <span class="title">Danh sách khách hàng</span></a>
        <!---->
    </li>

    <li class=""><a  href="/admin/quan-ly-yeu-cau"><span
        class="icon voyager-paper-plane"></span> <span class="title">Danh sách yêu cầu</span></a>
        <!---->
     </li>


    </ul>



    <ul class="nav navbar-nav">
        <p class="title-module"> Quản lý shipper</p>

        <li class=""><a  href="/admin/quan-ly-shippers/them-moi">
            <span class="icon voyager-truck"></span>
            <span class="title">Tạo mới
            </span></a>
        </li>
        <li class=""><a  href="/admin/quan-ly-shippers">
            <span class="icon voyager-people"></span>
            <span class="title">Danh sách
            </span></a>
        </li>
    </ul>

    <ul class="nav navbar-nav">
        <p class="title-module">Thống kê và báo cáo</p>


        <li class="">
            <a  href="/admin/thong-ke-don-hang">
            <span class="icon voyager-pie-chart"></span>
            <span class="title">Thống kê
            </span></a>
        </li>

    </ul>

    <ul class="nav navbar-nav">
        <p class="title-module"> Quản lý nâng cấp</p>

        <li class=""><a  href="/admin/goi-shipper">
            <span class="icon voyager-truck"></span>
            <span class="title">Quản lý gói
            </span></a>
        </li>
        <li class=""><a  href="/admin/danh-sach-shipper-tham-gia">
            <span class="icon voyager-people"></span>
            <span class="title">Danh sách đăng ký
            </span></a>
        </li>
    </ul>

    <ul class="nav navbar-nav">
        <p class="title-module">Thống kê và báo cáo</p>


        <li class="">
            <a  href="/admin/thong-ke-don-hang">
            <span class="icon voyager-pie-chart"></span>
            <span class="title">Thống kê
            </span></a>
        </li>

    </ul>

 
    <ul class="nav navbar-nav">
        <p class="title-module">Thông tin và cấu hình hệ thống</p>
         <li class="">
            <a  href="/admin/quan-ly-tai-khoan">
            <span class="icon voyager-credit-cards"></span>
            <span class="title">Quản lý tài khoản
            </span></a>
        </li>

        <li class="">
            <a  href="/admin/thong-tin">
            <span class="icon voyager-person"></span>
            <span class="title">Thông tin tài khoản
            </span></a>
        </li>
     
        
        <li class="">
            <a  href="javascript:void(0)" onclick="logoutAdmin()">
            <span class="icon voyager-power"></span>
            <span class="title">Đăng xuất
            </span></a>
        </li>


    </ul>



    {{-- <admin-menu :items="{{ menu('admin','_json') }}"></admin-menu> --}}
</div>
