
<style>


/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color:purple;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #62a8ea;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #a59d45;
  border-top: none;
}
</style>

<script>
function openTab(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
        <button  onclick="formSubmit()" class="btn btn-primary">Lưu lại</button>
        <a  href ="/admin/quan-ly-shop" class="btn btn-primary">Quay trở lại</a>

    </div>

    <form role="form" id ="formEditData"  onsubmit="return false"  >
        {{ csrf_field()}}
        <div class="form-group" >

           <input type="hidden" readonly class="form-control"  value="{{ $dataEdit->id }}"    name ="editId" placeholder="Mã khách hàng">
         </div>
        @if ($dataEdit->id >0)
        <div class="form-group" >
            <label for="inputEmail4">Mã khách hàng</label>
           <input type="text" readonly class="form-control" id ="editagencyCode" value="{{ $dataEdit->agencyCode }}"    name ="editagencyCode" placeholder="Mã khách hàng">
           </div>
             
        @else
        <div class="form-group">
            <label class="requriedInput" for="inputEmail4">Mã khách hàng</label>
        <input type="text"  class="form-control" id ="editagencyCode"    name ="editagencyCode" placeholder="Mã khách hàng">
        </div>
        @endif
        
        
      
        <div class="form-group">
            <label class ="requriedInput" for="inputEmail4">Họ và tên</label>
            <input type="text" class="form-control requriedInput"  id ="editfullName" name ="editfullName" placeholder="Tên khách hàng">
        </div>

      

       @if ($dataEdit->id >0)
       <div class="form-group">
        <label for="inputEmail4">Email</label>
        <input type="text" class="form-control readonly " id ="editemail"   name ="editemail" placeholder="Địa chỉ email">
   </div>
             
        @else
            <div class="form-group">
                <label for="inputEmail4" class="requriedInput">Email</label>
                <input type="text" class="form-control" id ="editemail"   name ="editemail" placeholder="Địa chỉ email">
           </div>
        @endif

           @if ($dataEdit->id < 1)
            <div class="form-group">
                <label for="inputEmail4"  class="requridInput" >Nhập mật khẩu</label>
                <input type="password" class="form-control" id ="editPassword"   name ="editPassword" placeholder="Nhập mật khẩu">
            </div>
           @else
            <div class="form-group">
                <label for="inputEmail4" >Nhập mật khẩu</label>
                <input type="password" class="form-control" id ="editPassword"   name ="editPassword" placeholder="Để trống nếu không thay đổi mật khẩu">
                </div>
           @endif
          
        
            <div class="form-group">
                <label for="inputEmail4" class="requriedInput">Điện thoại di động</label>
                <input type="text"  class="form-control requriedInput phoneNumber" id ="editmobilePhone" name ="editmobilePhone" placeholder="Điện thoại di động">
            </div>
        @include('vendor.voyager.location.compomentAddress')

        <div class="form-group">
            <label class="requriedInput" >Loại đại lý </label>
            <select class="form-control" id="edittype"  name="edittype">
             @foreach ($allCategoryNews as $item)
             <option value="{{$item->id}}" >{{$item->text}}</option>
             @endforeach
            </select>
         </div>
         
      
       
       <div class="form-group " >
            <label for="inputEmail4 ">Ghi chú</label>
            <input type="text" class="form-control" id ="editnoted" name ="editnoted" placeholder="Ghi chú">
        </div>
        
        <div class="form-group">
            <label class="requriedInput" >Trạng thái</label>
            <select class="form-control" id="editstatus"  name="editstatus">
                <option value="2" >Không hoạt động</option>
                <option value="1"  >Hoạt động</option>
            </select>
        </div>


    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
                 onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="news">
        {{ csrf_field() }}
    </form>

   





</div>


<script>


function formSubmit () {

    saveData();
}




function openUploadFile(element)
{
    element.parentElement.getElementsByTagName("input")[0].click();
}
function initView(data)
{

    if( data.linkShare ==null || data.linkShare =="")
    {
        return;
    }
    setValueImageControl("linkShare",data.linkShare);
}

 function getAllCart(id)
{
    var urlapi ="/admin/api/quan-ly-shop/getAllDetail";
    $.ajax({
        type:'get',
        url:urlapi,
        data:
        {
            id: id,

        },
        success:function(data) {

            $("#carthtml").empty();
            $("#carthtml").html (data.html);




        }
    });

}

function setValueImageControl(id,value)
{
 var inputtextLink = document.getElementById(id);
 inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/"+value;
// inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
}

function setImageRead(fileinput, fullLink,shortUrlLink) {

    fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
    // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
    fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


}

function uploadFile(element){
     console.log(element);
    // return;

    var file = element.files[0];
    var formData = new FormData();
    formData.append('image', file);
    formData.append('type_slug', 'news');
    formData.append('_token', '{{ csrf_token() }}');


    $.ajax({
        url: '{{ route('uploadImage') }}',  //Server script to process data
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        success: function(data){

            setImageRead(element, data.fullLink,data.shortUrlLink);



        }
    });
}


function saveData()
{
  
 if( !validateAllInputHasRequired())
 {
     return;
 }
  
// e.preventDefault(); // avoid to execute the actual submit of the form.
tinyMCE.triggerSave();
var data = $("#formEditData").serializeArray(); // convert form to array

var url = "/admin/api/quan-ly-shop/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: data, // serializes the form's elements.
       success: function(data)
       {
         
       
             if(data.success)
             {
                toastr.success(
                    'Cập nhật thành công',
                    '',
                    {
                      timeOut: 500,
                      fadeOut: 500,
                      onHidden: function () {
                          window.location.href="/admin/quan-ly-shop";
                        }
                    }
                  );
             }

       }
     });

}





function deleteSingle()
{
        var urlapi ="/admin/api/quan-ly-shop/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editagencyCode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }
 function bindadditional(data) {
    
        var image = "http://localhost:8000/storage/" + data.linkShare;
        $("#linkShareImage").attr("src", image);
        $("#linkShare").val(data.linkShare);

        $("#editKeyWord").val(data.keyWord);
        $("#editisActive").val(data.isActive);
        $("#editstatus").val(data.status);
        $("#editrelationCode").val(data.relationCode);

        bindDatinh(data);


    }

   

</script>


