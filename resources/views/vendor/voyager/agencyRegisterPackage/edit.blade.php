<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
           @if (Auth::user()->role_id < 2) 
            <button onclick="saveData()" class="btn btn-primary">Duyệt</button>
            @endif
            <button type="button" onclick="formSubmit()" class="btn btn-primary">Từ chối</button>

            <button type="button" onclick="backTo()" class="btn btn-primary"> Quay lại </button>
    </div>
    <div class="tab row">
        <div class="tablinks">Thông tin gói đăng ký</div>
    </div>
    <form role="form" id="formEditData" class="row" onsubmit="return false">
        {{ csrf_field()}}
        <div style="margin-top: 25px"></div>
        <div class="form-group" style="display:none">
            <label for="inputEmail4">id</label>
            <input type="text" readonly class="form-control"  id ="editId" value="{{$dataEdit->id}}" name="editId"
                placeholder="Mã ">
        </div>
       
        <div class="form-group">
            <label for="inputEmail4">Mã giao dịch</label>
            <input type="text" readonly class="form-control"  value="{{$dataEdit->code}}" name="code"
                placeholder="Name">
        </div>
        <p>Thông tin gói </p>
        <div class="form-group">
            <label for="inputEmail4">Mã gói</label>
            <input type="text" readonly  class="form-control"  value="{{$dataEdit->packageCode}}" name="packageCode"
                placeholder="Name">
        </div>
        <div class="form-group">
            <label for="inputEmail4">Gói tháng </label>
            <input type="text" readonly class="form-control"  value="{{$dataEdit->monthRegister}}" name="monthRegister"
                placeholder="Name">
        </div>

        <div class="form-group">
            <label for="inputEmail4">Số tháng được khuyến mãi </label>
            <input type="text" readonly class="form-control"  value="{{$dataEdit->voucher}}" name="voucher"
                placeholder="Name">
        </div>


        <div class="form-group">
            <label for="inputEmail4">Giá trị gói</label>
            <input type="text" readonly class="form-control"  value="{{$dataEdit->prices}}" name="prices"
                placeholder="Name">
        </div>


        <p>Thông tin đại lý </p>


        <div class="form-group">
            <label for="inputEmail4">Mã đại lý</label>
            <input type="text" readonly class="form-control" value="{{$dataEdit->agencyId }}"  name="agencyId"
                placeholder="Name">
        </div>
    
        <div class="form-group">
            <label for="inputEmail4">Tên đại lý</label>
            <input type="text" readonly class="form-control" value="{{ $agency->fullName }}"
                placeholder="Name">
        </div>


        <p>Thông tin shipper đăng ký </p>
       
        <div class="form-group">
            <label for="inputEmail4">Mã shipper</label>
            <input type="text" readonly class="form-control" value="{{$dataEdit->shipperId }}"   name="shipperId"
                placeholder="Name">
        </div>

        <div class="form-group">
            <label for="inputEmail4">Tên shipper</label>
            <input type="text" readonly class="form-control"   value="{{ $shipper->fullName }}"
                placeholder="Name">
        </div>

        <div class="form-group">
            <label for="inputEmail4">Số điện thoại</label>
            <input type="text" readonly class="form-control"   value="{{ $shipper->mobilePhone }}"
                placeholder="Name">
        </div>

        <p>Trạng thái đăng ký </p>

        <div class="form-group">
            <label for="inputEmail4">Ngày bắt đầu hiệu lực</label>
            <input type="text" readonly class="form-control"   value="{{$dataEdit->beginTo }}"   name="beginTo"
                placeholder="Name">
        </div>

        <div class="form-group">
            <label for="inputEmail4">Ngày hết hạn hiệu lực</label>
            <input type="text" readonly class="form-control"  value="{{$dataEdit->validTo }}"   name="validTo"
                placeholder="Name">
        </div>
        
        <div class="form-group">
            <label for="inputEmail4">Cú pháp đăng ký</label>
            <input type="text" readonly class="form-control"  value="{{$dataEdit->syntaxContent }}"  name="syntaxContent"
                placeholder="Name">
        </div>



        <div class="form-group">
            <label for="inputEmail4">Trạng thái thanh toán</label>
            

                <select   class="form-control"  name="paymentStatus"  required>
               
                    @if ($dataEdit->paymentStatus ==1)
                        <option value="1" selected> 
                            Đã thanh toán
                        </option>
                        <option value="0"> 
                            Chưa thanh toán
                         </option>
                    @else
                    <option value="1" > 
                        Đã thanh toán
                    </option>
                    <option value="0" selected> 
                        Chưa thanh toán
                     </option>
                    
                    @endif
                   
                 
                
                </select>
        </div>


        <div class="form-group">
            <label for="inputEmail4">Trạng thái</label>
            

                <select   class="form-control"  name="status"  required>
               
                    @if ($dataEdit->status ==1)
                        <option value="1" selected> 
                           Đã kích hoạt
                        </option>
                        <option value="0"> 
                            Đang yêu cầu
                         </option>
                    @else
                    <option value="1"> 
                        Đã kích hoạt
                     </option>
                     <option value="0" selected> 
                         Đang yêu cầu
                      </option>
                    
                    @endif
                   
                 
                
                </select>
        </div>
       

    



    </form>
</div>

<script>
    function formSubmit () {

    saveData();
}

function saveData()
{

tinyMCE.triggerSave();
var url = "/admin/api/danh-sach-shipper-tham-gia/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), 
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backTo();
             }

       }
     });

}

function deleteSingle()
{
        var urlapi ="/admin/api/danh-sach-shipper-tham-gia/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editId").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

 function backTo()
 {
    window.location.href ="/admin/danh-sach-shipper-tham-gia";
 }

</script>