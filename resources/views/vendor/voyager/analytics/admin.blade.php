<style>
* {box-sizing: border-box}

/* Set height of body and the document to 100% */
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial;
}

/* Style tab links */
.tablink {
  background-color: #555;
  color: white;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  font-size: 17px;
  width: 20%;
}

.tablink:hover {
  background-color: #777;
}

/* Style the tab content (and add height:100% for full page content) */
.tabcontent {
  
  display: none;
  padding: 100px 20px;
  height: 100vh;
}

</style>
</head>
<body>

<button class="tablink" onclick="openPage('Home', this, 'red')" id="defaultOpen">Tất cả đơn hàng</button>
<button class="tablink" onclick="openPage('News', this, 'green')" >Tiền hàng</button>
<button class="tablink" onclick="openPage('Contact', this, 'blue')">Tiền thu hộ (COD)</button>

<button class="tablink" onclick="openPage('shippingcost', this, 'blue')">Cước phí giao hàng</button>
<button class="tablink" onclick="openPage('About', this, 'orange')">Shipper</button>

<div id="Home" class="tabcontent">
  @include('vendor.voyager.analytics.all')
  
</div>

<div id="News" class="tabcontent">
  @include('vendor.voyager.analytics.store')
</div>

<div id="Contact" class="tabcontent">
  @include('vendor.voyager.analytics.cod')
</div>

<div id="About" class="tabcontent">
  @include('vendor.voyager.analytics.shipper')
</div>

<div id ="shippingcost" class="tabcontent">
  @include('vendor.voyager.analytics.shippingcost')
</div> 

<script>
function openPage(pageName,elmnt,color) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].style.backgroundColor = "";
  }
  document.getElementById(pageName).style.display = "block";
  elmnt.style.backgroundColor = color;
}



// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
loadData();
</script>

