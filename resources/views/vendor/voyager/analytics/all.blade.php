
<div class="filterArea"> 
      <form id ="fromSearch"  >
          @csrf
        <div class="form-row">
        <div class="form-group col-md-6">
            <label for="inputEmail4">Từ ngày</label>
            <input type="date" class="form-control" name= "fromdate"  placeholder="Từ ngày">
        </div>
        <div class="form-group col-md-6">
            <label for="inputPassword4">Tới ngày</label>
            <input type="date" class="form-control" name ="todate"  placeholder="Đến ngày">
        </div>
        </div>
        <div class="form-group col-md-6">
            <input type="radio" value="0" name="dayType">Ngày tạo
            <input type="radio" value="1" name="dayType">Ngày cập nhật
         </div>

        <div class="form-row">
            <div class="form-group col-md-12">
                <label for="inputEmail4">Cửa hàng</label>
                <select class="form-control"  name="shop">
                    <option value="" disabled selected>Chọn cửa hàng</option>
                      @foreach ($allStore as $item)
                     
                         <option value= "{{$item->id}}">{{ $item->fullName }} </option>
                     
                      @endforeach
                  </select>
            </div>
            
        </div>
        <div class="button-menu-page"> 

            <button type="button" onclick="loadData()" class="btn btn-primary">Tìm kiếm</button>
        </div>
        
      </form>



</div>

<div id ="body-contanier">
    
</div>


<script>
    function loadData() 
    {
         $.ajax({
        type: "GET",
        url: '/thong-ke/thong-ke-don-hang',
        data:
        $("#fromSearch").serialize(), 
        success: function( data ) {
             $("#body-contanier").html(data.html);
         }
    });
    }

 </script>