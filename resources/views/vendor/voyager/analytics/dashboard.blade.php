<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
        integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"
        integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
        integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
        crossorigin="anonymous" />
</head>

<body>
  @include("vendor.voyager.analytics.admin")
</body>
<script>
    $(document).ready(function() {
            $("#help_button").click(function() {
                $("#help").slideToggle(1000, function() {
                    if($("#help_button").val() == "close")
                    {
                        $("#help_button").val("show table");
                    }
                    else
                    {
                        $("#help_button").val("close");
                    }
                });
            });
            $("#close").click(function() {
                $("#help").slideToggle(1000, function() {
                    if($("#help_button").val() == "close")
                    {
                        $("#help_button").val("show table");
                    }
                    else
                    {
                        $("#help_button").val("close");
                    }
                });
            });

            $("#waittingbtn").click(function() {
                $("#waitting").slideToggle(1000, function() {
                    if($("#waittingbtn").val() == "close")
                    {
                        $("#waittingbtn").val("show table");
                    }
                    else
                    {
                        $("#waittingbtn").val("close");
                    }
                });
            });
            $("#closew").click(function() {
                $("#waitting").slideToggle(1000, function() {
                    if($("#waittingbtn").val() == "close")
                    {
                        $("#waittingbtn").val("show table");
                    }
                    else
                    {
                        $("#waittingbtn").val("close");
                    }
                });
            });

            $("#runningbtn").click(function() {
                $("#running").slideToggle(1000, function() {
                    if($("#runningbtn").val() == "close")
                    {
                        $("#runningbtn").val("show table");
                    }
                    else
                    {
                        $("#runningbtn").val("close");
                    }
                });
            });
            $("#closer").click(function() {
                $("#running").slideToggle(1000, function() {
                    if($("#runningbtn").val() == "close")
                    {
                        $("#runningbtn").val("show table");
                    }
                    else
                    {
                        $("#runningbtn").val("close");
                    }
                });
            });

            $("#donebtn").click(function() {
                $("#done").slideToggle(1000, function() {
                    if($("#donebtn").val() == "close")
                    {
                        $("#donebtn").val("show table");
                    }
                    else
                    {
                        $("#donebtn").val("close");
                    }
                });
            });
            $("#closed").click(function() {
                $("#done").slideToggle(1000, function() {
                    if($("#donebtn").val() == "close")
                    {
                        $("#donebtn").val("show table");
                    }
                    else
                    {
                        $("#donebtn").val("close");
                    }
                });
            });

            $("#canclebtn").click(function() {
                $("#cancle").slideToggle(1000, function() {
                    if($("#canclebtn").val() == "close")
                    {
                        $("#canclebtn").val("show table");
                    }
                    else
                    {
                        $("#canclebtn").val("close");
                    }
                });
            });
            $("#closec").click(function() {
                $("#cancle").slideToggle(1000, function() {
                    if($("#canclebtn").val() == "close")
                    {
                        $("#canclebtn").val("show table");
                    }
                    else
                    {
                        $("#canclebtn").val("close");
                    }
                });
            });
        });


</script>
