<div id="help" class="help">
    <div style=" position: relative;padding-left: 15px;height: 35px;line-height: 35px;color: white; background: #46be8a;margin-bottom: 15px"><a href="{{route('exportexcel',['q'=> 1])}}"><i class="fas fa-file-download"></i></a> Danh Sách Bưu Kiện Tạo mới <i class="fas fa-times-circle" id='close' style="position: absolute; right: 5px;top:10px;"></i></div>
    @if (count($newOrders) > 0)
    <table style="margin: 0 auto;padding: 10px">
        <thead>
            <tr style="text-align: center">
                <th>MÃ VẬN ĐƠN</th>
                <th>NGƯỜI GỬI </th>
                <th>NGƯỜI NHẬN</th>
                <th>COD</th>
                <th>HÌNH THỨC</th>
                <th>CƯỚC PHÍ</th>
                <th>NGÀY TẠO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($newOrders as $item)
                <tr>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->Code}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->sender}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->customer}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->COD}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">@php
                    if($item->MethodId =="S") {

                        echo "Giao nhanh 2h";
                        }
                        else  if($item->MethodId =="C") {

                        echo "Giao trong ngày";
                        }
                @endphp</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->TotalValue}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="text-center"><h3>KHÔNG CÓ KẾT QUẢ PHÙ HỢP</h3></div>

    @endif

</div>
<div id="waitting" class="help">
    <div style=" position: relative;padding-left: 15px;height: 35px;line-height: 35px;color: white; background: #57c7d4;margin-bottom: 15px"><a href="{{route('exportexcel',['q'=> 2])}}"><i class="fas fa-file-download"></i></a> Danh Sách Bưu Kiện Đang chờ Giao Hàng <i class="fas fa-times-circle" id='closew' style="position: absolute; right: 5px;top:10px;"></i></div>
    @if (count($waitingOrders) > 0)
    <table style="margin: 0 auto;padding: 10px">
        <thead>
            <tr style="text-align: center">
                <th>MÃ VẬN ĐƠN</th>
                <th>NGƯỜI GỬI </th>
                <th>NGƯỜI NHẬN</th>
                <th>COD</th>
                <th>HÌNH THỨC</th>
                <th>CƯỚC PHÍ</th>
                <th>NGÀY TẠO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($waitingOrders as $item)
                <tr>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->Code}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->sender}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->customer}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->COD}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">@php
                    if($item->MethodId =="S") {

                        echo "Giao nhanh 2h";
                        }
                        else  if($item->MethodId =="C") {

                        echo "Giao trong ngày";
                        }
                @endphp</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->TotalValue}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="text-center"><h3>KHÔNG CÓ KẾT QUẢ PHÙ HỢP</h3></div>

    @endif

</div>
<div id="running" class="help">
    <div style=" position: relative;padding-left: 15px;height: 35px;line-height: 35px;color: white; background: #f2a654;margin-bottom: 15px"><a href="{{route('exportexcel',['q'=> 3])}}"><i class="fas fa-file-download"></i></a> Danh Sách Bưu Kiện Đang Vận Chuyển <i class="fas fa-times-circle" id='closer' style="position: absolute; right: 5px;top:10px;"></i></div>
    @if (count($tranferringOrders) > 0 || $receivedCount > 0)
    <table style="margin: 0 auto;padding: 10px">
        <thead>
            <tr style="text-align: center">
                <th>MÃ VẬN ĐƠN</th>
                <th>NGƯỜI GỬI </th>
                <th>NGƯỜI NHẬN</th>
                <th>COD</th>
                <th>HÌNH THỨC</th>
                <th>CƯỚC PHÍ</th>
                <th>NGÀY TẠO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tranferringOrders as $item)
                <tr>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->Code}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->sender}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->customer}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->COD}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">@php
                    if($item->MethodId =="S") {

                        echo "Giao nhanh 2h";
                        }
                        else  if($item->MethodId =="C") {

                        echo "Giao trong ngày";
                        }
                @endphp</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->TotalValue}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->created_at}}</td>
                </tr>
            @endforeach
            @if ($receivedCount > 0)
            @foreach ($receivedOrders as $item)
            <tr>
            <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->Code}}</td>
            <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->sender}}</td>
            <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->customer}}</td>
            <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->COD}}</td>
            <td style="padding: 10px 15px;border: 1px solid #000;">@php
                if($item->MethodId =="S") {

                    echo "Giao nhanh 2h";
                    }
                    else  if($item->MethodId =="C") {

                    echo "Giao trong ngày";
                    }
            @endphp</td>
            <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->TotalValue}}</td>
            <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->created_at}}</td>
            </tr>
        @endforeach
            @endif

        </tbody>
    </table>
    @else
    <div class="text-center"><h3>KHÔNG CÓ KẾT QUẢ PHÙ HỢP</h3></div>

    @endif

</div>
<div id="done" class="help">
    <div style=" position: relative;padding-left: 15px;height: 35px;line-height: 35px;color: white; background: #46be8a;margin-bottom: 15px"><a href="{{route('exportexcel',['q'=> 4])}}"><i class="fas fa-file-download"></i></a> Danh Sách Bưu Kiện Đã Giao <i class="fas fa-times-circle" id='closed' style="position: absolute; right: 5px;top:10px;"></i></div>
    @if (count($completedOrders) > 0)
    <table style="margin: 0 auto;padding: 10px">
        <thead>
            <tr style="text-align: center">
                <th>MÃ VẬN ĐƠN</th>
                <th>NGƯỜI GỬI </th>
                <th>NGƯỜI NHẬN</th>
                <th>COD</th>
                <th>HÌNH THỨC</th>
                <th>CƯỚC PHÍ</th>
                <th>NGÀY TẠO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($completedOrders as $item)
                <tr>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->Code}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->sender}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->customer}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->COD}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">@php
                    if($item->MethodId =="S") {

                        echo "Giao nhanh 2h";
                        }
                        else  if($item->MethodId =="C") {

                        echo "Giao trong ngày";
                        }
                @endphp</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->TotalValue}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="text-center"><h3>KHÔNG CÓ KẾT QUẢ PHÙ HỢP</h3></div>

    @endif

</div>
<div id="cancle" class="help">
    <div style=" position: relative;padding-left: 15px;height: 35px;line-height: 35px;color: white; background: #f96868;margin-bottom: 15px"><a href="{{route('exportexcel',['q'=> 5])}}"><i class="fas fa-file-download"></i></a> Danh Sách Bưu Kiện Giao Không Thành Công <i class="fas fa-times-circle" id='closec' style="position: absolute; right: 5px;top:10px;"></i></div>
    @if (count($failedDeliveredOrders) > 0)
    <table style="margin: 0 auto;padding: 10px">
        <thead>
            <tr style="text-align: center">
                <th>MÃ VẬN ĐƠN</th>
                <th>NGƯỜI GỬI </th>
                <th>NGƯỜI NHẬN</th>
                <th>COD</th>
                <th>HÌNH THỨC</th>
                <th>CƯỚC PHÍ</th>
                <th>NGÀY TẠO</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($failedDeliveredOrders as $item)
                <tr>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->Code}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->sender}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->customer}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->COD}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">@php
                    if($item->MethodId =="S") {

                        echo "Giao nhanh 2h";
                        }
                        else  if($item->MethodId =="C") {

                        echo "Giao trong ngày";
                        }
                @endphp</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->TotalValue}}</td>
                <td style="padding: 10px 15px;border: 1px solid #000;">{{$item->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <div class="text-center"><h3>KHÔNG CÓ KẾT QUẢ PHÙ HỢP</h3></div>

    @endif

</div>