@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/dashboard.js') }}"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/dashboard.css') }}" />
@endsection
@section('page_title', 'Thống kê')
@section('view')
    @include('vendor.voyager.analytics.dashboard',[
        'totalOfOrders'=>$totalOfOrders,
        'completedCOD'=>$completedCOD,
        'processingCOD' => $processingCOD,
        'cancelledCOD' => $cancelledCOD,
        'newCount'=>$newCount,
        'tranferringCount'=>$tranferringCount,
        'receivedCount'=>$receivedCount,
        'completedCount'=>$completedCount,
        'cancelledCount'=>$cancelledCount,
        'failedDeliveredCount'=>$failedDeliveredCount,
        'filter_date'=>$filter_date,
        'filter_shipper'=>$filter_shipper,
        'filter_agency'=>$filter_agency,
        'start'=>$start,
        'end'=>$end,
        'shippers'=>$shippers,
        'shipperName'=>$shipperName,
        'agencies'=>$agencies,
        'agencyName'=>$agencyName,
        ])
@endsection
