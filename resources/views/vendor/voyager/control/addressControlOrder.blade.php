@php
$cofigValue = new \stdClass();

$cofigValue->idPovince = "province";
$cofigValue->valueProvince = null;
$cofigValue->nameProvince = "province";
$cofigValue->idDisinct= "huyen";
$cofigValue->valueDisinct = null;
$cofigValue->nameDisinct= "district";

$cofigValue->idReward= "reward";
$cofigValue->valueReward = null;
$cofigValue->nameReward= "reward";
$cofigValue->isAdd= false ;

if(isset($config))
{
$cofigValue = $config;
}

$allProvice = \App\Province::orderby('name')->get();
@endphp

<select name="{{  $cofigValue->nameProvince }}" id="{{  $cofigValue->idPovince }}" onchange="changeQuanTheoTinh(this)">
    <option selected hidden>Chọn tỉnh thành </option>
    @foreach ($allProvice as $item)
    <option value="{{$item->id}}"> {{$item->name }}</option>
    @endforeach
</select>
<select onchange="GetAllReward(this)" name="{{  $cofigValue->nameDisinct }}" id="{{  $cofigValue->idDisinct }}">
    <option selected hidden>Chọn quận/huyện</option>
</select>
<select name="{{  $cofigValue->nameReward }}" id="{{  $cofigValue->idReward }}">
    <option selected hidden>Chọn xã/phường</option>
</select>
<div id="diachi-danger" class="text-danger" style="left:0;"></div>
<div class="py-2 width-100"></div>

<script>
    var dataAdrress = <?php echo json_encode($cofigValue); ?>;
    var idphuongInput = dataAdrress.idReward;

    function GetAllReward(elementHuyen) {
        // var idphuong =elementHuyen.getAttribute(idphuongInput);
        var valueHuyen = elementHuyen.value;
        var elementReward = document.getElementById(idphuongInput);
        elementReward.innerHTML = "";
        var urlAjax = "/api/getward"
        $.ajax({
            type: "post",
            url: urlAjax,
            data: {
                "districts_id": valueHuyen
            },
            dataType: 'json',
            success: function(data) {
                var dataReponse = data;
                var htmlElement = "<option selected hidden>Chọn xã/phường</option>";
                dataReponse.forEach(huyen => {
                    htmlElement += "<option value =" + '"' + huyen.id + '"' + "> " + huyen.name + " </option>";
                });
                elementReward.innerHTML += htmlElement;
            },
            error: function(data) {}
        });
    }

    function changeQuanTheoTinh(elementTinh) {
        var idTinh = elementTinh.value;
        var elementHuyen = document.getElementById(dataAdrress.idDisinct);

        var elementReward = document.getElementById(dataAdrress.idReward);
        elementReward.innerHTML = "<option selected hidden>Chọn xã/phường</option>";
        elementHuyen.innerHTML = "";
        var urlAjax = "/api/getdistrict"
        $.ajax({
            type: "post",
            url: urlAjax,
            data: {
                "provinces_id": idTinh
            },
            dataType: 'json',
            success: function(data) {
                var dataHuyen = data;
                elementHuyen.innerHTML = "";
                var htmlElement = "  <option selected hidden>Chọn quận/huyện</option>";
                dataHuyen.forEach(huyen => {
                    htmlElement += "<option value =" + '"' + huyen.id + '"' + "> " + huyen.name + " </option>"
                });
                elementHuyen.innerHTML += htmlElement;
            },
            error: function(data) {}
        });
    }

    function setValue(proviceValue, districtValue, rewardvalue) {
        document.getElementById(dataAdrress.idPovince).value = proviceValue;
        document.getElementById(dataAdrress.idPovince).dispatchEvent(new Event("change"));
        setTimeout(() => {
            document.getElementById(dataAdrress.idDisinct).value = districtValue;
            document.getElementById(dataAdrress.idDisinct).dispatchEvent(new Event("change"));

            setTimeout(() => {
                document.getElementById(dataAdrress.idReward).value = rewardvalue;
                document.getElementById(dataAdrress.idReward).dispatchEvent(new Event("change"));
            }, 500);
        }, 500);
    }

    window.addEventListener('load',
        function() {
            if (dataAdrress.valueProvince == null) {

            } else {
                setValue(dataAdrress.valueProvince, dataAdrress.valueDisinct, dataAdrress.valueReward);
            }
        }, false);
</script>
