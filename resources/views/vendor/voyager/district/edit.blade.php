<script>
function openTab(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
        @if (Auth::user()->role_id < 2)
        <button  onclick="formSubmit()" class="btn btn-primary">Lưu</button>
        @endif
        <button type="button" onclick ="backTo()" class="btn btn-primary">Quay trở lại</button>
    </div>
    <div class="tab row">
        <div class="tablinks">Quận/Huyện</div>
     </div>

      <!-- Tab content -->

      <form role="form" id ="formEditData" class="row" onsubmit="return false"  >
        {{ csrf_field()}}
        <div style="margin-top: 25px"></div>
        <div class="form-group" style="display:none">
            <label for="inputEmail4">id</label>
            <input type="text" readonly class="form-control" id ="id" value="{{$dataEdit->id}}"    name ="editcode" placeholder="Mã ">
        </div>

        <div class="form-group" >
            <label for="inputEmail4">Tên </label>
        <input type="text" class="form-control" id ="fullName" value="{{$dataEdit->name}}"    name ="editfullName" placeholder="Name">
        </div>

        <div class="form-group">
            <label for="inputEmail4">Số GSO</label>
            <input type="text" class="form-control" id ="editgso" name ="editgso"  value="{{$dataEdit->gso_id}}"  placeholder="GSO ">
        </div>

        <div class="form-group">
          <label for="inputEmail4">id Tỉnh</label>
          <input type="text" class="form-control" id ="editgso" name ="editprovince_id"  value="{{$dataEdit->province_id}}"  placeholder="province id ">
      </div>


    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
                 onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="news">
        {{ csrf_field() }}
    </form>



</div>



<script>



function formSubmit () {

    saveData();
}




// function openUploadFile(element)
// {
//     element.parentElement.getElementsByTagName("input")[0].click();
// }
function initView(data)
{

    if( data.linkShare ==null || data.linkShare =="")
    {
        return;
    }
    setValueImageControl("linkShare",data.linkShare);
}



function setValueImageControl(id,value)
{
 var inputtextLink = document.getElementById(id);
 inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/"+value;
// inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
}

function setImageRead(fileinput, fullLink,shortUrlLink) {

    fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
    // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
    fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


}
function saveData()
{
// e.preventDefault(); // avoid to execute the actual submit of the form.
tinyMCE.triggerSave();
var url = "/admin/api/management-district/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), // serializes the form's elements.
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backTo();
             }

       }
     });

}





function deleteSingle()
{
        var urlapi ="/admin/api/management-province/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editcode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

 function backTo()
 {
    window.location.href ="/admin/management-province";
 }

</script>


