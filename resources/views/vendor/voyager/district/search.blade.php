
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css" integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g==" crossorigin="anonymous" />

        <style>

.checkbox-inline input[type=checkbox], .checkbox input[type=checkbox], .radio-inline input[type=radio], .radio input[type=radio] {
    position: absolute;
    margin-top: 4px\9;
     margin-left: unset !important;
}
        </style>
</head>

<div class="searchArea">
    <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Từ khóa</label>
            <input type="text" class="form-control" id ="serachText"  name ="serachText" placeholder="Từ khóa">
          </div>



          {{-- <div class="form-group col-md-6">
            <label for="inputPassword4">Trạng thái</label>
            <select id="selectStatus" name ="status" class="form-control">
                <option value="All">Tất cả</option>
                <option value="1">Hoạt động</option>
                <option value="0">Không hoạt động</option>
             </select>
          </div> --}}



        </div>



        <div class="form-group col-md-12 left-alignment" >
            {{-- <button type="submit" class="btn btn-primary">Search</button> --}}

            <button type  = "button" onclick="refeshSearch();" class = "btn btn-outline-dark">

                Tìm kiếm

            </button>


        </div>
    </form>
    @if (Auth::user()->role_id < 2)
    <div class="form-group col-md-12 left-alignment" >
        <button type="button" onclick="openNavPage()" class="btn btn-info">Xoá</button>
        <button type="button" onclick="openNewPage(-1)" class="btn btn-info">Thêm mới</button>
     </div>
    @endif
    
</div>

<div class="searchData">
    <table id="table_id" class="display" style="width:100%">
        <thead>
        <tr>
        <th> </th>
        <th>ID</th>
        <th>Tên  </th>
        <th>Số GSO</th>
        <th>ID Tỉnh</th>
        <th>Ngày tạo</th>
        </tr>
        </thead>
        <tbody>
        </tbody>

    </table>

</div>

<script>



window.onload = function ()
 {
        $('#table_id').DataTable({
        "info":     false,
        "searching":false,
        "paging": true,
        "columnDefs": [
            {
            'targets': 0,
            'render': function(data, type, row, meta){
               if(type === 'display'){
                  data = '<div style="text-align: center;" class=" checkbox"><input type="checkbox" value= "'+  row["id"]+ '" name ="productCodeList[]"  class="checkBoxCode"><label></label></div>';
               }

               return data;
            },
            'checkboxes': {
               'selectRow': true,
               'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
            }
            },

            {
                 "render": function ( data, type, row ) {

                    return '<a href ="javascript:void(0)" onclick ="editData('+ row["id"]+')">' +data + '</a>';
                },
                "targets": 2
            },
            {
                "render": function ( data, type, row ) {
                    return '<a href ="javascript:void(0)" onclick ="editData('+ row["id"]+')">' +data + '</a>';
                },
                "targets": 3
            }

        ],
        'select': 'multi',
        "processing": true,
        "lengthChange": false,
        "ajax": {
                 "url": "/admin/api/management-district/getAll",
                "dataType": "json",
                "type": "get",
                data: function(d) {

                    d.tokenText = $("#serachText").val(),
                    
                    d.status = $("#selectStatus").val()


                }
             },
                "deferRender": true,
                "serverSide": true,
                "columns": [

                { "data": "id" },
                { "data": "id" },
                { "data": "name" },
                { "data": "gso_id" },
                { "data": "province_id"},
                { "data": "created_at" },
        ]

        });
      
    }

    function editData(id)
    {
    
        window.location.href ="/admin/management-district/" +id;
    }

    function openNewPage()
    {
        window.location.href ="/admin/management-district/them-moi";
    }

    function openNavPage()
    {
        $('#deleteSearchModal').modal('toggle');
    }


   

    function deleteMutipleRecord()
{
     var arr = [];
     $('.checkBoxCode:checkbox:checked').each(function () {
        arr.push($(this).val());
    });
        var urlapi ="/admin/api/management-district/deleteMutiple";
        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "data": arr

            },
            success:function(data) {
                    if(data.success)
                    {
                        $('#deleteSearchModal').modal('hide');
                        toastr.success("Đã xóa thành công");
                        
                        backToSearch();
                    }else{
                        $('#deleteSearchModal').modal('hide');
                        toastr.error("Bạn chưa chọn đối tượng");
                    }
            }
        });

 }














 </script>
