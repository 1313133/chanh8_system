<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
        @if (Auth::user()->role_id < 2) <button onclick="formSubmit()" class="btn btn-primary">Lưu</button>
            @endif
            <button type="button" onclick="backTo()" class="btn btn-primary">Quay trở lại</button>
    </div>
    <div class="tab row">
        <div class="tablinks">Thông tin nhóm hàng hoá</div>
    </div>
    <form role="form" id="formEditData" class="row" onsubmit="return false">
        {{ csrf_field()}}
        <div style="margin-top: 25px"></div>
        <div class="form-group" style="display:none">
            <label for="inputEmail4">id</label>
            <input type="text" readonly class="form-control" id="id" value="{{$dataEdit->id}}" name="editcode"
                placeholder="Mã ">
        </div>

        @if ($dataEdit->code == "")
        <div class="form-group">
            <label for="inputEmail4">Mã nhóm hàng hoá </label>
            <input type="text" class="form-control" id="code" value="{{$dataEdit->code}}" name="editCodeGroup"
                placeholder="Mã định danh nhóm hàng hoá" >
            <span id = "codeError" style="display:none" class="errorInput">Yêu cầu bắt buộc nhập </span>
        </div> 
        @else
        <div class="form-group">
            <label for="inputEmail4">Mã nhóm hàng hoá </label>
            <input type="text" readonly class="form-control" id="code" value="{{$dataEdit->code}}" name="editCodeGroup"
                placeholder="Mã định danh nhóm hàng hoá" >
            <span id = "codeError" style="display:none" class="errorInput">Yêu cầu bắt buộc nhập </span>
        </div> 
        @endif
       
        <div class="form-group">
            <label for="inputEmail4">Tên nhóm hàng hoá </label>
            <input type="text" class="form-control" id="fullName" value="{{$dataEdit->name}}" name="editName"
                placeholder="Tên nhóm hàng hoá">

                <span id = "fullNameError" style="display:none" class="errorInput">Tên nhóm bắt buộc nhập </span>
        </div>

    
    </form>
</div>

<script>
    function formSubmit () {
        var isHasEror =  0;
        if($("#code").val() == "")
        {
            $("#codeError").show();
            isHasEror ++;
        }
        else {
            $("#codeError").hide();
        }

        if($("#fullName").val() == "")
        {
            $("#fullNameError").show();
            isHasEror ++;
        }
        else {
            $("#fullNameError").hide();
        }
        if(  isHasEror >0)
        {
            return;
        }
        saveData();
}

function saveData()
{
tinyMCE.triggerSave();
var url = "/admin/api/quan-ly-nhom-hang-hoa/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), 
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backTo();
             }

       }
     });

}

function deleteSingle()
{
        var urlapi ="/admin/api/quan-ly-nhom-hang-hoa/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editCode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

 function backTo()
 {
    window.location.href ="/admin/loai-hang-hoa";
 }

</script>