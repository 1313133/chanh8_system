
<style>
.tab {
  overflow: hidden;
  border: 1px solid yellow;
  background-color: #000000;
  color: #ffffff;
}

/* Style the buttons that are used to open the tab content */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color:purple;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #62a8ea;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #a59d45;
  border-top: none;
}
</style>

<script>
function openTab(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
        <button  onclick="formSubmit()" class="btn btn-primary">Lưu</button>
        <button type="button" onclick ="backToSearch()" class="btn btn-primary">Quay trở lại</button>

    </div>



    <div class="tab">
        <button class="tablinks" onclick="openTab(event, 'London')">Thông tin đại lý</button>

      </div>

      <!-- Tab content -->
      <div id="London" class="tabcontent">


        <form role="form" id ="formEditData"  onsubmit="return false"  >
            {{ csrf_field()}}


            <div class="form-group">
                <label for="inputEmail4">Mã đại lý</label>
            <input type="text" readonly class="form-control" id ="editid" readonly   name ="editcode" placeholder="Mã, Tiêu đề">
            </div>









        </form>
        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
            <input name="image" id="upload_file" type="file"
                     onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="news">
            {{ csrf_field() }}
        </form>



      </div>

      <div id="Paris" class="tabcontent">
        @include('vendor.voyager.news.cartEmpty')
      </div>





</div>



<script>



function formSubmit () {

    saveData();
}




function openUploadFile(element)
{
    element.parentElement.getElementsByTagName("input")[0].click();
}
function initView(data)
{

    if( data.linkShare ==null || data.linkShare =="")
    {
        return;
    }
    setValueImageControl("linkShare",data.linkShare);
}

 function getAllCart(id)
{
    var urlapi ="/admin/api/quan-ly-shop/getAllDetail";
    $.ajax({
        type:'get',
        url:urlapi,
        data:
        {
            id: id,

        },
        success:function(data) {

            $("#carthtml").empty();
            $("#carthtml").html (data.html);




        }
    });

}

function setValueImageControl(id,value)
{
 var inputtextLink = document.getElementById(id);
 inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/"+value;
// inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
}

function setImageRead(fileinput, fullLink,shortUrlLink) {

    fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
    // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
    fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


}

function uploadFile(element){
     console.log(element);
    // return;

    var file = element.files[0];
    var formData = new FormData();
    formData.append('image', file);
    formData.append('type_slug', 'news');
    formData.append('_token', '{{ csrf_token() }}');


    $.ajax({
        url: '{{ route('uploadImage') }}',  //Server script to process data
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        success: function(data){

            setImageRead(element, data.fullLink,data.shortUrlLink);



        }
    });
}


function saveData()
{
// e.preventDefault(); // avoid to execute the actual submit of the form.
tinyMCE.triggerSave();
var url = "api/quan-ly-shop/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), // serializes the form's elements.
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backToSearch();
             }

       }
     });

}





function deleteSingle()
{
        var urlapi ="/admin/api/quan-ly-shop/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editcode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

</script>


