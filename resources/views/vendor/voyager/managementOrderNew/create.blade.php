@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/order-new.css') }}">
@endsection
@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/order-new.js') }}"></script>
@endsection
@section('page_title', 'Tạo đơn hàng mới')
@section('view')
    @include('vendor.voyager.managementOrderNew.neworder',
    [
        "allStatus"=>$allStatus,
        "allAgency"=>$allAgency,
        "roledAdmin"=>$roledAdmin,
        "provinces"=>$provinces,
        "districts"=>$districts,
        "wards"=>$wards,
    ])
@endsection
