@php
$isEdit = true;

@endphp
@extends('vendor.voyager.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/order-new.css') }}">
@endsection
@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/order-new.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/js/orderUpdate.js') }}"></script>
@endsection
@section('page_title', 'Cập nhật thông tin đơn hàng')
@section('view')
    @include('vendor.voyager.managementOrderNew.newedit',
    [
        "allStatus"=>$allStatus,
        "allAgency"=>$allAgency,
        "roledAdmin"=>$roledAdmin,
        "provinces"=>$provinces,
        "districts"=>$districts,
        "wards"=>$wards,
        "orderInformation"=>$orderInformation,
        "allStock"=>$allStock,
    ])
@endsection



