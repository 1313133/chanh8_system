@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section ('css')
<link rel="stylesheet" href="{{ voyager_asset('css/admin-order') }}">
@endsection
@section ('javascript')

@endsection
@section('page_title', 'Quản lý đơn hàng')
@section('search')
    @include('vendor.voyager.managementOrderNew.search',["allStatus"=>$allStatus , "allAgency"=>$allAgency,"roledAdmin"=>$roledAdmin])
@endsection

