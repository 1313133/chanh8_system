@php
$isEdit = true;


@endphp

@php

if($addNew)
{
    if($role ==2)
    {


        $data->agentId = $agencyCurrent->id;
    }

    if($role ==1)
    {
        $data->agentId = null;

    }


}

@endphp
@extends('vendor.voyager.master')
@section ('css')
@endsection

@if ($addNew )
@section('page_title', 'Thêm mới đơn hàng')
@else
@section('page_title', 'Thông tin đơn hàng')
@endif

@section('view')
 @include('vendor.voyager.managementOrderNew.edit', [

        "isAdmin"=>$isAdmin ,"addNew"=>$addNew]

        )
@endsection

<script>

function loadData()
    {

    var data = {!! json_encode($data) !!}
    updateBreadCrump(data.id);
    BindingBasic(data);


    $("#editstatusOrder").val(data.statusOrder);
    $("#editshutleTaxRel").val(data.shutleTaxRel);
    $("#edittypeOrderRel").val(data.typeOrderRel);

    bindDatinh(data);
    if(data.tinhnhan)
    {

    }
    else
    {
        data.tinhnhan ="";

    }

    bindDatinh(data, "#tinhnhan","#huyennhan", data.tinhnhan, data.huyennhan);
    openTab(event, 'London');

    }

function openToPage ()
{
    window.location.href ="/admin/managementOrderNew";

}
</script>

