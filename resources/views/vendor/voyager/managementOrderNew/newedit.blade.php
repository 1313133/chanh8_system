@php
    $customerData = $orderInformation->customer;
@endphp

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
            integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
          integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
          crossorigin="anonymous"/>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.2/dist/sweetalert2.all.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap/5.1.0/js/bootstrap.min.js"
            integrity="sha512-NWNl2ZLgVBoi6lTcMsHgCQyrZVFnSmcaa3zRv0L3aoGXshwoxkGs3esa9zwQHsChGRL4aLDnJjJJeP6MjPX46Q=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script>

    </script>
</head>

<div class="card">
    <div class="header header-box">
        <div>

            <div>Cập nhật đơn hàng <span class="order-code">{{ $orderInformation->Code }}</span> -
                <select disabled
                        class="order-status" id="current-status" value="1">
                    @foreach ($allStatus as $status)
                        <option
                            value="{{ $status->id }}" {{ $orderInformation->Status==$status->id? "selected" : "" }}>{{ $status->text }}</option>
                    @endforeach
                </select>
            </div>
            <p><em>Xem và cập nhật thông tin đơn hàng</em></p>
        </div>
        <div class="mx-1">

            @if ($orderInformation->Status==1)
                <button class="btn btn-success confirm-order-btn" type="button">
                    Duyệt đơn
                </button>
            @else
                <a href="{{ url('/admin/printpdf/'.$orderInformation->Code)}} " class="btn btn-success">In Hóa Đơn</a>

            @endif

            @if ($orderInformation->Status=="2")
                <button class="btn btn-warning cancel-order-btn" type="button">
                    Hủy đơn
                </button>
            @else

            @endif


        </div>
        <div class="mx-1">
            <button class="btn btn-primary track-order-btn" id="track-order-btn" type="button">
                Theo dõi đơn hàng
            </button>
        </div>
    </div>
    <div class="main">
        <div class="stepwizard d-flex flex-column">
            <ul class="stepwizard-row setup-panel list-inline">
                <li class="stepwizard-step list-inline-item m-0 disabled">
                    <div id="step-1" type="button" class="btn btn-success btn-step btn-circle btn-info">
                        <i class="fa fa-user-alt"></i>
                    </div>
                    <p class="d-none d-md-block"><small>Thông tin bên nhận</small></p>
                </li>
                <li class="stepwizard-step list-inline-item m-0">
                    <div id="step-2" type="button" class="btn btn-success btn-step btn-circle">
                        <i class="fa fas fa-map-marked"></i>
                    </div>
                    <p class="d-none d-md-block"><small>Thông tin giao hàng</small></p>
                </li>
                <li class="stepwizard-step list-inline-item m-0">
                    <div id="step-3" type="button" class="btn btn-success btn-step btn-circle"><i
                            class="fa fa-wallet"></i></div>
                    <p class="d-none d-md-block"><small>Chi tiết thanh toán</small></p>
                </li>
                <li class="stepwizard-step list-inline-item m-0">
                    <div id="step-4" type="button" class="btn btn-success btn-step btn-circle"><i
                            class="fa fa-check"></i></div>
                    <p class="d-none d-md-block"><small>Xác nhận</small></p>
                </li>
            </ul>
        </div>
        <form id="order-form" role="form"
              name="createform" style="font-size: 0.875rem" class="d-flex justify-content-center width-100"
              autocomplete="off" novalidate>

            @csrf
            {{-- {{ csrf_field() }} --}}
            <div class="card box-content" id="step-1-form">
                <div class="card-header">Thông tin bên nhận</div>
                {{-- <div class="list-group list-group-numbered"> --}}
                <div class="card-body">
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Số điện thoại:</label>
                        <input name="" id="mobile-number" class="form-control form-control-sm" type="search"
                               placeholder="Nhập số điện thoại" required autocomplete="off"/>
                        <div id="mobile-danger" class="text-danger"></div>
                        <input name="CustomerId" id="customer-id" value="{{ $customerData->mobilePhone }}"
                               class="disabled" type="text"/>
                        <ul id="dd-list" class="dd-list disabled">
                            @foreach ($allCustomer as $customer)
                                <li onmousedown="handleSelectNumber({{ $customer }})"
                                    onmouseover="autoFill({{ $customer }})">
                                    {{ $customer->mobilePhone . ' - ' . $customer->fullName }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <input type="hidden" value="{{ $orderInformation->CustomerId }}" name="CustomerIdData"
                           id="CustomerIdData">

                    <input type="hidden" value="{{ $orderInformation->id }}" name="profileId">
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Mã vận đơn:</label>
                        <input id="lading-code" name="" class="form-control form-control-sm" type=""
                               placeholder="Nhập mã vận đơn" value="ladding-code" disabled/>
                        <div id="ladding-danger" class="text-danger"></div>
                    </div>
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Tên khách hàng:</label>
                        <input id="customer-name" name="" class="form-control form-control-sm" type=""
                               placeholder="Nhập tên khách hàng" required/>
                        <div id="customer-name-danger" class="text-danger"></div>
                    </div>

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Loại hàng hoá:</label>

                        <select name="typegoood" class="custom-select custom-select-sm"
                                required>
                            {{-- <option value="0">Chọn địa chỉ</option> --}}
                            @foreach ($allTypeGood as $goodItem)
                                @if ($orderInformation->typegoood == $goodItem->code)
                                    <option value="{{ $goodItem->code }}" selected>
                                        {{ $goodItem->name }}
                                    </option>
                                @else
                                    <option value="{{ $goodItem->code }}">
                                        {{ $goodItem->name }}
                                    </option>
                                @endif
                            @endforeach


                        </select>
                    </div>
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Kích thước:</label>
                        <input id="order-size" name="orderSize" class="form-control form-control-sm" type=""
                               placeholder="Nhập kích thước"/>
                    </div>


                    {{-- <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Giá trị đơn hàng:</label>

                        <div class="order-value">
                            <input id="order-value" name="COD" class="money-textbox form-control form-control-sm"
                                type="text" placeholder="Nhập giá trị đơn hàng" value={{ $orderInformation->COD }}
                                required />
                            <p>VND</p>
                            <div id="order-value-danger" class="text-danger"></div>
                        </div>
                    </div> --}}
                    {{-- <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Chỉ dẫn không phát được vật
                            phẩm:</label>
                        <div class="mt-1">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="radio" name="indicate" id="indicate-1" checked />
                                <label class="form-check-label" for="indicate-1">
                                    Hoàn trả
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="radio" name="indicate" id="indicate-2" />
                                <label class="form-check-label" for="indicate-2">
                                    Hủy
                                </label>
                            </div>
                        </div>
                    </div> --}}
                    {{-- </div> --}}
                    {{-- <div class="form-row justify-content-center"> --}}

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0 bold">Thay đổi Địa chỉ:</label>
                        <div class="address">


                            @php

                                $cofigValueImport = new \stdClass();

                                $cofigValueImport->idPovince = 'provinces';
                                $cofigValueImport->valueProvince = $orderInformation->provinces;
                                $cofigValueImport->nameProvince = 'provinces';
                                $cofigValueImport->idDisinct = 'districts';
                                $cofigValueImport->valueDisinct = $orderInformation->districts;
                                $cofigValueImport->nameDisinct = 'districts';

                                $cofigValueImport->idReward = 'wards';
                                $cofigValueImport->valueReward = $orderInformation->wards;
                                $cofigValueImport->nameReward = 'wards';
                                $cofigValueImport->isAdd = false;

                            @endphp

                            @include('vendor.voyager.control.addressControlOrder', ["config"=>$cofigValueImport])
                            <textarea class="form-control" id="address-details" name="receiveaddress" rows="2"
                                      placeholder="Nhập địa chỉ chi tiết.">{{ $orderInformation->receiveaddress}}</textarea>
                            <div id="diachicuthe-danger" class="text-danger" style="left:0;"></div>
                        </div>
                    </div>


                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Mã tham chiếu (Mã đơn hàng):</label>

                        <input type="text" name="relOrder" value="{{$orderInformation->relOrder}}"
                               aria-placeholder="Nhập mã đơn đặt hàng">
                    </div>

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Số lượng sản phẩm trong đơn hàng:</label>

                        <input type="text" name="countProduct" value="{{$orderInformation->countProduct}}"
                               aria-placeholder="Tham chiếu và đối soát">
                    </div>


                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Nội dung hàng hoá:</label>
                        <textarea class="form-control" id="order-description" name="description" rows="2"
                                  placeholder="Nhập nội dung.">{{ $orderInformation->description }}</textarea>
                    </div>

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Ghi chú:</label>
                        <textarea class="form-control" id="notedOrder" name="notedOrder" rows="3"
                                  placeholder="Nhập ghi chú đơn hàng.">{{ $orderInformation->notedOrder }}</textarea>
                    </div>
                </div>
                {{-- </div> --}}
            </div>
            <div class="card box-content disabled" id="step-2-form">
                <div class="card-header">
                    THÔNG TIN LẤY HÀNG
                </div>
                {{-- <div class="list-group list-group-numbered"> --}}
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fa fas fa-location-arrow"></i>
                                <span>Địa chỉ kho lấy hàng:</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <select id="stock-address" name="AddressStockId" class="custom-select custom-select-sm"
                                    onchange="handleSelectedAddress({{ $allStock }})" required>
                                {{-- <option value="0">Chọn địa chỉ</option> --}}
                                @foreach ($allStock as $stock)
                                    @if ($stock->id == $orderInformation->AddressSenderId )
                                        <option value="{{ $stock->id }}" selected>
                                            {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                        </option>
                                    @else
                                        <option value="{{ $stock->id }}">
                                            {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                        </option>
                                    @endif

                                @endforeach
                            </select>

                            <!-- <div class="valid-feedback">Phù hợp !</div> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fa fas fa-location-arrow"></i>
                                <span>Địa chỉ kho trả hàng:</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <select id="stock-address-return" name="AddressStockReturnId"
                                    class="custom-select custom-select-sm"
                                    onchange="handleSelectedAddress({{ $allStockReturn }})" required>
                                {{-- <option value="0">Chọn địa chỉ</option> --}}
                                @foreach ($allStockReturn as $stock)
                                    @if ($stock->id == $orderInformation->AddressStockReturnId )
                                        <option value="{{ $stock->id }}" selected>
                                            {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                        </option>
                                    @else
                                        <option value="{{ $stock->id }}">
                                            {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                        </option>
                                    @endif

                                @endforeach
                            </select>

                            <!-- <div class="valid-feedback">Phù hợp !</div> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fas fa-portrait"></i>
                                <span>Tài xế nhận đơn hàng</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <select id="shipper-name" class="custom-select custom-select-sm"
                                    onchange="handleSelectedPerson({{ $allShipper }})"
                                    value="{{ $allShipper->first()->fullName }}" name="ShipperId" required>
                                {{-- <option value="0" selected>Chọn người lấy hàng</option> --}}
                                @foreach ($allShipper as $shipper)
                                    <option value="{{ $shipper->id }}">
                                        <div>{{ $shipper->fullName }}</div>
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fas fa-portrait"></i>
                                <span>Số điện thoại:</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <input id="shipper-phone" name="" class="form-control form-control-sm" type=""
                                   placeholder="Số điện thoại" value="{{ $allShipper->first()->mobilePhone }}" autofocus
                                   required/>
                        </div>
                    </div>
                    <div class="form-group row" style="display:none">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fas fa-portrait"></i>
                                <span>Đã thanh toán:</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <div class="mt-1">
                                <label class="switch">
                                    <input type="hidden" value='0' name="isPayment">
                                    <input type="checkbox" value='1' id="payment-status" name="isPayment">
                                    <span class="slider round"></span>
                                </label>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fa fas fa-calendar"></i>
                                <span>Thời gian gửi hàng: </span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <input class="form-control form-control-sm" type="datetime-local"
                                   value="2021-07-01T13:45:00" id="datetime-input"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="form-control-label mb-0">Hình thức vận chuyển:</label>
                        </div>
                        <div class="col-md-9 input-group">

                            <div class="mt-1">
                                @foreach ($allChargeShipper as $item)
                                    <div class="form-check-inline">
                                        @if ($item->default)
                                            <input class="form-check-input" type="radio" checked name="MethodId"
                                                   id="trans-method-{{$item->code}}"
                                                   value="{{$item->code}}"/> {{$item->name }}
                                        @else
                                            <input class="form-check-input" type="radio" name="MethodId"
                                                   id="trans-method-{{$item->code}}"
                                                   value="{{$item->code}}"/> {{$item->name }}
                                        @endif

                                    </div>
                                @endforeach
                            </div>
                        </div>


                    </div>


                </div>
                {{-- </div> --}}

            </div>
            <div class="card box-content disabled" id="step-3-form">
                <div class="card-header">
                    THÔNG TIN CƯỚC PHÍ
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        {{-- <ul class="list-group"> --}}
                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header">Giá trị hàng hoá (VND)</span>
                            </div>

                            <div class="col-md-6">
                                <div class="order-value">
                                    <input id="order-value" name="COD"
                                           class="money-textbox form-control form-control-sm"
                                           type="text" placeholder="Nhập giá trị đơn hàng"
                                           value={{ $orderInformation->COD }}  required/>
                                    <p>VND</p>
                                    <div id="order-value-danger" class="text-danger"></div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block"><strong> Có thu tiền hàng không?</strong></span>
                            </div>

                            <div id="cost-payer-isPaymentOrder" class="col-md-6" onchange="handleSelectedPayer()">
                                <div class="form-check">
                                    <input class="form-check-input" id="paymentYes" type="radio" name="isPaymentOrder"
                                           value="0" checked/> Không
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" id="paymentNo" type="radio" name="isPaymentOrder"
                                           value="1"/> Có
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header">Cước giao hàng: <span id="feeShippingTextDisplay">  "Giao hàng hoả tốc" </span></span>
                            </div>

                            <div class="col-md-6">
                                <div class="order-value">
                                    <input id="shipping-cost" name="ChargeOrder"
                                           class="money-textbox form-control form-control-sm"
                                           type="text" readonly placeholder="Cước giao hàng" value=0 required/>
                                    <p>VND</p>


                                    <div id="order-value-danger" class="text-danger"></div>
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item" style="display:none">
                            <div class="col-md-6">
                                <span class="d-block">Cước liên tỉnh
                                </span>
                                <small class="font-italic">(VND)</small>
                            </div>

                            <div class="col-md-6">
                                <input id="intercity-cost" name="IntercityCost" class="money-textbox border-0"
                                       type="hidden"
                                       placeholder="Nhập cước liên tỉnh." value="0" readonly/>
                            </div>
                        </li>
                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block"><strong>Có thu tiền cước không?</strong></span>
                            </div>

                            <div id="cost-payer" class="col-md-6" onchange="handleSelectedPayer()">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isSenderPayment" id="sender"
                                           value="0" checked/> Không
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isSenderPayment" id="recipient"
                                           value="1"/> Có
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header"> Tổng thanh toán(Tiền hàng + Cước giao) (VNĐ)</span>


                            </div>

                            <div class="col-md-6">
                                <div class="order-value">
                                    <input id="total-cost" readonly
                                           class="money-textbox form-control form-control-sm bold-text"
                                           name="TotalValue" value="0"/>
                                    <p>VND</p>
                                </div>
                            </div>
                        </li>


                        {{-- </ul> --}}
                    </div>
                </div>
            </div>
            <div class="card box-content disabled" id="step-4-form">
                <div class="card-header">
                    XÁC NHẬN ĐƠN HÀNG
                </div>
                <div class="card-body">
                    <div class="form-group row">

                        <div class="main-header list-group-item">
                            <span class="col-md-6">Thông tin gói hàng</span>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Giá trị hàng hoá </span>

                            </div>

                            <div class="col-md-6">
                                <span id="confirm-cod-value"
                                      class="d-block bootstrap-switch-focused mediumPrice">0</span>
                            </div>
                        </div>


                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Cước giao hàng
                                </span>

                            </div>

                            <div class="col-md-6">
                                <span id="confirm-shipping-cost" class="d-block mediumPrice">0</span>
                            </div>
                        </div>


                        {{-- <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Giá trị COD </span>
                                <small class="font-italic">(VND)</small>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-cod-value" class="d-block">0</span>
                            </div>
                        </div> --}}

                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Tổng tiền thanh toán(đã bao gồm cước giao hàng) </span>

                            </div>

                            <div class="col-md-6">
                                <span id="confirm-cod-value-textr" class="d-block largePrice">0</span>
                            </div>
                        </div>

                        {{-- <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Cước giao hàng
                                </span>
                                <small class="font-italic" >(VND)</small>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-shipping-cost"  class="d-block">0</span>
                            </div>
                        </div> --}}

                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Nội dung hàng hoá </span>

                            </div>

                            <div class="col-md-6">
                                <span id="notedDisplay" class="d-block">0</span>
                            </div>
                        </div>

                        <div class="main-header list-group-item">
                            <span class="col-md-6">Thông tin lấy hàng</span>
                            <div class="col-md-6"></div>
                        </div>
                        {{-- <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Cước liên tỉnh
                                </span>
                                <small class="font-italic">(VND)</small>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-intercity-cost" class="d-block">0</span>
                            </div>
                        </div> --}}

                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Tài xế nhận đơn hàng
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-shipper-name" class="d-block"></span>
                            </div>
                        </div>


                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Địa chỉ lấy
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-stock-address" class="d-block"></span>
                            </div>
                        </div>


                        <div class="main-header list-group-item">
                            <span class="col-md-6">Thông tin bên nhận</span>
                            <div class="col-md-6"></div>
                        </div>


                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Tên người nhận
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-recipient-name" class="d-block">{{ $customerData->fullName }}</span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Số điện thoại
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-recipient-phone"
                                      class="d-block">{{ $customerData->mobilePhone }}</span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Địa chỉ
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-recipient-address"
                                      class="d-block"></span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6 main-header">
                                <span class="d-block">Phương thức</span>
                            </div>
                            <div class="col-md-6">
                                <div id="confirm-trans-method"></div>
                            </div>
                        </div>
                        <div class="list-group-item" style="display:none">
                            <div class="col-md-6 main-header">
                                <span
                                    class="d-block"><strong> Có thu cước giao hàng khách hàng hay không?</strong></span>
                            </div>
                            <div class="col-md-6">
                                <div id="confirm-cost-payer"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-xl-right bg-transparent footerNavBar">
                <button class="btn btn-primary nextBtn" type="button">
                    Tiếp tục
                </button>
                <button class="btn btn-primary  approveStatus" type="button" onclick="updateOrder()">
                    Lưu thay đổi
                </button>
                <button class="btn btn-primary approveStatus btnSave disabled" style="display:none" id="btnSave"
                        onclick="createOrder()" type="button">
                    Lưu và duyệt
                </button>

            </div>
        </form>
    </div>
</div>
<script>
    var customerData = {!! json_encode($customerData) !!};
    var orderInformation = {!! json_encode($orderInformation) !!};
    log(orderInformation);
    var allChargeShipperGlobal = {!! json_encode($allChargeShipper) !!};
    var allStoreLocation = {!! json_encode($allStock) !!};
</script>
