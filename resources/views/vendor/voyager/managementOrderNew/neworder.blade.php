<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
            integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
          integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
          crossorigin="anonymous"/>
</head>

<div class="card">
    <div class="header header-box">
        <div>
            <div>Thực hiện các bước tạo đơn hàng</div>
            <p><em>Điền vào tất cả trường biểu mẫu để chuyển sang bước tiếp theo</em></p>
        </div>
    </div>
    <div class="main">
        <div class="stepwizard d-flex flex-column">
            <ul class="stepwizard-row setup-panel list-inline">
                <li class="stepwizard-step list-inline-item m-0 disabled">
                    <div id="step-1" type="button" class="btn btn-success btn-step btn-circle btn-info">
                        <i class="fa fa-user-alt"></i>
                    </div>
                    <p class="d-none d-md-block"><small>Thông tin bên nhận</small></p>
                </li>
                <li class="stepwizard-step list-inline-item m-0">
                    <div id="step-2" type="button" class="btn btn-success btn-step btn-circle">
                        <i class="fa fas fa-map-marked"></i>
                    </div>
                    <p class="d-none d-md-block"><small>THÔNG TIN LẤY HÀNG</small></p>
                </li>
                <li class="stepwizard-step list-inline-item m-0">
                    <div id="step-3" type="button" class="btn btn-success btn-step btn-circle"><i
                            class="fa fa-wallet"></i></div>
                    <p class="d-none d-md-block"><small>Chi tiết thanh toán</small></p>
                </li>
                <li class="stepwizard-step list-inline-item m-0">
                    <div id="step-4" type="button" class="btn btn-success btn-step btn-circle"><i
                            class="fa fa-check"></i></div>
                    <p class="d-none d-md-block"><small>Xác nhận</small></p>
                </li>
            </ul>
        </div>
        <form id="order-form" name="changeQuanTheoTinhcreateform"
              style="font-size: 0.875rem" class="d-flex justify-content-center width-100" autocomplete="off" novalidate>
            {{ csrf_field() }}
            <div class="card box-content" id="step-1-form">
                <div class="card-header">Thông tin bên nhận</div>
                {{-- <div class="list-group list-group-numbered"> --}}
                <div class="card-body">
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Số điện thoại:</label>
                        <input name="" id="mobile-number" class="form-control form-control-sm" type="search"
                               placeholder="Nhập số điện thoại" required autocomplete="off"/>
                        <div id="mobile-danger" class="text-danger"></div>
                        <input name="CustomerId" id="customer-id" class="disabled" type="text"/>
                        <ul id="dd-list" class="dd-list disabled">
                            @foreach ($allCustomer as $customer)
                                <li onmousedown="handleSelectNumber({{ $customer }})"
                                    onmouseover="autoFill({{ $customer }})">
                                    {{ $customer->mobilePhone . ' - ' . $customer->fullName }}
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="form-item col-md-12">
                        <label class="form-control-label">Thêm khách hàng mới vào sổ địa chỉ:</label>
                        <div class="mt-0 ml-3">
                            <input type="checkbox" style="margin-top:2px;" id="add-new-customer">
                        </div>
                    </div>
                    {{-- <input type="hidden" value="" name="CustomerIdData" id="CustomerIdData"> --}}
                    <div class="form-item col-md-12" style="display:none">
                        <label class="form-control-label mb-0">Mã vận đơn:</label>
                        <input id="lading-code" name="" class="form-control form-control-sm" type=""
                               placeholder="Nhập mã vận đơn" value="ladding-code" disabled/>
                        <div id="ladding-danger" class="text-danger"></div>
                    </div>
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Tên khách hàng:</label>
                        <input id="customer-name" name="" class="form-control form-control-sm" type=""
                               placeholder="Nhập tên khách hàng" required/>
                        <div id="customer-name-danger" class="text-danger"></div>
                    </div>


                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Loại hàng hoá:</label>

                        <select name="typegoood" class="custom-select custom-select-sm"
                                required>
                            {{-- <option value="0">Chọn địa chỉ</option> --}}
                            @foreach ($allTypeGood as $goodItem)
                                <option value="{{ $goodItem->code }}">
                                    {{ $goodItem->name }}
                                </option>
                            @endforeach


                        </select>

                        <div id="typegoood-danger" class="text-danger"></div>
                    </div>

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Kích thước:</label>
                        <input id="order-size" name="orderSize" class="form-control form-control-sm" type=""
                               placeholder="Nhập kích thước"/>
                    </div>
                    <div class="form-item col-md-12" style="display:none">
                        <label class="form-control-label mb-0">Đã thanh toán:</label>
                        <div class="mt-1">
                            <label class="switch">
                                {{-- <input type="hidden" value='0' name="isPayment"> --}}
                                <input type="checkbox" value='1' id="payment-status" name="isPayment">
                                <span class="slider round"></span>
                            </label>
                        </div>
                    </div>
                    <div class="form-item col-md-12" style="display:none">
                        <label class="form-control-label mb-0">Giá trị đơn hàng:</label>

                        {{-- <div class="order-value">
                            <input id="order-value" name="COD" class="money-textbox form-control form-control-sm"
                                type="text" placeholder="Nhập giá trị đơn hàng" value=0 required />
                            <p>VND</p>
                            <div id="order-value-danger" class="text-danger"></div>
                        </div> --}}
                    </div>
                    {{-- <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Chỉ dẫn không phát được vật
                            phẩm:</label>
                        <div class="mt-1">
                            <div class="form-check-inline">
                                <input class="form-check-input" type="radio" name="indicate" id="indicate-1" checked />
                                <label class="form-check-label" for="indicate-1">
                                    Hoàn trả
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input" type="radio" name="indicate" id="indicate-2" />
                                <label class="form-check-label" for="indicate-2">
                                    Hủy
                                </label>
                            </div>
                        </div>
                    </div> --}}
                    {{-- </div> --}}
                    {{-- <div class="form-row justify-content-center"> --}}
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Địa chỉ:</label>
                        <div class="address oncreateorder">
                            @include('vendor.voyager.control.addressControlOrder')
                            <textarea class="form-control" id="address-details" name="receiveaddress" rows="2"
                                      placeholder="Nhập địa chỉ chi tiết."></textarea>

                            <div id="diachicuthe-danger" class="text-danger" style="left:0;"></div>
                        </div>
                    </div>
                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Mã tham chiếu (Mã đơn hàng):</label>

                        <input type="text" name="relOrder" aria-placeholder="Nhập mã đơn đặt hàng">
                    </div>

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Số lượng sản phẩm trong đơn hàng:</label>

                        <input type="text" name="countProduct" aria-placeholder="Tham chiếu và đối soát">
                    </div>


                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Nội dung hàng hoá:</label>
                        <textarea class="form-control" id="order-description" name="description" rows="2"
                                  placeholder="Nhập nội dung."></textarea>
                    </div>

                    <div class="form-item col-md-12">
                        <label class="form-control-label mb-0">Ghi chú:</label>
                        <textarea class="form-control" name="notedOrder" rows="3"
                                  placeholder="Nhập ghi chú đơn hàng."></textarea>
                    </div>
                </div>
                {{-- </div> --}}
            </div>
            <div class="card box-content disabled" id="step-2-form">
                <div class="card-header">
                    THÔNG TIN LẤY HÀNG
                </div>
                {{-- <div class="list-group list-group-numbered"> --}}
                <div class="card-body">
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fa fas fa-location-arrow"></i>
                                <span>Địa chỉ kho lấy hàng:</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">

                            <select id="stock-address" name="AddressStockId" class="custom-select custom-select-sm"
                                    onchange="handleSelectedAddress()" required>
                                {{-- <option value="0">Chọn địa chỉ</option> --}}
                                @foreach ($allStock as $stock)
                                    <option value="{{ $stock->id }}">
                                        {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                    </option>
                                @endforeach
                            </select>

                            <!-- <div class="valid-feedback">Phù hợp !</div> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fa fas fa-location-arrow"></i>
                                <span>Địa chỉ kho trả hàng:</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">

                            <select id="stock-address" name="AddressStockReturnId"
                                    class="custom-select custom-select-sm"
                                    onchange="handleSelectedAddress()" required>
                                {{-- <option value="0">Chọn địa chỉ</option> --}}
                                @foreach ($allStockReturn as $stock)
                                    @if ($loop->first)
                                        <option value="{{ $stock->id }}" selected>
                                            {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                        </option>
                                    @else
                                        <option value="{{ $stock->id }}">
                                            {{ $stock->title . ' - ' . $stock->addresssInfo . " ". $stock->wardText . " ". $stock->districtText ." ". $stock->provinceText}}
                                        </option>
                                    @endif
                                @endforeach
                            </select>

                            <!-- <div class="valid-feedback">Phù hợp !</div> -->
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fas fa-portrait"></i>
                                <span>Tài xế nhận đơn hàng</span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            @if (count($allShipper) > 0)
                                <select id="shipper-name" class="custom-select custom-select-sm"
                                        onchange="handleSelectedPerson({{ $allShipper }})"
                                        value="{{ $allShipper->first()->fullName }}" name="ShipperId" required>
                                    {{-- <option value="0" selected>Chọn người lấy hàng</option> --}}
                                    @foreach ($allShipper as $shipper)
                                        <option value="{{ $shipper->id }}">
                                            <div>{{ $shipper->fullName }}</div>
                                        </option>
                                    @endforeach
                                </select>
                            @else
                                <select id="shipper-name" class="custom-select custom-select-sm"
                                        name="ShipperId" required>


                                </select>
                            @endif

                        </div>
                    </div>
                    @if (count($allShipper) > 0)
                        <div class="form-group row">
                            <div class="col-md-3">
                                <label class="control-label fw-bold d-flex align-items-center">
                                    <i class="fas fa-portrait"></i>
                                    <span>Số điện thoại:</span>
                                </label>
                            </div>
                            <div class="col-md-9 input-group">
                                <input id="shipper-phone" name="" class="form-control form-control-sm" type=""
                                       placeholder="Số điện thoại" value="{{ $allShipper->first()->mobilePhone }}"
                                       autofocus
                                       disabled
                                       required/>
                            </div>
                        </div>
                    @else

                    @endif


                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="control-label fw-bold d-flex align-items-center">
                                <i class="fa fas fa-calendar"></i>
                                <span>Thời gian gửi hàng: </span>
                            </label>
                        </div>
                        <div class="col-md-9 input-group">
                            <input class="form-control form-control-sm" type="datetime-local"
                                   value="2021-07-01T13:45:00" id="datetime-input"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-3">
                            <label class="form-control-label mb-0">Hình thức vận chuyển:</label>
                        </div>
                        <div class="col-md-9 input-group">

                            <div class="mt-1">
                                @foreach ($allChargeShipper as $item)
                                    <div class="form-check-inline">


                                        @if ($item->default)
                                            <input class="form-check-input" type="radio" checked name="MethodId"
                                                   id="trans-method-{{$item->code}}"
                                                   value="{{$item->code}}"/> {{$item->name }}
                                        @else
                                            <input class="form-check-input" type="radio" name="MethodId"
                                                   id="trans-method-{{$item->code}}"
                                                   value="{{$item->code}}"/> {{$item->name }}
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>


                    </div>


                </div>
                {{-- </div> --}}

            </div>
            <div class="card box-content disabled" id="step-3-form">
                <div class="card-header">
                    THÔNG TIN THANH TOÁN
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        {{-- <ul class="list-group"> --}}
                        {{-- <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header">Tổng tiền thu (VND)</span>
                            </div>

                            <div class="col-md-6">
                                <input id="total-cost" class="highlight-text" name="TotalValue" value="0" />
                            </div>
                        </li> --}}

                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header">Giá trị hàng hoá (VND)</span>
                            </div>

                            <div class="col-md-6">
                                <div class="order-value">
                                    <input id="order-value" name="COD"
                                           class="money-textbox form-control form-control-sm"
                                           type="text" placeholder="Nhập giá trị đơn hàng" value=0 required/>
                                    <p>VND</p>
                                    <div id="order-value-danger" class="text-danger"></div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block"><strong> Có thu tiền hàng không?</strong></span>
                            </div>

                            <div id="cost-payer-isPaymentOrder" class="col-md-6" onchange="handleSelectedPayer()">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isPaymentOrder"
                                           value="0" checked/> Không
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isPaymentOrder"
                                           value="1"/> Có
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header">Cước giao hàng: <span id="feeShippingTextDisplay">  "Giao hàng hoả tốc" </span></span>
                            </div>

                            <div class="col-md-6">
                                <div class="order-value">
                                    <input id="shipping-cost" name="ChargeOrder"
                                           class="money-textbox form-control form-control-sm"
                                           type="text" readonly placeholder="Cước giao hàng" value=0 required/>
                                    <p>VND</p>


                                    <div id="order-value-danger" class="text-danger"></div>
                                </div>
                            </div>
                        </li>

                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block"><strong>Có thu tiền cước không?</strong></span>
                            </div>

                            <div id="cost-payer" class="col-md-6" onchange="handleSelectedPayer()">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isSenderPayment" id="sender"
                                           value="0" checked/> Không
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isSenderPayment" id="recipient"
                                           value="1"/> Có
                                </div>
                            </div>
                        </li>


                        <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header"> Tổng thanh toán(Tiền hàng + Cước giao) (VNĐ)</span>


                            </div>

                            <div class="col-md-6">
                                <div class="order-value">
                                    <input id="total-cost" readonly
                                           class="money-textbox form-control form-control-sm bold-text"
                                           name="TotalValue" value="0"/>
                                    <p>VND</p>
                                </div>
                            </div>
                        </li>
                        {{-- <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block"><strong class ="bold">Cước giao hàng</strong>
                                </span>
                                <small class="font-italic bold">(VND)</small>
                            </div>

                            <div class="col-md-6">
                                <input id="shipping-cost" name="ChargeOrder"
                                    class="money-textbox form-control form-control-sm border-0" type="text"
                                    placeholder="Nhập cước giao hàng." autofocus required />
                            </div>
                        </li> --}}
                        {{-- <li class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Cước liên tỉnh
                                </span>
                                <small class="font-italic">(VND)</small>
                            </div>

                            <div class="col-md-6">
                                <input id="intercity-cost" name="IntercityCost" class="money-textbox border-0" type="text"
                                    placeholder="Nhập cước liên tỉnh." value="0" autofocus required />
                            </div>
                        </li> --}}
                        {{-- <li class="list-group-item">
                            <div class="col-md-6">

                                <span class="main-header"><strong> Có thu cước giao hàng khách hàng hay không?</strong></span>

                            </div>

                            <div id="cost-payer" class="col-md-6" onchange="handleSelectedPayer()">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isSenderPayment" id="sender"
                                        value="0" checked /> Không
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="isSenderPayment" id="recipient"
                                        value="1" /> Có
                                </div>
                            </div>
                        </li> --}}
                        {{-- </ul> --}}
                    </div>
                </div>
            </div>
            <div class="card box-content " id="step-4-form">
                <div class="card-header">
                    XÁC NHẬN ĐƠN HÀNG
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        {{-- <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="main-header">Tổng tiền thu (VND)</span>
                            </div>
                            <div class="col-md-6">
                                <span id="confirm-total-cost" class="highlight-text">0</span>
                            </div>
                        </div> --}}
                        <div class="main-header list-group-item">
                            <span class="col-md-6">Thông tin gói hàng</span>
                            <div class="col-md-6"></div>
                        </div>

                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Giá trị hàng hoá </span>

                            </div>

                            <div class="col-md-6">
                                <span id="confirm-cod-value"
                                      class="d-block bootstrap-switch-focused mediumPrice">0</span>
                            </div>
                        </div>
                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Cước giao hàng
                                </span>

                            </div>

                            <div class="col-md-6">
                                <span id="confirm-shipping-cost" class="d-block mediumPrice">0</span>
                            </div>
                        </div>
                      

                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Nội dung hàng hoá </span>

                            </div>

                            <div class="col-md-6">
                                <span id="notedDisplay" class="d-block">0</span>
                            </div>
                        </div>

                        <div class="main-header list-group-item">
                            <span class="col-md-6">Thông tin lấy hàng</span>
                            <div class="col-md-6"></div>
                        </div>

                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Tài xế nhận đơn hàng
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-shipper-name" class="d-block"></span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Địa chỉ lấy
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-stock-address" class="d-block"></span>
                            </div>
                        </div>
                        <div class="main-header list-group-item">
                            <span class="col-md-6">Thông tin bên nhận</span>
                            <div class="col-md-6"></div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Tên người nhận
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-recipient-name" class="d-block"></span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Số điện thoại
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-recipient-phone" class="d-block"></span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6">
                                <span class="d-block">Địa chỉ
                                </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-recipient-address" class="d-block"></span>
                            </div>
                        </div>
                        <div class="list-group-item">
                            <div class="col-md-6 main-header">
                                <span class="d-block">Hình thức vận chuyển</span>
                            </div>
                            <div class="col-md-6">
                                <div id="confirm-trans-method"></div>
                            </div>
                        </div>
                        <!-- <div class="list-group-item">
                            <div class="col-md-6 main-header">
                                <span class="d-block">Tuỳ chọn thanh toán</span>
                            </div>
                            <div class="col-md-6">
                                <div id="confirm-cost-payer"></div>
                            </div>
                        </div> -->
                        <div class="list-group-item no-border">
                            <div class="col-md-6">
                                <span class="d-block">Tổng tiền thanh toán(đã bao gồm cước giao hàng) </span>
                            </div>

                            <div class="col-md-6">
                                <span id="confirm-cod-value-textr" class="d-block largePrice">0</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-xl-right bg-transparent footerNavBar">
                <button class="btn btn-primary nextBtn" type="button">
                    Tiếp tục
                </button>
                <button class="btn btn-primary approveStatus confirmBtn saveOrder" onclick="createOrder()"
                        type="button">
                    Lưu
                </button>
                <button class="btn btn-primary approveStatus btnSave disabled" id="btnSave" onclick="createOrder()"
                        type="button">
                    Lưu và duyệt đơn
                </button>

                {{-- <button class="btn btn-primary nextBtn" type="button">
                    Lưu nháp
                </button> --}}


            </div>
        </form>
    </div>
</div>


<script>

    var allChargeShipperGlobal = {!! json_encode($allChargeShipper) !!};
    var allStoreLocation = {!! json_encode($allStock) !!};

</script>
