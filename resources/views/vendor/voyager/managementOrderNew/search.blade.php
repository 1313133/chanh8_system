@php
$userCurrent = Auth::user();
@endphp

<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
        integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
        crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
        integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
        crossorigin="anonymous" />
    <style>
        .checkbox-inline input[type=checkbox],
        .checkbox input[type=checkbox],
        .radio-inline input[type=radio],
        .radio input[type=radio] {
            position: absolute;
            margin-top: 4px\9;
            margin-left: unset !important;
        }

        .button2 {
            background-color: white;
            color: black;
            border: 2px solid #00844b;
        }

        .button2:hover {
            background-color: #00844b;
            color: white;
        }

        .nav-bar {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin: 0;
            align-items: flex-end;
        }

        /* #loader {
            background-color: rgba(31, 187, 44, 0.637);
            height: 3rem;
            padding: 0.5rem 0;
            color: white;
            margin: 0;
            z-index: 99;
        } */


        #loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid rgba(31, 187, 44, 0.637);
            width: 100px;
            height: 100px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
            margin: auto;
        }
        .paginate {
            margin: auto 5px;
        }
    </style>
</head>

<body>
    <div class="nav-bar">
        <h3>Vận đơn</h3>
        @if (Auth::user()->role_id != 3)
        <div>
            <button class="btn btn-primary" onclick="openNewPage(-1)"><i class="fas fa-plus-circle"></i> Tạo vận
                đơn</button>
            @if ($roledAdmin)
                <button class="btn btn-primary" onclick="importFileExcel()"><i class="fas fa-print"></i> In vận
                    đơn</button>
            @endif
            <button class="btn btn-warning"><i class="fas fa-trash"></i> Xóa vận
                đơn</button>
        </div>
        @endif

    </div>

    <div class="searchArea">
        <form>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Từ ngày</label>
                    <div class="input-group date">
                        <input id="tungay" name="tungay" class="datetimepicker form-control"
                            data-date-format="dd/mm/yyyy" type="text">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>


                <div class="form-group col-md-6">
                    <label for="inputEmail4">Đến ngày</label>
                    <div class="input-group date">
                        <input name="denngay" id="denngay" class="datetimepicker form-control"
                            data-date-format="dd/mm/yyyy" type="text" value="">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>



            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Từ khóa</label>
                    <input type="text" class="form-control" id="serachText" name="serachText" placeholder="Từ khóa">
                </div>



                <div class="form-group col-md-6">
                    <label for="inputPassword4">Trạng thái</label>
                    <select id="selectStatus" name="status" class="form-control">
                        <option value="All">Tất cả</option>
                        @foreach ($allStatus as $item)
                            <option value="{{ $item->id }}">{{ $item->text }}</option>
                        @endforeach
                    </select>
                </div>



            </div>

            @if ($roledAdmin)
                <div class="form-row">
                @else
                    <div class="form-row" style="display:none">
            @endif


            {{-- <div class="form-group col-md-6">
                <label for="inputPassword4">Tìm theo đại lý</label>
                <select id="selectAgen" name="status" class="form-control">
                    <option value="-1">Tất cả</option>
                    @foreach ($allAgency as $item)
                        <option value="{{ $item->id }}">{{ $item->fullName }}</option>
                    @endforeach


                </select>
            </div> --}}


    </div>


    <div class="form-group col-md-12 left-alignment">
        {{-- <button type="submit" class="btn btn-primary">Search</button> --}}

        <button type="button" onclick="refeshSearch();" class="btn btn-outline-dark button2 ">
            <i class="voyager-search"> </i>
            Tìm kiếm
        </button>


    </div>
    </form>

    </div>

    <div class="searchData">
        <table id="table_id" class="display" style="width:100%">
            <thead>
                <tr>
                    <th> </th>
                    <th>MÃ VẬN ĐƠN</th>
                    <th>TÊN TÀI XẾ</th>
                    
                    <th>TRẠNG THÁI</th>
                    <th>ĐỊA CHỈ GIAO HÀNG</th>
                    <th>HÌNH THỨC GIAO HÀNG</th>
                    <th>GÍA TRỊ HÀNG HOÁ</th>
                   
                    
                    <th>CƯỚC PHÍ</th>
                    <th>KHÁCH HÀNG THANH TOÁN (COD)</th>
                    <th>ĐIỂM LẤY HÀNG</th>
                    <th>NGÀY TẠO</th>
                </tr>
            </thead>


            <tbody>
            </tbody>

        </table>

    </div>
    <script>
        window.onload = function() {
            $('.datetimepicker').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true
            });
            $('#table_id').DataTable({
                "info": false,
                "searching": false,
                "paging": true,
                "columnDefs": [{
                        'targets': 0,
                        'render': function(data, type, row, meta) {
                            if (type === 'display') {
                                data =
                                    '<div style="text-align: center;" class=" checkbox"><input type="checkbox" value= "' +
                                    row["id"] +
                                    '" name ="productCodeList[]"  class="checkBoxCode"><label></label></div>';
                            }

                            return data;
                        },
                        'checkboxes': {
                            'selectRow': true,
                            'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                        }
                    },

                    {
                        "render": function(data, type, row) {
                            if (row.role == 3) {
                                return '<a href ="#" >' + data + '</a>';
                            } else {
                                return '<a href ="javascript:void(0)" onclick ="editData(' + row["id"] +
                                ')">' + data + '</a>';
                            }

                        },
                        "targets": 1
                    },
                    {
                        "render": function(data) {
                            return `<span style="background-color:${getColor(data)};padding: 3px;color: white;">` +
                                data + '</span>';
                        },
                        "targets": 3
                    },
                    // {
                    //     "render": function(data, type, row) {

                    //         var addressFullText = row.receiveaddress;

                    //         if(row.districtText)
                    //         {
                    //             addressFullText += "," + row.districtText;
                    //         }
                    //         if(row.provinceText)
                    //         {
                    //             addressFullText += "," + row.provinceText;
                    //         }
                    //         if(row.provinceText)
                    //         {
                    //             addressFullText += "," + row.wardText;
                    //         }
                    //         return  addressFullText;
                    //     },
                    //     "targets": 4
                    // },

                    {
                        "render": function(data, type, row) {
                          
                            var addressFullText = row.receiveaddress;

                            if(row.wardText)
                            {
                                addressFullText += "," + row.wardText;
                            }
                            if(row.districtText)
                            {
                                addressFullText += "," + row.districtText;
                            }
                            
                            if(row.provinceText)
                            {
                                addressFullText += "," + row.provinceText;
                            }
                            return  addressFullText;
                        },
                        "targets": 4
                    },
                    {
                        "render": function(data, type, row) {
                          
                            if(data.isSenderPayment)
                            {
                                return 0;
                            }
                            return  data;
                        },
                        "targets": 7
                    }
                   
                   
                ],
                'select': 'multi',
                "processing": true,
                "language": {
                    "processing": "<div id='loader'></div>",
                    "paginate": {
				             "previous": "<div class='paginate previous'>Trước</div>",
                             "next": "<div class='paginate next'>Sau</div>",
				          }
                },
                "lengthChange": false,
                "ajax": {
                    "url": "/admin/quan-ly-don-hang/getall",
                    "dataType": "json",
                    "type": "get",
                    data: function(d) {
                        d.tokenText = $("#serachText").val();
                        d.status = $("#selectStatus").val();
                        d.selectAgen = $("#selectAgen").val();
                        d.tungay = $("#tungay").val();
                        d.denngay = $("#denngay").val();
                    }
                },
                "deferRender": true,
                "serverSide": true,
                "columns": [

                    {
                        "data": "id"
                    },
                    {
                        "data": "Code"
                    },
                    {
                        "data": "shipperName"
                    },
                    {
                        "data": "profileText"
                    },
                    {
                        "data": null
                    },
                    {
                        "data": "methodName"
                    },
                    {
                        "data": "COD"
                    },
                    {
                        "data": "ChargeOrder"
                    },
                    {
                        "data": "TotalValue"
                    },
                    {
                        "data" :"stockaddress"
                    },

                    {
                        "data": "created_at"
                    }
                ]

            });

            // var pathway = window.location.pathname.split('/');

            // if (pathway.length == 4) {
            //     var id = pathway[3];
            //     editData(id);
            // }

        }

        var getColor = function(text) {
            if (text === "Tạo mới") return '#46CB18';
            if (text === "Đang xử lý đơn hàng") return '#eaea00';
            if (text === "Đã nhận đơn hàng") return '#E48900';
            if (text === "Giao hàng thành công") return '#9EDE73';
            if (text === "Đang vận chuyển") return '#E48955';
            if (text === "Hủy đơn hàng") return '#fe3e00';
            if (text === "Giao không thành công") return '#ff3b00';
            return "#BE0000";
        };

        function openNewPage() {
            editData(-1);
        }


        function editData(id = -1) {
            if (id > 0) {
                window.location.href = "/admin/thong-tin-don-hang/edit/" + id;
            } else {
                window.location.href = "/admin/quan-ly-don-hang/neworder";
            }


        }

        // function getdataById(id) {
        //     var urlapi = "/admin/api/quan-ly-shop/getDetail";
        //     $.ajax({
        //         type: 'get',
        //         url: urlapi,
        //         data: {
        //             id: id,

        //         },
        //         success: function(data) {
        //             updateBreadCrump(data.title);
        //             BindingBasic(data);

        //             openTab(event, 'London');
        //             bindadditional(data);
        //             activeTab("edit");

        //             getAllCart(data.id);

        //         }
        //     });

        // }

        function bindadditional(data) {

            var image = "http://localhost:8000/storage/" + data.linkShare;
            $("#linkShareImage").attr("src", image);
            $("#linkShare").val(data.linkShare);

            $("#editKeyWord").val(data.keyWord);
            $("#editisActive").val(data.isActive);
            $("#editstatus").val(data.status);
            $("#editrelationCode").val(data.relationCode);

        }

        function deleteMutipleRecord()

        {

            var arr = [];


            var arr = [];
            $('.checkBoxCode:checkbox:checked').each(function() {
                arr.push($(this).val());
            });


            var urlapi = "/admin/api/quan-ly-shop/deleteMutiple";
            $.ajax({
                type: 'post',
                url: urlapi,
                data: {
                    "_token": "{{ csrf_token() }}",
                    "data": arr

                },
                success: function(data) {

                    if (data.success) {

                        toastr.success("Đã xóa thành công");
                        $('#deleteSearchModal').modal('toggle');
                        backToSearch();
                    }
                }
            });

        }
    </script>
</body>
