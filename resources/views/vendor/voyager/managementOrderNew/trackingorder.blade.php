@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
{{-- @section('javascript')
    <script type="text/javascript" src="{{ asset('/js/dashboard.js') }}"></script>
@endsection --}}
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/tracking.css') }}" />

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.2/dist/sweetalert2.all.min.js"></script>
@endsection
{{-- @section('page_title', 'Thống kê') --}}

@section('view')

    <head>
    </head>

    <body>
        <div class="container">
            <div class="main-row">
                <h4>Theo dõi đơn hàng -{{$orderInfo->Code }}</h4>
            </div>
            <div class="main-row">
                <div>
                    <label for="status-note"></label>
                    <textarea name="status-note" id="status-note" placeholder="Ghi chú" cols="60" rows="2"></textarea>
                </div>
                <div>
                    <label for="status">Trạng thái</label>
                    <select name="status" id="status">
                        <option>Chọn trạng thái</option>
                        @foreach ($allStatus as $status)
                            <option value="{{ $status->id }}">{{ $status->text }}</option>
                        @endforeach
                    </select>
                </div>
                <button onclick="updateStatusOrder()" class="btn btn-success save-status-btn" type="button">
                    Cập nhật
                </button>
            </div>
            <div class="main-row">
                <h4 class="header">{{ $orderInfo->statusText }}</h4>
                <div class="content gallery">
                    @php
                            $weekMap = [
                            0 => 'Chủ nhật',
                            1 => 'Thứ 2',
                            2 => 'Thứ 3',
                            3 => 'Thứ 4',
                            4 => 'Thứ 5',
                            5 => 'Thứ 6',
                            6 => 'Thứ 7',
                            ];
                    @endphp
                    <ul>
                        @foreach ($allLog as $item)
                            @php
                                    $dayOfTheWeek = \Carbon\Carbon::now()->dayOfWeek;
                                    $weekday = $weekMap[$dayOfTheWeek];
                            @endphp
                            <li>
                                <div class="step-bar">
                                    <div class="step__circle"></div>
                                    <div class="step__line"></div>
                                </div>
                                <div class="step-info">
                                    <span class="status-text">
                                        @foreach ($allStatus as $status)
                                            @if($status->id == $item->status)
                                                {{ $status->text }}
                                            @endif
                                        @endforeach

                                    </span>
                                    <span class="time">{{ \Carbon\Carbon::parse($item->created_at)->format('H:i')}},{{$weekday }} {{ \Carbon\Carbon::parse($item->created_at)->format('d-m-Y')}}</span>
                                    <span class="note">
                                        {{ $item->content }}
                                    </span>
                                    @if($item->LinkImage != null)
                                    <span class="image-config">
                                        <a href="{{ $item->LinkImage }}"><img width="150px" height="150px" src="{{ $item->LinkImage }}" alt="{{ $item->content }}" title="{{ $item->content }}"/></a>
                                    </span>
                                    @endif
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </body>

    <script type="text/javascript" src="{{ asset('js/lib/lightbox/simple-lightbox.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('js/lib/lightbox/simple-lightbox.css') }}">
    <script>
            var lightbox = new SimpleLightbox('.gallery a', { /* options */ });

function updateStatusOrder() {

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success btn-confirm',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        });

        var dataTransfer = {
            "status":$("#status").val(),
            "editId": window.location.pathname.split("/").pop(),
            "noted": $("#status-note").val()

        };

        $.ajax({
            type: 'POST',
            url: '/admin/api/quan-ly-don-hang/updateStatus',
            data: dataTransfer,
            success: function(data) {

                swalWithBootstrapButtons.fire(
                    'Trạng thái!',
                    'Đơn hàng đã được cập nhật',
                    'success'
                )
                setTimeout(() => {
                    location.reload();
                }, 1000);
            }
          });
        }
    </script>
@endsection
