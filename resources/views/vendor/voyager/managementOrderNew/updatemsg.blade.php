@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
{{-- @section('javascript')
    <script type="text/javascript" src="{{ asset('/js/dashboard.js') }}"></script>
@endsection
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/dashboard.css') }}" />
@endsection --}}
{{-- @section('page_title', 'Thống kê') --}}

@section('view')

    <head>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.2/dist/sweetalert2.all.min.js"></script>
        <script>
            Swal.fire({
                title: 'Cập nhật thành công!',
                text: 'Quay lại trang danh sách đơn hàng?',
                icon: 'success',
                confirmButtonText: 'OK'
            }).then(function() {
                window.location.href = "/admin/danh-sach-don-hang";
            });
        </script>
    </head>

    <body></body>
@endsection
