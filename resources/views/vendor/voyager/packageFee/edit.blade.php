<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">


@if (Auth::user()->role_id !=1)
                
@else
<button onclick="formSubmit()" class="btn btn-primary">Lưu</button>
@endif
           
           
            <button type="button" onclick="backTo()" class="btn btn-primary">Quay trở lại</button>
    </div>
    <div class="tab row">
        <div class="tablinks">Thông tin gói cước</div>
    </div>
    <form role="form" id="formEditData" class="row" onsubmit="return false">
        {{ csrf_field()}}
        <div style="margin-top: 25px"></div>
        <div class="form-group" style="display:none">
            <label for="inputEmail4">id</label>
            <input type="text" readonly class="form-control" id="id" value="{{$dataEdit->id}}" name="editcode"
                placeholder="Mã ">
        </div>

        @if ($dataEdit->code == "")
        <div class="form-group">
            <label for="inputEmail4">Mã gói cước </label>
            <input type="text" class="form-control" id="code" value="{{$dataEdit->code}}" name="editCodeGroup"
                placeholder="Mã gói cước" >
            <span id = "codeError" style="display:none" class="errorInput">Yêu cầu bắt buộc nhập </span>
        </div> 
        @else
        <div class="form-group">
            <label for="inputEmail4">Mã gói cước </label>
            <input type="text" readonly class="form-control" id="code" value="{{$dataEdit->code}}" name="editCodeGroup"
                placeholder="Mã gói cước" >
            <span id = "codeError" style="display:none" class="errorInput">Yêu cầu bắt buộc nhập </span>
        </div> 
        @endif
       
        <div class="form-group">
            <label for="inputEmail4">Tên gói cước </label>
            <input type="text" class="form-control" id="fullName" value="{{$dataEdit->name}}" name="editName"
                placeholder="Tên gói cước">

                <span id = "fullNameError" style="display:none" class="errorInput">Tên gói cước bắt buộc nhập </span>
        </div>

        <div class="form-group">
            <label for="inputEmail4">Cước vận chuyển </label>
            <input type="text" class="form-control" id="chargeFee" value="{{$dataEdit->chargeFee}}" name="chargeFee"
                placeholder="Cước phí giao hàng trên mỗi đơn hàng">

                <span id = "rateShipperError" style="display:none" class="errorInput">Chưa nhập cước giao hàng </span>
        </div>

        <div class="form-group">
            <label for="inputEmail4">Chiết khấu cho shipepr  </label>
            <input type="text" class="form-control" id="rateShipper" value="{{$dataEdit->rateShipper}}" name="rateShipper"
                placeholder=" Phí trả cho Shipper trên mỗi đơn hàng ">

              
                <span id = "rateShipperError" style="display:none" class="errorInput">Chưa nhập chiết khấu cho shipper </span>
               
        </div>

        <div class="form-group">
            <label for="inputEmail4">Độ ựu tiên hiển thị </label>
            <input type="text" class="form-control" id="priorite" value="{{$dataEdit->priorite}}" name="priorite"
                placeholder="Ưu tiên hiển thị theo thứ tự " value="100">

              
              
        </div>
        <div class="form-group">
            <label for="inputEmail4">Gói mặc định</label>
            <label class="switch">
                        @if ($dataEdit->default)
                              <input type="checkbox"  checked name="default">   
                        @else
                             <input type="checkbox"   name="default">  
                        @endif
            </label>
        
        </div>
      
        

        <div class="form-group">
            <label for="inputEmail4">Hình ảnh logo nếu có </label>
            <input type="file" onchange="uploadFile(this)">
             <img id="frame" src="" width="200px" />
             <input type ="text" style = "display:none" name ="logoImage">
         </div>



    
    </form>
</div>

<script>
    function preview() {
  
    var frame =   document.getElementById("frame");

    frame.src=URL.createObjectURL(event.target.files[0]);
}
    function formSubmit () {
        var isHasEror =  0;
        if($("#code").val() == "")
        {
            $("#codeError").show();
            isHasEror ++;
        }
        else {
            $("#codeError").hide();
        }

        if($("#fullName").val() == "")
        {
            $("#fullNameError").show();
            isHasEror ++;
        }
        else {
            $("#fullNameError").hide();
        }

        if($("#rateShipper").val() == "")
        {
            $("#rateShipperError").show();
            isHasEror ++;
        }
        else {
            $("#rateShipperError").hide();
        }
        if(  isHasEror >0)
        {
            return;
        }
        saveData();
}

function saveData()
{
tinyMCE.triggerSave();
var url = "/admin/api/quan-ly-goi-cuoc/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), 
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backTo();
             }

       }
     });

}

function deleteSingle()
{
        var urlapi ="/admin/api/quan-ly-goi-cuoc/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editCode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

 function backTo()
 {
    window.location.href ="/admin/thong-tin-goi-cuoc";
 }

 function uploadFile(element){
    
    
    var file = element.files[0];
    var formData = new FormData();
    formData.append('image', file);
    formData.append('type_slug', 'packageFee');
    formData.append('_token', '{{ csrf_token() }}');


    $.ajax({
        url: '{{ route('uploadImage') }}',  //Server script to process data
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        success: function(data){

            setImageRead(element, data.fullLink,data.shortUrlLink);



        }
    });
}

function setImageRead(fileinput, fullLink,shortUrlLink) {
    
    fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
    // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
    fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


}


</script>