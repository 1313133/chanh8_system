<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">
        @if (Auth::user()->role_id < 2) <button onclick="formSubmit()" class="btn btn-primary">Lưu</button>
            @endif
            <button type="button" onclick="backTo()" class="btn btn-primary">Quay trở lại</button>
    </div>
    <div class="tab row">
        <div class="tablinks">Thông tin gói</div>
    </div>
    <form role="form" id="formEditData" class="row" onsubmit="return false">
        {{ csrf_field()}}
        <div style="margin-top: 25px"></div>
        <div class="form-group" style="display:none">
            <label for="inputEmail4">id</label>
            <input type="text" readonly class="form-control"  id ="editId" value="{{$dataEdit->id}}" name="editId"
                placeholder="Mã ">
        </div>
        @if ($dataEdit->id <1)
        <div class="form-group">
            <label for="inputEmail4">Mã gói</label>
            <input type="text" class="form-control"  value="{{$dataEdit->code}}" name="code"
                placeholder="Name">
        </div>
    
        @else

        <div class="form-group">
            <label for="inputEmail4">Mã gói</label>
            <input type="text" class="form-control" readonly value="{{$dataEdit->code}}" name="code"
                placeholder="Name">
        </div>
    
            
        @endif
        
        <div class="form-group">
            <label for="inputEmail4">Tên gói </label>
            <input type="text" class="form-control"  value="{{$dataEdit->text}}" name="text"
                placeholder="Name">
        </div>
        <div class="form-group">
            <label for="inputEmail4">Số tháng </label>
            <input type="text" class="form-control"  value="{{$dataEdit->value}}" name="value"
                placeholder="Name">
        </div>
        <div class="form-group">
            <label for="inputEmail4">Số tháng khuyến mãi </label>
            <input type="text" class="form-control"  value="{{$dataEdit->voucherMonth}}" name="voucherMonth"
                placeholder="Name">
        </div>


        <div class="form-group">
            <label for="inputEmail4">Giá trị gói</label>
            <input type="text" class="form-control"  value="{{$dataEdit->price}}" name="price"
                placeholder="Name">
        </div>


        
        <div class="form-group">
            <label for="inputEmail4">Mô tả</label>
            <input type="text" class="form-control"  value="{{$dataEdit->description}}" name="description"
                placeholder="Name">
        </div>

        <div class="form-group">
                <label for="inputEmail4">Trạng thái  </label>
                <select  class="form-control"  name="active"  required>
               
                    @if ($dataEdit->active ==1)
                        <option value="1" selected> 
                            Hoạt động
                        </option>
                        <option value="0"> 
                            Không hoạt động
                         </option>
                    @else
                    <option value="0" selected> 
                        Không hoạt động
                     </option>

                     <option value="1"> 
                        Hoạt động
                    </option>
                    
                    @endif
                   
                 
                
                </select>
        </div>
    
    </form>
</div>

<script>
    function formSubmit () {

    saveData();
}

function saveData()
{
tinyMCE.triggerSave();
var url = "/admin/api/goi-shipper/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), 
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backTo();
             }

       }
     });

}

function deleteSingle()
{
        var urlapi ="/admin/api/goi-shipper/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editId").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

 function backTo()
 {
    window.location.href ="/admin/goi-shipper";
 }

</script>