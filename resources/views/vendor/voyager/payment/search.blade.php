<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
            integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
          integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
          crossorigin="anonymous"/>

    <style>
        .checkbox-inline input[type=checkbox],
        .checkbox input[type=checkbox],
        .radio-inline input[type=radio],
        .radio input[type=radio] {
            position: absolute;
            margin-top: 4px \9;
            margin-left: unset !important;
        }

        #loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid rgba(31, 187, 44, 0.637);
            width: 100px;
            height: 100px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
            margin: auto;
        }

        .paginate {
            margin: auto 5px;
        }

    </style>
</head>

<div class="searchArea">
    <form>
        <div class="form-group col-md-6">
            <label for="inputPassword4">Tình trạng thanh toán của Khách Hàng</label>
            <select id="isCustomerPayment" name="isCustomerPayment" class="form-control">
                <option value="">Tất cả</option>
                <option value="0">Chưa chuyển tiền</option>
                <option value="1">Đã chuyển tiền</option>
            </select>
        </div>

        <div class="form-group col-md-12 left-alignment">
            <button type="button" onclick="refeshSearch();" class="btn btn-outline-dark">
                Tìm kiếm
            </button>
        </div>
    </form>
</div>

<div class="searchData">
    <table id="table_id" class="display" style="width:100%">
        <thead>
        <tr>
            <th></th>
            <th>Đại lý</th>
            <th>Shipper</th>
            <th>Gói đăng ký</th>
            <th>Cú pháp thanh toán</th>
            <th>Ngày bắt đầu</th>
            <th>Ngày kết thúc</th>
            <th>Khách hàng</th>
            <th>Xác nhận thanh toán</th>
            <th>Thực hiện</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>

<script>
    window.onload = function () {
        $('#table_id').DataTable({
            "info": false,
            "searching": false,
            "paging": true,
            "columnDefs": [
                {
                    "render": function (data, type, row) {
                        return data["name"] + " (D" + data["id"] + ")";
                    },
                    "targets": 1
                },

                {
                    "render": function (data, type, row) {
                        return data["fullName"] + " (SP" + data["id"] + ")";
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row) {
                        return data["text"] + " (" + data["code"] + ")";
                    },
                    "targets": 3
                },
                {
                    "render": function (data, type, row) {
                        return "D" + row["shop"]["id"] + "S" + row["shipper"]["id"] + "C" + row["package"]["code"];
                    },
                    "targets": 4
                },
                {
                    "render": function (data, type, row) {
                        if (data == null) {
                            return row["created_at"];
                        } else {
                            return data;
                        }
                    },
                    "targets": 5
                },
                {
                    "render": function (data, type, row) {
                        return data;
                    },
                    "targets": 6
                },
                {
                    "render": function (data, type, row) {
                        if (data == 1) {
                            return "<span class='text-info'>Đã chuyển tiền</span>";
                        } else {
                            return "<span class='text-danger'>Chưa chuyển tiền</span>";
                        }
                    },
                    "targets": 7
                },
                {
                    "render": function (data, type, row) {
                        if (data == 1) {
                            return "<span class='text-success'>Đã thanh toán</span>";
                        } else {
                            return "<span class='text-danger'>Chưa thanh toán</span>";
                        }
                    },
                    "targets": 8
                },
                {
                    "render": function (data, type, row) {
                        if (!data) {
                            return '<a href="javascript:void(0)" onclick="approvalPayment(' + row["id"] + ')" class="btn btn-sm btn-default">Xác nhận</a>';
                        } else {
                            return "";
                        }
                    },
                    "targets": 9
                },
            ],
            'select': 'multi',
            "processing": true,
            "language": {
                "processing": "<div id='loader'></div>",
                "paginate": {
                    "previous": "<div class='paginate previous'>Trước</div>",
                    "next": "<div class='paginate next'>Sau</div>",
                }
            },
            "lengthChange": false,
            "ajax": {
                "url": "/admin/api/danh-sach-shipper-tham-gia/getAll",
                "dataType": "json",
                "type": "get",
                data: function (d) {
                    //d.isCustomerPayment = $("#isCustomerPayment").val();
                    // d.tokenText = $("#serachText").val(),
                    //     d.status = $("#selectStatus").val()
                }
            },
            "deferRender": true,
            "serverSide": true,
            "columns": [
                {
                    "data": "id"
                },
                {
                    "data": "shop"
                },
                {
                    "data": "shipper"
                },
                {
                    "data": "package"
                },
                {
                    "data": "packageId"
                },
                {
                    "data": "approvalDate"
                },
                {
                    "data": "expiredDate"
                },
                {
                    "data": "isCustomerPayment"
                }
                ,
                {
                    "data": "isPayment"
                }
                ,
                {
                    "data": "isPayment"
                }
            ]

        });

    }

    function approvalPayment(id) {
        var urlapi = "{{route('paymentConfirm')}}";
        $.ajax({
            type: 'post',
            url: urlapi,
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
            },
            success: function (data) {
                if (data.status == 'ok') {
                    toastr.success("Xác Nhận thành công");
                    refeshSearch();
                } else {
                    toastr.error("Xác Nhận không thành công");
                }
            }
        });
    }
</script>
