@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section ('css')
@endsection
@section('page_title', 'Danh sách ')
@section('search')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    @include('vendor.voyager.pdf.search')
@endsection
{{-- ["allCategoryNews"=>$allCategoryNews,"allLevelAgency"=>$allLevelAgency] --}}

