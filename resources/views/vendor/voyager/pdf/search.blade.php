
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ==" crossorigin="anonymous"></script>

        <style>

.checkbox-inline input[type=checkbox], .checkbox input[type=checkbox], .radio-inline input[type=radio], .radio input[type=radio] {
    position: absolute;
    margin-top: 4px\9;
     margin-left: unset !important;
}
.breadcrumb > li:nth-child(2) ,.breadcrumb > li:nth-child(3){
    display: none;
}
.border{
    border: 1px solid;
}
.row>[class*=col-] {
    margin-bottom: 0px;
}
        </style>
</head>

<div class="searchArea">
    <form>
        <div class="form-row">
          <div class="form-group col-md-6">
            <label for="inputEmail4">Từ khóa</label>
            <input type="text" class="form-control" id ="serachText"  name ="serachText" placeholder="Từ khóa">
          </div>



          <div class="form-group col-md-6">
            <label for="inputPassword4">Dạng tìm kiếm</label>
            <select id="type" name ="type" class="form-control">
                <option value="1">Mã Đơn Hàng</option>
                @if (Auth::user()->role_id < 2)
                <option value="0">ID Đơn Hàng</option>
                @endif
                
             </select>
          </div>



        </div>



        <div class="form-group col-md-12 left-alignment" >
            
            <a class = "btn btn-success" id="lh" href="http://" target="_blank">In Hóa đơn</a>
            <button type  = "button" id="btnsearch" onclick="refeshSearch()" class = "btn btn-outline-dark">

                Tìm kiếm

            </button>


        </div>
        <div class="form-group col-md-12 text-center" id="er">
            
            <p class="text-danger">không tìm thấy kết quả vui lòng kiểm tra lại </p>


        </div>
    </form>
</div>

<div class="searchData">
    <div class="container" >
        <div class="row " >
            <div class="col-md-6 col-xl-6 border text-center border-dark" style="height: 150px">
                <div class="text-center">
                    <img src="https://chanh8.com/assets/img/logo/logo_header.jpg" class="img-fluid rounded" alt="..."
                        style="max-height: 130px">
                </div>
            </div>
            <div class="col-md-6 col-xl-6 border border-dark" style="height: 150px">
                <p style="bottom: 15px;position: absolute;">mã vận đơn: <b id="codevd"></b></p>
                <p style="bottom: -5px;position: absolute;">mã đơn hàng: <b id="code"></b></p>
            </div>
        </div>
        <div class="row border" style="height: 120px">
            <div class="col-md-6 col-xl-6 ">
                <p class="m-0" style="margin-top:10px!important">Từ: <b id="ns"></b> </p class="m-0">
                <p class="m-0">SĐT: <b id="sdts"></b> </p class="m-0">
                <p class="m-0" id="adds"></p>
            </div>
            <div class="col-md-6 col-xl-6 ">
                <p class="m-0" style="margin-top:10px!important">Đến: <b id="nr"></b> </p >
                <p class="m-0">SĐT: <b id="sdtr"></b> </p class="m-0">
                <p class="m-0" id="addr"></p>
            </div>
        </div>
        <div class="row  border" >
            <div class="col-md-9 " style="font-size: 18px" style="height: 450px">
                <p class="m-0" style="margin-top:10px!important"><b>Nội dung hoặc ghi chú đơn hàng</b> </p>
                <p class="m-0">hình thức: <b id="torder"></b> </p>
                <p class="m-0">phương thức vận chuyển: <b id="mtrans"></b> </p>
                <p class="m-0">Mô tả: <b id="desr"></b> </p>
            </div>
            <div class="col-md-3  p-0" style="height: 450px; padding: 0">
                <div style="height: 40%;position: relative" class="border " ><div class="qr" id="qr" style="
                    position: absolute;
                    top: 50%;
                    left: 50%;
                    margin-top: -100px;
                    margin-left: -100px;
                ">
                    <img id= "qrimg" src="">
            </div></div>
                <div style="height: 30%" class="border "></div>
                <div style="height: 30%" class="border text-center">
                    <p class="m-0" style="margin-top:10px!important">Ngày xuất đơn</p>
                    <p>{{ date('d-m-Y H:i') }}</p>
                </div>
            </div>
        </div>
        <div class="row " >
            <div class="col-md-6 border text-center border-dark" style="position: relative;height: 150px">
                <div class="text-center " style="margin-top:10px!important; ">
                    <h3>Tiền thu hộ: <br> <br><b id=pirce> </b>VNĐ</h3>
                </div>
            </div>
            <div class="col-md-6 border border-dark text-center" style="position: relative;height: 150px">
                <h3 style="margin-top:10px!important"><b>Chữ kí người nhận</b> </h3>
            </div>
        </div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function (){
        $('a#lh').hide();
        $('#er').hide();
        $(document).on('click','button[id="btnsearch"]', function(){
            var arr = $('#serachText').val();
            var a = $('#type').val();
            var urlapi ="/api/searchpdf";
        $.ajax({
            url: urlapi,
            method:'POST',
            data:{serachText:arr , type:a},
            success: function(result){
               
                console.log(result);
                if (!result.message) {
                    $('#code').html(result[0].code);
                  
                $('#ns').html(result[0].sender);
                $('#sdts').html(result[0].sendermobile);
                $('#adds').html(result[0].senderaddress);
                $('#nr').html(result[0].customer);
                $('#sdtr').html(result[0].customermobile);
                $('#addr').html(result[0].customeraddress);
                $('#desr').html(result[0].description);
                $('#torder').html(result[0].TypeOrderId);
                $('#mtrans').html(result[0].Methodtrans);
                $('#pirce').html(result[0].price);
                $('#lh').attr('href',result[0].link);
                $('a#lh').show();
                $('#er').hide();
                    
                } else {
                    $('#er').show();
                    $('a#lh').hide();
                    $('#code').html('');
                    $('#ns').html('');
                    $('#sdts').html('');
                    $('#adds').html('');
                    $('#nr').html('');
                    $('#sdtr').html('');
                    $('#addr').html('');
                    $('#desr').html('');
                    $('#torder').html('');
                    $('#mtrans').html('');
                    $('#pirce').html('');
                    $('#lh').attr('href','#');
                }
               
                // console.log(result[0].code);
            }
        });
        
        });
    });
</script>