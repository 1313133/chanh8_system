@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/edit.css') }}">
@endsection
@section('page_title', 'Thông tin khách hàng')
@section('search')


@include('vendor.voyager.province.edit')


<script>


var data = {{ $dataEdit }}


$( document ).ready(function() {

if(data.id <0)
{
    updateBreadCrump("thêm mới");
}
else
{
    updateBreadCrump(data.title);
}
// BindingBasic(data);
// bindadditional(data);

activeTab("search",true);
});
</script>
@endsection
