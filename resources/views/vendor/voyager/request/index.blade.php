@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
@endsection
@section('javascript')
    <script type="text/javascript" src="{{ asset('/js/customadmin.js') }}"></script>
@endsection
@section('page_title', 'Quản lý đăng kí đại lý')
@section('search')
    @include('vendor.voyager.request.search',["allCategoryNews"=>$allCategoryNews,"allLevelAgency"=>$allLevelAgency])
@endsection
@section('edit')

    @include('vendor.voyager.request.edit',["allCategoryNews"=>$allCategoryNews,"allLevelAgency"=>$allLevelAgency])
@endsection
