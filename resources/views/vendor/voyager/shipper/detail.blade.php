@php
$isEdit = true;
@endphp
@extends('vendor.voyager.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('/css/edit.css') }}">
@endsection
@section('page_title', 'Thông tin shipper')
@section('search')

@include('vendor.voyager.shipper.edit')
<script>

var data ={!! json_encode($dataEdit ) !!}

</script>
@endsection
