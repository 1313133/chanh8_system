<script>
function openTab(evt, cityName) {
  // Declare all variables
  var i, tabcontent, tablinks;

  // Get all elements with class="tabcontent" and hide them
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  // Get all elements with class="tablinks" and remove the class "active"
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  // Show the current tab, and add an "active" class to the button that opened the tab
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>

<div class="editArea">
    <div class="form-group col-md-12 left-alignment" style="
    text-align: right;
    float: right;
">

        @php
            $currentUrl = str_replace('/send', '', url()->current());
            $check = strpos($currentUrl,'them-moi');
        @endphp
        <button  onclick="formSubmit()" class="btn btn-primary">Lưu</button>
        @if (!$check    )
        <button  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Đổi mật khẩu</button>

        
        @endif
        <button type="button" onclick ="backTo()" class="btn btn-primary">Quay trở lại</button>
     
    </div>



    <div class="tab row">
        <div class="tablinks">Thông tin shipper</div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
     </div>

      <!-- Tab content -->

      <form role="form" id ="formEditData" class="row mt-2" onsubmit="return false"  >
        {{ csrf_field()}}

        <div class="form-group" style="display:none">
            <label for="inputEmail4">Mã shipper</label>
            <input type="text" readonly class="form-control" id ="id" value="{{$dataEdit->id}}"    name ="editcode" placeholder="Mã shipper">
        </div>

        <div class="form-group" >
            <label for="inputEmail4" class="requriedInput">Tên shipper</label>
        <input type="text" class="form-control" id ="fullName" value="{{$dataEdit->fullName}}"    name ="editfullName" placeholder="Họ và tên">
        </div>

        <div class="form-group">
            <label for="inputEmail4" class="requriedInput">Số điện thoại</label>
            <input type="text" class="form-control" id ="editmobilePhone" name ="mobilePhone"  value="{{$dataEdit->mobilePhone}}"  placeholder="Số điện thoại">
            <span style="color: #da5d5d" id="mobilePhoneError"></span>
        </div>

        @if ($dataEdit->id >0)
            
        @else
        <div class="form-group">
            <label for="inputEmail4" class="requriedInput">Mật khẩu:  </label>
            <input type="text" class="form-control" id ="editPasword" name ="editPasword"    placeholder="Nếu để trống, thì mật khẩu là số điện thoại">
        </div>

        @endif

        <div class="form-group" class="requriedInput">
            <label for="inputEmail4">Địa chỉ</label>
            <input type="text"  class="form-control" id ="editaddresssInfo" value="{{$dataEdit->addresssInfo}}" name ="editaddresssInfo" placeholder="Đia chỉ">
        </div>




        @include('vendor.voyager.location.compomentAddress')
        @if (Auth::user()->role_id < 2)
            @if (strpos(url()->current(),'them-moi') != null)
                
                
            @else
            <div class="form-group">
                <label for="inputEmail4">Quản lý</label>
                <input type="text"  class="form-control" id ="editaddresssInfo" value="{{$dataEdit->agnetname}}"  placeholder="Đia chỉ" disabled>
            </div>
            @endif
        @endif
        <div class="form-group">
            <label class="requriedInput" >Trạng thái</label>
            <select class="form-control custom-validate-status" id="editstatus" name="editstatus">
                @if ($dataEdit->status =="0")
                    <option value="0" selected  >Không hoạt động</option>
                    <option value="1"  >Hoạt động</option>
                @else
                    <option value="0"  >Không hoạt động</option>
                    <option value="1" selected  >Hoạt động</option>
                @endif
             </select>
         </div>


    </form>
    <iframe id="form_target" name="form_target" style="display:none"></iframe>
    <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
            enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
        <input name="image" id="upload_file" type="file"
                 onchange="$('#my_form').submit();this.value='';">
        <input type="hidden" name="type_slug" id="type_slug" value="news">
        {{ csrf_field() }}
    </form>



</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thay đổi mật khẩu</h5>
        </div>
        <div class="modal-body">
            <form role="form" id ="resetpass" onsubmit="return false"  >
                {{ csrf_field()}}
                <div class="form-group" >
                    <label for="inputEmail4">Mật khẩu cũ </label>
                <input type="password" class="form-control" id ="curpassword"     name ="curpassword" placeholder="Mật khẩu cũ">
                </div>
                <div class="form-group">
                    <label for="inputEmail4">Mật khẩu mới</label>
                    <input type="password" class="form-control" id ="newpassword" name ="newpassword" placeholder="Mật khẩu mới">
                </div>
                <div class="form-group">
                    <label for="inputEmail4">Nhập lại Mật khẩu mới</label>
                    <input type="password" class="form-control" id ="password_confirmation" name ="password_confirmation" placeholder="Nhập lại Mật khẩu mới">
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id ="id" name ="id" value="{{$dataEdit->id}}" placeholder="Nhập lại Mật khẩu mới">
                </div>
       
           
        
            </form>
        </div>
        <div class="modal-footer">
            <button type="button"  onclick="resetPass()" class="btn btn-primary">Lưu thay đổi </button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
      </div>
    </div>
  </div>






<div class="modal fade" id="popupInfomation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" >THÔNG TIN HƯU ÍCH</h5>
        </div>
        <div class="modal-body">
            
            <div class="popup-image"> 
                <img src="/banner.png"> 

            </div>
        </div>
        <div class="modal-footer">
            <button type="button"  class="btn btn-primary"  data-dismiss="modal">Tôi đã hiểu và tiếp tục tạo </button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Quay lại trang thông tin</button>
        </div>
      </div>
    </div>
  </div>





<script >

document.addEventListener("DOMContentLoaded", function(event) {
    bindDatinh(data);
    $("#popupInfomation").modal();

});

function formSubmit () {

    saveData();
}




function openUploadFile(element)
{
    element.parentElement.getElementsByTagName("input")[0].click();
}
function initView(data)
{

    if( data.linkShare ==null || data.linkShare =="")
    {
        return;
    }
    setValueImageControl("linkShare",data.linkShare);
}



function setValueImageControl(id,value)
{
 var inputtextLink = document.getElementById(id);
 inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/"+value;
// inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
}

function setImageRead(fileinput, fullLink,shortUrlLink) {

    fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
    // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
    fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;


}

function uploadFile(element){
     console.log(element);
    // return;

    var file = element.files[0];
    var formData = new FormData();
    formData.append('image', file);
    formData.append('type_slug', 'news');
    formData.append('_token', '{{ csrf_token() }}');


    $.ajax({
        url: '{{ route('uploadImage') }}',  //Server script to process data
        type: 'POST',
        processData: false,
        contentType: false,
        data: formData,
        success: function(data){

            setImageRead(element, data.fullLink,data.shortUrlLink);



        }
    });
}


function saveData()
{
    if( !validateAllInputHasRequired())
 {
     return;
 }
  
// e.preventDefault(); // avoid to execute the actual submit of the form.
tinyMCE.triggerSave();
var url = "/admin/api/quan-ly-shippers/CreateOrEdit";
$.ajax({
       type: "POST",
       url: url,
       data: $("#formEditData").serialize(), // serializes the form's elements.
       success: function(data)
       {
             if(data.success)
             {
                toastr.success("Cập nhật thành công");
                backTo();
             }
             else if(data.message != null){
                 const objError = data.message;
                 Object.keys(objError).forEach(kName => {
                     $(`#${kName}Error`).text(objError[kName]);
                 })

                // toastr.error(data.message);
             }

       }
     });

}




function resetPass()
    {
        tinyMCE.triggerSave();
        var url = "/api/reset";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#resetpass").serialize(), // serializes the form's elements.
            success: function(data)
            {
                    if(data.mess)
                    {
                        toastr.success("Cập nhật thành công");
                        backTo();
                    }
                    else{
                        toastr.error(data.error);
                    }
        
            },
            error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    toastr.error(err.error);
                }
            }).done(function(data) {
                    console.log(data);
                });
        
    }
function deleteSingle()
{
        var urlapi ="/admin/api/quan-ly-shop/delete";

        $.ajax({
            type:'post',
            url:urlapi,
            data:
            {
                "_token": "{{ csrf_token() }}",
                "code": $("#editcode").val()

            },
            success:function(data) {

                    if(data.success)
                    {
                        location.reload();
                    }
            }
        });

 }

 function backTo()
 {
    window.location.href ="/admin/quan-ly-shippers";
 }

</script>


