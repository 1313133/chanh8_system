<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js"
            integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ=="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/fontawesome.min.css"
          integrity="sha512-kJ30H6g4NGhWopgdseRb8wTsyllFUYIx3hiUwmGAkgA9B/JbzUBDQVr2VVlWGde6sdBVOG7oU8AL35ORDuMm8g=="
          crossorigin="anonymous"/>

    <style>
        .checkbox-inline input[type=checkbox],
        .checkbox input[type=checkbox],
        .radio-inline input[type=radio],
        .radio input[type=radio] {
            position: absolute;
            margin-top: 4px \9;
            margin-left: unset !important;
        }

        #loader {
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid rgba(31, 187, 44, 0.637);
            width: 100px;
            height: 100px;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
            margin: auto;
        }

        .paginate {
            margin: auto 5px;
        }
    </style>
</head>

<div class="searchArea">
    <form>
        <div class="form-row">
            <div class="form-group col-md-6">
                <label for="inputEmail4">Từ khóa</label>
                <input type="text" class="form-control" id="serachText" name="serachText" placeholder="Từ khóa">
            </div>


            <div class="form-group col-md-6">
                <label for="inputPassword4">Trạng thái</label>
                <select id="selectStatus" name="status" class="form-control">
                    <option value="All">Tất cả</option>
                    <option value="1">Hoạt động</option>
                    <option value="0">Không hoạt động</option>
                </select>
            </div>
            <div class="form-group col-md-6">
                <label for="inputPassword4">Đại lý</label>
                <select id="selectagentcy" name="agency" class="form-control">
                    <option>Tất cả</option>
                    @foreach ($allagent as $item)
                        <option value="{{ $item->id }}">{{ $item->fullName }}</option>
                    @endforeach
                </select>
            </div>


        </div>


        <div class="form-group col-md-12 left-alignment">
            {{-- <button type="submit" class="btn btn-primary">Search</button> --}}

            <button type="button" onclick="refeshSearch();" class="btn btn-outline-dark">

                Tìm kiếm

            </button>


        </div>
    </form>

    <div class="form-group col-md-12 left-alignment">
        <button type="button" data-toggle="modal" data-target="#bankInfoGeneral" class="btn btn-warning mr-2">Thông tin chuyển tiền</button>
        <button type="button" onclick="openNavPage()" class="btn btn-info mr-2">Xoá</button>
        <button type="button" onclick="openNewPage(-1)" class="btn btn-info">Thêm mới</button>
    </div>
</div>

<div class="searchData">
    <table id="table_id" class="display" style="width:100%">
        <thead>
        <tr>
            <th></th>
            <th>ID</th>
            <th>Tên shipper</th>
            <th>Số điện thoại</th>
            <th>Địa chỉ</th>
            <th>Ngày tạo</th>
            <th>Trạng thái</th>
            <th>Tình trạng dịch vụ</th>
            <th>Thực hiện</th>
        </tr>
        </thead>


        <tbody>
        </tbody>

    </table>

</div>

<div class="modal fade" id="choosePackageDialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">

                <h4>Đăng ký nâng cấp cho Shipper: <span class="shipper_name label label-info"></span></h4>

                <form>
                    <div class="form-group">
                        <input id="shipperIdSelect" type="hidden" value="">
                        <select id="select-package" class="form-control">
                            @foreach($allPackage as $package)
                                @if($package->code == "MP")
                                    <option value="{{$package->id}}"> {{$package->text}}</option>
                                @else
                                    <option value="{{$package->id}}"> {{$package->text}} ({{$package->value}}ngày)
                                    </option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                <button type="button" class="btn btn-primary" onclick="registerPackage()">Đăng ký nâng cấp</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="bankInfoGeneral" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body ">
                <h4>Thông tin tài khoản </h4>
                <p>
                    Tên ngân hàng: <span class="text-success">Ngân hàng Ngoại Thương VietComBank</span><br/><br/>
                    Số tài khoản: <span class="text-success">0771000714047</span><br/><br/>
                    Tên công ty: <span class="text-success">Công ty cổ phần Chanh 8</span><br/><br/>
                    Nội dung chuyển tiền: <span class="text-danger paymentCode">(mã chuyển tiền của gói dịch vụ đăng ký)</span><br/><br/>
                    Số tiền: <span class="text-danger paymentValue">(số tiền gói dịch vụ đăng ký)</span><br/><br/>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="bankInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Thông tin tài khoản </h4>
                <p>
                    Tên ngân hàng: <span class="text-success">Ngân hàng Ngoại Thương VietComBank</span><br/><br/>
                    Số tài khoản: <span class="text-success">0771000714047</span><br/><br/>
                    Tên công ty: <span class="text-success">Công ty cổ phần Chanh 8</span><br/><br/>
                    Nội dung chuyển tiền: <span class="text-danger paymentCode">0771000714047</span><br/><br/>
                    Số tiền: <span class="text-danger paymentValue">0771000714047</span><br/><br/>
                </p>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
            </div>
        </div>
    </div>
</div>

<script>
    window.onload = function () {
        $('#table_id').DataTable({
            "info": false,
            "searching": false,
            "paging": true,
            "columnDefs": [{
                'targets': 0,
                'render': function (data, type, row, meta) {
                    if (type === 'display') {
                        data =
                            '<div style="text-align: center;" class=" checkbox"><input type="checkbox" value= "' +
                            row["id"] +
                            '" name ="productCodeList[]"  class="checkBoxCode"><label></label></div>';
                    }

                    return data;
                },
                'checkboxes': {
                    'selectRow': true,
                    'selectAllRender': '<div class="checkbox"><input type="checkbox" class="dt-checkboxes"><label></label></div>'
                }
            },

                {
                    "render": function (data, type, row) {

                        return '<a href ="javascript:void(0)" onclick ="editData(' + row["id"] +
                            ')">' + data + '</a>';
                    },
                    "targets": 2
                },
                {
                    "render": function (data, type, row) {
                        return '<a href ="javascript:void(0)" onclick ="editData(' + row["id"] +
                            ')">' + data + '</a>';
                    },
                    "targets": 3
                },
                {
                    "render": function (data, type, row) {
                        if (data == null) {
                            return "Chưa đăng ký dịch vụ";
                        } else {
                            if(data["isPayment"] == true) {
                                if(row["payments"]["isExpired"] == true) {
                                    return data["packageName"] + "<br> Ngày hết hạn: <span class='text-danger'>"+ data["expiredDate"] +"</span>";
                                } else {

                                    if(data["expiredDate"]!= null) {
                                        return data["packageName"] + "<br/> Ngày hết hạn: <br/>"+ data["expiredDate"];
                                    } else {
                                        return data["packageName"] + '<br/> Ngày hết hạn: <br/><span class="text-success">Không giới hạn</span>';
                                    }
                                }
                            } else {
                                if(row["payments"]["isCustomerPayment"] == true) {
                                    return '<span class="text-info">'+ data["packageName"] +'</span>';
                                } else {
                                    let code = "D{{$user->id}}"+ "S" +row["id"]+ "C" + data["packageTime"];
                                    return '<span class="text-info">'+ data["packageName"] +'</span>' + '<br/> <span class="text-danger">Giá: '+ formatVND(data["price"]) +' </span><br>Mã chuyển tiền:  <br> <span class="text-danger">'+code+'</span>';
                                }

                            }
                        }
                        return '';
                    },
                    "targets": 6
                },
                {
                    "render": function (data, type, row) {

                        if (row["payments"] == null) {
                            return '<span class="text-danger" >Cần nâng cấp</span>';
                        } else {
                            if(row["payments"]["isPayment"] == true) {

                                if(row["payments"]["isExpired"] == true) {
                                    return '<span class="text-danger" >Hết hạn</span>';
                                } else {
                                    return "<span class='text-success'>Đang hoạt động</span>";
                                }
                            } else {
                                if(row["payments"]["isCustomerPayment"] == true) {
                                    return "<span class='text-info'>Đã chuyển tiền <br/> chờ hệ thống xác nhận </span>";
                                } else {
                                    return "<span class='text-warning'>Chờ thanh toán</span>";
                                }
                            }
                        }
                        return "...";

                    },
                    "targets": 7
                },
                {
                    "render": function (data, type, row) {

                        if (row["payments"] == null) {
                            return '<a class="btn btn-sm btn-success" onclick ="setShipperToDialog(' + row["id"] + ',\'' + row["fullName"] + '\')" data-toggle="modal" data-target="#choosePackageDialog">Nâng cấp</a>';
                        } else {
                            if(row["payments"]["isPayment"] == true) {

                                if(row["payments"]["isExpired"] == true) {
                                    return '<a class="btn btn-sm btn-success" onclick ="setShipperToDialog(' + row["id"] + ',\'' + row["fullName"] + '\')" data-toggle="modal" data-target="#choosePackageDialog">Nâng cấp</a>';
                                } else {

                                }
                            } else {
                                if(row["payments"]["isCustomerPayment"] == true) {

                                } else {
                                    return '<a class="btn btn-sm btn-info" onclick ="customerPaymentConfirm(' + row["payments"]["id"] + ')">Xác nhận đã thanh toán</a><br/> <a class="btn btn-sm btn-danger" onclick ="customerPaymentCancel(' + row["payments"]["id"] + ')">Huỷ đăng ký</a>';
                                }
                            }
                        }
                        return "";

                    },
                    "targets": 8
                },

            ],
            'select': 'multi',
            "processing": true,
            "language": {
                "processing": "<div id='loader'></div>",
                "paginate": {
                    "previous": "<div class='paginate previous'>Trước</div>",
                    "next": "<div class='paginate next'>Sau</div>",
                }
            },
            "lengthChange": false,
            "ajax": {
                "url": "/admin/api/quan-ly-shippers/getAll",
                "dataType": "json",
                "type": "get",
                data: function (d) {

                    d.tokenText = $("#serachText").val(),

                        d.status = $("#selectStatus").val(),
                        d.agency = $("#selectagentcy").val()

                }
            },
            "deferRender": true,
            "serverSide": true,
            "columns": [

                {
                    "data": "id"
                },
                {
                    "data": "id"
                },
                {
                    "data": "fullName"
                },
                {
                    "data": "mobilePhone"
                },
                {
                    "data": "addr"
                },
                {
                    "data": "created_at"
                },
                {
                    "data": "payments"
                },
                {
                    "data": "status"
                },
                {
                    "data": "status"
                },
            ]

        });

    }

    function formatVND(num) {
        let p = parseFloat(num).toFixed(2).split(".");
        return p[0].split("").reverse().reduce(function(acc, num, i, orig) {
            return num + (num != "-" && i && !(i % 3) ? "," : "") + acc;
        }, "") + " vnđ";
    }

    function editData(id) {

        window.location.href = "/admin/quan-ly-shippers/" + id;
    }

    function openNewPage() {
        window.location.href = "/admin/quan-ly-shippers/them-moi";
    }

    function openNavPage() {
        $('#deleteSearchModal').modal('toggle');
    }


    function deleteMutipleRecord() {

        var arr = [];


        var arr = [];
        $('.checkBoxCode:checkbox:checked').each(function () {
            arr.push($(this).val());
        });

        var urlapi = "/admin/api/quan-ly-shippers/deleteMutiple";
        $.ajax({
            type: 'post',
            url: urlapi,
            data: {
                "_token": "{{ csrf_token() }}",
                "data": arr

            },
            success: function (data) {
                if (data.success) {
                    $('#deleteSearchModal').modal('hide');
                    toastr.success("Đã xóa thành công");

                    backToSearch();
                } else {
                    $('#deleteSearchModal').modal('hide');
                    toastr.error("Bạn chưa chọn đối tượng");
                }
            }
        });

    }

    function customerPaymentConfirm(id) {
        if(id == null || id == '') {
            return;
        }

        var urlapi = "{{ route("customerPaymentConfirm") }}";
        $.ajax({
            type: 'post',
            url: urlapi,
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
            },
            success: function (data) {
                if (data.status == "ok") {

                    toastr.success("Đã xác nhận thanh toán thành công");
                    refeshSearch();
                    // show thong tin ngan hang
                } else {
                    toastr.error("Đăng ký không thành công");
                }
            }
        });

    }

    function customerPaymentCancel(id) {
        if(id == null || id == '') {
            return;
        }

        var urlapi = "{{ route("customerPaymentCancel") }}";
        $.ajax({
            type: 'post',
            url: urlapi,
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
            },
            success: function (data) {
                if (data.status == "ok") {
                    toastr.success("Đã huỷ thành công");
                    refeshSearch();
                } else {
                    toastr.error("Huỷ không thành công");
                }
            }
        });

    }

    var allPackage = @php echo json_encode($allPackage) @endphp

    function getPackagePrice(packageId) {
        var price = 0;

        for (let i = 0; i < allPackage.length; i++) {
            if(allPackage[i].id == packageId) {
                price = allPackage[i].price;
            }
        }

        return formatVND(price);
    }

    function getPackageCode(packageId) {
        var code = "";

        for (let i = 0; i < allPackage.length; i++) {
            if(allPackage[i].id == packageId) {
                code = allPackage[i].value;
            }
        }

        return code;
    }

    function setShipperToDialog(id, fullName) {
        $(".shipper_name").html(fullName);
        $("#shipperIdSelect").val(id);
    }

    function registerPackage() {

        var urlapi = "{{ route("registerPackage") }}";
        var shipperId =  $("#shipperIdSelect").val();
        var packageId =  $("#select-package").val();

        $.ajax({
            type: 'post',
            url: urlapi,
            data: {
                "_token": "{{ csrf_token() }}",
                "shipperId": shipperId,
                "packageId": packageId,
            },
            success: function (data) {
                if (data.status == "ok") {
                    $('#choosePackageDialog').modal('hide');
                    toastr.success("Đã đăng ký thành công");
                    refeshSearch();
                    // show thong tin ngan hang
                    // set thông tin code, số tiền
                    //shipperId, packageId
                    let paymentCode = "D{{$user->id}}"+ "S" + shipperId + "C" + getPackageCode(packageId);
                    $(".paymentCode").text(paymentCode);

                    log(getPackagePrice(packageId));

                    $(".paymentValue").text(getPackagePrice(packageId));

                    $("#bankInfo").modal('show');
                } else {
                    $('#choosePackageDialog').modal('hide');
                    toastr.error("Đăng ký không thành công");
                }
            }
        });
    }

</script>
