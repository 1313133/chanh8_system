<style>
    .title-module {
        font-weight: 500;
        color: #ffffff;
        font-size: 16px;
        padding-top: 20px;
        padding-left: 10px;
    }

</style>
<div class="side-menu sidebar-inverse">
    <nav class="navbar navbar-default" role="navigation">
        <div class="side-menu-container">
            <div class="navbar-header">
                <a class="navbar-brand" href="{{ route('voyager.dashboard') }}">
                    <div class="logo-icon-container">
                          <img src="https://chanh8.com/assets/img/logo/logo_header.jpg" alt="Logo Icon">
                    </div>
                    <div class="title">ChanhExpress</div>
                </a>
            </div>

         

        </div>
        @if (Auth::user()->role_id ==1)
        @include('vendor.voyager.adminmenu')
        @elseif (Auth::user()->role_id ==2 )
            
        @include('vendor.voyager.adminAgency')
        @elseif(Auth::user()->role_id ==6)
        @include('vendor.voyager.adminBranch')
         @else
        @include('vendor.voyager.adminshipper')
        @endif

    </nav>
</div>
