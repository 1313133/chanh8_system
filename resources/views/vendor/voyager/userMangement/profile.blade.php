
    
    <style>
        .editArea h3 
        {
            text-align: center;
            color: #000;
            font-weight: 700;
        }
        .bottom-button
        {
            text-align: right;
        }
        strong {
            color: #000;
            padding: 5px 0px;
            border-bottom: 1px dashed #cccccc;

        }
    </style>
    <div class="editArea" style="max-width:600px;margin:auto">
        
        <h3>THÔNG TIN CÁ NHÂN </h3>
    
    
        <form role="form" id ="formEditData" class="row" onsubmit="return false"  >
            {{ csrf_field()}}
    
            <div class="form-group" style="display:none">
                <label for="inputEmail4">Mã</label>
                <input type="text" readonly class="form-control" id ="id" value="{{$dataEdit->id}}"    name ="editcode" placeholder="Mã tài xế">
            </div>

            <div class="form-group">
            
                <strong>Thông tin </strong>
            </div>
    
            <div class="form-group" >
                <label for="inputEmail4">Họ tên</label>
            <input type="text" class="form-control" id ="name" value="{{$dataEdit->name}}"    name ="name" placeholder="Họ và tên">
            </div>
    
           
    
            <div class="form-group">
                <label for="inputEmail4">Số điện thoại</label>
                <input type="text" class="form-control" id ="editmobilePhone" name ="phoneNumber"  value="{{$dataEdit->phoneNumber}}"  placeholder="Số điện thoại">
            </div>
            <div class="form-group">
            
            <strong>Thông tin tài khoản </strong>
            </div>
            <div class="form-group">
                <label for="inputEmail4">Tên đăng nhập</label>
                <input type="text" class="form-control" id ="email" name ="email" readonly  value="{{$dataEdit->email}}"  placeholder="Số điện thoại">
            </div>

            @if ($dataEdit->role_id ==1)
            <div class="form-group">
                <label for="inputEmail4">Nhóm quyền</label>
                <select class="form-control" name="roleId">
                     @foreach ($allRoles as $item)
                            @if ($item->id ==  $dataEdit->role_id )
                            <option  selected value ="{{ $item->id }}"> {{ $item->name }}   </option>
                            @else
                            <option  value ="{{ $item->id }}"> {{ $item->name }}   </option>
                            @endif
                    @endforeach
                </select>
               
            </div>
            
            <div class="form-group">
                <label >Trạng thái</label>
                <select class="form-control" id="editstatus" name="editstatus">
                    @if ($dataEdit->active =="0")
                        <option value="0" selected  >Không hoạt động</option>
                        <option value="1"  >Hoạt động</option>
                    @else
                        <option value="0"  >Không hoạt động</option>
                        <option value="1" selected  >Hoạt động</option>
                    @endif
                 </select>
             </div>
             @else
                
             @endif
             <div class="bottom-button">

                <button  onclick="formSubmit()" class="btn btn-primary">Lưu</button>
                <button  class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Đổi mật khẩu</button>
                <button type="button" onclick ="backTo()" class="btn btn-primary">Quay trở lại</button>
        
            </div>
          
            
          
        </form>
       
        <iframe id="form_target" name="form_target" style="display:none"></iframe>
        <form id="my_form" action="{{ route('voyager.upload') }}" target="form_target" method="post"
                enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
            <input name="image" id="upload_file" type="file"
                     onchange="$('#my_form').submit();this.value='';">
            <input type="hidden" name="type_slug" id="type_slug" value="news">
            {{ csrf_field() }}
        </form>
    
    
    </div>
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Thay đổi mật khẩu</h5>
        </div>
        <div class="modal-body">
            <form role="form" id ="resetpass" onsubmit="return false"  >
                {{ csrf_field()}}
                <div class="form-group" >
                    <label for="inputEmail4">Mật khẩu cũ </label>
                <input type="password" class="form-control" id ="curpassword"     name ="curpassword" placeholder="Mật khẩu cũ">
                </div>
                <div class="form-group">
                    <label for="inputEmail4">Mật khẩu mới</label>
                    <input type="password" class="form-control" id ="newpassword" name ="newpassword" placeholder="Mật khẩu mới">
                </div>
                <div class="form-group">
                    <label for="inputEmail4">Nhập lại Mật khẩu mới</label>
                    <input type="password" class="form-control" id ="password_confirmation" name ="password_confirmation" placeholder="Nhập lại Mật khẩu mới">
                </div>
                <div class="form-group">
                    <input type="hidden" class="form-control" id ="id" name ="id" value="{{$dataEdit->id}}" placeholder="Nhập lại Mật khẩu mới">
                </div>
       
           
        
            </form>
        </div>
        <div class="modal-footer">
            <button type="button"  onclick="resetPass()" class="btn btn-primary">Lưu thay đổi </button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
        </div>
      </div>
    </div>
  </div>
    
    
    <script>
    
    
    
    function formSubmit () {
    
        saveData();
    }
    
    
    
    
    function openUploadFile(element)
    {
        element.parentElement.getElementsByTagName("input")[0].click();
    }
    function initView(data)
    {
    
        if( data.linkShare ==null || data.linkShare =="")
        {
            return;
        }
        setValueImageControl("linkShare",data.linkShare);
    }
    
    
    
    function setValueImageControl(id,value)
    {
     var inputtextLink = document.getElementById(id);
     inputtextLink.parentElement.getElementsByTagName("img")[0].src = "/storage/"+value;
    // inputtextLink.parentElement.getElementsByTagName("input")[1].src = "/storage/"+value;
    }
    
    function setImageRead(fileinput, fullLink,shortUrlLink) {
    
        fileinput.parentElement.getElementsByTagName("img")[0].src = fullLink;
        // fileinput.parentElement.getElementsByTagName("input")[0].value = shortUrlLink;
        fileinput.parentElement.getElementsByTagName("input")[1].value = shortUrlLink;
    
    
    }
    
    function uploadFile(element){
    
    
        var file = element.files[0];
        var formData = new FormData();
        formData.append('image', file);
        formData.append('type_slug', 'news');
        formData.append('_token', '{{ csrf_token() }}');
    
    
        $.ajax({
            url: '{{ route('uploadImage') }}',  //Server script to process data
            type: 'POST',
            processData: false,
            contentType: false,
            data: formData,
            success: function(data){
    
                setImageRead(element, data.fullLink,data.shortUrlLink);
    
    
    
            }
        });
    }
    
    
    function saveData()
    {
    
       
    // e.preventDefault(); // avoid to execute the actual submit of the form.
    tinyMCE.triggerSave();
  
    var url = "/admin/thong-tin/cap-nhat-tai-khoan";
    $.ajax({
           type: "POST",
           url: url,
           data: $("#formEditData").serialize(), // serializes the form's elements.
           success: function(data)
           {

            
            
                 if(data.success)
                 {
                    toastr.success("Cập nhật thành công");
                  
                 }
                 else{
                    toastr.error("Số điện thoại này đã được sử dụng bởi 1 shipper khác vui lòng kiểm tra lại thông tin");
                 }
    
           }
         });
    
    }
    
    function resetPass()
    {
        tinyMCE.triggerSave();
        var url = "/api/reset";
        $.ajax({
            type: "POST",
            url: url,
            data: $("#resetpass").serialize(), // serializes the form's elements.
            success: function(data)
            {

              
                    if(data.mess)
                    {
                        toastr.success("Cập nhật thành công");
                        backTo();
                    }
                    else{
                        toastr.error(data.error);
                    }
        
            },
            error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    toastr.error(err.error);
                }
            }).done(function(data) {
                    console.log(data);
                });
        
    }
    
    
    
    function deleteSingle()
    {
            var urlapi ="/admin/api/quan-ly-shop/delete";
    
            $.ajax({
                type:'post',
                url:urlapi,
                data:
                {
                    "_token": "{{ csrf_token() }}",
                    "code": $("#editcode").val()
    
                },
                success:function(data) {
    
                        if(data.success)
                        {
                            location.reload();
                        }
                }
            });
    
     }
    
     function backTo()
     {
        window.location.href ="/admin/quan-ly-tai-khoan";
     }
    
    </script>
    
    
    