
@php
    $homeManagement = Cache::get('homeManagement', []);

@endphp

@extends('layouthomepage')
@section('content')

<div class="main main__home">

    <!-- SLIDE PRODUCTS -->
    <div class="proShow owl-theme owl-carousel">

        @foreach ($homeManagement as $item)

        @if ($item->slug=="")
        <a href="javascript:void(0)" class="proShow__linkImg">
            <img class="proShow__img fit-contain" src="{{Voyager::image($item->linkImage)}}" alt="">
        </a>
        @else
        <a href="{{$item->slug}}" class="proShow__linkImg">
            <img class="proShow__img fit-contain" src="{{Voyager::image($item->linkImage)}}" alt="">
        </a>
        @endif


        @endforeach



    </div>

    <!-- CIRCLE COLOR -->
    <div class="circleSlide">
        <div class="navSlide">
            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>

            <div class="navSlide__item">
            </div>
        </div>
    </div>


</div>
@endsection
