<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//thong tin user

Route::post('/getdistrict', 'Api\AddressController@getdistrict');
Route::post('/getward', 'Api\AddressController@getward');
Route::group(['middleware' => ['cors', 'json.response']], function () {
    Route::post('/login', 'Api\AuthController@login')->name('login.api');
    Route::post('/resgiteragent', 'Api\AuthController@resgiteragent');
    Route::post('/agencylogin', 'Api\AuthController@loginEmail')->name('login.api.agency');
    Route::post('/verify', 'Api\AuthController@otpverify')->name('verify.api');
  
    Route::get('/getstatus/{code}', 'Api\OderController@getstatus');
    Route::get('/getprovince', 'Api\AddressController@getprovince');
    Route::post('/getdistrict', 'Api\AddressController@getdistrict');
    Route::post('/getward', 'Api\AddressController@getward');
    Route::post('/getdistricts', 'Api\AddressController@getdistricts');
    Route::post('/getwards', 'Api\AddressController@getwards');
    Route::get('/get', 'Api\AddressController@get');
    Route::post('/searchpdf', 'Api\OderController@generate');
    Route::get('/getordernote/{id}', 'Api\OderController@getordernote');
    Route::get('/getnote/{id}', 'Api\OderController@getnote');
    Route::post('/reset', 'Api\AuthController@changePassword');
    Route::post('/resetAdmin', 'Api\AuthController@changePasswordAdmin');
});


Route::middleware(['auth:api','json.response','cors'])->group(function () {
    Route::get('/user', 'Api\AuthController@getuserdata')->name('user.api');
    Route::post('/logout', 'Api\AuthController@logout')->name('logout.api');
    Route::get('/getlistorder', 'Api\OderController@getlistorder');
    //  thong tin đơn hàng
    Route::post('/storeorder', 'Api\OderController@Store');
    Route::post('/updateorder/{id}', 'Api\OderController@Update');
    Route::get('/find/{id}', 'Api\OderController@Find');
   
    Route::get('/getdata', 'Api\OderController@getsomeinfortostore');
    //Route::delete('/order/{id}', 'Api\OderController@Destroy');
   
    Route::post('/updateStatus', 'Api\OderController@updateStatus');
  
    // address-sender
    Route::get('/addresssender', 'Api\AddressSenderController@index');
    Route::post('/store-addresssender', 'Api\AddressSenderController@Store');
    Route::post('/update-addresssender/{id}', 'Api\AddressSenderController@update');
    Route::delete('/addresssender/{id}', 'Api\AddressSenderController@Destroy');
    Route::get('/search-addresssender/{phone}', 'Api\AddressSenderController@Search');
    //address-stock 
    Route::get('/addressstock', 'Api\AddressStockController@index');
    Route::post('/store-addressstock', 'Api\AddressStockController@Store');
    Route::post('/update-addressstock/{id}', 'Api\AddressStockController@update');
    Route::delete('/addressstock/{id}', 'Api\AddressStockController@Destroy');
    Route::get('/search-addressstock/{phone}', 'Api\AddressStockController@Search');
    //customer 
    Route::get('/customer', 'Api\CustomerController@index');
    Route::post('/store-customer', 'Api\CustomerController@Store');
    Route::post('/update-customer/{id}', 'Api\CustomerController@update');
    Route::delete('/customer/{id}', 'Api\CustomerController@Destroy');
    Route::get('/search-customer/{phone}', 'Api\CustomerController@Search');
    Route::get('/search/{code}', 'Api\OderController@Search');
    Route::post('/file/upload', 'FileController@mobileUploadImage')->name('uploadImage');
    

    Route::post('/add-order-noted', 'Api\OderController@addNoted');

    Route::get('/get-history-order-noted', 'Api\OderController@getHistoryNote');
  
});
    
