<?php

use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;
use Illuminate\Support\Facades\Artisan;

Route::get('/getFile', 'TaskController@exportFile')->name('index');
Auth::routes(['verify' => true]);
Route::get('login', function () {
    return redirect('/admin');
});
// Route::get('/admin', function () {
//     return redirect('admin/quan-ly-don-hang');
// });

Route::get('/', function () {
    return redirect('admin/quan-ly-don-hang');
    });
Route::get('linkstorage2', 'AdminCustomerkController@editimg');
Route::get('pdf', 'pdfController@index');
Route::get('xuat-hoa-don/{slug}', 'pdfController@index');
Route::get('/sendmail', 'TaskController@index')->name('index');
Route::post('/task', 'TaskController@store')->name('store.task');
Route::delete('/task/{task}', 'TaskController@delete')->name('delete.task');
Route::group(['prefix' => 'admin'], function () {

    Route::get('/quan-ly-don-hang/filterAll', 'AdminOrderNewController@filterAll');
    Voyager::routes();
    Route::get('level-agencies', function () {
        return redirect('admin/quan-ly-cap-bac-dai-ly');
    });
    Route::get('type-agencies', function () {
        return redirect('admin/quan-ly-loai-dai-ly');
    });

    Route::get('/quan-ly-tai-khoan', 'AdminUserController@index');
    Route::get('/quan-ly-tai-khoan/{slug}', 'AdminUserController@edit');
    Route::get('api/quan-ly-tai-khoan/getAll', 'AdminUserController@getAll');

    //
    Route::post('/api/order/addMutipleToCard', 'AdminOrderController@addMutipleToCard')->name('cart');
    Route::get('/thong-tin-don-dat-hang', 'AdminOrderController@informationCartOrder');
    Route::post('/file/upload', 'FileController@upload')->name('uploadImage');
    Route::get('/cart-orders', 'AdminNewsController@news');
    Route::get('typeText/location', 'AdminLocationController@index');
    //
    //agency
    Route::get('/quan-ly-shop', 'AgencyController@index');
    Route::get('/quan-ly-shop/{slug}', 'AgencyController@getDetailInfomation');
    Route::get('api/quan-ly-shop/getAll', 'AgencyController@getAll');
    Route::get('api/quan-ly-shop/getDetail', 'AgencyController@getDetail');
    Route::post('api/quan-ly-shop/CreateOrEdit', 'AgencyController@createOrUpdate');
    Route::post('api/quan-ly-shop/delete', 'AgencyController@delete');
    Route::post('api/quan-ly-shop/deleteMutiple', 'AgencyController@deleteMutiple');
    Route::get('api/quan-ly-shop/getAllDetail', 'AgencyController@getAllDetail');
    Route::get('/quan-ly-shop/{slug}', 'AgencyController@getDetailInfomation');

    Route::get('/quan-ly-yeu-cau', 'RequestController@index');
    Route::get('/quan-ly-yeu-cau/{slug}', 'RequestController@getDetailInfomation');
    Route::get('api/quan-ly-yeu-cau/getAll', 'RequestController@getAll');
    Route::get('api/quan-ly-yeu-cau/getDetail', 'RequestController@getDetail');
    Route::post('api/quan-ly-yeu-cau/delete', 'RequestController@delete');
    Route::post('api/quan-ly-yeu-cau/deleteMutiple', 'RequestController@deleteMutiple');
    Route::get('api/quan-ly-yeu-cau/getAllDetail', 'RequestController@getAllDetail');
    Route::post('api/quan-ly-yeu-cau', 'RequestController@updateStatus');

    Route::get('/quan-ly-cap-bac-dai-ly', 'LvAgencyController@index');
    Route::get('/quan-ly-cap-bac-dai-ly/{slug}', 'LvAgencyController@getDetailInfomation');
    Route::get('api/quan-ly-cap-bac-dai-ly/getAll', 'LvAgencyController@getAll');
    Route::post('api/quan-ly-cap-bac-dai-ly/deleteMutiple', 'LvAgencyController@deleteMutiple');
    Route::post('api/quan-ly-cap-bac-dai-ly/CreateOrEdit', 'LvAgencyController@CreateOrEdit');

    Route::get('/quan-ly-loai-dai-ly', 'TypeAgencyController@index');
    Route::get('/quan-ly-loai-dai-ly/{slug}', 'TypeAgencyController@getDetailInfomation');
    Route::get('api/quan-ly-loai-dai-ly/getAll', 'TypeAgencyController@getAll');
    Route::post('api/quan-ly-loai-dai-ly/deleteMutiple', 'TypeAgencyController@deleteMutiple');
    Route::post('api/quan-ly-loai-dai-ly/CreateOrEdit', 'TypeAgencyController@CreateOrEdit');


    Route::get('management-product', 'AdminProductController@index');
    Route::post('/import-Product', 'AdminProductController@import');
    Route::get('/management-product/add', 'AdminProductController@addNew');
    Route::get('management-product/{slug}', 'AdminProductController@Detail');
    Route::get('api/product/GetAll', 'AdminProductController@GetAll');
    Route::get('api/product/getDetail', 'AdminProductController@getDetail');
    Route::post('api/product/CreateOrEdit', 'AdminProductController@CreateOrUpdate');
    Route::post('api/product/delete', 'AdminProductController@delete');
    Route::post('api/product/deleteMutiple', 'AdminProductController@deleteMutiple');
    Route::get('/search_product', 'AdminOrderController@index');
    Route::get('/search_product/GetAll', 'AdminOrderController@getAll');
    Route::post('/check-outAgency', 'AdminOrderController@checkout')->name('cart');
    //locations
    Route::get('/location', 'AdminLocationController@index');
    Route::get('api/location/getAll', 'AdminLocationController@GetAll');
    Route::get('api/location/getDetail', 'AdminLocationController@view');
    Route::post('api/location/CreateOrEdit', 'AdminLocationController@createOrUpdate');
    Route::post('api/location/delete', 'AdminLocationController@delete');
    Route::post('api/location/deleteMutiple', 'AdminLocationController@deleteMutiple');
    Route::get('api/location/getAllDetail', 'AdminLocationController@getAllDetail');
    Route::get('/location/getAllTinh', 'LocationController@getAllTinh');
    Route::get('/location/getallHuyen', 'LocationController@getallHuyen');
    Route::get('/location/{slug}', 'AdminLocationController@getDetailInfomation');

    Route::get('/quan-ly-don-hang', 'AdminOrderNewController@news')->name('orderindex');
    Route::get('/quan-ly-don-hang/getall', 'AdminOrderNewController@getAll');
    Route::get('/quan-ly-don-hang/{id}', 'AdminOrderNewController@editOrder');
    Route::post('/quan-ly-don-hang/CreateOrEdit', 'AdminOrderNewController@createOrUpdate');
    Route::post('/api/quan-ly-don-hang/updateStatus', 'AdminOrderNewController@UpdateStatus');

    Route::get('/quan-ly-shippers', 'AdminShipperController@index');
    Route::get('api/quan-ly-shippers/getAll', 'AdminShipperController@getAll');
    Route::get('api/quan-ly-shippers/getDetail', 'AdminShipperController@getDetail');
    Route::post('api/quan-ly-shippers/CreateOrEdit', 'AdminShipperController@createOrUpdate');
    Route::post('api/quan-ly-shippers/delete', 'AdminShipperController@delete');
    Route::post('api/quan-ly-shippers/deleteMutiple', 'AdminShipperController@deleteMutiple');
    Route::get('api/quan-ly-shippers/getAllDetail', 'AdminShipperController@getAllDetail');
    Route::get('/quan-ly-shippers/{slug}', 'AdminShipperController@getDetailInfomation');

    Route::post('api/quan-ly-tai-khoan/createAcountAdmin', 'AdminShipperController@createAccount');

    Route::get('/management-addrsender', 'AdminAddressSenderController@index');
    Route::get('api/management-addrsender/getAll', 'AdminAddressSenderController@getAll');
    Route::post('api/management-addrsender/CreateOrEdit', 'AdminAddressSenderController@createOrUpdate');
    Route::post('api/management-addrsender/deleteMutiple', 'AdminAddressSenderController@deleteMutiple');
    Route::get('/management-addrsender/{slug}', 'AdminAddressSenderController@getDetailInfomation');

    Route::get('/dia-chi-lay-hang', 'AdminAddressStockController@index');
    Route::get('api/dia-chi-lay-hang/getAll', 'AdminAddressStockController@getAll');
    Route::post('api/dia-chi-lay-hang/CreateOrEdit', 'AdminAddressStockController@createOrUpdate');
    Route::post('api/dia-chi-lay-hang/createOrUpdateNOBranch', 'AdminAddressStockController@createOrUpdateNOBranch');
    Route::post('api/dia-chi-lay-hang/deleteMutiple', 'AdminAddressStockController@deleteMutiple');
    Route::get('/dia-chi-lay-hang/{slug}', 'AdminAddressStockController@getDetailInfomation');

    Route::get('/dia-chi-tra-hang', 'AdminReturnAddressStockController@index');
    Route::get('api/dia-chi-tra-hang/getAll', 'AdminReturnAddressStockController@getAll');
    Route::post('api/dia-chi-tra-hang/CreateOrEdit', 'AdminReturnAddressStockController@createOrUpdate');
    Route::post('api/dia-chi-tra-hang/createOrUpdateNOBranch', 'AdminReturnAddressStockController@createOrUpdateNOBranch');
    Route::post('api/dia-chi-tra-hang/deleteMutiple', 'AdminReturnAddressStockController@deleteMutiple');
    Route::get('/dia-chi-tra-hang/{slug}', 'AdminReturnAddressStockController@getDetailInfomation');

    Route::get('/management-customer', 'AdminCustomerkController@index');
    Route::get('api/management-customer/getAll', 'AdminCustomerkController@getAll');
    Route::post('api/management-customer/CreateOrEdit', 'AdminCustomerkController@createOrUpdate');
    Route::post('api/management-customer/deleteMutiple', 'AdminCustomerkController@deleteMutiple');
    Route::post('api/management-customer/delete', 'AdminCustomerkController@delete');
    Route::get('/management-customer/{slug}', 'AdminCustomerkController@getDetailInfomation');

    Route::get('/management-province', 'AdminProvinceController@index');
    Route::get('api/management-province/getAll', 'AdminProvinceController@getAll');
    Route::post('api/management-province/CreateOrEdit', 'AdminProvinceController@createOrUpdate');
    Route::post('api/management-province/deleteMutiple', 'AdminProvinceController@deleteMutiple');
    Route::post('api/management-province/delete', 'AdminProvinceController@delete');
    Route::get('/management-province/{slug}', 'AdminProvinceController@getDetailInfomation');


    Route::get('/management-district', 'AdminDistrictController@index');
    Route::get('api/management-district/getAll', 'AdminDistrictController@getAll');
    Route::post('api/management-district/CreateOrEdit', 'AdminDistrictController@createOrUpdate');
    Route::post('api/management-district/deleteMutiple', 'AdminDistrictController@deleteMutiple');
    Route::post('api/management-district/delete', 'AdminDistrictController@delete');
    Route::get('/management-district/{slug}', 'AdminDistrictController@getDetailInfomation');

    Route::get('/management-ward', 'AdminWardController@index');
    Route::get('api/management-ward/getAll', 'AdminWardController@getAll');
    Route::post('api/management-ward/CreateOrEdit', 'AdminWardController@createOrUpdate');
    Route::post('api/management-ward/deleteMutiple', 'AdminWardController@deleteMutiple');
    Route::post('api/management-ward/delete', 'AdminWardController@delete');
    Route::get('/management-ward/{slug}', 'AdminWardController@getDetailInfomation');

    Route::get('/thong-ke-don-hang', 'OrderDashboardController@index');
    Route::post('/thong-ke-don-hang', 'OrderDashboardController@search')->name('filter');
    Route::get('/tao-don-hang-moi', 'OrderNewController@create');

    Route::get('/thong-tin-don-hang/edit/{slug}', 'OrderNewController@information');
    Route::get('/thong-tin-don-hang/tracking/{slug}', 'OrderNewController@track');
    Route::post('/thong-tin-don-hang/update', 'OrderNewController@update');
    Route::post('/tao-don-hang-moi', 'OrderNewController@store')->name('createorder');
    Route::get('/danh-sach-don-hang', 'OrderNewController@show');
    Route::get('/genpdf', 'Api\OderController@generate_pdf')->name('genpdf');
    Route::get('/printpdf/{code}', 'Api\OderController@generatepdf');

    // Route::get('/orders/export', 'OrderDashboardController@export');
    Route::get('/orders-export', 'OrderDashboardController@export')->name('exportexcel');

    Route::get('/loai-hang-hoa', 'GroupFoodController@index');
    Route::get('/quan-ly-nhom-hang-hoa/{slug}', 'GroupFoodController@getDetailInfomation');


    Route::get('api/quan-ly-nhom-hang-hoa/getAll', 'GroupFoodController@getAll');
    Route::post('api/quan-ly-nhom-hang-hoa/deleteMutiple', 'GroupFoodController@deleteMutiple');
    Route::post('api/quan-ly-nhom-hang-hoa/CreateOrEdit', 'GroupFoodController@CreateOrEdit');



    Route::get('/thong-tin-goi-cuoc', 'PackageFreeController@index');
    Route::get('/quan-ly-goi-cuoc/{slug}', 'PackageFreeController@getDetailInfomation');


    Route::get('api/quan-ly-goi-cuoc/getAll', 'PackageFreeController@getAll');
    Route::post('api/quan-ly-goi-cuoc/deleteMutiple', 'PackageFreeController@deleteMutiple');
    Route::post('api/quan-ly-goi-cuoc/CreateOrEdit', 'PackageFreeController@CreateOrEdit');

    Route::get('/thong-tin', 'AdminUserController@profile');
    Route::post('/thong-tin/cap-nhat-tai-khoan', 'AdminUserController@update');

    Route::get('/goi-shipper', 'PackageShipperController@index');
    Route::get('/goi-shipper/{slug}', 'PackageShipperController@getDetailInfomation');

    Route::get('api/goi-shipper/getAll', 'PackageShipperController@getAll');
    Route::post('api/goi-shipper/deleteMutiple', 'PackageShipperController@deleteMutiple');
    Route::post('api/goi-shipper/CreateOrEdit', 'PackageShipperController@CreateOrEdit');


    Route::get('/danh-sach-shipper-tham-gia', 'PaymentController@index');
//    Route::get('/danh-sach-shipper-tham-gia/{slug}', 'ShipperRegisterPackageController@getDetailInfomation');


    Route::get('/api/danh-sach-shipper-tham-gia/getAll', 'PaymentController@getAll');
    Route::post('api/payment/paymentConfirm', 'PaymentController@paymentConfirm')->name('paymentConfirm');
    Route::post('api/payment/register', 'PaymentController@registerPackage')->name('registerPackage');
    Route::post('api/payment/customer-payment-confirm', 'PaymentController@customerPaymentConfirm')->name('customerPaymentConfirm');
    Route::post('api/payment/customer-payment-cancel', 'PaymentController@customerPaymentCancel')->name('customerPaymentCancel');

    
});

Route::get('/', 'HomePageController@index')->name('homePage');

Route::post('/addresssender/creatorUpdate', 'Api\AddressSenderController@CreateOrupdate');

Route::post('/createOrUpdateCustomer', 'Api\CustomerController@store');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/tao-ma-don-hang', 'HomeController@index')->name('creae-order');
Route::get("/create-code", 'ShipperRegisterPackageController@agencyRegisterSipper');
Route::get('/get-full-address-text-display', 'LocationController@getFullAdress')->name('creae-order');
Route::get('/test-image', 'Test1Controller@test')->name('home');


Route::get('/thong-ke/thong-ke-don-hang', 'OrderSumController@filterOrder')->name('sumOrders');

Route::get('/thong-ke/thong-ke-shipper', 'OrderSumController@filterShipper')->name('sumOrders');

Route::get('/admin/thong-ke/xem-danh-sach-don-hang', 'OrderSumController@searchorder')->name('sumOrders');

